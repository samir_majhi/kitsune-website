import express from 'express'
import path from 'path'
import fs from 'fs'

const root = path.join(__dirname, '../../')

const app = express()

let apiRoutes = () => (
    express.Router()
        .get('/nav', (req, res) => {
            const nav = JSON.parse(fs.readFileSync(path.join(root, 'server/data.json'), 'utf8')).nav
            const navigation = []
            let tags = nav['tags']

            tags.forEach(tag => {
                navigation.push({
                    'parent': tag
                })
            })

            navigation.forEach(tag => {
                if (nav['widgets'][tag['parent']]) {
                    tag['children'] = []
                    nav['widgets'][tag['parent']].forEach(widget => {
                        tag['children'].push({
                            'parent': widget
                        })
                    })
                }
            })

            navigation.forEach(tag => {
                if (tag['children']) {
                    tag['children'].forEach(widget => {
                        if (nav['widgetProps'][widget['parent']]) {
                            widget['children'] = []
                            nav['widgetProps'][widget['parent']].forEach(prop => {
                                widget['children'].push(
                                    {
                                        'parent': prop
                                    }
                                )
                            })
                        }
                    })
                }
            })

            res.send(navigation)
        })
)

app.use('/assets', express.static(path.resolve(__dirname, '../client')))

app.use('/api', apiRoutes())

app.get('/biz', (req, res) => {
    res.sendFile(path.resolve(__dirname, '../client/index.html'))
})

app.get('/dev', (req, res) => {
    res.sendFile(path.resolve(__dirname, '../client/dev.html'))
})

app.get('/docs', (req, res) => {
    res.sendFile(path.resolve(__dirname, '../client/docs.html'))
})

app.get('/', (req, res) => {
    res.sendFile(path.resolve(__dirname, '../client/index.html'))
})

app.listen(process.env.PORT || 8081, () => {
    console.log('serving at port 8081')
})