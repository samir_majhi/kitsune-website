import { GET_NAV } from '../actions/index.js'

export default function (state = [], action) {
    switch (action.type) {
        case GET_NAV:
            return action.payload.data
    }
    return state
} 