import { combineReducers } from 'redux'
import NavReduer from './reducer_nav.js'

const rootReducer = combineReducers({
    nav: NavReduer
})

export default rootReducer