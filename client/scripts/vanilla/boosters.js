import { returnArray } from './helpers.js'

const boosters = {
    config: {
        nodes: {
            boosters: returnArray(document.querySelectorAll('.container-booster')),
            section: document.querySelector('.section-boosters'),
            containerBoosters: document.querySelector('.container-boosters'),
            close: document.querySelectorAll('.container-boosters .close')
        },
        containerBoostersHeight: document.querySelector('.container-boosters') ? document.querySelector('.container-boosters').offsetHeight : undefined
    },

    state: {
        current: undefined,
        currentIndex: undefined
    },

    init() {
        boosters.bindEvents()
    },

    bindEvents() {

        // When a booster gets clicked
        boosters.config.nodes.boosters.forEach((booster, index) => {
            booster.addEventListener('click', (event) => {
                event.stopPropagation()
                boosters.state.current = event.currentTarget
                boosters.state.currentIndex = index
                boosters.methods.open()
                boosters.methods.adjustHeight()
            })
        })

        //Close button gets clicked
        boosters.config.nodes.close.forEach(close => {
            close.addEventListener('click', (event) => {
                event.stopPropagation()
                boosters.methods.close()
                boosters.methods.adjustHeight()
            })
        })

        //Click anywhere on the document
        document.addEventListener('click', () => {
            boosters.methods.close()
            boosters.methods.adjustHeight()
        })

        //Press escape
        document.addEventListener('keydown', (event) => {
            if (event.which == 27) {
                boosters.methods.close()
                boosters.methods.adjustHeight()
            }
        })
    },

    methods: {
        open() {
            let alignIndex = (boosters.state.currentIndex % 4) + 1
            boosters.state.current.classList.add('booster-expand', 'booster-top', `booster-expand-${alignIndex}`)
            boosters.config.nodes.boosters.forEach((booster, index) => {
                if (index != boosters.state.currentIndex) {
                    booster.classList.add('booster-no-click')
                }
            })
        },

        close() {
            boosters.config.nodes.boosters.forEach((booster, index) => {
                booster.classList.remove('booster-no-click','booster-expand', 'booster-expand-1', 'booster-expand-2', 'booster-expand-3', 'booster-expand-4')
                if (index != boosters.state.currentIndex) {
                    booster.classList.remove('booster-top')
                }
            })
            setTimeout(() => {
                boosters.state.current.classList.remove('booster-top')
            }, 300)
        },

        adjustHeight() {
            let row = Math.floor((boosters.state.currentIndex + 1) % 4)
            let sectionHeight = boosters.config.nodes.section.offsetHeight
            let containerBoostersHeight = boosters.config.nodes.containerBoosters.offsetHeight
            let increment = boosters.state.current.childNodes[1].childNodes[3].offsetHeight - containerBoostersHeight
            let incremented = increment + containerBoostersHeight * 1.08

            if (boosters.state.current.classList.contains('booster-expand')) {
                if (increment > 0) {
                    boosters.config.nodes.containerBoosters.style.height = incremented + 'px'
                }
            }
            else {
                boosters.config.nodes.containerBoosters.style.height = boosters.config.containerBoostersHeight + 'px'
            }
        }
    }
}

export default boosters