import { returnArray, scrollToY } from './helpers.js'

let Nav = {
    config: {
        nodes: {
            nav: document.querySelector('.section-navigation'),
            links: returnArray(document.querySelectorAll(`.section-navigation .nav-main li`)),
            mobile: document.querySelector('.nav-mobile'),
            hamburger: document.querySelector('.nav-mobile .hamburger'),
            mobileLinks: returnArray(document.querySelectorAll('.nav-mobile .links li'))
        },
        triggers: {
            stick: undefined,
            home: undefined,
            features: undefined,
            structure: undefined
        },
        pos: undefined
    },

    init() {
        if (window.location.pathname == '/dev') {
            Nav.methods.setTriggers()
            Nav.bindEvents()
        }
    },

    bindEvents() {

        //On page scroll
        document.addEventListener('scroll', () => {
            Nav.methods.setPos()
            Nav.methods.setTriggers()
            Nav.methods.stick()
            Nav.methods.setActive()
        })

        //On nav click
        Nav.config.nodes.links.forEach((link, index) => {
            link.addEventListener('click', () => {
                Nav.methods.scroll(index + 1)
            })
        })

        //On hamburger click
        Nav.config.nodes.mobile.addEventListener('click', (event) => {
            Nav.config.nodes.mobile.classList.toggle('nav-mobile-open')
        })

        //On mobile links click
        Nav.config.nodes.mobileLinks.filter(link => typeof (link) != 'number').forEach((link, index) => {
            link.addEventListener('click', () => {
                Nav.methods.scroll(index + 1)
            })
        })
    },

    methods: {

        setPos() {
            if (window.pageYOffset > 0) {
                Nav.config.pos = window.pageYOffset
            }
            else if (document.documentElement) {
                Nav.config.pos = document.documentElement.scrollTop
            }
            else if (document.body) {
                Nav.config.pos = document.body.scrollTop
            }
            else if (document.body.parentNode) {
                Nav.config.pos = document.body.parentNode.scrollTop
            }
        },

        setTriggers() {
            Nav.config.triggers.stick = document.querySelector('.section-intro .banner').clientHeight / 1.8,
                Nav.config.triggers.home = document.querySelector('.section-intro .wrapper-banner').clientHeight,
                Nav.config.triggers.features = Nav.config.triggers.home + document.querySelector('.section-intro .wrapper-content').clientHeight,
                Nav.config.triggers.structure = Nav.config.triggers.features + document.querySelector('.section-intro .wrapper-features').clientHeight
        },

        stick() {
            if (Nav.config.pos > Nav.config.triggers.stick) {
                if (!Nav.config.nodes.nav.classList.contains('nav-stick')) {
                    Nav.config.nodes.nav.classList.add('nav-stick')
                }
            }
            else {
                Nav.config.nodes.nav.classList.remove('nav-stick')
            }
        },

        setActive() {

            if (Nav.config.pos < Nav.config.triggers.home) {
                activate(1)
            }
            else if (Nav.config.pos >= Nav.config.triggers.home && Nav.config.pos < Nav.config.triggers.features) {
                activate(2)
            }
            else if (Nav.config.pos >= Nav.config.triggers.features && Nav.config.pos < Nav.config.triggers.structure) {
                activate(3)
            }
            else if (Nav.config.pos >= Nav.config.triggers.structure) {
                activate(4)
            }

            function activate(child) {
                Nav.config.nodes.links.forEach(link => {
                    link.classList.remove('active')
                })
                document.querySelector(`.section-navigation .nav-main li:nth-child(${child})`).classList.add('active')
            }
        },

        scroll(child) {
            switch (child) {
                case 1:
                    scroll(0)
                    break
                case 2:
                    scroll(Nav.config.triggers.home)
                    break
                case 3:
                    scroll(Nav.config.triggers.features)
                    break
                case 4:
                    scroll(Nav.config.triggers.structure)
                    break
            }

            function scroll(pos) {
                scrollToY(pos, 1000, 'easeInOutQuint')
            }
        }
    }
}

export default Nav