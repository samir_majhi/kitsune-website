import Nav from './nav.js'
import Docs from './doc.js'
import Console from './console.js'
import Boosters from './boosters.js'
import Features from './features.js'
import skrollr from './skrollr.min.js'
import axios from 'axios'
import { returnArray, scrollToY } from './helpers.js'

if (window.location.pathname == '/biz' || window.location.pathname == '/') {

    function getIntroHeight() {
        let banner = document.querySelector('section.banner')
        let height = parseInt(window.getComputedStyle(banner, null).height)
        let padding = parseInt(window.getComputedStyle(banner, null).fontSize) - 10
        return document.querySelector('section.intro').offsetHeight + height + padding
    }

    function getFeatureHeight(feature) {
        if (feature != 0) {
            return document.querySelector(`.features .feature-${feature}`).offsetHeight
        }
        else {
            return 0
        }
    }

    // Navigation

    let ham = document.querySelector('.hamburger')
    let nav = document.querySelector('.nav-mobile')
    let items = returnArray(document.querySelectorAll('.nav-mobile li'))

    function getFeaturesHeight(feature) {
        let height = 0
        for (let x = 1; x <= feature; x++) {
            height = height + getFeatureHeight(x - 1)
        }
        return height + getIntroHeight()
    }

    ham.addEventListener('click', function () {
        nav.classList.toggle('nav-mobile-open')
    })

    items.filter(item => typeof (item) != 'number').forEach(item => {
        item.addEventListener('click', function () {
            nav.classList.remove('nav-mobile-open')
            let feature = item.getAttribute('data-scroll')
            if (feature == 6) {
                scrollToY(contactHeight + 100, 1000, 'easeInOutQuint')
            }
            else {
                scrollToY(getFeaturesHeight(feature), 1000, 'easeInOutQuint')
            }

        })
    })

    function getActiveWidth(active) {
        let link = document.querySelector(`.nav-main li:nth-child(${active})`)
        return window.getComputedStyle(link, null).width
    }

    function getActiveLeft(active) {
        let links = returnArray(document.querySelectorAll(`.nav-main li`))
        let left = links.map((link, index) => {
            if (index < active - 1) {
                let width = parseInt(window.getComputedStyle(link, null).width)
                let margin = parseInt(window.getComputedStyle(link, null).marginRight)
                return width + margin
            }
            return 0
        })
        left = left.reduce((pre, next) => {
            return pre + next
        })

        return left + 'px'
    }

    var contactHeight = parseInt(window.getComputedStyle(document.querySelector('body'), null).height) - window.innerHeight

    let active = document.querySelector('.nav-main .underline')
    active.setAttribute(`data-0`, `opacity:!0`)
    active.setAttribute(`data-${getFeaturesHeight(1)}`, `width:!${getActiveWidth(1)}; left:!${getActiveLeft(1)};opacity:!1;`)
    active.setAttribute(`data-${getFeaturesHeight(2)}`, `width:!${getActiveWidth(2)}; left:!${getActiveLeft(2)};`)
    active.setAttribute(`data-${getFeaturesHeight(3)}`, `width:!${getActiveWidth(3)}; left:!${getActiveLeft(3)};`)
    active.setAttribute(`data-${getFeaturesHeight(4)}`, `width:!${getActiveWidth(4)}; left:!${getActiveLeft(4)};`)
    active.setAttribute(`data-${getFeaturesHeight(5)}`, `width:!${getActiveWidth(5)}; left:!${getActiveLeft(5)};`)
    active.setAttribute(`data-${contactHeight}`, `width:!${getActiveWidth(6)}; left:!${getActiveLeft(6)};`)

    returnArray(document.querySelectorAll(`.nav-main li`)).forEach((link, index) => {
        link.addEventListener('click', () => {
            if (index + 1 == 6) {
                scrollToY(contactHeight + 100, 1000, 'easeInOutQuint')
            }
            else {
                scrollToY(getFeaturesHeight(index + 1), 1000, 'easeInOutQuint')
            }
        })
    })

    // <!------ Form Submission -------!>

    let form = document.querySelector('section.enquiry form')
    let overlay = document.querySelector('section.enquiry .overlay')
    let clientId = '731EBAB8596648E79882096E4733E7173B314BECC649423DB67897BF8CC596EC'
    let key = '8NNEzoruRPvZ0sBJxnOcgw'

    form.addEventListener('submit', (event) => {
        event.preventDefault()
        let name = form.childNodes[1].childNodes[1].value
        let email = form.childNodes[1].childNodes[3].value
        let message = form.childNodes[3].childNodes[1].value
        axios.post('https://mandrillapp.com/api/1.0/messages/send.json', {
            "key": key,
            'message': {
                'from_email': 'info@kitsune.tools',
                'fram_name': name,
                'subject': 'Enquiry from Kitsune Website.',
                'html': `<h3> From - ${name}<h3><h3>Email - ${email}</h3><h3>Message</h3><p>${message}</p>`,
                'to': [
                    {
                        'email': 'kitsune@nowfloats.com',
                        'name': 'Kitsune'
                    }
                ],
            }
        })
            .then(function (response) {
                console.log(response.data)
                overlay.classList.add('overlay-show')
                setTimeout(() => {
                    form.reset()
                    overlay.classList.remove('overlay-show')
                }, 3000)
            })
    })
}

if (window.location.pathname == '/dev') {
    Nav.init()
    Console.init()
    Docs.init()
    Boosters.init()
}

if ((window.location.pathname == '/biz'|| window.location.pathname == '/') && window.outerWidth > 699) {
    Features.init()
}