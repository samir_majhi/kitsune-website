import { returnArray } from './helpers.js'

const Console = {
    config: {
        active: undefined,
        order: ['entity', 'view', 'widget', 'widgetProps'],
        nodes: {
            indicators: returnArray(document.querySelectorAll(`.indicator`)),
            markers: returnArray(document.querySelectorAll('.section-console .marker')),
            code: returnArray(document.querySelectorAll(`.code`))
        },
        state: {
            cycleInterval: undefined
        }
    },

    init() {
        Console.bindEvents()
        document.querySelector('.indicator-entity').click()
    },

    bindEvents() {
        //On clicking an indicator
        Console.config.nodes.indicators.forEach(indicator => {
            indicator.addEventListener('click', function (event) {
                event.stopPropagation()
                Console.methods.toggle.init(event.currentTarget)
            })
        })

        //Start cycling when scrolled to section
        document.addEventListener('scroll', () => {
            Console.methods.cycle()
        })
    },

    methods: {
        toggle: {
            init(indicator, nocycle) {
                if (!nocycle) {
                    clearInterval(Console.config.state.cycleInterval)
                }
                Console.methods.toggle.activate(indicator)
                Console.methods.toggle.component()
                Console.methods.toggle.explaination()
                Console.methods.toggle.markers()
                Console.methods.toggle.indicator()
                Console.methods.toggle.next()
                Console.methods.toggle.code()
            },

            activate(indicator) {
                let name = indicator.classList[1].split('-')[1]
                let component = document.querySelector(`.component-${name}`)
                Console.config.active = { component, name }
            },

            component() {
                let components = [...Console.config.order]
                let index = components.indexOf(Console.config.active.name) + 1
                let number = components.length - index
                let toFocus = [...components].splice(0, index)
                let toUnfocus = [...components].splice(-number, number)
                toUnfocus.forEach(component => {
                    document.querySelector(`.component-${component}`).setAttribute('class', `component component-${component}`)
                })
                toFocus.forEach(component => {
                    document.querySelector(`.component-${component}`).setAttribute('class', `component component-${component} focus-${component}`)
                })
                components.forEach(component => {
                    document.querySelector(`.component-${component}`).classList.remove(`color-${component}`)
                })
                Console.config.active.component.classList.add(`color-${Console.config.active.name}`)
            },

            explaination() {
                let exps = returnArray(document.querySelectorAll('.exp'))
                exps.forEach(exp => {
                    let current = exp.classList[1].split('-')[1]
                    if (current != Console.config.active.name) {
                        exp.setAttribute('class', `exp exp-${current}`)
                    }
                    else {
                        exp.setAttribute('class', `exp exp-${current} exp-focus`)
                    }
                })
            },

            markers() {
                Console.config.nodes.markers.forEach(marker => {
                    let defaultClass = marker.classList[1]
                    marker.setAttribute('class', `marker ${defaultClass}`)
                    marker.classList.add(`marker-${Console.config.active.name}`)
                })
            },

            indicator() {
                Console.config.nodes.indicators.forEach(indicator => {
                    indicator.classList.remove('indicator-focus')
                })
                document.querySelector(`.indicator-${Console.config.active.name}`).classList.add('indicator-focus')
            },

            next() {
                let button = document.querySelector('.next-button')
                let defaultClass = button.classList[0]
                button.setAttribute('class', defaultClass)
                button.classList.add(`next-${Console.config.active.name}`)
            },

            code() {
                Console.config.nodes.code.forEach(line => {
                    let defaultClass = line.classList[1]
                    line.setAttribute('class', `code ${defaultClass}`)
                })

                let selection = returnArray(document.querySelectorAll(`.code-${Console.config.active.name}`))
                selection.forEach(selected => {
                    selected.classList.add(`focus-${Console.config.active.name}`)
                })
            }
        },

        cycle() {
            let scrollPos = document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop
            let trigger = document.querySelector('.section-intro .wrapper-banner').clientHeight + document.querySelector('.section-intro .wrapper-content').clientHeight + document.querySelector('.section-intro .wrapper-features').clientHeight
            if (scrollPos >= trigger && Console.config.state.cycleInterval == undefined) {
                Console.config.state.cycleInterval = setInterval(() => {
                    let current = Console.config.order.indexOf(Console.config.active.name)
                    let next = current < 3 ? Console.config.order[current + 1] : 'entity'
                    let component = document.querySelector(`.component-${next}`)
                    Console.methods.toggle.init(document.querySelector(`.indicator-${next}`), 1)
                }, 5000)
            }
        }
    }
}

export default Console