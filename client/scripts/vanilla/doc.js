import { returnArray, scrollToY } from './helpers.js'

const Docs = {
    config: {
        views: {
            master: {
                widgets: [
                    'Business-Name',
                    'Business-Description',
                    'Update',
                    'Product',
                    'Business-Hours',
                    'All-Products',
                    'Subscribers-Count',
                    'Tags',
                    'Visits',
                    'Business-Enquiries',
                    'Video-Gallery',
                    'Site-Link',
                    'Address',
                    'Mobile-Numbers',
                    'Subscribe',
                    'Search',
                    'Logo',
                    'Map',
                    'Facebook-Like-Box',
                    'Background-Image',
                    'Favicon'
                ],
                mandatory: [
                    'Business-Name',
                    'Business-Description',
                    'Update',
                    'Product',
                    'Business-Hours',
                    'All-Products',
                    'Subscribers-Count',
                    'Tags',
                    'Visits',
                    'Business-Enquiries',
                    'Video-Gallery',
                    'Site-Link',
                    'Address',
                    'Mobile-Numbers',
                    'Subscribe',
                    'Search',
                    'Logo',
                    'Map',
                    'Facebook-Like-Box',
                    'Background-Image',
                    'Favicon'
                ],
            },
            Home: {
                widgets: ['Business-Name', 'Address', 'Mobile-Numbers', 'Business-Enquiries', 'Business-Description', 'Update', 'Offers', 'All-Products', 'Business-Hours', 'Background-Image', 'Image-Gallery', 'Subscribers-Count', 'Tags', 'Map', 'Visits', 'Custom', 'Video-Gallery', 'Logo', 'Facebook-Like-Box'],
                mandatory: ['Business-Name', 'Address', 'Mobile-Numbers', 'Business-Enquiries'],
            },
            Offers: {
                widgets: ['Update', 'Mobile-Numbers', 'Business-Enquiries', 'Business-Name', 'Business-Description', 'Address', 'Background-Image'],
                mandatory: ['Update'],
            },
            Update: {
                widgets: ['Update', 'Business-Name', 'Address', 'Mobile-Numbers', 'Business-Enquiries', 'Business-Description', 'Tags', 'Background-Image'],
                mandatory: ['Update'],
            },
            'All-Products': {
                widgets: ['Product', 'Business-Enquiries', 'Mobile-Numbers', 'Address', 'Business-Name', 'Business-Description', 'Background-Image'],
                mandatory: ['Product'],
            },
            'Update-Details': {
                widgets: ['Update', 'Mobile-Numbers', 'Business-Enquiries', 'Address', 'Business-Name', 'Business-Description', 'Background-Image'],
                mandatory: ['Update'],
            },
            'Product-Details': {
                widgets: ['Product', 'Mobile-Numbers', 'Business-Enquiries', 'Address', 'Business-Name', 'Business-Description', 'Background-Image'],
                mandatory: ['Product'],
            },
            Custom: {
                widgets: ['Background-Image', 'Address', 'Mobile-Numbers', 'Business-Enquiries', 'Business-Name', 'Business-Hours'],
                mandatory: [],
            },
            Search: {
                widgets: ['Update', 'Background-Image', 'Business-Name'],
                mandatory: ['Update'],
            },
            'Product-Search': {
                widgets: ['Product', 'Background-Image'],
                mandatory: ['Product'],
            },
            'Image-Gallery': {
                widgets: ['Image'],
                mandatory: ['Image'],
            },
            Map: {
                widgets: ['Map', 'Business-Name', 'Background-Image'],
                mandatory: ['Map'],
            }
        },
        widgets: {
            'Business-Name': {
                props: ['Business-Name'],
                mandatory: ['Business-Name'],
                copy: {
                    widget: 'This is the Business-Name widget',
                    props: ['This is the Business-Name prop']
                }
            },
            'Business-Description': {
                props: ['Business-Description'],
                mandatory: ['Business-Description'],
                copy: {
                    widget: 'This is the Business-Description widget',
                    props: ['This is the Business-Description prop']
                }
            },
            Update: {
                props: ['Update', 'TimeStamp', 'Url', 'Business-Image', 'Type', 'Tags'],
                mandatory: ['Update', 'TimeStamp', 'Url'],
                copy: {
                    widget: 'This is the Update widget',
                    props: [
                        'This is the Update prop',
                        'This is the TimeStamp prop',
                        'This is the Url prop',
                        'This is the Business-Image prop',
                        'This is the Type (LATEST/FEATURED/POPULAR) prop',
                        'This is the Tags prop'
                    ]
                }
            },
            Product: {
                props: ['Name', 'Description', 'Cost', 'Url', 'Currency', 'Availability', 'Image'],
                mandatory: ['Name', 'Description', 'Cost', 'Url', 'Currency'],
                copy: {
                    widget: 'This is the Product widget',
                    props: [
                        'This is the Name prop This is the Name prop This is the Name prop This is the Name prop This is the Name prop This is the Name prop This is the Name prop',
                        'This is the Description prop This is the Name prop This is the Name prop This is the Name prop This is the Name prop This is the Name prop This is the Name prop',
                        'This is the Cost prop This is the Name prop This is the Name prop This is the Name prop This is the Name prop This is the Name prop This is the Name prop',
                        'This is the Url prop This is the Name prop This is the Name prop This is the Name prop This is the Name prop This is the Name prop This is the Name prop',
                        'This is the Currency prop This is the Name prop This is the Name prop This is the Name prop This is the Name prop This is the Name prop This is the Name prop',
                        'This is the Availability prop This is the Name prop This is the Name prop This is the Name prop This is the Name prop This is the Name prop This is the Name prop',
                        'This is the Image prop This is the Name prop This is the Name prop This is the Name prop This is the Name prop This is the Name prop This is the Name prop'
                    ]
                }
            },
            'Business-Hours': {
                props: ['Open-Now', 'Days-Open', 'Time-Open'],
                mandatory: ['Open-Now', 'Days-Open', 'Time-Open'],
                copy: {
                    widget: 'This is the Business-Hours widget',
                    props: ['This is the Open-Now prop', 'This is the Days-Open prop', 'This is the Time-Open prop']
                }
            },
            'All-Products': {
                props: ['Name', 'Url', 'Price', 'Currency'],
                mandatory: ['Name', 'Url', 'Price', 'Currency'],
                copy: {
                    widget: 'This is the All-Products widget',
                    props: [
                        'This is the Name prop',
                        'This is the Url prop',
                        'This is the Price prop',
                        'This is the Currency prop',
                    ]
                }
            },
            'Subscribers-Count': {
                props: ['Count'],
                mandatory: ['Count'],
                copy: {
                    widget: 'This is the Subscribers-Count widget',
                    props: ['This is the Count prop']
                }
            },
            Tags: {
                props: ['Tags'],
                mandatory: ['Tags'],
                copy: {
                    widget: 'This is the Tags widget',
                    props: ['This is the Tags prop']
                }
            },
            Visits: {
                props: ['Count'],
                mandatory: ['Count'],
                copy: {
                    widget: 'This is the Visits widget',
                    props: ['This is the Count prop']
                }
            },
            'Business-Enquiries': {
                props: ['Enquiry-Contact', 'Enquiry-Message', 'Enquiry-Button'],
                mandatory: ['Enquiry-Contact', 'Enquiry-Message', 'Enquiry-Button'],
                copy: {
                    widget: 'This is the Business-Enquiries widget',
                    props: [
                        'This is the Enquiry-Contact prop',
                        'This is the Enquiry-Message prop',
                        'This is the Enquiry-Button prop'
                    ]
                }
            },
            'Video-Gallery': {
                props: ['Url'],
                mandatory: ['Url'],
                copy: {
                    widget: 'This is the Video-Gallery widget',
                    props: ['This is the Url prop']
                }
            },
            'Site-Link': {
                props: ['Url'],
                mandatory: ['Url'],
                copy: {
                    widget: 'This is the Site-Link widget',
                    props: ['This is the Url prop']
                }
            },
            Address: {
                props: ['Country', 'Line-1', 'City', 'Pincode'],
                mandatory: ['Country'],
                copy: {
                    widget: 'This is the Address widget',
                    props: [
                        'This is the Country prop',
                        'This is the Line-1 prop',
                        'This is the City prop',
                        'This is the Pincode prop'
                    ]
                }
            },
            'Mobile-Numbers': {
                props: ['Primary', 'Work', 'Home'],
                mandatory: ['Primary'],
                copy: {
                    widget: 'This is the Mobile-Numbers widget',
                    props: [
                        'This is the Primary prop',
                        'This is the Work prop',
                        'This is the Home prop'
                    ]
                }
            },
            Subscribe: {
                props: ['Subscribe-Button', 'Subscribe-Contact'],
                mandatory: ['Subscribe-Button', 'Subscribe-Contact'],
                copy: {
                    widget: 'This is the Subscribe widget',
                    props: [
                        'This is the Subscribe-Button prop',
                        'This is the Subscribe-Contact prop'
                    ]
                }
            },
            Search: {
                props: ['Search-Button', 'Search-Query'],
                mandatory: ['Search-Button', 'Search-Query'],
                copy: {
                    widget: 'This is the Search widget',
                    props: [
                        'This is the Search-Button prop',
                        'This is the Search-Query prop'
                    ]
                }
            },
            Logo: {
                props: ['Url'],
                mandatory: ['Url'],
                copy: {
                    widget: 'This is the Logo widget',
                    props: ['This is the Url prop']
                }
            },
            Map: {
                props: ['Lat ', 'Long'],
                mandatory: ['Lat ', 'Long'],
                copy: {
                    widget: 'This is the Map widget',
                    props: [
                        'This is the Lat prop',
                        'This is the Long prop'
                    ]
                }
            },
            'Facebook-Like-Box': {
                props: ['Page-Name'],
                mandatory: ['Page-Name'],
                copy: {
                    widget: 'This is the Facebook-Like-Box widget',
                    props: ['This is the Page-Name prop']
                }
            },
            'Background-Image': {
                props: ['Url'],
                mandatory: ['Url'],
                copy: {
                    widget: 'This is the Background-Image widget',
                    props: ['This is the Url prop']
                }
            },
            Favicon: {
                props: ['Url'],
                mandatory: ['Url'],
                copy: {
                    widget: 'This is the Favicon widget',
                    props: ['This is the Url prop']
                }
            }
        },
        state: {
            timeoutTop: undefined,
            index: 1,
            view: undefined,
            init: undefined
        },
        nodes: {
            dropDown: document.querySelector('.select-view'),
            dropDownList: document.querySelector('.view-list'),
            dropDownListItems: undefined,
            view: document.querySelector('.select-view .view'),
            buttonExpand: document.querySelector('.bar-view button'),
            docs: undefined,
            docsVisible: undefined,
            docsContainer: document.querySelector('.container-docs')
        },
        order: ['entity', 'view', 'widget', 'widgetProps']
    },

    dropDown: {

        init() {
            Docs.dropDown.populate()
            Docs.config.nodes.dropDownListItems = returnArray(document.querySelectorAll('.view-list li'))
            Docs.dropDown.bindEvents()
            // Select Master View
            document.querySelector('.view-list li:nth-child(2)').click()
        },

        bindEvents() {
            //On clicking document
            document.addEventListener('click', () => {
                Docs.dropDown.collapse()
            })

            //On pressing escape
            document.addEventListener('keydown', (event) => {
                if (event.which == 27) {
                    Docs.dropDown.collapse()
                }
            })

            //On clicking dropdown
            Docs.config.nodes.dropDown.addEventListener('click', (event) => {
                event.stopPropagation()
                Docs.dropDown.toggle()
            })

            //On selecting from dropdown
            Docs.config.nodes.dropDownListItems.forEach(view => {
                view.addEventListener('click', (event) => {
                    event.stopPropagation()
                    Docs.dropDown.select(event.currentTarget)
                    Docs.dropDown.collapse()
                    Docs.dropDown.scroll()
                    Docs.doc.select()

                })
            })

            //On clicking expand
            Docs.config.nodes.buttonExpand.addEventListener('click', (event) => {
                event.stopPropagation()
                Docs.doc.methods.toggleSemi()
            })
        },

        populate() {
            Object.keys(Docs.config.views).forEach(view => {
                let item = document.createElement('li')
                let name = document.createTextNode(view)
                item.appendChild(name)
                Docs.config.nodes.dropDownList.appendChild(item)
            })
        },

        collapse() {
            Docs.config.nodes.dropDown.classList.remove('select-expand')
        },

        toggle() {
            Docs.config.nodes.dropDown.classList.toggle('select-expand')
        },

        select(view) {
            Docs.config.nodes.view.innerHTML = Docs.config.state.view = view.innerHTML
        },

        scroll() {
            let wrapper = parseInt(document.querySelector('.wrapper-sections').clientHeight)
            let documentation = parseInt(document.querySelector('.section-documentation').clientHeight) + parseInt(window.getComputedStyle(document.querySelector('.section-documentation'))['margin-top']) - parseInt(window.getComputedStyle(document.querySelector('.section-documentation'))['margin-bottom']) / 1.35
            let enquiry = parseInt(document.querySelector('.section-enquiry').clientHeight)
            let footer = parseInt(document.querySelector('.section-footer').clientHeight)
            let bar = parseInt(document.querySelector('.bar-view').clientHeight)
            let boosters = parseInt(document.querySelector('.section-boosters').offsetHeight)
            let scroll = wrapper - documentation - footer - bar - enquiry - boosters
            if (Docs.config.state.init != undefined) {
                scrollToY(scroll, 500, 'easeInOutQuint')
            }
            Docs.config.state.init = 1
        }
    },

    doc: {
        init() {
            Docs.doc.create.init()
            Docs.config.nodes.docs = returnArray(document.querySelectorAll('.wrapper-doc'))
            Docs.config.nodes.docsVisible = Docs.config.nodes.docs
            Docs.doc.methods.align()
            Docs.doc.bindEvents()
        },

        bindEvents() {

            // When a doc is clicked
            Docs.config.nodes.docs.forEach((doc, index) => {
                doc.addEventListener('click', (event) => {
                    event.stopPropagation()
                    if (window.innerWidth > 640) {
                        Docs.doc.methods.toggle(event.currentTarget)
                        Docs.doc.methods.adjustContainerHeight(event.currentTarget)
                    }
                    else {
                        Docs.doc.methods.toggleMobile(event.currentTarget)
                        Docs.doc.methods.adjustContainerHeightMobile(event.currentTarget)
                    }
                })
            })

            //When escape is pressed
            document.addEventListener('keydown', (event) => {
                if (event.which == 27) {
                    Docs.doc.methods.contract()
                }
            })

            //On clicking anywhere on the document
            document.addEventListener('click', () => {
                Docs.doc.methods.contract()
            })
        },

        create: {
            init() {
                Docs.config.nodes.docsContainer.innerHTML = ''
                Docs.doc.create.wrapper().forEach((wrapper, index) => {
                    wrapper.setAttribute('index', index)
                    Docs.config.nodes.docsContainer.appendChild(wrapper)
                })
            },

            widgets() {
                let view = Docs.config.state.view
                let views = Docs.config.views
                return views[view].widgets.map(widget => `
                    <span class="widget">
                        <span class="name">${widget}</span>
                    </span>
                `)
            },

            props() {
                let view = Docs.config.state.view
                let views = Docs.config.views
                return views[view].widgets.map(widget => {
                    return Docs.config.widgets[widget].props.map(prop => {
                        return `<span class="prop prop-mandatory"><span>${prop}</span></span>`
                    })
                })
            },

            overview() {
                let widgets = Docs.doc.create.widgets()
                let Props = Docs.doc.create.props()
                let overview = Props.map((props, index) => {
                    let propNode = ''
                    props.map(prop => {
                        propNode = propNode + prop
                    })
                    return widgets[index] + propNode
                })
                return overview.map(val => `<div class="overview">${val}</div>`)
            },

            widgetDetails() {
                let view = Docs.config.state.view
                let views = Docs.config.views
                return views[view].widgets.map(widget => {
                    let detail = Docs.config.widgets[widget].copy.widget
                    return `<div class="detail-widget"><span>${detail}</span></div>`
                })
            },

            propDetails() {
                let view = Docs.config.state.view
                let views = Docs.config.views
                return views[view].widgets.map(widget => {
                    let propNodes = ''
                    Docs.config.widgets[widget].copy.props.map(prop => {
                        propNodes = propNodes + `<div class="detail-prop"><span>${prop}</span></div>`
                    })
                    return propNodes
                })
            },

            details() {
                return Docs.doc.create.widgetDetails().map((widget, index) => {
                    return `<div class="detail">${widget + Docs.doc.create.propDetails()[index]}</div>`
                })
            },

            doc() {
                let details = Docs.doc.create.details()
                return Docs.doc.create.overview().map((overview, index) => {
                    return `
                    <div class="doc">
                        <div class="inner-wrapper-doc">
                            ${overview}
                            ${details[index]}
                            <div class="label">
                                <span class="widget">widget</span>
                                <span class="widgetProps">widget props</span>
                            </div>
                            <div class="button-expand">
                                <span class="expand">+</span>
                                <span class="collapse">&ndash;</span>
                            </div>
                        </div>
                        <div class="underlay-border"></div>
                        <div class="underlay-color"></div>
                    </div>
                `})
            },

            wrapper() {
                return Docs.doc.create.doc().map((doc, index) => {
                    let widget = Docs.config.views[Docs.config.state.view].widgets[index]
                    let wrapper = document.createElement('div')
                    wrapper.innerHTML = doc
                    wrapper.className = 'wrapper-doc'
                    wrapper.setAttribute('widget', widget)
                    return wrapper
                })
            }
        },

        select() {
            Docs.doc.methods.filter()
            Docs.config.nodes.docsVisible = returnArray(document.querySelectorAll('.container-docs .doc-show'))
            Docs.doc.methods.toggleMandatory(event.currentTarget)
            Docs.doc.methods.mandatoryFirst()
            Docs.config.nodes.docsVisible = returnArray(document.querySelectorAll('.container-docs .doc-show'))
            Docs.doc.methods.align()
            Docs.doc.methods.adjustContainerHeight(document.querySelector('.container-docs .wrapper-doc:first-child'))
        },

        methods: {

            align() {
                Docs.config.nodes.docsVisible.forEach((doc, index) => {
                    doc.setAttribute('index', index)
                    let defaultClass = doc.classList[0]
                    doc.setAttribute('class', defaultClass)
                    switch (index % 3) {
                        case 0:
                            doc.classList.add('wrapper-doc-left')
                            break
                        case 1:
                            doc.classList.add('wrapper-doc-center')
                            break
                        case 2:
                            doc.classList.add('wrapper-doc-right')
                            break
                    }
                })
            },

            toggle(current) {
                Docs.config.nodes.docs.forEach(doc => {
                    if (doc.getAttribute('index') != current.getAttribute('index')) {
                        Docs.doc.methods.contract(doc)
                    }
                    else {
                        if (doc.classList.contains('wrapper-doc-expand')) {
                            Docs.doc.methods.toggleSemi()
                            doc.classList.add('doc-expand')
                            Docs.config.state.index++
                            clearTimeout(Docs.config.state.timeoutTop)
                            current.style.zIndex = Docs.config.state.index
                        }
                    }
                })
                let index = current.getAttribute('index') % 3
                let doc = current.classList
                let expand

                switch (index) {
                    case 0:
                        expand = 'doc-expand-left'
                        break
                    case 1:
                        expand = 'doc-expand-center'
                        break
                    case 2:
                        expand = 'doc-expand-right'
                        break
                }

                if (!doc.contains('doc-expand')) {
                    Docs.config.state.index++
                    clearTimeout(Docs.config.state.timeoutTop)
                    doc.add('doc-expand')
                    current.style.zIndex = Docs.config.state.index
                }
                else {
                    if (!doc.contains(expand)) {
                        doc.add(expand)
                    }
                    else {
                        doc.remove(expand, 'doc-expand')
                        Docs.config.state.timeoutTop = setTimeout(() => {
                            current.style.zIndex = '1'
                        }, 2000)
                    }
                }
            },

            toggleSemi() {
                Docs.config.nodes.docsVisible.forEach(doc => {
                    if (!doc.classList.contains('wrapper-doc-expand')) {
                        doc.classList.add('wrapper-doc-expand', 'doc-expand')
                        Docs.config.nodes.buttonExpand.classList.add('button-collapse')
                    }
                    else {
                        doc.classList.remove('wrapper-doc-expand', 'doc-expand')
                        Docs.config.nodes.buttonExpand.classList.remove('button-collapse')
                    }
                })
            },

            toggleMobile(current) {
                Docs.config.nodes.docs.forEach(doc => {
                    if (doc.getAttribute('index') != current.getAttribute('index')) {
                        doc.classList.remove('doc-expand')
                        setTimeout(() => {
                            doc.style.zIndex = 1
                        }, 300)
                    }
                    else {
                        current.classList.toggle('doc-expand')
                        Docs.config.state.index++
                        current.style.zIndex = Docs.config.state.index
                    }
                })
            },

            contract(doc) {
                if (doc) {
                    doc.classList.remove('doc-expand-left', 'doc-expand-center', 'doc-expand-right', 'doc-expand')
                    setTimeout(() => {
                        doc.style.zIndex = 1
                    }, 500)
                }
                else {
                    Docs.config.nodes.docs.forEach(doc => {
                        doc.classList.remove('doc-expand-left', 'doc-expand-center', 'doc-expand-right', 'doc-expand', 'wrapper-doc-expand')
                        setTimeout(() => {
                            doc.style.zIndex = 1
                        }, 500)
                    })
                }
            },

            adjustContainerHeight(doc) {
                if (doc) {
                    let wrapperMargin = parseInt(window.getComputedStyle(doc)['margin-top'])
                    let wrapperHeight = (parseInt(window.getComputedStyle(doc)['font-size']) * 9.9) + wrapperMargin
                    let docHeight = wrapperHeight * 4
                    let rows = Math.ceil((Docs.config.nodes.docsVisible.filter(doc => typeof (doc) != 'number')).length / 3)
                    let problemStart = rows - 3
                    let containerHeight = wrapperHeight * rows
                    let currentRow = Math.ceil((parseInt(doc.getAttribute('index')) + 1) / 3)
                    problemStart = problemStart < 1 ? 0 : problemStart
                    let problemRows = rows - problemStart

                    function reset() {
                        Docs.config.nodes.docsContainer.style.transitionDelay = '0.3s'
                        Docs.config.nodes.docsContainer.style.height = (wrapperHeight * rows) + 'px'
                    }

                    if (doc.classList.contains('doc-expand')) {
                        if (currentRow > problemStart) {
                            let position = problemRows + (problemStart - currentRow) + 1
                            let increase = containerHeight + (docHeight - ((wrapperHeight - wrapperMargin) * position) - (wrapperMargin * position))
                            Docs.config.nodes.docsContainer.style.transitionDelay = '0s'
                            Docs.config.nodes.docsContainer.style.height = increase + 'px'
                        }
                        else {
                            reset()
                        }
                    }
                    else {
                        reset()
                    }
                }
            },

            adjustContainerHeightMobile(doc) {
                if (doc) {
                    let wrapperMargin = parseInt(window.getComputedStyle(doc)['margin-top'])
                    let wrapperHeight = (parseInt(window.getComputedStyle(doc)['font-size']) * 10) + wrapperMargin
                    let docHeight = wrapperHeight * 4
                    let rows = Math.ceil((Docs.config.nodes.docsVisible.filter(doc => typeof (doc) != 'number')).length / 2)
                    let problemStart = rows - 3
                    let containerHeight = wrapperHeight * rows
                    let currentRow = Math.ceil((parseInt(doc.getAttribute('index')) + 1) / 2)
                    problemStart = problemStart < 1 ? 0 : problemStart
                    let problemRows = rows - problemStart

                    function reset() {
                        Docs.config.nodes.docsContainer.style.transitionDelay = '0.3s'
                        Docs.config.nodes.docsContainer.style.height = (wrapperHeight * rows) + 'px'
                    }

                    if (doc.classList.contains('doc-expand')) {
                        if (currentRow > problemStart) {
                            let position = problemRows + (problemStart - currentRow) + 1
                            let increase = containerHeight + (docHeight - ((wrapperHeight - wrapperMargin) * position) - (wrapperMargin * position))
                            Docs.config.nodes.docsContainer.style.transitionDelay = '0s'
                            Docs.config.nodes.docsContainer.style.height = increase + 'px'
                        }
                        else {
                            reset()
                        }
                    }
                    else {
                        reset()
                    }
                }
            },

            filter() {
                if (Docs.config.nodes.docs != undefined) {
                    let activeWidgets = Docs.config.views[Docs.config.state.view].widgets
                    Docs.config.nodes.docs.forEach(doc => {
                        let widget = doc.getAttribute('widget')
                        let present = activeWidgets.indexOf(widget)
                        if (present < 0) {
                            doc.classList.add('doc-hide')
                            doc.classList.remove('doc-show')
                        }
                        else {
                            doc.classList.add('doc-show')
                            doc.classList.remove('doc-hide')
                        }
                    })
                }
            },

            toggleMandatory(view) {
                let mandatory = Docs.config.views[Docs.config.state.view].mandatory
                Docs.config.nodes.docsVisible.forEach(doc => {
                    let widget = doc.getAttribute('widget')
                    let present = mandatory.indexOf(widget)
                    if (present < 0) {
                        doc.childNodes[1].classList.remove('doc-mandatory')
                        doc.childNodes[1].classList.add('doc-optional')
                    }
                    else {
                        doc.childNodes[1].classList.add('doc-mandatory')
                        doc.childNodes[1].classList.remove('doc-optional')
                    }
                })
            },

            mandatoryFirst() {
                if (Docs.config.state.view != 'master') {
                    let index = 0
                    Docs.config.nodes.docsVisible.forEach(doc => {
                        if (doc.childNodes[1].classList.contains('doc-mandatory')) {
                            let current = doc
                            doc.remove()
                            Docs.config.nodes.docsContainer.insertBefore(current, Docs.config.nodes.docsContainer.childNodes[index])
                            index++
                        }
                    })
                }
            }
        }
    },

    init() {
        Docs.dropDown.init()
        Docs.doc.init()
    }
}

export default Docs