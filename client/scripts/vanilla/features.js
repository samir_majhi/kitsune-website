//1280 X 595

import { returnArray } from './helpers.js'
import skrollr from './skrollr.min.js'

let Features = {
    config: {
        nodes: {
            banner: document.querySelector('section.banner'),
            intro: document.querySelector('section.intro'),
            getFeature(feature) {
                return document.querySelector(`.features .feature-${feature}`)
            },
            features: {
                1: {
                    rocket: document.querySelector('.feature-1 .container-path'),
                    arrow: document.querySelector('.feature-1 .arrow')
                },
                2: {
                    arm: document.querySelector('.feature-2 .arm'),
                    screen: document.querySelector('.feature-2 .thankyou'),
                    light: document.querySelector('.feature-2 .light')
                },
                3: {
                    city1: document.querySelector('.feature-3 .city-1'),
                    city2: document.querySelector('.feature-3 .city-2'),
                    city3: document.querySelector('.feature-3 .city-3'),
                    site1: document.querySelector('.feature-3 .site-1'),
                    site2: document.querySelector('.feature-3 .site-2'),
                    site3: document.querySelector('.feature-3 .site-3')
                },
                4: {
                    cable: document.querySelector('.feature-4 .cable'),
                    box: document.querySelector('.feature-4 .box')
                },
                5: {
                    coin1: document.querySelector('.feature-5 .coin-1'),
                    coins: document.querySelector('.feature-5 .container-coins')
                }
            }
        },
        windowWidth: undefined
    },

    helpers: {
        getIntroHeight() {
            let bannerHeight = parseInt(window.getComputedStyle(Features.config.nodes.banner, null).height)
            let bannerPadding = parseInt(window.getComputedStyle(Features.config.nodes.banner, null).fontSize) - 10
            let introHeight = parseInt(window.getComputedStyle(Features.config.nodes.intro, null).height)
            return introHeight + bannerHeight + bannerPadding
        },

        getFeatureHeight(feature) {
            if (feature == 0) {
                return 0
            }
            return parseInt(window.getComputedStyle(Features.config.nodes.getFeature(feature), null).height) + parseInt(window.getComputedStyle(Features.config.nodes.getFeature(feature), null).paddingBottom) 
        },

        setWindowWidth() {
            Features.config.windowWidth = window.outerWidth
        },

        getFraction(fraction) {

            let proportion = undefined

            if (window.outerWidth > 1400) {
                proportion = 1
            }
            else if (window.outerWidth > 1050 && window.outerWidth <= 1400) {
                proportion = 1.1
            }
            else if (window.outerWidth <= 1050 && window.outerWidth > 800) {
                proportion = 0.95
            }
            else if (window.outerWidth <= 800 && window.outerWidth > 699) {
                proportion = 0.6
            }

            return proportion * fraction
        },

        getNodes() {
            let nodes = returnArray(Features.config.nodes.features).map((feature, index) => {
                return returnArray(Features.config.nodes.features[index + 1]).map(node => {
                    return node
                })
            })

            nodes = nodes.reduce((prev, next) => {
                return prev.concat(next)
            })

            return nodes
        },

        resetAnchors(node) {
            returnArray(node.attributes).forEach(attribute => {
                if (attribute.name.split('-')[0] == 'data') {
                    node.removeAttribute(attribute.name)
                }
            })
        }
    },

    methods: {
        init() {
            Features.methods.setFeature1()
            Features.methods.setFeature2()
            Features.methods.setFeature3()
            Features.methods.setFeature4()
            Features.methods.setFeature5()
            skrollr.init({ forceHeight: false })
        },

        setFeature1() {
            let rocketStart = Math.floor(Features.helpers.getIntroHeight() * Features.helpers.getFraction(0.55))
            let arrowStart = Math.floor(Features.helpers.getIntroHeight() * Features.helpers.getFraction(0.78))
            let rocketEnd = Math.floor(Features.helpers.getIntroHeight() * Features.helpers.getFraction(0.85))

            Features.config.nodes.features[1].rocket.setAttribute(`data-${rocketStart}`, 'height:7em;')
            Features.config.nodes.features[1].rocket.setAttribute(`data-${rocketEnd}`, 'height:31em;')
            Features.config.nodes.features[1].arrow.setAttribute(`data-${rocketStart}`, 'opacity:!0;')
            Features.config.nodes.features[1].arrow.setAttribute(`data-${arrowStart}`, 'opacity:!1;right:0em;')
            Features.config.nodes.features[1].arrow.setAttribute(`data-${rocketEnd}`, 'right:16.15em;')
        },

        setFeature2() {
            let cardStart = Math.floor(Features.helpers.getIntroHeight() + Features.helpers.getFeatureHeight(1) * Features.helpers.getFraction(0.5))
            let cardEnd = Math.floor(Features.helpers.getIntroHeight() + Features.helpers.getFeatureHeight(1) * Features.helpers.getFraction(0.7))
            let flashRange = Math.floor(Features.helpers.getIntroHeight() + Features.helpers.getFeatureHeight(1) * Features.helpers.getFraction(0.84) - cardEnd)
            let flashStep = flashRange / 6

            let lightFlast0 = cardEnd - flashStep
            let lightFlast1 = lightFlast0 + flashStep
            let lightFlast2 = lightFlast1 + flashStep
            let lightFlast3 = lightFlast2 + flashStep
            let lightFlast4 = lightFlast3 + flashStep
            let lightFlast5 = lightFlast4 + flashStep
            let lightFlast6 = lightFlast5 + flashStep

            Features.config.nodes.features[2].arm.setAttribute(`data-${cardStart}`, 'height: 0em;')
            Features.config.nodes.features[2].arm.setAttribute(`data-${cardEnd}`, 'height: 7.4em;')

            Features.config.nodes.features[2].light.setAttribute(`data-${lightFlast0}`, 'opacity: !0;')
            Features.config.nodes.features[2].light.setAttribute(`data-${lightFlast1}`, 'opacity: !1;')
            Features.config.nodes.features[2].light.setAttribute(`data-${lightFlast2}`, 'opacity: !0;')
            Features.config.nodes.features[2].light.setAttribute(`data-${lightFlast3}`, 'opacity: !1;')
            Features.config.nodes.features[2].light.setAttribute(`data-${lightFlast4}`, 'opacity: !0;')
            Features.config.nodes.features[2].light.setAttribute(`data-${lightFlast5}`, 'opacity: !1;')
            Features.config.nodes.features[2].light.setAttribute(`data-${lightFlast6}`, 'opacity: !0;')

            Features.config.nodes.features[2].screen.setAttribute(`data-${cardStart}`, 'opacity: !0;')
            Features.config.nodes.features[2].screen.setAttribute(`data-${lightFlast6}`, 'opacity: !1;')
        },

        setFeature3() {
            let cityStart = Math.floor(Features.helpers.getIntroHeight() + Features.helpers.getFeatureHeight(1) * Features.helpers.getFraction(1.5))
            let cityEnd = Math.floor(Features.helpers.getIntroHeight() + Features.helpers.getFeatureHeight(1) + Features.helpers.getFeatureHeight(2) * Features.helpers.getFraction(0.9))
            let cityRange = cityEnd - cityStart
            let cityStep = cityRange / 2
            let cityStep1 = cityStart + cityStep
            let cityStep2 = cityStep1 + cityStep

            Features.config.nodes.features[3].city1.setAttribute(`data-${cityStart - 100}`, 'opacity:!1')
            Features.config.nodes.features[3].site1.setAttribute(`data-${cityStart}`, 'left:0em')

            Features.config.nodes.features[3].city1.setAttribute(`data-${cityStart}`, 'opacity:!0')
            Features.config.nodes.features[3].site1.setAttribute(`data-${cityStep1}`, 'left:0em')

            Features.config.nodes.features[3].city2.setAttribute(`data-${cityStart - 100}`, 'opacity:!0')
            Features.config.nodes.features[3].site2.setAttribute(`data-${cityStart}`, 'left:-43em')

            Features.config.nodes.features[3].city2.setAttribute(`data-${cityStart}`, 'opacity:!1')
            Features.config.nodes.features[3].site2.setAttribute(`data-${cityStep1}`, 'left:0em')

            Features.config.nodes.features[3].city2.setAttribute(`data-${cityStep2}`, 'opacity:!0')
            Features.config.nodes.features[3].site2.setAttribute(`data-${cityStep2}`, 'left:0em')

            Features.config.nodes.features[3].city3.setAttribute(`data-${cityStep1}`, 'opacity:!0')
            Features.config.nodes.features[3].site3.setAttribute(`data-${cityStep1 + 100}`, 'left:-43em')

            Features.config.nodes.features[3].city3.setAttribute(`data-${cityStep2}`, 'opacity:!1')
            Features.config.nodes.features[3].site3.setAttribute(`data-${cityStep2 + 100}`, 'left:0em')
        },

        setFeature4() {
            let craneStart = Math.floor(Features.helpers.getIntroHeight() + Features.helpers.getFeatureHeight(1) + Features.helpers.getFeatureHeight(2) + Features.helpers.getFeatureHeight(3) * Features.helpers.getFraction(0.45))
            let boxEnd = Math.floor(Features.helpers.getIntroHeight() + Features.helpers.getFeatureHeight(1) + Features.helpers.getFeatureHeight(2) + Features.helpers.getFeatureHeight(3) * Features.helpers.getFraction(0.85))
            let cableEnd = Math.floor(Features.helpers.getIntroHeight() + Features.helpers.getFeatureHeight(1) + Features.helpers.getFeatureHeight(2) + Features.helpers.getFeatureHeight(3))

            Features.config.nodes.features[4].cable.setAttribute(`data-${craneStart}`, 'height:15.2em;')
            Features.config.nodes.features[4].cable.setAttribute(`data-${boxEnd}`, 'height:5em;')
            Features.config.nodes.features[4].cable.setAttribute(`data-${cableEnd}`, 'height:2.7em;')
            Features.config.nodes.features[4].box.setAttribute(`data-${craneStart}`, 'bottom:11.1em;')
            Features.config.nodes.features[4].box.setAttribute(`data-${boxEnd}`, 'bottom:21.15em;')

        },

        setFeature5() {
            let bankStart = Math.floor(Features.helpers.getIntroHeight() + Features.helpers.getFeatureHeight(1) + Features.helpers.getFeatureHeight(2) + Features.helpers.getFeatureHeight(3) + Features.helpers.getFeatureHeight(4) * Features.helpers.getFraction(0.7))
            let bankCoinEnd = bankStart + Math.floor(Features.helpers.getFeatureHeight(4) * Features.helpers.getFraction(0.65))
            let bankEnd = bankStart + Math.floor(Features.helpers.getFeatureHeight(4) * Features.helpers.getFraction(1.1))

            Features.config.nodes.features[5].coins.setAttribute(`data-${bankStart}`, 'top:-2.6em')
            Features.config.nodes.features[5].coin1.setAttribute(`data-${bankStart}`, 'opacity:!1')
            Features.config.nodes.features[5].coins.setAttribute(`data-${bankEnd}`, 'top:23em')
            Features.config.nodes.features[5].coin1.setAttribute(`data-${bankCoinEnd}`, 'opacity:!0')
        }
    },

    bindEvents() {
        //When window is being resized
        window.addEventListener('resize', () => {
            Features.helpers.setWindowWidth()
            Features.helpers.getNodes().forEach(node => Features.helpers.resetAnchors(node))
            Features.methods.init()
        })
    },

    init() {
        Features.bindEvents()
        Features.helpers.setWindowWidth()
        Features.methods.init()
    }
}

export default Features