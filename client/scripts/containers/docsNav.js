import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { getNav } from '../actions/index.js'

class DocsNav extends Component {
    componentWillMount() {
        this.props.getNav()
    }

    listGen(tree,level) {
        return (
            <ul className={`level-${level}`}>
                {
                    tree.map(leaf => {
                        if (leaf.children) {
                            return (
                                <li key={leaf.parent} className={`${leaf.parent}-${level}`}>
                                    <span>{leaf.parent}</span>
                                    {this.listGen(leaf.children,level + 1) }
                                </li>
                            )
                        }
                        else {
                            return (
                                <li key={leaf.parent} className={`${leaf.parent}-${level}`}><span>{leaf.parent}</span></li>
                            )
                        }
                    })
                }
                <div className={`indicator-level-${level}`}></div>
            </ul>
        )
    }

    render() {
        return (
            <div className="docs-nav">
                {this.listGen(this.props.nav, 1)}
            </div>
        )
    }
}

function mapStateToProps({ nav }) {
    return { nav }
}

export default connect(mapStateToProps, { getNav })(DocsNav)