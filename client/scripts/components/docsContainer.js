import React, { Component } from 'react'
import hljs from 'highlight.js'

class DocsContainer extends Component {
    componentWillMount() {
        hljs.initHighlightingOnLoad()
    }
    
    render() {
        return(
            <div className="docs-container">

                <h3 className="head-main">
                    Update
                </h3>

                <p className="copy">
                    An update about the business that the owner shares on his or her website.
                    <br/>
                    In the age of Facebook and Twitter, this is how people now want to consume data.
                    <br/>
                    Fresh and Relevant updates are one of the most powerful drivers of SEO
                </p>

                <h3 className="head-main head-1">
                    k-WidgetProps
                </h3>

                <table className="prop-table" cellspacing="0" cellpadding="0">
                    <tbody>
                        <tr>
                            <td className="prop-name">Update-Description</td>
                            <td className="prop-desc">The text content of the update.</td>
                        </tr>
                        <tr>
                            <td className="prop-name">Time-Stamp</td>
                            <td className="prop-desc">
                                When the update was posted.
                                <br/>
                                Use k-DisplayAs=”Date” to display it as a Date
                                <br/>
                                You can specify the format to specify the date using k-DisplayFormat=”FORMAT” Where FORMAT can be either of the following: dd-mm-yyyy, yyyy-mm-dd, epoch-timestamp, dd/mm/yyyy, yyyy/mm/dd
                            </td>
                        </tr>
                        <tr>
                            <td className="prop-name">Update-URL</td>
                            <td className="prop-desc">This is the link to the details page for this particular update. It will replace the value of href with the link to the details page for this particular update.</td>
                        </tr>
                        <tr>
                            <td className="prop-name">Update-Image</td>
                            <td className="prop-desc">The business owner can choose to add a picture to the update. It will be replaced in img url=””</td>
                        </tr>
                        <tr>
                            <td className="prop-name">Update-View-Count</td>
                            <td className="prop-desc">The number of times this update has been seen</td>
                        </tr>
                        <tr>
                            <td className="prop-name">Update-Tile-Image</td>
                            <td className="prop-desc">The Tile sized image for the picture attached with the update</td>
                        </tr>
                        <tr>
                            <td className="prop-name">Update-Type</td>
                            <td className="prop-desc">use k-WidgetProp=”LATEST/FEATURED/POPULAR”. Latest means newest. Featured are certain updates that the business owner wants pinned to the top. Popular are updates that have the highest views.</td>
                        </tr>
                        <tr>
                            <td className="prop-name">Update-Tags</td>
                            <td className="prop-desc">These will contain the most important keywords extracted from the Update-Description</td>
                        </tr>
                        <tr>
                            <td className="prop-name">Video-URL</td>
                            <td className="prop-desc">If a Video is added, this will contain the URL</td>
                        </tr>
                        <tr>
                            <td className="prop-name">New-Update-Flag</td>
                            <td className="prop-desc">This will contain information on whether the update is new or not. You can set what “new” means in the New-Update-Cutoff k-WidgetProp</td>
                        </tr>
                        <tr>
                            <td className="prop-name">New-Update-Cutoff</td>
                            <td className="prop-desc">UX Developer specification of how recent is new (E.g 2m or 1h or 8d or 12w or 3M or 19y)</td>
                        </tr>
                        <tr>
                            <td className="prop-name">Update-Share-Url</td>
                            <td className="prop-desc">This is used for sharing on Facebook, Twitter, Google+ etc. It will append the URL for the details page for this particular update to the value of href</td>
                        </tr>
                    </tbody>
                </table>

                <h3 className="head-main head-2">
                    Code
                </h3>

                <pre>
                    <code className="html">
                        &lt;div k-Widget="Update"&gt;<br/>
                            &nbsp;&nbsp;&nbsp;&nbsp;&lt;p k-WidgetProp="Update-Description"&gt;&lt;/p&gt;<br/>
                            &nbsp;&nbsp;&nbsp;&nbsp;&lt;span k-WidgetProp="Time-Stamp">&lt;/span&gt;<br/>
                            &nbsp;&nbsp;&nbsp;&nbsp;&lt;a href="" k-WidgetProp="Update-Url" k-DisplayAs="Link" ><br/>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;View Details<br/>
                            &nbsp;&nbsp;&nbsp;&nbsp;&lt;/a&gt;<br/>
                            &nbsp;&nbsp;&nbsp;&nbsp;&lt;img src=”” k-WidgetProp=”Update-Image”/&gt;<br/>
                            &nbsp;&nbsp;&nbsp;&nbsp;&lt;span k-WidgetProp=”Update-View-Count”&gt;<br/>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;span&gt; is the number of views this update has had&lt;/span&gt;<br/>
                            &nbsp;&nbsp;&nbsp;&nbsp;&lt;/span&gt;<br/>
                            &nbsp;&nbsp;&nbsp;&nbsp;&lt;img src=”” k-WidgetProp=”Update-Tile-Image”/&gt;<br/>
                            &nbsp;&nbsp;&nbsp;&nbsp;&lt;span k-WidgetProp=”Latest”&gt;&lt;/span&gt;<br/>
                            &nbsp;&nbsp;&nbsp;&nbsp;&lt;a k-WidgetProp="Update-Tags" href=""&gt;&lt;/a&gt;<br/>
                            &nbsp;&nbsp;&nbsp;&nbsp;&lt;iframe k-WidgetProp=”Video-URL” src=""/&gt;<br/>
                            &nbsp;&nbsp;&nbsp;&nbsp;&lt;span k-WidgetProp=”New-Update-Flag”&gt;<br/>
                            &nbsp;&nbsp;&nbsp;&nbsp;&lt;span k-WidgetProp=”New-Flag-Cutoff”&gt;5h<br/>
                            &nbsp;&nbsp;&nbsp;&nbsp;&lt;a href=”https://www.facebook.com/sharer/sharer.php?u=” k-WidgetProp=”Update-Share-URL”&gt;<br/>
                        &lt;/div&gt;
                    </code>
                    <div className="type">Template</div>
                </pre>

                <pre>
                    <code className="html">
                        &lt;div k-Widget="Update"&gt;<br/>
                            &nbsp;&nbsp;&nbsp;&nbsp;&lt;p k-WidgetProp="Update-Description"&gt;&lt;/p&gt;<br/>
                            &nbsp;&nbsp;&nbsp;&nbsp;&lt;span k-WidgetProp="Time-Stamp">&lt;/span&gt;<br/>
                            &nbsp;&nbsp;&nbsp;&nbsp;&lt;a href="" k-WidgetProp="Update-Url" k-DisplayAs="Link" ><br/>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;View Details<br/>
                            &nbsp;&nbsp;&nbsp;&nbsp;&lt;/a&gt;<br/>
                            &nbsp;&nbsp;&nbsp;&nbsp;&lt;img src=”” k-WidgetProp=”Update-Image”/&gt;<br/>
                            &nbsp;&nbsp;&nbsp;&nbsp;&lt;span k-WidgetProp=”Update-View-Count”&gt;<br/>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;span&gt; is the number of views this update has had&lt;/span&gt;<br/>
                            &nbsp;&nbsp;&nbsp;&nbsp;&lt;/span&gt;<br/>
                            &nbsp;&nbsp;&nbsp;&nbsp;&lt;img src=”” k-WidgetProp=”Update-Tile-Image”/&gt;<br/>
                            &nbsp;&nbsp;&nbsp;&nbsp;&lt;span k-WidgetProp=”Latest”&gt;&lt;/span&gt;<br/>
                            &nbsp;&nbsp;&nbsp;&nbsp;&lt;a k-WidgetProp="Update-Tags" href=""&gt;&lt;/a&gt;<br/>
                            &nbsp;&nbsp;&nbsp;&nbsp;&lt;iframe k-WidgetProp=”Video-URL” src=""/&gt;<br/>
                            &nbsp;&nbsp;&nbsp;&nbsp;&lt;span k-WidgetProp=”New-Update-Flag”&gt;<br/>
                            &nbsp;&nbsp;&nbsp;&nbsp;&lt;span k-WidgetProp=”New-Flag-Cutoff”&gt;5h<br/>
                            &nbsp;&nbsp;&nbsp;&nbsp;&lt;a href=”https://www.facebook.com/sharer/sharer.php?u=” k-WidgetProp=”Update-Share-URL”&gt;<br/>
                        &lt;/div&gt;
                    </code>
                    <div className="type">Server Response</div>
                </pre>
            </div>
        )
    }
}

export default DocsContainer