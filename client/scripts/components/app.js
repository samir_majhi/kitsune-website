import React, { Component } from 'react'
import MainNav from './mainNav.js'
import DocsMain from './docsMain.js'

class App extends Component {
    render() {
        return (
            <div>
                <MainNav/>
                <DocsMain/>
            </div>
        )
    }
}

export default App