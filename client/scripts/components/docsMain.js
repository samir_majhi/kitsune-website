import React from 'react'
import DocsNav from '../containers/docsNav.js'
import DocsContainer from './docsContainer.js'

const DocsMain = () => (
    <section className="docs-main">
        <DocsNav/>
        <DocsContainer/>
        <div className="underlay-docs"></div>
    </section>
)

export default DocsMain