import React from 'react'

const MainNav = () => (
    <section className="nav">
        <img src="assets/images/logo.png" className="logo"/>

            <ul className="nav-main">
                <li><span>Business</span></li>
                <li><span>Developers</span></li>
                <li><span>Documentation</span></li>
                <div className="underline"></div>
            </ul>

            <div className="nav-mobile">
                <div className="hamburger">
                    <span class="line line-1"></span>
                    <span class="line line-2"></span>
                    <span class="line line-3"></span>
                </div>

                <ul className="links">
                    <div class="wrapper-links">
                        <li data-scroll="1"><span>Discovery</span></li>
                        <li data-scroll="2"><span>Action</span></li>
                        <li data-scroll="3"><span>Adaptation</span></li>
                        <li data-scroll="4"><span>Design</span></li>
                        <li data-scroll="5"><span>Economical</span></li>
                        <li data-scroll="6"><span>Contact</span></li>
                    </div>
                </ul>

            </div>
    </section>
)

export default MainNav
