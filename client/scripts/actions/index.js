import axios from 'axios'

export const GET_NAV = 'GET_TAGS'

export function getNav() {
    const request = axios.get('/api/nav')
    return {
        type: GET_NAV,
        payload: request 
    }
}