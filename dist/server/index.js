'use strict';

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

var _fs = require('fs');

var _fs2 = _interopRequireDefault(_fs);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var root = _path2.default.join(__dirname, '../../');

var app = (0, _express2.default)();

var apiRoutes = function apiRoutes() {
    return _express2.default.Router().get('/nav', function (req, res) {
        var nav = JSON.parse(_fs2.default.readFileSync(_path2.default.join(root, 'server/data.json'), 'utf8')).nav;
        var navigation = [];
        var tags = nav['tags'];

        tags.forEach(function (tag) {
            navigation.push({
                'parent': tag
            });
        });

        navigation.forEach(function (tag) {
            if (nav['widgets'][tag['parent']]) {
                tag['children'] = [];
                nav['widgets'][tag['parent']].forEach(function (widget) {
                    tag['children'].push({
                        'parent': widget
                    });
                });
            }
        });

        navigation.forEach(function (tag) {
            if (tag['children']) {
                tag['children'].forEach(function (widget) {
                    if (nav['widgetProps'][widget['parent']]) {
                        widget['children'] = [];
                        nav['widgetProps'][widget['parent']].forEach(function (prop) {
                            widget['children'].push({
                                'parent': prop
                            });
                        });
                    }
                });
            }
        });

        res.send(navigation);
    });
};

app.use('/assets', _express2.default.static(_path2.default.resolve(__dirname, '../client')));

app.use('/api', apiRoutes());

app.get('/biz', function (req, res) {
    res.sendFile(_path2.default.resolve(__dirname, '../client/index.html'));
});

app.get('/dev', function (req, res) {
    res.sendFile(_path2.default.resolve(__dirname, '../client/dev.html'));
});

app.get('/docs', function (req, res) {
    res.sendFile(_path2.default.resolve(__dirname, '../client/docs.html'));
});

app.get('/', function (req, res) {
    res.sendFile(_path2.default.resolve(__dirname, '../client/index.html'));
});

app.listen(process.env.PORT || 8081, function () {
    console.log('serving at port 8081');
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImluZGV4LmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUE7Ozs7QUFDQTs7OztBQUNBOzs7Ozs7QUFFQSxJQUFNLE9BQU8sZUFBSyxJQUFMLENBQVUsU0FBVixFQUFxQixRQUFyQixDQUFiOztBQUVBLElBQU0sTUFBTSx3QkFBWjs7QUFFQSxJQUFJLFlBQVksU0FBWixTQUFZO0FBQUEsV0FDWixrQkFBUSxNQUFSLEdBQ0ssR0FETCxDQUNTLE1BRFQsRUFDaUIsVUFBQyxHQUFELEVBQU0sR0FBTixFQUFjO0FBQ3ZCLFlBQU0sTUFBTSxLQUFLLEtBQUwsQ0FBVyxhQUFHLFlBQUgsQ0FBZ0IsZUFBSyxJQUFMLENBQVUsSUFBVixFQUFnQixrQkFBaEIsQ0FBaEIsRUFBcUQsTUFBckQsQ0FBWCxFQUF5RSxHQUFyRjtBQUNBLFlBQU0sYUFBYSxFQUFuQjtBQUNBLFlBQUksT0FBTyxJQUFJLE1BQUosQ0FBWDs7QUFFQSxhQUFLLE9BQUwsQ0FBYSxlQUFPO0FBQ2hCLHVCQUFXLElBQVgsQ0FBZ0I7QUFDWiwwQkFBVTtBQURFLGFBQWhCO0FBR0gsU0FKRDs7QUFNQSxtQkFBVyxPQUFYLENBQW1CLGVBQU87QUFDdEIsZ0JBQUksSUFBSSxTQUFKLEVBQWUsSUFBSSxRQUFKLENBQWYsQ0FBSixFQUFtQztBQUMvQixvQkFBSSxVQUFKLElBQWtCLEVBQWxCO0FBQ0Esb0JBQUksU0FBSixFQUFlLElBQUksUUFBSixDQUFmLEVBQThCLE9BQTlCLENBQXNDLGtCQUFVO0FBQzVDLHdCQUFJLFVBQUosRUFBZ0IsSUFBaEIsQ0FBcUI7QUFDakIsa0NBQVU7QUFETyxxQkFBckI7QUFHSCxpQkFKRDtBQUtIO0FBQ0osU0FURDs7QUFXQSxtQkFBVyxPQUFYLENBQW1CLGVBQU87QUFDdEIsZ0JBQUksSUFBSSxVQUFKLENBQUosRUFBcUI7QUFDakIsb0JBQUksVUFBSixFQUFnQixPQUFoQixDQUF3QixrQkFBVTtBQUM5Qix3QkFBSSxJQUFJLGFBQUosRUFBbUIsT0FBTyxRQUFQLENBQW5CLENBQUosRUFBMEM7QUFDdEMsK0JBQU8sVUFBUCxJQUFxQixFQUFyQjtBQUNBLDRCQUFJLGFBQUosRUFBbUIsT0FBTyxRQUFQLENBQW5CLEVBQXFDLE9BQXJDLENBQTZDLGdCQUFRO0FBQ2pELG1DQUFPLFVBQVAsRUFBbUIsSUFBbkIsQ0FDSTtBQUNJLDBDQUFVO0FBRGQsNkJBREo7QUFLSCx5QkFORDtBQU9IO0FBQ0osaUJBWEQ7QUFZSDtBQUNKLFNBZkQ7O0FBaUJBLFlBQUksSUFBSixDQUFTLFVBQVQ7QUFDSCxLQXpDTCxDQURZO0FBQUEsQ0FBaEI7O0FBNkNBLElBQUksR0FBSixDQUFRLFNBQVIsRUFBbUIsa0JBQVEsTUFBUixDQUFlLGVBQUssT0FBTCxDQUFhLFNBQWIsRUFBd0IsV0FBeEIsQ0FBZixDQUFuQjs7QUFFQSxJQUFJLEdBQUosQ0FBUSxNQUFSLEVBQWdCLFdBQWhCOztBQUVBLElBQUksR0FBSixDQUFRLE1BQVIsRUFBZ0IsVUFBQyxHQUFELEVBQU0sR0FBTixFQUFjO0FBQzFCLFFBQUksUUFBSixDQUFhLGVBQUssT0FBTCxDQUFhLFNBQWIsRUFBd0Isc0JBQXhCLENBQWI7QUFDSCxDQUZEOztBQUlBLElBQUksR0FBSixDQUFRLE1BQVIsRUFBZ0IsVUFBQyxHQUFELEVBQU0sR0FBTixFQUFjO0FBQzFCLFFBQUksUUFBSixDQUFhLGVBQUssT0FBTCxDQUFhLFNBQWIsRUFBd0Isb0JBQXhCLENBQWI7QUFDSCxDQUZEOztBQUlBLElBQUksR0FBSixDQUFRLE9BQVIsRUFBaUIsVUFBQyxHQUFELEVBQU0sR0FBTixFQUFjO0FBQzNCLFFBQUksUUFBSixDQUFhLGVBQUssT0FBTCxDQUFhLFNBQWIsRUFBd0IscUJBQXhCLENBQWI7QUFDSCxDQUZEOztBQUlBLElBQUksR0FBSixDQUFRLEdBQVIsRUFBYSxVQUFDLEdBQUQsRUFBTSxHQUFOLEVBQWM7QUFDdkIsUUFBSSxRQUFKLENBQWEsZUFBSyxPQUFMLENBQWEsU0FBYixFQUF3QixzQkFBeEIsQ0FBYjtBQUNILENBRkQ7O0FBSUEsSUFBSSxNQUFKLENBQVcsUUFBUSxHQUFSLENBQVksSUFBWixJQUFvQixJQUEvQixFQUFxQyxZQUFNO0FBQ3ZDLFlBQVEsR0FBUixDQUFZLHNCQUFaO0FBQ0gsQ0FGRCIsImZpbGUiOiJpbmRleC5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBleHByZXNzIGZyb20gJ2V4cHJlc3MnXG5pbXBvcnQgcGF0aCBmcm9tICdwYXRoJ1xuaW1wb3J0IGZzIGZyb20gJ2ZzJ1xuXG5jb25zdCByb290ID0gcGF0aC5qb2luKF9fZGlybmFtZSwgJy4uLy4uLycpXG5cbmNvbnN0IGFwcCA9IGV4cHJlc3MoKVxuXG5sZXQgYXBpUm91dGVzID0gKCkgPT4gKFxuICAgIGV4cHJlc3MuUm91dGVyKClcbiAgICAgICAgLmdldCgnL25hdicsIChyZXEsIHJlcykgPT4ge1xuICAgICAgICAgICAgY29uc3QgbmF2ID0gSlNPTi5wYXJzZShmcy5yZWFkRmlsZVN5bmMocGF0aC5qb2luKHJvb3QsICdzZXJ2ZXIvZGF0YS5qc29uJyksICd1dGY4JykpLm5hdlxuICAgICAgICAgICAgY29uc3QgbmF2aWdhdGlvbiA9IFtdXG4gICAgICAgICAgICBsZXQgdGFncyA9IG5hdlsndGFncyddXG5cbiAgICAgICAgICAgIHRhZ3MuZm9yRWFjaCh0YWcgPT4ge1xuICAgICAgICAgICAgICAgIG5hdmlnYXRpb24ucHVzaCh7XG4gICAgICAgICAgICAgICAgICAgICdwYXJlbnQnOiB0YWdcbiAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgfSlcblxuICAgICAgICAgICAgbmF2aWdhdGlvbi5mb3JFYWNoKHRhZyA9PiB7XG4gICAgICAgICAgICAgICAgaWYgKG5hdlsnd2lkZ2V0cyddW3RhZ1sncGFyZW50J11dKSB7XG4gICAgICAgICAgICAgICAgICAgIHRhZ1snY2hpbGRyZW4nXSA9IFtdXG4gICAgICAgICAgICAgICAgICAgIG5hdlsnd2lkZ2V0cyddW3RhZ1sncGFyZW50J11dLmZvckVhY2god2lkZ2V0ID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRhZ1snY2hpbGRyZW4nXS5wdXNoKHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAncGFyZW50Jzogd2lkZ2V0XG4gICAgICAgICAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pXG5cbiAgICAgICAgICAgIG5hdmlnYXRpb24uZm9yRWFjaCh0YWcgPT4ge1xuICAgICAgICAgICAgICAgIGlmICh0YWdbJ2NoaWxkcmVuJ10pIHtcbiAgICAgICAgICAgICAgICAgICAgdGFnWydjaGlsZHJlbiddLmZvckVhY2god2lkZ2V0ID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChuYXZbJ3dpZGdldFByb3BzJ11bd2lkZ2V0WydwYXJlbnQnXV0pIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB3aWRnZXRbJ2NoaWxkcmVuJ10gPSBbXVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG5hdlsnd2lkZ2V0UHJvcHMnXVt3aWRnZXRbJ3BhcmVudCddXS5mb3JFYWNoKHByb3AgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB3aWRnZXRbJ2NoaWxkcmVuJ10ucHVzaChcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAncGFyZW50JzogcHJvcFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KVxuXG4gICAgICAgICAgICByZXMuc2VuZChuYXZpZ2F0aW9uKVxuICAgICAgICB9KVxuKVxuXG5hcHAudXNlKCcvYXNzZXRzJywgZXhwcmVzcy5zdGF0aWMocGF0aC5yZXNvbHZlKF9fZGlybmFtZSwgJy4uL2NsaWVudCcpKSlcblxuYXBwLnVzZSgnL2FwaScsIGFwaVJvdXRlcygpKVxuXG5hcHAuZ2V0KCcvYml6JywgKHJlcSwgcmVzKSA9PiB7XG4gICAgcmVzLnNlbmRGaWxlKHBhdGgucmVzb2x2ZShfX2Rpcm5hbWUsICcuLi9jbGllbnQvaW5kZXguaHRtbCcpKVxufSlcblxuYXBwLmdldCgnL2RldicsIChyZXEsIHJlcykgPT4ge1xuICAgIHJlcy5zZW5kRmlsZShwYXRoLnJlc29sdmUoX19kaXJuYW1lLCAnLi4vY2xpZW50L2Rldi5odG1sJykpXG59KVxuXG5hcHAuZ2V0KCcvZG9jcycsIChyZXEsIHJlcykgPT4ge1xuICAgIHJlcy5zZW5kRmlsZShwYXRoLnJlc29sdmUoX19kaXJuYW1lLCAnLi4vY2xpZW50L2RvY3MuaHRtbCcpKVxufSlcblxuYXBwLmdldCgnLycsIChyZXEsIHJlcykgPT4ge1xuICAgIHJlcy5zZW5kRmlsZShwYXRoLnJlc29sdmUoX19kaXJuYW1lLCAnLi4vY2xpZW50L2luZGV4Lmh0bWwnKSlcbn0pXG5cbmFwcC5saXN0ZW4ocHJvY2Vzcy5lbnYuUE9SVCB8fCA4MDgxLCAoKSA9PiB7XG4gICAgY29uc29sZS5sb2coJ3NlcnZpbmcgYXQgcG9ydCA4MDgxJylcbn0pIl0sInNvdXJjZVJvb3QiOiIvc291cmNlLyJ9
