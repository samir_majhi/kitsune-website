(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _helpers = require('./helpers.js');

var boosters = {
    config: {
        nodes: {
            boosters: (0, _helpers.returnArray)(document.querySelectorAll('.container-booster')),
            section: document.querySelector('.section-boosters'),
            containerBoosters: document.querySelector('.container-boosters'),
            close: document.querySelectorAll('.container-boosters .close')
        },
        containerBoostersHeight: document.querySelector('.container-boosters') ? document.querySelector('.container-boosters').offsetHeight : undefined
    },

    state: {
        current: undefined,
        currentIndex: undefined
    },

    init: function init() {
        boosters.bindEvents();
    },
    bindEvents: function bindEvents() {

        // When a booster gets clicked
        boosters.config.nodes.boosters.forEach(function (booster, index) {
            booster.addEventListener('click', function (event) {
                event.stopPropagation();
                boosters.state.current = event.currentTarget;
                boosters.state.currentIndex = index;
                boosters.methods.open();
                boosters.methods.adjustHeight();
            });
        });

        //Close button gets clicked
        boosters.config.nodes.close.forEach(function (close) {
            close.addEventListener('click', function (event) {
                event.stopPropagation();
                boosters.methods.close();
                boosters.methods.adjustHeight();
            });
        });

        //Click anywhere on the document
        document.addEventListener('click', function () {
            boosters.methods.close();
            boosters.methods.adjustHeight();
        });

        //Press escape
        document.addEventListener('keydown', function (event) {
            if (event.which == 27) {
                boosters.methods.close();
                boosters.methods.adjustHeight();
            }
        });
    },


    methods: {
        open: function open() {
            var alignIndex = boosters.state.currentIndex % 4 + 1;
            boosters.state.current.classList.add('booster-expand', 'booster-top', 'booster-expand-' + alignIndex);
            boosters.config.nodes.boosters.forEach(function (booster, index) {
                if (index != boosters.state.currentIndex) {
                    booster.classList.add('booster-no-click');
                }
            });
        },
        close: function close() {
            boosters.config.nodes.boosters.forEach(function (booster, index) {
                booster.classList.remove('booster-no-click', 'booster-expand', 'booster-expand-1', 'booster-expand-2', 'booster-expand-3', 'booster-expand-4');
                if (index != boosters.state.currentIndex) {
                    booster.classList.remove('booster-top');
                }
            });
            setTimeout(function () {
                boosters.state.current.classList.remove('booster-top');
            }, 300);
        },
        adjustHeight: function adjustHeight() {
            var row = Math.floor((boosters.state.currentIndex + 1) % 4);
            var sectionHeight = boosters.config.nodes.section.offsetHeight;
            var containerBoostersHeight = boosters.config.nodes.containerBoosters.offsetHeight;
            var increment = boosters.state.current.childNodes[1].childNodes[3].offsetHeight - containerBoostersHeight;
            var incremented = increment + containerBoostersHeight * 1.08;

            if (boosters.state.current.classList.contains('booster-expand')) {
                if (increment > 0) {
                    boosters.config.nodes.containerBoosters.style.height = incremented + 'px';
                }
            } else {
                boosters.config.nodes.containerBoosters.style.height = boosters.config.containerBoostersHeight + 'px';
            }
        }
    }
};

exports.default = boosters;

},{"./helpers.js":5}],2:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _helpers = require('./helpers.js');

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

var Console = {
    config: {
        active: undefined,
        order: ['entity', 'view', 'widget', 'widgetProps'],
        nodes: {
            indicators: (0, _helpers.returnArray)(document.querySelectorAll('.indicator')),
            markers: (0, _helpers.returnArray)(document.querySelectorAll('.section-console .marker')),
            code: (0, _helpers.returnArray)(document.querySelectorAll('.code'))
        },
        state: {
            cycleInterval: undefined
        }
    },

    init: function init() {
        Console.bindEvents();
        document.querySelector('.indicator-entity').click();
    },
    bindEvents: function bindEvents() {
        //On clicking an indicator
        Console.config.nodes.indicators.forEach(function (indicator) {
            indicator.addEventListener('click', function (event) {
                event.stopPropagation();
                Console.methods.toggle.init(event.currentTarget);
            });
        });

        //Start cycling when scrolled to section
        document.addEventListener('scroll', function () {
            Console.methods.cycle();
        });
    },


    methods: {
        toggle: {
            init: function init(indicator, nocycle) {
                if (!nocycle) {
                    clearInterval(Console.config.state.cycleInterval);
                }
                Console.methods.toggle.activate(indicator);
                Console.methods.toggle.component();
                Console.methods.toggle.explaination();
                Console.methods.toggle.markers();
                Console.methods.toggle.indicator();
                Console.methods.toggle.next();
                Console.methods.toggle.code();
            },
            activate: function activate(indicator) {
                var name = indicator.classList[1].split('-')[1];
                var component = document.querySelector('.component-' + name);
                Console.config.active = { component: component, name: name };
            },
            component: function component() {
                var components = [].concat(_toConsumableArray(Console.config.order));
                var index = components.indexOf(Console.config.active.name) + 1;
                var number = components.length - index;
                var toFocus = [].concat(_toConsumableArray(components)).splice(0, index);
                var toUnfocus = [].concat(_toConsumableArray(components)).splice(-number, number);
                toUnfocus.forEach(function (component) {
                    document.querySelector('.component-' + component).setAttribute('class', 'component component-' + component);
                });
                toFocus.forEach(function (component) {
                    document.querySelector('.component-' + component).setAttribute('class', 'component component-' + component + ' focus-' + component);
                });
                components.forEach(function (component) {
                    document.querySelector('.component-' + component).classList.remove('color-' + component);
                });
                Console.config.active.component.classList.add('color-' + Console.config.active.name);
            },
            explaination: function explaination() {
                var exps = (0, _helpers.returnArray)(document.querySelectorAll('.exp'));
                exps.forEach(function (exp) {
                    var current = exp.classList[1].split('-')[1];
                    if (current != Console.config.active.name) {
                        exp.setAttribute('class', 'exp exp-' + current);
                    } else {
                        exp.setAttribute('class', 'exp exp-' + current + ' exp-focus');
                    }
                });
            },
            markers: function markers() {
                Console.config.nodes.markers.forEach(function (marker) {
                    var defaultClass = marker.classList[1];
                    marker.setAttribute('class', 'marker ' + defaultClass);
                    marker.classList.add('marker-' + Console.config.active.name);
                });
            },
            indicator: function indicator() {
                Console.config.nodes.indicators.forEach(function (indicator) {
                    indicator.classList.remove('indicator-focus');
                });
                document.querySelector('.indicator-' + Console.config.active.name).classList.add('indicator-focus');
            },
            next: function next() {
                var button = document.querySelector('.next-button');
                var defaultClass = button.classList[0];
                button.setAttribute('class', defaultClass);
                button.classList.add('next-' + Console.config.active.name);
            },
            code: function code() {
                Console.config.nodes.code.forEach(function (line) {
                    var defaultClass = line.classList[1];
                    line.setAttribute('class', 'code ' + defaultClass);
                });

                var selection = (0, _helpers.returnArray)(document.querySelectorAll('.code-' + Console.config.active.name));
                selection.forEach(function (selected) {
                    selected.classList.add('focus-' + Console.config.active.name);
                });
            }
        },

        cycle: function cycle() {
            var scrollPos = document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop;
            var trigger = document.querySelector('.section-intro .wrapper-banner').clientHeight + document.querySelector('.section-intro .wrapper-content').clientHeight + document.querySelector('.section-intro .wrapper-features').clientHeight;
            if (scrollPos >= trigger && Console.config.state.cycleInterval == undefined) {
                Console.config.state.cycleInterval = setInterval(function () {
                    var current = Console.config.order.indexOf(Console.config.active.name);
                    var next = current < 3 ? Console.config.order[current + 1] : 'entity';
                    var component = document.querySelector('.component-' + next);
                    Console.methods.toggle.init(document.querySelector('.indicator-' + next), 1);
                }, 5000);
            }
        }
    }
};

exports.default = Console;

},{"./helpers.js":5}],3:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _helpers = require('./helpers.js');

var Docs = {
    config: {
        views: {
            master: {
                widgets: ['Business-Name', 'Business-Description', 'Update', 'Product', 'Business-Hours', 'All-Products', 'Subscribers-Count', 'Tags', 'Visits', 'Business-Enquiries', 'Video-Gallery', 'Site-Link', 'Address', 'Mobile-Numbers', 'Subscribe', 'Search', 'Logo', 'Map', 'Facebook-Like-Box', 'Background-Image', 'Favicon'],
                mandatory: ['Business-Name', 'Business-Description', 'Update', 'Product', 'Business-Hours', 'All-Products', 'Subscribers-Count', 'Tags', 'Visits', 'Business-Enquiries', 'Video-Gallery', 'Site-Link', 'Address', 'Mobile-Numbers', 'Subscribe', 'Search', 'Logo', 'Map', 'Facebook-Like-Box', 'Background-Image', 'Favicon']
            },
            Home: {
                widgets: ['Business-Name', 'Address', 'Mobile-Numbers', 'Business-Enquiries', 'Business-Description', 'Update', 'Offers', 'All-Products', 'Business-Hours', 'Background-Image', 'Image-Gallery', 'Subscribers-Count', 'Tags', 'Map', 'Visits', 'Custom', 'Video-Gallery', 'Logo', 'Facebook-Like-Box'],
                mandatory: ['Business-Name', 'Address', 'Mobile-Numbers', 'Business-Enquiries']
            },
            Offers: {
                widgets: ['Update', 'Mobile-Numbers', 'Business-Enquiries', 'Business-Name', 'Business-Description', 'Address', 'Background-Image'],
                mandatory: ['Update']
            },
            Update: {
                widgets: ['Update', 'Business-Name', 'Address', 'Mobile-Numbers', 'Business-Enquiries', 'Business-Description', 'Tags', 'Background-Image'],
                mandatory: ['Update']
            },
            'All-Products': {
                widgets: ['Product', 'Business-Enquiries', 'Mobile-Numbers', 'Address', 'Business-Name', 'Business-Description', 'Background-Image'],
                mandatory: ['Product']
            },
            'Update-Details': {
                widgets: ['Update', 'Mobile-Numbers', 'Business-Enquiries', 'Address', 'Business-Name', 'Business-Description', 'Background-Image'],
                mandatory: ['Update']
            },
            'Product-Details': {
                widgets: ['Product', 'Mobile-Numbers', 'Business-Enquiries', 'Address', 'Business-Name', 'Business-Description', 'Background-Image'],
                mandatory: ['Product']
            },
            Custom: {
                widgets: ['Background-Image', 'Address', 'Mobile-Numbers', 'Business-Enquiries', 'Business-Name', 'Business-Hours'],
                mandatory: []
            },
            Search: {
                widgets: ['Update', 'Background-Image', 'Business-Name'],
                mandatory: ['Update']
            },
            'Product-Search': {
                widgets: ['Product', 'Background-Image'],
                mandatory: ['Product']
            },
            'Image-Gallery': {
                widgets: ['Image'],
                mandatory: ['Image']
            },
            Map: {
                widgets: ['Map', 'Business-Name', 'Background-Image'],
                mandatory: ['Map']
            }
        },
        widgets: {
            'Business-Name': {
                props: ['Business-Name'],
                mandatory: ['Business-Name'],
                copy: {
                    widget: 'This is the Business-Name widget',
                    props: ['This is the Business-Name prop']
                }
            },
            'Business-Description': {
                props: ['Business-Description'],
                mandatory: ['Business-Description'],
                copy: {
                    widget: 'This is the Business-Description widget',
                    props: ['This is the Business-Description prop']
                }
            },
            Update: {
                props: ['Update', 'TimeStamp', 'Url', 'Business-Image', 'Type', 'Tags'],
                mandatory: ['Update', 'TimeStamp', 'Url'],
                copy: {
                    widget: 'This is the Update widget',
                    props: ['This is the Update prop', 'This is the TimeStamp prop', 'This is the Url prop', 'This is the Business-Image prop', 'This is the Type (LATEST/FEATURED/POPULAR) prop', 'This is the Tags prop']
                }
            },
            Product: {
                props: ['Name', 'Description', 'Cost', 'Url', 'Currency', 'Availability', 'Image'],
                mandatory: ['Name', 'Description', 'Cost', 'Url', 'Currency'],
                copy: {
                    widget: 'This is the Product widget',
                    props: ['This is the Name prop This is the Name prop This is the Name prop This is the Name prop This is the Name prop This is the Name prop This is the Name prop', 'This is the Description prop This is the Name prop This is the Name prop This is the Name prop This is the Name prop This is the Name prop This is the Name prop', 'This is the Cost prop This is the Name prop This is the Name prop This is the Name prop This is the Name prop This is the Name prop This is the Name prop', 'This is the Url prop This is the Name prop This is the Name prop This is the Name prop This is the Name prop This is the Name prop This is the Name prop', 'This is the Currency prop This is the Name prop This is the Name prop This is the Name prop This is the Name prop This is the Name prop This is the Name prop', 'This is the Availability prop This is the Name prop This is the Name prop This is the Name prop This is the Name prop This is the Name prop This is the Name prop', 'This is the Image prop This is the Name prop This is the Name prop This is the Name prop This is the Name prop This is the Name prop This is the Name prop']
                }
            },
            'Business-Hours': {
                props: ['Open-Now', 'Days-Open', 'Time-Open'],
                mandatory: ['Open-Now', 'Days-Open', 'Time-Open'],
                copy: {
                    widget: 'This is the Business-Hours widget',
                    props: ['This is the Open-Now prop', 'This is the Days-Open prop', 'This is the Time-Open prop']
                }
            },
            'All-Products': {
                props: ['Name', 'Url', 'Price', 'Currency'],
                mandatory: ['Name', 'Url', 'Price', 'Currency'],
                copy: {
                    widget: 'This is the All-Products widget',
                    props: ['This is the Name prop', 'This is the Url prop', 'This is the Price prop', 'This is the Currency prop']
                }
            },
            'Subscribers-Count': {
                props: ['Count'],
                mandatory: ['Count'],
                copy: {
                    widget: 'This is the Subscribers-Count widget',
                    props: ['This is the Count prop']
                }
            },
            Tags: {
                props: ['Tags'],
                mandatory: ['Tags'],
                copy: {
                    widget: 'This is the Tags widget',
                    props: ['This is the Tags prop']
                }
            },
            Visits: {
                props: ['Count'],
                mandatory: ['Count'],
                copy: {
                    widget: 'This is the Visits widget',
                    props: ['This is the Count prop']
                }
            },
            'Business-Enquiries': {
                props: ['Enquiry-Contact', 'Enquiry-Message', 'Enquiry-Button'],
                mandatory: ['Enquiry-Contact', 'Enquiry-Message', 'Enquiry-Button'],
                copy: {
                    widget: 'This is the Business-Enquiries widget',
                    props: ['This is the Enquiry-Contact prop', 'This is the Enquiry-Message prop', 'This is the Enquiry-Button prop']
                }
            },
            'Video-Gallery': {
                props: ['Url'],
                mandatory: ['Url'],
                copy: {
                    widget: 'This is the Video-Gallery widget',
                    props: ['This is the Url prop']
                }
            },
            'Site-Link': {
                props: ['Url'],
                mandatory: ['Url'],
                copy: {
                    widget: 'This is the Site-Link widget',
                    props: ['This is the Url prop']
                }
            },
            Address: {
                props: ['Country', 'Line-1', 'City', 'Pincode'],
                mandatory: ['Country'],
                copy: {
                    widget: 'This is the Address widget',
                    props: ['This is the Country prop', 'This is the Line-1 prop', 'This is the City prop', 'This is the Pincode prop']
                }
            },
            'Mobile-Numbers': {
                props: ['Primary', 'Work', 'Home'],
                mandatory: ['Primary'],
                copy: {
                    widget: 'This is the Mobile-Numbers widget',
                    props: ['This is the Primary prop', 'This is the Work prop', 'This is the Home prop']
                }
            },
            Subscribe: {
                props: ['Subscribe-Button', 'Subscribe-Contact'],
                mandatory: ['Subscribe-Button', 'Subscribe-Contact'],
                copy: {
                    widget: 'This is the Subscribe widget',
                    props: ['This is the Subscribe-Button prop', 'This is the Subscribe-Contact prop']
                }
            },
            Search: {
                props: ['Search-Button', 'Search-Query'],
                mandatory: ['Search-Button', 'Search-Query'],
                copy: {
                    widget: 'This is the Search widget',
                    props: ['This is the Search-Button prop', 'This is the Search-Query prop']
                }
            },
            Logo: {
                props: ['Url'],
                mandatory: ['Url'],
                copy: {
                    widget: 'This is the Logo widget',
                    props: ['This is the Url prop']
                }
            },
            Map: {
                props: ['Lat ', 'Long'],
                mandatory: ['Lat ', 'Long'],
                copy: {
                    widget: 'This is the Map widget',
                    props: ['This is the Lat prop', 'This is the Long prop']
                }
            },
            'Facebook-Like-Box': {
                props: ['Page-Name'],
                mandatory: ['Page-Name'],
                copy: {
                    widget: 'This is the Facebook-Like-Box widget',
                    props: ['This is the Page-Name prop']
                }
            },
            'Background-Image': {
                props: ['Url'],
                mandatory: ['Url'],
                copy: {
                    widget: 'This is the Background-Image widget',
                    props: ['This is the Url prop']
                }
            },
            Favicon: {
                props: ['Url'],
                mandatory: ['Url'],
                copy: {
                    widget: 'This is the Favicon widget',
                    props: ['This is the Url prop']
                }
            }
        },
        state: {
            timeoutTop: undefined,
            index: 1,
            view: undefined,
            init: undefined
        },
        nodes: {
            dropDown: document.querySelector('.select-view'),
            dropDownList: document.querySelector('.view-list'),
            dropDownListItems: undefined,
            view: document.querySelector('.select-view .view'),
            buttonExpand: document.querySelector('.bar-view button'),
            docs: undefined,
            docsVisible: undefined,
            docsContainer: document.querySelector('.container-docs')
        },
        order: ['entity', 'view', 'widget', 'widgetProps']
    },

    dropDown: {
        init: function init() {
            Docs.dropDown.populate();
            Docs.config.nodes.dropDownListItems = (0, _helpers.returnArray)(document.querySelectorAll('.view-list li'));
            Docs.dropDown.bindEvents();
            // Select Master View
            document.querySelector('.view-list li:nth-child(2)').click();
        },
        bindEvents: function bindEvents() {
            //On clicking document
            document.addEventListener('click', function () {
                Docs.dropDown.collapse();
            });

            //On pressing escape
            document.addEventListener('keydown', function (event) {
                if (event.which == 27) {
                    Docs.dropDown.collapse();
                }
            });

            //On clicking dropdown
            Docs.config.nodes.dropDown.addEventListener('click', function (event) {
                event.stopPropagation();
                Docs.dropDown.toggle();
            });

            //On selecting from dropdown
            Docs.config.nodes.dropDownListItems.forEach(function (view) {
                view.addEventListener('click', function (event) {
                    event.stopPropagation();
                    Docs.dropDown.select(event.currentTarget);
                    Docs.dropDown.collapse();
                    Docs.dropDown.scroll();
                    Docs.doc.select();
                });
            });

            //On clicking expand
            Docs.config.nodes.buttonExpand.addEventListener('click', function (event) {
                event.stopPropagation();
                Docs.doc.methods.toggleSemi();
            });
        },
        populate: function populate() {
            Object.keys(Docs.config.views).forEach(function (view) {
                var item = document.createElement('li');
                var name = document.createTextNode(view);
                item.appendChild(name);
                Docs.config.nodes.dropDownList.appendChild(item);
            });
        },
        collapse: function collapse() {
            Docs.config.nodes.dropDown.classList.remove('select-expand');
        },
        toggle: function toggle() {
            Docs.config.nodes.dropDown.classList.toggle('select-expand');
        },
        select: function select(view) {
            Docs.config.nodes.view.innerHTML = Docs.config.state.view = view.innerHTML;
        },
        scroll: function scroll() {
            var wrapper = parseInt(document.querySelector('.wrapper-sections').clientHeight);
            var documentation = parseInt(document.querySelector('.section-documentation').clientHeight) + parseInt(window.getComputedStyle(document.querySelector('.section-documentation'))['margin-top']) - parseInt(window.getComputedStyle(document.querySelector('.section-documentation'))['margin-bottom']) / 1.35;
            var enquiry = parseInt(document.querySelector('.section-enquiry').clientHeight);
            var footer = parseInt(document.querySelector('.section-footer').clientHeight);
            var bar = parseInt(document.querySelector('.bar-view').clientHeight);
            var boosters = parseInt(document.querySelector('.section-boosters').offsetHeight);
            var scroll = wrapper - documentation - footer - bar - enquiry - boosters;
            if (Docs.config.state.init != undefined) {
                (0, _helpers.scrollToY)(scroll, 500, 'easeInOutQuint');
            }
            Docs.config.state.init = 1;
        }
    },

    doc: {
        init: function init() {
            Docs.doc.create.init();
            Docs.config.nodes.docs = (0, _helpers.returnArray)(document.querySelectorAll('.wrapper-doc'));
            Docs.config.nodes.docsVisible = Docs.config.nodes.docs;
            Docs.doc.methods.align();
            Docs.doc.bindEvents();
        },
        bindEvents: function bindEvents() {

            // When a doc is clicked
            Docs.config.nodes.docs.forEach(function (doc, index) {
                doc.addEventListener('click', function (event) {
                    event.stopPropagation();
                    if (window.innerWidth > 640) {
                        Docs.doc.methods.toggle(event.currentTarget);
                        Docs.doc.methods.adjustContainerHeight(event.currentTarget);
                    } else {
                        Docs.doc.methods.toggleMobile(event.currentTarget);
                        Docs.doc.methods.adjustContainerHeightMobile(event.currentTarget);
                    }
                });
            });

            //When escape is pressed
            document.addEventListener('keydown', function (event) {
                if (event.which == 27) {
                    Docs.doc.methods.contract();
                }
            });

            //On clicking anywhere on the document
            document.addEventListener('click', function () {
                Docs.doc.methods.contract();
            });
        },


        create: {
            init: function init() {
                Docs.config.nodes.docsContainer.innerHTML = '';
                Docs.doc.create.wrapper().forEach(function (wrapper, index) {
                    wrapper.setAttribute('index', index);
                    Docs.config.nodes.docsContainer.appendChild(wrapper);
                });
            },
            widgets: function widgets() {
                var view = Docs.config.state.view;
                var views = Docs.config.views;
                return views[view].widgets.map(function (widget) {
                    return '\n                    <span class="widget">\n                        <span class="name">' + widget + '</span>\n                    </span>\n                ';
                });
            },
            props: function props() {
                var view = Docs.config.state.view;
                var views = Docs.config.views;
                return views[view].widgets.map(function (widget) {
                    return Docs.config.widgets[widget].props.map(function (prop) {
                        return '<span class="prop prop-mandatory"><span>' + prop + '</span></span>';
                    });
                });
            },
            overview: function overview() {
                var widgets = Docs.doc.create.widgets();
                var Props = Docs.doc.create.props();
                var overview = Props.map(function (props, index) {
                    var propNode = '';
                    props.map(function (prop) {
                        propNode = propNode + prop;
                    });
                    return widgets[index] + propNode;
                });
                return overview.map(function (val) {
                    return '<div class="overview">' + val + '</div>';
                });
            },
            widgetDetails: function widgetDetails() {
                var view = Docs.config.state.view;
                var views = Docs.config.views;
                return views[view].widgets.map(function (widget) {
                    var detail = Docs.config.widgets[widget].copy.widget;
                    return '<div class="detail-widget"><span>' + detail + '</span></div>';
                });
            },
            propDetails: function propDetails() {
                var view = Docs.config.state.view;
                var views = Docs.config.views;
                return views[view].widgets.map(function (widget) {
                    var propNodes = '';
                    Docs.config.widgets[widget].copy.props.map(function (prop) {
                        propNodes = propNodes + ('<div class="detail-prop"><span>' + prop + '</span></div>');
                    });
                    return propNodes;
                });
            },
            details: function details() {
                return Docs.doc.create.widgetDetails().map(function (widget, index) {
                    return '<div class="detail">' + (widget + Docs.doc.create.propDetails()[index]) + '</div>';
                });
            },
            doc: function doc() {
                var details = Docs.doc.create.details();
                return Docs.doc.create.overview().map(function (overview, index) {
                    return '\n                    <div class="doc">\n                        <div class="inner-wrapper-doc">\n                            ' + overview + '\n                            ' + details[index] + '\n                            <div class="label">\n                                <span class="widget">widget</span>\n                                <span class="widgetProps">widget props</span>\n                            </div>\n                            <div class="button-expand">\n                                <span class="expand">+</span>\n                                <span class="collapse">&ndash;</span>\n                            </div>\n                        </div>\n                        <div class="underlay-border"></div>\n                        <div class="underlay-color"></div>\n                    </div>\n                ';
                });
            },
            wrapper: function wrapper() {
                return Docs.doc.create.doc().map(function (doc, index) {
                    var widget = Docs.config.views[Docs.config.state.view].widgets[index];
                    var wrapper = document.createElement('div');
                    wrapper.innerHTML = doc;
                    wrapper.className = 'wrapper-doc';
                    wrapper.setAttribute('widget', widget);
                    return wrapper;
                });
            }
        },

        select: function select() {
            Docs.doc.methods.filter();
            Docs.config.nodes.docsVisible = (0, _helpers.returnArray)(document.querySelectorAll('.container-docs .doc-show'));
            Docs.doc.methods.toggleMandatory(event.currentTarget);
            Docs.doc.methods.mandatoryFirst();
            Docs.config.nodes.docsVisible = (0, _helpers.returnArray)(document.querySelectorAll('.container-docs .doc-show'));
            Docs.doc.methods.align();
            Docs.doc.methods.adjustContainerHeight(document.querySelector('.container-docs .wrapper-doc:first-child'));
        },


        methods: {
            align: function align() {
                Docs.config.nodes.docsVisible.forEach(function (doc, index) {
                    doc.setAttribute('index', index);
                    var defaultClass = doc.classList[0];
                    doc.setAttribute('class', defaultClass);
                    switch (index % 3) {
                        case 0:
                            doc.classList.add('wrapper-doc-left');
                            break;
                        case 1:
                            doc.classList.add('wrapper-doc-center');
                            break;
                        case 2:
                            doc.classList.add('wrapper-doc-right');
                            break;
                    }
                });
            },
            toggle: function toggle(current) {
                Docs.config.nodes.docs.forEach(function (doc) {
                    if (doc.getAttribute('index') != current.getAttribute('index')) {
                        Docs.doc.methods.contract(doc);
                    } else {
                        if (doc.classList.contains('wrapper-doc-expand')) {
                            Docs.doc.methods.toggleSemi();
                            doc.classList.add('doc-expand');
                            Docs.config.state.index++;
                            clearTimeout(Docs.config.state.timeoutTop);
                            current.style.zIndex = Docs.config.state.index;
                        }
                    }
                });
                var index = current.getAttribute('index') % 3;
                var doc = current.classList;
                var expand = void 0;

                switch (index) {
                    case 0:
                        expand = 'doc-expand-left';
                        break;
                    case 1:
                        expand = 'doc-expand-center';
                        break;
                    case 2:
                        expand = 'doc-expand-right';
                        break;
                }

                if (!doc.contains('doc-expand')) {
                    Docs.config.state.index++;
                    clearTimeout(Docs.config.state.timeoutTop);
                    doc.add('doc-expand');
                    current.style.zIndex = Docs.config.state.index;
                } else {
                    if (!doc.contains(expand)) {
                        doc.add(expand);
                    } else {
                        doc.remove(expand, 'doc-expand');
                        Docs.config.state.timeoutTop = setTimeout(function () {
                            current.style.zIndex = '1';
                        }, 2000);
                    }
                }
            },
            toggleSemi: function toggleSemi() {
                Docs.config.nodes.docsVisible.forEach(function (doc) {
                    if (!doc.classList.contains('wrapper-doc-expand')) {
                        doc.classList.add('wrapper-doc-expand', 'doc-expand');
                        Docs.config.nodes.buttonExpand.classList.add('button-collapse');
                    } else {
                        doc.classList.remove('wrapper-doc-expand', 'doc-expand');
                        Docs.config.nodes.buttonExpand.classList.remove('button-collapse');
                    }
                });
            },
            toggleMobile: function toggleMobile(current) {
                Docs.config.nodes.docs.forEach(function (doc) {
                    if (doc.getAttribute('index') != current.getAttribute('index')) {
                        doc.classList.remove('doc-expand');
                        setTimeout(function () {
                            doc.style.zIndex = 1;
                        }, 300);
                    } else {
                        current.classList.toggle('doc-expand');
                        Docs.config.state.index++;
                        current.style.zIndex = Docs.config.state.index;
                    }
                });
            },
            contract: function contract(doc) {
                if (doc) {
                    doc.classList.remove('doc-expand-left', 'doc-expand-center', 'doc-expand-right', 'doc-expand');
                    setTimeout(function () {
                        doc.style.zIndex = 1;
                    }, 500);
                } else {
                    Docs.config.nodes.docs.forEach(function (doc) {
                        doc.classList.remove('doc-expand-left', 'doc-expand-center', 'doc-expand-right', 'doc-expand', 'wrapper-doc-expand');
                        setTimeout(function () {
                            doc.style.zIndex = 1;
                        }, 500);
                    });
                }
            },
            adjustContainerHeight: function adjustContainerHeight(doc) {
                if (doc) {
                    (function () {
                        var reset = function reset() {
                            Docs.config.nodes.docsContainer.style.transitionDelay = '0.3s';
                            Docs.config.nodes.docsContainer.style.height = wrapperHeight * rows + 'px';
                        };

                        var wrapperMargin = parseInt(window.getComputedStyle(doc)['margin-top']);
                        var wrapperHeight = parseInt(window.getComputedStyle(doc)['font-size']) * 9.9 + wrapperMargin;
                        var docHeight = wrapperHeight * 4;
                        var rows = Math.ceil(Docs.config.nodes.docsVisible.filter(function (doc) {
                            return typeof doc != 'number';
                        }).length / 3);
                        var problemStart = rows - 3;
                        var containerHeight = wrapperHeight * rows;
                        var currentRow = Math.ceil((parseInt(doc.getAttribute('index')) + 1) / 3);
                        problemStart = problemStart < 1 ? 0 : problemStart;
                        var problemRows = rows - problemStart;

                        if (doc.classList.contains('doc-expand')) {
                            if (currentRow > problemStart) {
                                var position = problemRows + (problemStart - currentRow) + 1;
                                var increase = containerHeight + (docHeight - (wrapperHeight - wrapperMargin) * position - wrapperMargin * position);
                                Docs.config.nodes.docsContainer.style.transitionDelay = '0s';
                                Docs.config.nodes.docsContainer.style.height = increase + 'px';
                            } else {
                                reset();
                            }
                        } else {
                            reset();
                        }
                    })();
                }
            },
            adjustContainerHeightMobile: function adjustContainerHeightMobile(doc) {
                if (doc) {
                    (function () {
                        var reset = function reset() {
                            Docs.config.nodes.docsContainer.style.transitionDelay = '0.3s';
                            Docs.config.nodes.docsContainer.style.height = wrapperHeight * rows + 'px';
                        };

                        var wrapperMargin = parseInt(window.getComputedStyle(doc)['margin-top']);
                        var wrapperHeight = parseInt(window.getComputedStyle(doc)['font-size']) * 10 + wrapperMargin;
                        var docHeight = wrapperHeight * 4;
                        var rows = Math.ceil(Docs.config.nodes.docsVisible.filter(function (doc) {
                            return typeof doc != 'number';
                        }).length / 2);
                        var problemStart = rows - 3;
                        var containerHeight = wrapperHeight * rows;
                        var currentRow = Math.ceil((parseInt(doc.getAttribute('index')) + 1) / 2);
                        problemStart = problemStart < 1 ? 0 : problemStart;
                        var problemRows = rows - problemStart;

                        if (doc.classList.contains('doc-expand')) {
                            if (currentRow > problemStart) {
                                var position = problemRows + (problemStart - currentRow) + 1;
                                var increase = containerHeight + (docHeight - (wrapperHeight - wrapperMargin) * position - wrapperMargin * position);
                                Docs.config.nodes.docsContainer.style.transitionDelay = '0s';
                                Docs.config.nodes.docsContainer.style.height = increase + 'px';
                            } else {
                                reset();
                            }
                        } else {
                            reset();
                        }
                    })();
                }
            },
            filter: function filter() {
                if (Docs.config.nodes.docs != undefined) {
                    (function () {
                        var activeWidgets = Docs.config.views[Docs.config.state.view].widgets;
                        Docs.config.nodes.docs.forEach(function (doc) {
                            var widget = doc.getAttribute('widget');
                            var present = activeWidgets.indexOf(widget);
                            if (present < 0) {
                                doc.classList.add('doc-hide');
                                doc.classList.remove('doc-show');
                            } else {
                                doc.classList.add('doc-show');
                                doc.classList.remove('doc-hide');
                            }
                        });
                    })();
                }
            },
            toggleMandatory: function toggleMandatory(view) {
                var mandatory = Docs.config.views[Docs.config.state.view].mandatory;
                Docs.config.nodes.docsVisible.forEach(function (doc) {
                    var widget = doc.getAttribute('widget');
                    var present = mandatory.indexOf(widget);
                    if (present < 0) {
                        doc.childNodes[1].classList.remove('doc-mandatory');
                        doc.childNodes[1].classList.add('doc-optional');
                    } else {
                        doc.childNodes[1].classList.add('doc-mandatory');
                        doc.childNodes[1].classList.remove('doc-optional');
                    }
                });
            },
            mandatoryFirst: function mandatoryFirst() {
                if (Docs.config.state.view != 'master') {
                    (function () {
                        var index = 0;
                        Docs.config.nodes.docsVisible.forEach(function (doc) {
                            if (doc.childNodes[1].classList.contains('doc-mandatory')) {
                                var current = doc;
                                doc.remove();
                                Docs.config.nodes.docsContainer.insertBefore(current, Docs.config.nodes.docsContainer.childNodes[index]);
                                index++;
                            }
                        });
                    })();
                }
            }
        }
    },

    init: function init() {
        Docs.dropDown.init();
        Docs.doc.init();
    }
};

exports.default = Docs;

},{"./helpers.js":5}],4:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _helpers = require('./helpers.js');

var _skrollrMin = require('./skrollr.min.js');

var _skrollrMin2 = _interopRequireDefault(_skrollrMin);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//1280 X 595

var Features = {
    config: {
        nodes: {
            banner: document.querySelector('section.banner'),
            intro: document.querySelector('section.intro'),
            getFeature: function getFeature(feature) {
                return document.querySelector('.features .feature-' + feature);
            },

            features: {
                1: {
                    rocket: document.querySelector('.feature-1 .container-path'),
                    arrow: document.querySelector('.feature-1 .arrow')
                },
                2: {
                    arm: document.querySelector('.feature-2 .arm'),
                    screen: document.querySelector('.feature-2 .thankyou'),
                    light: document.querySelector('.feature-2 .light')
                },
                3: {
                    city1: document.querySelector('.feature-3 .city-1'),
                    city2: document.querySelector('.feature-3 .city-2'),
                    city3: document.querySelector('.feature-3 .city-3'),
                    site1: document.querySelector('.feature-3 .site-1'),
                    site2: document.querySelector('.feature-3 .site-2'),
                    site3: document.querySelector('.feature-3 .site-3')
                },
                4: {
                    cable: document.querySelector('.feature-4 .cable'),
                    box: document.querySelector('.feature-4 .box')
                },
                5: {
                    coin1: document.querySelector('.feature-5 .coin-1'),
                    coins: document.querySelector('.feature-5 .container-coins')
                }
            }
        },
        windowWidth: undefined
    },

    helpers: {
        getIntroHeight: function getIntroHeight() {
            var bannerHeight = parseInt(window.getComputedStyle(Features.config.nodes.banner, null).height);
            var bannerPadding = parseInt(window.getComputedStyle(Features.config.nodes.banner, null).fontSize) - 10;
            var introHeight = parseInt(window.getComputedStyle(Features.config.nodes.intro, null).height);
            return introHeight + bannerHeight + bannerPadding;
        },
        getFeatureHeight: function getFeatureHeight(feature) {
            if (feature == 0) {
                return 0;
            }
            return parseInt(window.getComputedStyle(Features.config.nodes.getFeature(feature), null).height) + parseInt(window.getComputedStyle(Features.config.nodes.getFeature(feature), null).paddingBottom);
        },
        setWindowWidth: function setWindowWidth() {
            Features.config.windowWidth = window.outerWidth;
        },
        getFraction: function getFraction(fraction) {

            var proportion = undefined;

            if (window.outerWidth > 1400) {
                proportion = 1;
            } else if (window.outerWidth > 1050 && window.outerWidth <= 1400) {
                proportion = 1.1;
            } else if (window.outerWidth <= 1050 && window.outerWidth > 800) {
                proportion = 0.95;
            } else if (window.outerWidth <= 800 && window.outerWidth > 699) {
                proportion = 0.6;
            }

            return proportion * fraction;
        },
        getNodes: function getNodes() {
            var nodes = (0, _helpers.returnArray)(Features.config.nodes.features).map(function (feature, index) {
                return (0, _helpers.returnArray)(Features.config.nodes.features[index + 1]).map(function (node) {
                    return node;
                });
            });

            nodes = nodes.reduce(function (prev, next) {
                return prev.concat(next);
            });

            return nodes;
        },
        resetAnchors: function resetAnchors(node) {
            (0, _helpers.returnArray)(node.attributes).forEach(function (attribute) {
                if (attribute.name.split('-')[0] == 'data') {
                    node.removeAttribute(attribute.name);
                }
            });
        }
    },

    methods: {
        init: function init() {
            Features.methods.setFeature1();
            Features.methods.setFeature2();
            Features.methods.setFeature3();
            Features.methods.setFeature4();
            Features.methods.setFeature5();
            _skrollrMin2.default.init({ forceHeight: false });
        },
        setFeature1: function setFeature1() {
            var rocketStart = Math.floor(Features.helpers.getIntroHeight() * Features.helpers.getFraction(0.55));
            var arrowStart = Math.floor(Features.helpers.getIntroHeight() * Features.helpers.getFraction(0.78));
            var rocketEnd = Math.floor(Features.helpers.getIntroHeight() * Features.helpers.getFraction(0.85));

            Features.config.nodes.features[1].rocket.setAttribute('data-' + rocketStart, 'height:7em;');
            Features.config.nodes.features[1].rocket.setAttribute('data-' + rocketEnd, 'height:31em;');
            Features.config.nodes.features[1].arrow.setAttribute('data-' + rocketStart, 'opacity:!0;');
            Features.config.nodes.features[1].arrow.setAttribute('data-' + arrowStart, 'opacity:!1;right:0em;');
            Features.config.nodes.features[1].arrow.setAttribute('data-' + rocketEnd, 'right:16.15em;');
        },
        setFeature2: function setFeature2() {
            var cardStart = Math.floor(Features.helpers.getIntroHeight() + Features.helpers.getFeatureHeight(1) * Features.helpers.getFraction(0.5));
            var cardEnd = Math.floor(Features.helpers.getIntroHeight() + Features.helpers.getFeatureHeight(1) * Features.helpers.getFraction(0.7));
            var flashRange = Math.floor(Features.helpers.getIntroHeight() + Features.helpers.getFeatureHeight(1) * Features.helpers.getFraction(0.84) - cardEnd);
            var flashStep = flashRange / 6;

            var lightFlast0 = cardEnd - flashStep;
            var lightFlast1 = lightFlast0 + flashStep;
            var lightFlast2 = lightFlast1 + flashStep;
            var lightFlast3 = lightFlast2 + flashStep;
            var lightFlast4 = lightFlast3 + flashStep;
            var lightFlast5 = lightFlast4 + flashStep;
            var lightFlast6 = lightFlast5 + flashStep;

            Features.config.nodes.features[2].arm.setAttribute('data-' + cardStart, 'height: 0em;');
            Features.config.nodes.features[2].arm.setAttribute('data-' + cardEnd, 'height: 7.4em;');

            Features.config.nodes.features[2].light.setAttribute('data-' + lightFlast0, 'opacity: !0;');
            Features.config.nodes.features[2].light.setAttribute('data-' + lightFlast1, 'opacity: !1;');
            Features.config.nodes.features[2].light.setAttribute('data-' + lightFlast2, 'opacity: !0;');
            Features.config.nodes.features[2].light.setAttribute('data-' + lightFlast3, 'opacity: !1;');
            Features.config.nodes.features[2].light.setAttribute('data-' + lightFlast4, 'opacity: !0;');
            Features.config.nodes.features[2].light.setAttribute('data-' + lightFlast5, 'opacity: !1;');
            Features.config.nodes.features[2].light.setAttribute('data-' + lightFlast6, 'opacity: !0;');

            Features.config.nodes.features[2].screen.setAttribute('data-' + cardStart, 'opacity: !0;');
            Features.config.nodes.features[2].screen.setAttribute('data-' + lightFlast6, 'opacity: !1;');
        },
        setFeature3: function setFeature3() {
            var cityStart = Math.floor(Features.helpers.getIntroHeight() + Features.helpers.getFeatureHeight(1) * Features.helpers.getFraction(1.5));
            var cityEnd = Math.floor(Features.helpers.getIntroHeight() + Features.helpers.getFeatureHeight(1) + Features.helpers.getFeatureHeight(2) * Features.helpers.getFraction(0.9));
            var cityRange = cityEnd - cityStart;
            var cityStep = cityRange / 2;
            var cityStep1 = cityStart + cityStep;
            var cityStep2 = cityStep1 + cityStep;

            Features.config.nodes.features[3].city1.setAttribute('data-' + (cityStart - 100), 'opacity:!1');
            Features.config.nodes.features[3].site1.setAttribute('data-' + cityStart, 'left:0em');

            Features.config.nodes.features[3].city1.setAttribute('data-' + cityStart, 'opacity:!0');
            Features.config.nodes.features[3].site1.setAttribute('data-' + cityStep1, 'left:0em');

            Features.config.nodes.features[3].city2.setAttribute('data-' + (cityStart - 100), 'opacity:!0');
            Features.config.nodes.features[3].site2.setAttribute('data-' + cityStart, 'left:-43em');

            Features.config.nodes.features[3].city2.setAttribute('data-' + cityStart, 'opacity:!1');
            Features.config.nodes.features[3].site2.setAttribute('data-' + cityStep1, 'left:0em');

            Features.config.nodes.features[3].city2.setAttribute('data-' + cityStep2, 'opacity:!0');
            Features.config.nodes.features[3].site2.setAttribute('data-' + cityStep2, 'left:0em');

            Features.config.nodes.features[3].city3.setAttribute('data-' + cityStep1, 'opacity:!0');
            Features.config.nodes.features[3].site3.setAttribute('data-' + (cityStep1 + 100), 'left:-43em');

            Features.config.nodes.features[3].city3.setAttribute('data-' + cityStep2, 'opacity:!1');
            Features.config.nodes.features[3].site3.setAttribute('data-' + (cityStep2 + 100), 'left:0em');
        },
        setFeature4: function setFeature4() {
            var craneStart = Math.floor(Features.helpers.getIntroHeight() + Features.helpers.getFeatureHeight(1) + Features.helpers.getFeatureHeight(2) + Features.helpers.getFeatureHeight(3) * Features.helpers.getFraction(0.45));
            var boxEnd = Math.floor(Features.helpers.getIntroHeight() + Features.helpers.getFeatureHeight(1) + Features.helpers.getFeatureHeight(2) + Features.helpers.getFeatureHeight(3) * Features.helpers.getFraction(0.85));
            var cableEnd = Math.floor(Features.helpers.getIntroHeight() + Features.helpers.getFeatureHeight(1) + Features.helpers.getFeatureHeight(2) + Features.helpers.getFeatureHeight(3));

            Features.config.nodes.features[4].cable.setAttribute('data-' + craneStart, 'height:15.2em;');
            Features.config.nodes.features[4].cable.setAttribute('data-' + boxEnd, 'height:5em;');
            Features.config.nodes.features[4].cable.setAttribute('data-' + cableEnd, 'height:2.7em;');
            Features.config.nodes.features[4].box.setAttribute('data-' + craneStart, 'bottom:11.1em;');
            Features.config.nodes.features[4].box.setAttribute('data-' + boxEnd, 'bottom:21.15em;');
        },
        setFeature5: function setFeature5() {
            var bankStart = Math.floor(Features.helpers.getIntroHeight() + Features.helpers.getFeatureHeight(1) + Features.helpers.getFeatureHeight(2) + Features.helpers.getFeatureHeight(3) + Features.helpers.getFeatureHeight(4) * Features.helpers.getFraction(0.7));
            var bankCoinEnd = bankStart + Math.floor(Features.helpers.getFeatureHeight(4) * Features.helpers.getFraction(0.65));
            var bankEnd = bankStart + Math.floor(Features.helpers.getFeatureHeight(4) * Features.helpers.getFraction(1.1));

            Features.config.nodes.features[5].coins.setAttribute('data-' + bankStart, 'top:-2.6em');
            Features.config.nodes.features[5].coin1.setAttribute('data-' + bankStart, 'opacity:!1');
            Features.config.nodes.features[5].coins.setAttribute('data-' + bankEnd, 'top:23em');
            Features.config.nodes.features[5].coin1.setAttribute('data-' + bankCoinEnd, 'opacity:!0');
        }
    },

    bindEvents: function bindEvents() {
        //When window is being resized
        window.addEventListener('resize', function () {
            Features.helpers.setWindowWidth();
            Features.helpers.getNodes().forEach(function (node) {
                return Features.helpers.resetAnchors(node);
            });
            Features.methods.init();
        });
    },
    init: function init() {
        Features.bindEvents();
        Features.helpers.setWindowWidth();
        Features.methods.init();
    }
};

exports.default = Features;

},{"./helpers.js":5,"./skrollr.min.js":8}],5:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
var returnArray = function returnArray(object) {
    var keys = Object.keys(object);
    return keys.map(function (key) {
        return object[key];
    }).filter(function (item) {
        return typeof item != 'number';
    });
};

window.requestAnimFrame = function () {
    return window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || function (callback) {
        window.setTimeout(callback, 1000 / 60);
    };
}();

function scrollToY(scrollTargetY, speed, easing) {
    // scrollTargetY: the target scrollY property of the window
    // speed: time in pixels per second
    // easing: easing equation to use

    var scrollY = window.scrollY,
        scrollTargetY = scrollTargetY || 0,
        speed = speed || 2000,
        easing = easing || 'easeOutSine',
        currentTime = 0;

    // min time .1, max time .8 seconds
    var time = Math.max(.1, Math.min(Math.abs(scrollY - scrollTargetY) / speed, .8));

    // easing equations from https://github.com/danro/easing-js/blob/master/easing.js
    var PI_D2 = Math.PI / 2,
        easingEquations = {
        easeOutSine: function easeOutSine(pos) {
            return Math.sin(pos * (Math.PI / 2));
        },
        easeInOutSine: function easeInOutSine(pos) {
            return -0.5 * (Math.cos(Math.PI * pos) - 1);
        },
        easeInOutQuint: function easeInOutQuint(pos) {
            if ((pos /= 0.5) < 1) {
                return 0.5 * Math.pow(pos, 5);
            }
            return 0.5 * (Math.pow(pos - 2, 5) + 2);
        }
    };

    // add animation loop
    function tick() {
        currentTime += 1 / 60;

        var p = currentTime / time;
        var t = easingEquations[easing](p);

        if (p < 1) {
            requestAnimFrame(tick);

            window.scrollTo(0, scrollY + (scrollTargetY - scrollY) * t);
        } else {
            window.scrollTo(0, scrollTargetY);
        }
    }

    // call it once to get started
    tick();
}

exports.returnArray = returnArray;
exports.scrollToY = scrollToY;

},{}],6:[function(require,module,exports){
'use strict';

var _nav = require('./nav.js');

var _nav2 = _interopRequireDefault(_nav);

var _doc = require('./doc.js');

var _doc2 = _interopRequireDefault(_doc);

var _console = require('./console.js');

var _console2 = _interopRequireDefault(_console);

var _boosters = require('./boosters.js');

var _boosters2 = _interopRequireDefault(_boosters);

var _features = require('./features.js');

var _features2 = _interopRequireDefault(_features);

var _skrollrMin = require('./skrollr.min.js');

var _skrollrMin2 = _interopRequireDefault(_skrollrMin);

var _axios = require('axios');

var _axios2 = _interopRequireDefault(_axios);

var _helpers = require('./helpers.js');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

if (window.location.pathname == '/biz' || window.location.pathname == '/') {
    var contactHeight;

    (function () {
        var getIntroHeight = function getIntroHeight() {
            var banner = document.querySelector('section.banner');
            var height = parseInt(window.getComputedStyle(banner, null).height);
            var padding = parseInt(window.getComputedStyle(banner, null).fontSize) - 10;
            return document.querySelector('section.intro').offsetHeight + height + padding;
        };

        var getFeatureHeight = function getFeatureHeight(feature) {
            if (feature != 0) {
                return document.querySelector('.features .feature-' + feature).offsetHeight;
            } else {
                return 0;
            }
        };

        // Navigation

        var getFeaturesHeight = function getFeaturesHeight(feature) {
            var height = 0;
            for (var x = 1; x <= feature; x++) {
                height = height + getFeatureHeight(x - 1);
            }
            return height + getIntroHeight();
        };

        var getActiveWidth = function getActiveWidth(active) {
            var link = document.querySelector('.nav-main li:nth-child(' + active + ')');
            return window.getComputedStyle(link, null).width;
        };

        var getActiveLeft = function getActiveLeft(active) {
            var links = (0, _helpers.returnArray)(document.querySelectorAll('.nav-main li'));
            var left = links.map(function (link, index) {
                if (index < active - 1) {
                    var width = parseInt(window.getComputedStyle(link, null).width);
                    var margin = parseInt(window.getComputedStyle(link, null).marginRight);
                    return width + margin;
                }
                return 0;
            });
            left = left.reduce(function (pre, next) {
                return pre + next;
            });

            return left + 'px';
        };

        var ham = document.querySelector('.hamburger');
        var nav = document.querySelector('.nav-mobile');
        var items = (0, _helpers.returnArray)(document.querySelectorAll('.nav-mobile li'));

        ham.addEventListener('click', function () {
            nav.classList.toggle('nav-mobile-open');
        });

        items.filter(function (item) {
            return typeof item != 'number';
        }).forEach(function (item) {
            item.addEventListener('click', function () {
                nav.classList.remove('nav-mobile-open');
                var feature = item.getAttribute('data-scroll');
                if (feature == 6) {
                    (0, _helpers.scrollToY)(contactHeight + 100, 1000, 'easeInOutQuint');
                } else {
                    (0, _helpers.scrollToY)(getFeaturesHeight(feature), 1000, 'easeInOutQuint');
                }
            });
        });

        contactHeight = parseInt(window.getComputedStyle(document.querySelector('body'), null).height) - window.innerHeight;


        var active = document.querySelector('.nav-main .underline');
        active.setAttribute('data-0', 'opacity:!0');
        active.setAttribute('data-' + getFeaturesHeight(1), 'width:!' + getActiveWidth(1) + '; left:!' + getActiveLeft(1) + ';opacity:!1;');
        active.setAttribute('data-' + getFeaturesHeight(2), 'width:!' + getActiveWidth(2) + '; left:!' + getActiveLeft(2) + ';');
        active.setAttribute('data-' + getFeaturesHeight(3), 'width:!' + getActiveWidth(3) + '; left:!' + getActiveLeft(3) + ';');
        active.setAttribute('data-' + getFeaturesHeight(4), 'width:!' + getActiveWidth(4) + '; left:!' + getActiveLeft(4) + ';');
        active.setAttribute('data-' + getFeaturesHeight(5), 'width:!' + getActiveWidth(5) + '; left:!' + getActiveLeft(5) + ';');
        active.setAttribute('data-' + contactHeight, 'width:!' + getActiveWidth(6) + '; left:!' + getActiveLeft(6) + ';');

        (0, _helpers.returnArray)(document.querySelectorAll('.nav-main li')).forEach(function (link, index) {
            link.addEventListener('click', function () {
                if (index + 1 == 6) {
                    (0, _helpers.scrollToY)(contactHeight + 100, 1000, 'easeInOutQuint');
                } else {
                    (0, _helpers.scrollToY)(getFeaturesHeight(index + 1), 1000, 'easeInOutQuint');
                }
            });
        });

        // <!------ Form Submission -------!>

        var form = document.querySelector('section.enquiry form');
        var overlay = document.querySelector('section.enquiry .overlay');
        var clientId = '731EBAB8596648E79882096E4733E7173B314BECC649423DB67897BF8CC596EC';
        var key = '8NNEzoruRPvZ0sBJxnOcgw';

        form.addEventListener('submit', function (event) {
            event.preventDefault();
            var name = form.childNodes[1].childNodes[1].value;
            var email = form.childNodes[1].childNodes[3].value;
            var message = form.childNodes[3].childNodes[1].value;
            _axios2.default.post('https://mandrillapp.com/api/1.0/messages/send.json', {
                "key": key,
                'message': {
                    'from_email': 'info@kitsune.tools',
                    'fram_name': name,
                    'subject': 'Enquiry from Kitsune Website.',
                    'html': '<h3> From - ' + name + '<h3><h3>Email - ' + email + '</h3><h3>Message</h3><p>' + message + '</p>',
                    'to': [{
                        'email': 'kitsune@nowfloats.com',
                        'name': 'Kitsune'
                    }]
                }
            }).then(function (response) {
                console.log(response.data);
                overlay.classList.add('overlay-show');
                setTimeout(function () {
                    form.reset();
                    overlay.classList.remove('overlay-show');
                }, 3000);
            });
        });
    })();
}

if (window.location.pathname == '/dev') {
    _nav2.default.init();
    _console2.default.init();
    _doc2.default.init();
    _boosters2.default.init();
}

if ((window.location.pathname == '/biz' || window.location.pathname == '/') && window.outerWidth > 699) {
    _features2.default.init();
}

},{"./boosters.js":1,"./console.js":2,"./doc.js":3,"./features.js":4,"./helpers.js":5,"./nav.js":7,"./skrollr.min.js":8,"axios":9}],7:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _helpers = require('./helpers.js');

var Nav = {
    config: {
        nodes: {
            nav: document.querySelector('.section-navigation'),
            links: (0, _helpers.returnArray)(document.querySelectorAll('.section-navigation .nav-main li')),
            mobile: document.querySelector('.nav-mobile'),
            hamburger: document.querySelector('.nav-mobile .hamburger'),
            mobileLinks: (0, _helpers.returnArray)(document.querySelectorAll('.nav-mobile .links li'))
        },
        triggers: {
            stick: undefined,
            home: undefined,
            features: undefined,
            structure: undefined
        },
        pos: undefined
    },

    init: function init() {
        if (window.location.pathname == '/dev') {
            Nav.methods.setTriggers();
            Nav.bindEvents();
        }
    },
    bindEvents: function bindEvents() {

        //On page scroll
        document.addEventListener('scroll', function () {
            Nav.methods.setPos();
            Nav.methods.setTriggers();
            Nav.methods.stick();
            Nav.methods.setActive();
        });

        //On nav click
        Nav.config.nodes.links.forEach(function (link, index) {
            link.addEventListener('click', function () {
                Nav.methods.scroll(index + 1);
            });
        });

        //On hamburger click
        Nav.config.nodes.mobile.addEventListener('click', function (event) {
            Nav.config.nodes.mobile.classList.toggle('nav-mobile-open');
        });

        //On mobile links click
        Nav.config.nodes.mobileLinks.filter(function (link) {
            return typeof link != 'number';
        }).forEach(function (link, index) {
            link.addEventListener('click', function () {
                Nav.methods.scroll(index + 1);
            });
        });
    },


    methods: {
        setPos: function setPos() {
            if (window.pageYOffset > 0) {
                Nav.config.pos = window.pageYOffset;
            } else if (document.documentElement) {
                Nav.config.pos = document.documentElement.scrollTop;
            } else if (document.body) {
                Nav.config.pos = document.body.scrollTop;
            } else if (document.body.parentNode) {
                Nav.config.pos = document.body.parentNode.scrollTop;
            }
        },
        setTriggers: function setTriggers() {
            Nav.config.triggers.stick = document.querySelector('.section-intro .banner').clientHeight / 1.8, Nav.config.triggers.home = document.querySelector('.section-intro .wrapper-banner').clientHeight, Nav.config.triggers.features = Nav.config.triggers.home + document.querySelector('.section-intro .wrapper-content').clientHeight, Nav.config.triggers.structure = Nav.config.triggers.features + document.querySelector('.section-intro .wrapper-features').clientHeight;
        },
        stick: function stick() {
            if (Nav.config.pos > Nav.config.triggers.stick) {
                if (!Nav.config.nodes.nav.classList.contains('nav-stick')) {
                    Nav.config.nodes.nav.classList.add('nav-stick');
                }
            } else {
                Nav.config.nodes.nav.classList.remove('nav-stick');
            }
        },
        setActive: function setActive() {

            if (Nav.config.pos < Nav.config.triggers.home) {
                activate(1);
            } else if (Nav.config.pos >= Nav.config.triggers.home && Nav.config.pos < Nav.config.triggers.features) {
                activate(2);
            } else if (Nav.config.pos >= Nav.config.triggers.features && Nav.config.pos < Nav.config.triggers.structure) {
                activate(3);
            } else if (Nav.config.pos >= Nav.config.triggers.structure) {
                activate(4);
            }

            function activate(child) {
                Nav.config.nodes.links.forEach(function (link) {
                    link.classList.remove('active');
                });
                document.querySelector('.section-navigation .nav-main li:nth-child(' + child + ')').classList.add('active');
            }
        },
        scroll: function scroll(child) {
            switch (child) {
                case 1:
                    scroll(0);
                    break;
                case 2:
                    scroll(Nav.config.triggers.home);
                    break;
                case 3:
                    scroll(Nav.config.triggers.features);
                    break;
                case 4:
                    scroll(Nav.config.triggers.structure);
                    break;
            }

            function scroll(pos) {
                (0, _helpers.scrollToY)(pos, 1000, 'easeInOutQuint');
            }
        }
    }
};

exports.default = Nav;

},{"./helpers.js":5}],8:[function(require,module,exports){
"use strict";

/*! skrollr 0.6.30 (2015-08-12) | Alexander Prinzhorn - https://github.com/Prinzhorn/skrollr | Free to use under terms of MIT license */
!function (a, b, c) {
  "use strict";
  function d(c) {
    if (e = b.documentElement, f = b.body, T(), ha = this, c = c || {}, ma = c.constants || {}, c.easing) for (var d in c.easing) {
      W[d] = c.easing[d];
    }ta = c.edgeStrategy || "set", ka = { beforerender: c.beforerender, render: c.render, keyframe: c.keyframe }, la = c.forceHeight !== !1, la && (Ka = c.scale || 1), na = c.mobileDeceleration || y, pa = c.smoothScrolling !== !1, qa = c.smoothScrollingDuration || A, ra = { targetTop: ha.getScrollTop() }, Sa = (c.mobileCheck || function () {
      return (/Android|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent || navigator.vendor || a.opera)
      );
    })(), Sa ? (ja = b.getElementById(c.skrollrBody || z), ja && ga(), X(), Ea(e, [s, v], [t])) : Ea(e, [s, u], [t]), ha.refresh(), wa(a, "resize orientationchange", function () {
      var a = e.clientWidth,
          b = e.clientHeight;(b !== Pa || a !== Oa) && (Pa = b, Oa = a, Qa = !0);
    });var g = U();return function h() {
      $(), va = g(h);
    }(), ha;
  }var e,
      f,
      g = { get: function get() {
      return ha;
    }, init: function init(a) {
      return ha || new d(a);
    }, VERSION: "0.6.30" },
      h = Object.prototype.hasOwnProperty,
      i = a.Math,
      j = a.getComputedStyle,
      k = "touchstart",
      l = "touchmove",
      m = "touchcancel",
      n = "touchend",
      o = "skrollable",
      p = o + "-before",
      q = o + "-between",
      r = o + "-after",
      s = "skrollr",
      t = "no-" + s,
      u = s + "-desktop",
      v = s + "-mobile",
      w = "linear",
      x = 1e3,
      y = .004,
      z = "skrollr-body",
      A = 200,
      B = "start",
      C = "end",
      D = "center",
      E = "bottom",
      F = "___skrollable_id",
      G = /^(?:input|textarea|button|select)$/i,
      H = /^\s+|\s+$/g,
      I = /^data(?:-(_\w+))?(?:-?(-?\d*\.?\d+p?))?(?:-?(start|end|top|center|bottom))?(?:-?(top|center|bottom))?$/,
      J = /\s*(@?[\w\-\[\]]+)\s*:\s*(.+?)\s*(?:;|$)/gi,
      K = /^(@?[a-z\-]+)\[(\w+)\]$/,
      L = /-([a-z0-9_])/g,
      M = function M(a, b) {
    return b.toUpperCase();
  },
      N = /[\-+]?[\d]*\.?[\d]+/g,
      O = /\{\?\}/g,
      P = /rgba?\(\s*-?\d+\s*,\s*-?\d+\s*,\s*-?\d+/g,
      Q = /[a-z\-]+-gradient/g,
      R = "",
      S = "",
      T = function T() {
    var a = /^(?:O|Moz|webkit|ms)|(?:-(?:o|moz|webkit|ms)-)/;if (j) {
      var b = j(f, null);for (var c in b) {
        if (R = c.match(a) || +c == c && b[c].match(a)) break;
      }if (!R) return void (R = S = "");R = R[0], "-" === R.slice(0, 1) ? (S = R, R = { "-webkit-": "webkit", "-moz-": "Moz", "-ms-": "ms", "-o-": "O" }[R]) : S = "-" + R.toLowerCase() + "-";
    }
  },
      U = function U() {
    var b = a.requestAnimationFrame || a[R.toLowerCase() + "RequestAnimationFrame"],
        c = Ha();return (Sa || !b) && (b = function b(_b) {
      var d = Ha() - c,
          e = i.max(0, 1e3 / 60 - d);return a.setTimeout(function () {
        c = Ha(), _b();
      }, e);
    }), b;
  },
      V = function V() {
    var b = a.cancelAnimationFrame || a[R.toLowerCase() + "CancelAnimationFrame"];return (Sa || !b) && (b = function b(_b2) {
      return a.clearTimeout(_b2);
    }), b;
  },
      W = { begin: function begin() {
      return 0;
    }, end: function end() {
      return 1;
    }, linear: function linear(a) {
      return a;
    }, quadratic: function quadratic(a) {
      return a * a;
    }, cubic: function cubic(a) {
      return a * a * a;
    }, swing: function swing(a) {
      return -i.cos(a * i.PI) / 2 + .5;
    }, sqrt: function sqrt(a) {
      return i.sqrt(a);
    }, outCubic: function outCubic(a) {
      return i.pow(a - 1, 3) + 1;
    }, bounce: function bounce(a) {
      var b;if (.5083 >= a) b = 3;else if (.8489 >= a) b = 9;else if (.96208 >= a) b = 27;else {
        if (!(.99981 >= a)) return 1;b = 91;
      }return 1 - i.abs(3 * i.cos(a * b * 1.028) / b);
    } };d.prototype.refresh = function (a) {
    var d,
        e,
        f = !1;for (a === c ? (f = !0, ia = [], Ra = 0, a = b.getElementsByTagName("*")) : a.length === c && (a = [a]), d = 0, e = a.length; e > d; d++) {
      var g = a[d],
          h = g,
          i = [],
          j = pa,
          k = ta,
          l = !1;if (f && F in g && delete g[F], g.attributes) {
        for (var m = 0, n = g.attributes.length; n > m; m++) {
          var p = g.attributes[m];if ("data-anchor-target" !== p.name) {
            if ("data-smooth-scrolling" !== p.name) {
              if ("data-edge-strategy" !== p.name) {
                if ("data-emit-events" !== p.name) {
                  var q = p.name.match(I);if (null !== q) {
                    var r = { props: p.value, element: g, eventType: p.name.replace(L, M) };i.push(r);var s = q[1];s && (r.constant = s.substr(1));var t = q[2];/p$/.test(t) ? (r.isPercentage = !0, r.offset = (0 | t.slice(0, -1)) / 100) : r.offset = 0 | t;var u = q[3],
                        v = q[4] || u;u && u !== B && u !== C ? (r.mode = "relative", r.anchors = [u, v]) : (r.mode = "absolute", u === C ? r.isEnd = !0 : r.isPercentage || (r.offset = r.offset * Ka));
                  }
                } else l = !0;
              } else k = p.value;
            } else j = "off" !== p.value;
          } else if (h = b.querySelector(p.value), null === h) throw 'Unable to find anchor target "' + p.value + '"';
        }if (i.length) {
          var w, x, y;!f && F in g ? (y = g[F], w = ia[y].styleAttr, x = ia[y].classAttr) : (y = g[F] = Ra++, w = g.style.cssText, x = Da(g)), ia[y] = { element: g, styleAttr: w, classAttr: x, anchorTarget: h, keyFrames: i, smoothScrolling: j, edgeStrategy: k, emitEvents: l, lastFrameIndex: -1 }, Ea(g, [o], []);
        }
      }
    }for (Aa(), d = 0, e = a.length; e > d; d++) {
      var z = ia[a[d][F]];z !== c && (_(z), ba(z));
    }return ha;
  }, d.prototype.relativeToAbsolute = function (a, b, c) {
    var d = e.clientHeight,
        f = a.getBoundingClientRect(),
        g = f.top,
        h = f.bottom - f.top;return b === E ? g -= d : b === D && (g -= d / 2), c === E ? g += h : c === D && (g += h / 2), g += ha.getScrollTop(), g + .5 | 0;
  }, d.prototype.animateTo = function (a, b) {
    b = b || {};var d = Ha(),
        e = ha.getScrollTop(),
        f = b.duration === c ? x : b.duration;return oa = { startTop: e, topDiff: a - e, targetTop: a, duration: f, startTime: d, endTime: d + f, easing: W[b.easing || w], done: b.done }, oa.topDiff || (oa.done && oa.done.call(ha, !1), oa = c), ha;
  }, d.prototype.stopAnimateTo = function () {
    oa && oa.done && oa.done.call(ha, !0), oa = c;
  }, d.prototype.isAnimatingTo = function () {
    return !!oa;
  }, d.prototype.isMobile = function () {
    return Sa;
  }, d.prototype.setScrollTop = function (b, c) {
    return sa = c === !0, Sa ? Ta = i.min(i.max(b, 0), Ja) : a.scrollTo(0, b), ha;
  }, d.prototype.getScrollTop = function () {
    return Sa ? Ta : a.pageYOffset || e.scrollTop || f.scrollTop || 0;
  }, d.prototype.getMaxScrollTop = function () {
    return Ja;
  }, d.prototype.on = function (a, b) {
    return ka[a] = b, ha;
  }, d.prototype.off = function (a) {
    return delete ka[a], ha;
  }, d.prototype.destroy = function () {
    var a = V();a(va), ya(), Ea(e, [t], [s, u, v]);for (var b = 0, d = ia.length; d > b; b++) {
      fa(ia[b].element);
    }e.style.overflow = f.style.overflow = "", e.style.height = f.style.height = "", ja && g.setStyle(ja, "transform", "none"), ha = c, ja = c, ka = c, la = c, Ja = 0, Ka = 1, ma = c, na = c, La = "down", Ma = -1, Oa = 0, Pa = 0, Qa = !1, oa = c, pa = c, qa = c, ra = c, sa = c, Ra = 0, ta = c, Sa = !1, Ta = 0, ua = c;
  };var X = function X() {
    var d, g, h, j, o, p, q, r, s, t, u, v;wa(e, [k, l, m, n].join(" "), function (a) {
      var e = a.changedTouches[0];for (j = a.target; 3 === j.nodeType;) {
        j = j.parentNode;
      }switch (o = e.clientY, p = e.clientX, t = a.timeStamp, G.test(j.tagName) || a.preventDefault(), a.type) {case k:
          d && d.blur(), ha.stopAnimateTo(), d = j, g = q = o, h = p, s = t;break;case l:
          G.test(j.tagName) && b.activeElement !== j && a.preventDefault(), r = o - q, v = t - u, ha.setScrollTop(Ta - r, !0), q = o, u = t;break;default:case m:case n:
          var f = g - o,
              w = h - p,
              x = w * w + f * f;if (49 > x) {
            if (!G.test(d.tagName)) {
              d.focus();var y = b.createEvent("MouseEvents");y.initMouseEvent("click", !0, !0, a.view, 1, e.screenX, e.screenY, e.clientX, e.clientY, a.ctrlKey, a.altKey, a.shiftKey, a.metaKey, 0, null), d.dispatchEvent(y);
            }return;
          }d = c;var z = r / v;z = i.max(i.min(z, 3), -3);var A = i.abs(z / na),
              B = z * A + .5 * na * A * A,
              C = ha.getScrollTop() - B,
              D = 0;C > Ja ? (D = (Ja - C) / B, C = Ja) : 0 > C && (D = -C / B, C = 0), A *= 1 - D, ha.animateTo(C + .5 | 0, { easing: "outCubic", duration: A });}
    }), a.scrollTo(0, 0), e.style.overflow = f.style.overflow = "hidden";
  },
      Y = function Y() {
    var a,
        b,
        c,
        d,
        f,
        g,
        h,
        j,
        k,
        l,
        m,
        n = e.clientHeight,
        o = Ba();for (j = 0, k = ia.length; k > j; j++) {
      for (a = ia[j], b = a.element, c = a.anchorTarget, d = a.keyFrames, f = 0, g = d.length; g > f; f++) {
        h = d[f], l = h.offset, m = o[h.constant] || 0, h.frame = l, h.isPercentage && (l *= n, h.frame = l), "relative" === h.mode && (fa(b), h.frame = ha.relativeToAbsolute(c, h.anchors[0], h.anchors[1]) - l, fa(b, !0)), h.frame += m, la && !h.isEnd && h.frame > Ja && (Ja = h.frame);
      }
    }for (Ja = i.max(Ja, Ca()), j = 0, k = ia.length; k > j; j++) {
      for (a = ia[j], d = a.keyFrames, f = 0, g = d.length; g > f; f++) {
        h = d[f], m = o[h.constant] || 0, h.isEnd && (h.frame = Ja - h.offset + m);
      }a.keyFrames.sort(Ia);
    }
  },
      Z = function Z(a, b) {
    for (var c = 0, d = ia.length; d > c; c++) {
      var e,
          f,
          i = ia[c],
          j = i.element,
          k = i.smoothScrolling ? a : b,
          l = i.keyFrames,
          m = l.length,
          n = l[0],
          s = l[l.length - 1],
          t = k < n.frame,
          u = k > s.frame,
          v = t ? n : s,
          w = i.emitEvents,
          x = i.lastFrameIndex;if (t || u) {
        if (t && -1 === i.edge || u && 1 === i.edge) continue;switch (t ? (Ea(j, [p], [r, q]), w && x > -1 && (za(j, n.eventType, La), i.lastFrameIndex = -1)) : (Ea(j, [r], [p, q]), w && m > x && (za(j, s.eventType, La), i.lastFrameIndex = m)), i.edge = t ? -1 : 1, i.edgeStrategy) {case "reset":
            fa(j);continue;case "ease":
            k = v.frame;break;default:case "set":
            var y = v.props;for (e in y) {
              h.call(y, e) && (f = ea(y[e].value), 0 === e.indexOf("@") ? j.setAttribute(e.substr(1), f) : g.setStyle(j, e, f));
            }continue;}
      } else 0 !== i.edge && (Ea(j, [o, q], [p, r]), i.edge = 0);for (var z = 0; m - 1 > z; z++) {
        if (k >= l[z].frame && k <= l[z + 1].frame) {
          var A = l[z],
              B = l[z + 1];for (e in A.props) {
            if (h.call(A.props, e)) {
              var C = (k - A.frame) / (B.frame - A.frame);C = A.props[e].easing(C), f = da(A.props[e].value, B.props[e].value, C), f = ea(f), 0 === e.indexOf("@") ? j.setAttribute(e.substr(1), f) : g.setStyle(j, e, f);
            }
          }w && x !== z && ("down" === La ? za(j, A.eventType, La) : za(j, B.eventType, La), i.lastFrameIndex = z);break;
        }
      }
    }
  },
      $ = function $() {
    Qa && (Qa = !1, Aa());var a,
        b,
        d = ha.getScrollTop(),
        e = Ha();if (oa) e >= oa.endTime ? (d = oa.targetTop, a = oa.done, oa = c) : (b = oa.easing((e - oa.startTime) / oa.duration), d = oa.startTop + b * oa.topDiff | 0), ha.setScrollTop(d, !0);else if (!sa) {
      var f = ra.targetTop - d;f && (ra = { startTop: Ma, topDiff: d - Ma, targetTop: d, startTime: Na, endTime: Na + qa }), e <= ra.endTime && (b = W.sqrt((e - ra.startTime) / qa), d = ra.startTop + b * ra.topDiff | 0);
    }if (sa || Ma !== d) {
      La = d > Ma ? "down" : Ma > d ? "up" : La, sa = !1;var h = { curTop: d, lastTop: Ma, maxTop: Ja, direction: La },
          i = ka.beforerender && ka.beforerender.call(ha, h);i !== !1 && (Z(d, ha.getScrollTop()), Sa && ja && g.setStyle(ja, "transform", "translate(0, " + -Ta + "px) " + ua), Ma = d, ka.render && ka.render.call(ha, h)), a && a.call(ha, !1);
    }Na = e;
  },
      _ = function _(a) {
    for (var b = 0, c = a.keyFrames.length; c > b; b++) {
      for (var d, e, f, g, h = a.keyFrames[b], i = {}; null !== (g = J.exec(h.props));) {
        f = g[1], e = g[2], d = f.match(K), null !== d ? (f = d[1], d = d[2]) : d = w, e = e.indexOf("!") ? aa(e) : [e.slice(1)], i[f] = { value: e, easing: W[d] };
      }h.props = i;
    }
  },
      aa = function aa(a) {
    var b = [];return P.lastIndex = 0, a = a.replace(P, function (a) {
      return a.replace(N, function (a) {
        return a / 255 * 100 + "%";
      });
    }), S && (Q.lastIndex = 0, a = a.replace(Q, function (a) {
      return S + a;
    })), a = a.replace(N, function (a) {
      return b.push(+a), "{?}";
    }), b.unshift(a), b;
  },
      ba = function ba(a) {
    var b,
        c,
        d = {};for (b = 0, c = a.keyFrames.length; c > b; b++) {
      ca(a.keyFrames[b], d);
    }for (d = {}, b = a.keyFrames.length - 1; b >= 0; b--) {
      ca(a.keyFrames[b], d);
    }
  },
      ca = function ca(a, b) {
    var c;for (c in b) {
      h.call(a.props, c) || (a.props[c] = b[c]);
    }for (c in a.props) {
      b[c] = a.props[c];
    }
  },
      da = function da(a, b, c) {
    var d,
        e = a.length;if (e !== b.length) throw "Can't interpolate between \"" + a[0] + '" and "' + b[0] + '"';var f = [a[0]];for (d = 1; e > d; d++) {
      f[d] = a[d] + (b[d] - a[d]) * c;
    }return f;
  },
      ea = function ea(a) {
    var b = 1;return O.lastIndex = 0, a[0].replace(O, function () {
      return a[b++];
    });
  },
      fa = function fa(a, b) {
    a = [].concat(a);for (var c, d, e = 0, f = a.length; f > e; e++) {
      d = a[e], c = ia[d[F]], c && (b ? (d.style.cssText = c.dirtyStyleAttr, Ea(d, c.dirtyClassAttr)) : (c.dirtyStyleAttr = d.style.cssText, c.dirtyClassAttr = Da(d), d.style.cssText = c.styleAttr, Ea(d, c.classAttr)));
    }
  },
      ga = function ga() {
    ua = "translateZ(0)", g.setStyle(ja, "transform", ua);var a = j(ja),
        b = a.getPropertyValue("transform"),
        c = a.getPropertyValue(S + "transform"),
        d = b && "none" !== b || c && "none" !== c;d || (ua = "");
  };g.setStyle = function (a, b, c) {
    var d = a.style;if (b = b.replace(L, M).replace("-", ""), "zIndex" === b) isNaN(c) ? d[b] = c : d[b] = "" + (0 | c);else if ("float" === b) d.styleFloat = d.cssFloat = c;else try {
      R && (d[R + b.slice(0, 1).toUpperCase() + b.slice(1)] = c), d[b] = c;
    } catch (e) {}
  };var ha,
      ia,
      ja,
      ka,
      la,
      ma,
      na,
      oa,
      pa,
      qa,
      ra,
      sa,
      ta,
      ua,
      va,
      wa = g.addEvent = function (b, c, d) {
    var e = function e(b) {
      return b = b || a.event, b.target || (b.target = b.srcElement), b.preventDefault || (b.preventDefault = function () {
        b.returnValue = !1, b.defaultPrevented = !0;
      }), d.call(this, b);
    };c = c.split(" ");for (var f, g = 0, h = c.length; h > g; g++) {
      f = c[g], b.addEventListener ? b.addEventListener(f, d, !1) : b.attachEvent("on" + f, e), Ua.push({ element: b, name: f, listener: d });
    }
  },
      xa = g.removeEvent = function (a, b, c) {
    b = b.split(" ");for (var d = 0, e = b.length; e > d; d++) {
      a.removeEventListener ? a.removeEventListener(b[d], c, !1) : a.detachEvent("on" + b[d], c);
    }
  },
      ya = function ya() {
    for (var a, b = 0, c = Ua.length; c > b; b++) {
      a = Ua[b], xa(a.element, a.name, a.listener);
    }Ua = [];
  },
      za = function za(a, b, c) {
    ka.keyframe && ka.keyframe.call(ha, a, b, c);
  },
      Aa = function Aa() {
    var a = ha.getScrollTop();Ja = 0, la && !Sa && (f.style.height = ""), Y(), la && !Sa && (f.style.height = Ja + e.clientHeight + "px"), Sa ? ha.setScrollTop(i.min(ha.getScrollTop(), Ja)) : ha.setScrollTop(a, !0), sa = !0;
  },
      Ba = function Ba() {
    var a,
        b,
        c = e.clientHeight,
        d = {};for (a in ma) {
      b = ma[a], "function" == typeof b ? b = b.call(ha) : /p$/.test(b) && (b = b.slice(0, -1) / 100 * c), d[a] = b;
    }return d;
  },
      Ca = function Ca() {
    var a,
        b = 0;return ja && (b = i.max(ja.offsetHeight, ja.scrollHeight)), a = i.max(b, f.scrollHeight, f.offsetHeight, e.scrollHeight, e.offsetHeight, e.clientHeight), a - e.clientHeight;
  },
      Da = function Da(b) {
    var c = "className";return a.SVGElement && b instanceof a.SVGElement && (b = b[c], c = "baseVal"), b[c];
  },
      Ea = function Ea(b, d, e) {
    var f = "className";if (a.SVGElement && b instanceof a.SVGElement && (b = b[f], f = "baseVal"), e === c) return void (b[f] = d);for (var g = b[f], h = 0, i = e.length; i > h; h++) {
      g = Ga(g).replace(Ga(e[h]), " ");
    }g = Fa(g);for (var j = 0, k = d.length; k > j; j++) {
      -1 === Ga(g).indexOf(Ga(d[j])) && (g += " " + d[j]);
    }b[f] = Fa(g);
  },
      Fa = function Fa(a) {
    return a.replace(H, "");
  },
      Ga = function Ga(a) {
    return " " + a + " ";
  },
      Ha = Date.now || function () {
    return +new Date();
  },
      Ia = function Ia(a, b) {
    return a.frame - b.frame;
  },
      Ja = 0,
      Ka = 1,
      La = "down",
      Ma = -1,
      Na = Ha(),
      Oa = 0,
      Pa = 0,
      Qa = !1,
      Ra = 0,
      Sa = !1,
      Ta = 0,
      Ua = [];"function" == typeof define && define.amd ? define([], function () {
    return g;
  }) : "undefined" != typeof module && module.exports ? module.exports = g : a.skrollr = g;
}(window, document);

},{}],9:[function(require,module,exports){
module.exports = require('./lib/axios');
},{"./lib/axios":11}],10:[function(require,module,exports){
(function (process){
'use strict';

var utils = require('./../utils');
var buildURL = require('./../helpers/buildURL');
var parseHeaders = require('./../helpers/parseHeaders');
var transformData = require('./../helpers/transformData');
var isURLSameOrigin = require('./../helpers/isURLSameOrigin');
var btoa = (typeof window !== 'undefined' && window.btoa) || require('./../helpers/btoa');
var settle = require('../helpers/settle');

module.exports = function xhrAdapter(resolve, reject, config) {
  var requestData = config.data;
  var requestHeaders = config.headers;

  if (utils.isFormData(requestData)) {
    delete requestHeaders['Content-Type']; // Let the browser set it
  }

  var request = new XMLHttpRequest();
  var loadEvent = 'onreadystatechange';
  var xDomain = false;

  // For IE 8/9 CORS support
  // Only supports POST and GET calls and doesn't returns the response headers.
  // DON'T do this for testing b/c XMLHttpRequest is mocked, not XDomainRequest.
  if (process.env.NODE_ENV !== 'test' && typeof window !== 'undefined' && window.XDomainRequest && !('withCredentials' in request) && !isURLSameOrigin(config.url)) {
    request = new window.XDomainRequest();
    loadEvent = 'onload';
    xDomain = true;
    request.onprogress = function handleProgress() {};
    request.ontimeout = function handleTimeout() {};
  }

  // HTTP basic authentication
  if (config.auth) {
    var username = config.auth.username || '';
    var password = config.auth.password || '';
    requestHeaders.Authorization = 'Basic ' + btoa(username + ':' + password);
  }

  request.open(config.method.toUpperCase(), buildURL(config.url, config.params, config.paramsSerializer), true);

  // Set the request timeout in MS
  request.timeout = config.timeout;

  // Listen for ready state
  request[loadEvent] = function handleLoad() {
    if (!request || (request.readyState !== 4 && !xDomain)) {
      return;
    }

    // The request errored out and we didn't get a response, this will be
    // handled by onerror instead
    if (request.status === 0) {
      return;
    }

    // Prepare the response
    var responseHeaders = 'getAllResponseHeaders' in request ? parseHeaders(request.getAllResponseHeaders()) : null;
    var responseData = !config.responseType || config.responseType === 'text' ? request.responseText : request.response;
    var response = {
      data: transformData(
        responseData,
        responseHeaders,
        config.transformResponse
      ),
      // IE sends 1223 instead of 204 (https://github.com/mzabriskie/axios/issues/201)
      status: request.status === 1223 ? 204 : request.status,
      statusText: request.status === 1223 ? 'No Content' : request.statusText,
      headers: responseHeaders,
      config: config,
      request: request
    };

    settle(resolve, reject, response);

    // Clean up request
    request = null;
  };

  // Handle low level network errors
  request.onerror = function handleError() {
    // Real errors are hidden from us by the browser
    // onerror should only fire if it's a network error
    reject(new Error('Network Error'));

    // Clean up request
    request = null;
  };

  // Handle timeout
  request.ontimeout = function handleTimeout() {
    var err = new Error('timeout of ' + config.timeout + 'ms exceeded');
    err.timeout = config.timeout;
    err.code = 'ECONNABORTED';
    reject(err);

    // Clean up request
    request = null;
  };

  // Add xsrf header
  // This is only done if running in a standard browser environment.
  // Specifically not if we're in a web worker, or react-native.
  if (utils.isStandardBrowserEnv()) {
    var cookies = require('./../helpers/cookies');

    // Add xsrf header
    var xsrfValue = config.withCredentials || isURLSameOrigin(config.url) ?
        cookies.read(config.xsrfCookieName) :
        undefined;

    if (xsrfValue) {
      requestHeaders[config.xsrfHeaderName] = xsrfValue;
    }
  }

  // Add headers to the request
  if ('setRequestHeader' in request) {
    utils.forEach(requestHeaders, function setRequestHeader(val, key) {
      if (typeof requestData === 'undefined' && key.toLowerCase() === 'content-type') {
        // Remove Content-Type if data is undefined
        delete requestHeaders[key];
      } else {
        // Otherwise add header to the request
        request.setRequestHeader(key, val);
      }
    });
  }

  // Add withCredentials to request if needed
  if (config.withCredentials) {
    request.withCredentials = true;
  }

  // Add responseType to request if needed
  if (config.responseType) {
    try {
      request.responseType = config.responseType;
    } catch (e) {
      if (request.responseType !== 'json') {
        throw e;
      }
    }
  }

  // Handle progress if needed
  if (config.progress) {
    if (config.method === 'post' || config.method === 'put') {
      request.upload.addEventListener('progress', config.progress);
    } else if (config.method === 'get') {
      request.addEventListener('progress', config.progress);
    }
  }

  if (requestData === undefined) {
    requestData = null;
  }

  // Send the request
  request.send(requestData);
};

}).call(this,require('_process'))

},{"../helpers/settle":24,"./../helpers/btoa":16,"./../helpers/buildURL":17,"./../helpers/cookies":19,"./../helpers/isURLSameOrigin":21,"./../helpers/parseHeaders":23,"./../helpers/transformData":26,"./../utils":27,"_process":28}],11:[function(require,module,exports){
'use strict';

var defaults = require('./defaults');
var utils = require('./utils');
var dispatchRequest = require('./core/dispatchRequest');
var InterceptorManager = require('./core/InterceptorManager');
var isAbsoluteURL = require('./helpers/isAbsoluteURL');
var combineURLs = require('./helpers/combineURLs');
var bind = require('./helpers/bind');
var transformData = require('./helpers/transformData');

function Axios(defaultConfig) {
  this.defaults = utils.merge({}, defaultConfig);
  this.interceptors = {
    request: new InterceptorManager(),
    response: new InterceptorManager()
  };
}

Axios.prototype.request = function request(config) {
  /*eslint no-param-reassign:0*/
  // Allow for axios('example/url'[, config]) a la fetch API
  if (typeof config === 'string') {
    config = utils.merge({
      url: arguments[0]
    }, arguments[1]);
  }

  config = utils.merge(defaults, this.defaults, { method: 'get' }, config);

  // Support baseURL config
  if (config.baseURL && !isAbsoluteURL(config.url)) {
    config.url = combineURLs(config.baseURL, config.url);
  }

  // Don't allow overriding defaults.withCredentials
  config.withCredentials = config.withCredentials || this.defaults.withCredentials;

  // Transform request data
  config.data = transformData(
    config.data,
    config.headers,
    config.transformRequest
  );

  // Flatten headers
  config.headers = utils.merge(
    config.headers.common || {},
    config.headers[config.method] || {},
    config.headers || {}
  );

  utils.forEach(
    ['delete', 'get', 'head', 'post', 'put', 'patch', 'common'],
    function cleanHeaderConfig(method) {
      delete config.headers[method];
    }
  );

  // Hook up interceptors middleware
  var chain = [dispatchRequest, undefined];
  var promise = Promise.resolve(config);

  this.interceptors.request.forEach(function unshiftRequestInterceptors(interceptor) {
    chain.unshift(interceptor.fulfilled, interceptor.rejected);
  });

  this.interceptors.response.forEach(function pushResponseInterceptors(interceptor) {
    chain.push(interceptor.fulfilled, interceptor.rejected);
  });

  while (chain.length) {
    promise = promise.then(chain.shift(), chain.shift());
  }

  return promise;
};

var defaultInstance = new Axios(defaults);
var axios = module.exports = bind(Axios.prototype.request, defaultInstance);
axios.request = bind(Axios.prototype.request, defaultInstance);

// Expose Axios class to allow class inheritance
axios.Axios = Axios;

// Expose properties from defaultInstance
axios.defaults = defaultInstance.defaults;
axios.interceptors = defaultInstance.interceptors;

// Factory for creating new instances
axios.create = function create(defaultConfig) {
  return new Axios(defaultConfig);
};

// Expose all/spread
axios.all = function all(promises) {
  return Promise.all(promises);
};
axios.spread = require('./helpers/spread');

// Provide aliases for supported request methods
utils.forEach(['delete', 'get', 'head'], function forEachMethodNoData(method) {
  /*eslint func-names:0*/
  Axios.prototype[method] = function(url, config) {
    return this.request(utils.merge(config || {}, {
      method: method,
      url: url
    }));
  };
  axios[method] = bind(Axios.prototype[method], defaultInstance);
});

utils.forEach(['post', 'put', 'patch'], function forEachMethodWithData(method) {
  /*eslint func-names:0*/
  Axios.prototype[method] = function(url, data, config) {
    return this.request(utils.merge(config || {}, {
      method: method,
      url: url,
      data: data
    }));
  };
  axios[method] = bind(Axios.prototype[method], defaultInstance);
});

},{"./core/InterceptorManager":12,"./core/dispatchRequest":13,"./defaults":14,"./helpers/bind":15,"./helpers/combineURLs":18,"./helpers/isAbsoluteURL":20,"./helpers/spread":25,"./helpers/transformData":26,"./utils":27}],12:[function(require,module,exports){
'use strict';

var utils = require('./../utils');

function InterceptorManager() {
  this.handlers = [];
}

/**
 * Add a new interceptor to the stack
 *
 * @param {Function} fulfilled The function to handle `then` for a `Promise`
 * @param {Function} rejected The function to handle `reject` for a `Promise`
 *
 * @return {Number} An ID used to remove interceptor later
 */
InterceptorManager.prototype.use = function use(fulfilled, rejected) {
  this.handlers.push({
    fulfilled: fulfilled,
    rejected: rejected
  });
  return this.handlers.length - 1;
};

/**
 * Remove an interceptor from the stack
 *
 * @param {Number} id The ID that was returned by `use`
 */
InterceptorManager.prototype.eject = function eject(id) {
  if (this.handlers[id]) {
    this.handlers[id] = null;
  }
};

/**
 * Iterate over all the registered interceptors
 *
 * This method is particularly useful for skipping over any
 * interceptors that may have become `null` calling `eject`.
 *
 * @param {Function} fn The function to call for each interceptor
 */
InterceptorManager.prototype.forEach = function forEach(fn) {
  utils.forEach(this.handlers, function forEachHandler(h) {
    if (h !== null) {
      fn(h);
    }
  });
};

module.exports = InterceptorManager;

},{"./../utils":27}],13:[function(require,module,exports){
(function (process){
'use strict';

/**
 * Dispatch a request to the server using whichever adapter
 * is supported by the current environment.
 *
 * @param {object} config The config that is to be used for the request
 * @returns {Promise} The Promise to be fulfilled
 */
module.exports = function dispatchRequest(config) {
  return new Promise(function executor(resolve, reject) {
    try {
      var adapter;

      if (typeof config.adapter === 'function') {
        // For custom adapter support
        adapter = config.adapter;
      } else if (typeof XMLHttpRequest !== 'undefined') {
        // For browsers use XHR adapter
        adapter = require('../adapters/xhr');
      } else if (typeof process !== 'undefined') {
        // For node use HTTP adapter
        adapter = require('../adapters/http');
      }

      if (typeof adapter === 'function') {
        adapter(resolve, reject, config);
      }
    } catch (e) {
      reject(e);
    }
  });
};


}).call(this,require('_process'))

},{"../adapters/http":10,"../adapters/xhr":10,"_process":28}],14:[function(require,module,exports){
'use strict';

var utils = require('./utils');
var normalizeHeaderName = require('./helpers/normalizeHeaderName');

var PROTECTION_PREFIX = /^\)\]\}',?\n/;
var DEFAULT_CONTENT_TYPE = {
  'Content-Type': 'application/x-www-form-urlencoded'
};

function setContentTypeIfUnset(headers, value) {
  if (!utils.isUndefined(headers) && utils.isUndefined(headers['Content-Type'])) {
    headers['Content-Type'] = value;
  }
}

module.exports = {
  transformRequest: [function transformRequest(data, headers) {
    normalizeHeaderName(headers, 'Content-Type');
    if (utils.isFormData(data) ||
      utils.isArrayBuffer(data) ||
      utils.isStream(data) ||
      utils.isFile(data) ||
      utils.isBlob(data)
    ) {
      return data;
    }
    if (utils.isArrayBufferView(data)) {
      return data.buffer;
    }
    if (utils.isURLSearchParams(data)) {
      setContentTypeIfUnset(headers, 'application/x-www-form-urlencoded;charset=utf-8');
      return data.toString();
    }
    if (utils.isObject(data)) {
      setContentTypeIfUnset(headers, 'application/json;charset=utf-8');
      return JSON.stringify(data);
    }
    return data;
  }],

  transformResponse: [function transformResponse(data) {
    /*eslint no-param-reassign:0*/
    if (typeof data === 'string') {
      data = data.replace(PROTECTION_PREFIX, '');
      try {
        data = JSON.parse(data);
      } catch (e) { /* Ignore */ }
    }
    return data;
  }],

  headers: {
    common: {
      'Accept': 'application/json, text/plain, */*'
    },
    patch: utils.merge(DEFAULT_CONTENT_TYPE),
    post: utils.merge(DEFAULT_CONTENT_TYPE),
    put: utils.merge(DEFAULT_CONTENT_TYPE)
  },

  timeout: 0,

  xsrfCookieName: 'XSRF-TOKEN',
  xsrfHeaderName: 'X-XSRF-TOKEN',

  maxContentLength: -1,

  validateStatus: function validateStatus(status) {
    return status >= 200 && status < 300;
  }
};

},{"./helpers/normalizeHeaderName":22,"./utils":27}],15:[function(require,module,exports){
'use strict';

module.exports = function bind(fn, thisArg) {
  return function wrap() {
    var args = new Array(arguments.length);
    for (var i = 0; i < args.length; i++) {
      args[i] = arguments[i];
    }
    return fn.apply(thisArg, args);
  };
};

},{}],16:[function(require,module,exports){
'use strict';

// btoa polyfill for IE<10 courtesy https://github.com/davidchambers/Base64.js

var chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';

function E() {
  this.message = 'String contains an invalid character';
}
E.prototype = new Error;
E.prototype.code = 5;
E.prototype.name = 'InvalidCharacterError';

function btoa(input) {
  var str = String(input);
  var output = '';
  for (
    // initialize result and counter
    var block, charCode, idx = 0, map = chars;
    // if the next str index does not exist:
    //   change the mapping table to "="
    //   check if d has no fractional digits
    str.charAt(idx | 0) || (map = '=', idx % 1);
    // "8 - idx % 1 * 8" generates the sequence 2, 4, 6, 8
    output += map.charAt(63 & block >> 8 - idx % 1 * 8)
  ) {
    charCode = str.charCodeAt(idx += 3 / 4);
    if (charCode > 0xFF) {
      throw new E();
    }
    block = block << 8 | charCode;
  }
  return output;
}

module.exports = btoa;

},{}],17:[function(require,module,exports){
'use strict';

var utils = require('./../utils');

function encode(val) {
  return encodeURIComponent(val).
    replace(/%40/gi, '@').
    replace(/%3A/gi, ':').
    replace(/%24/g, '$').
    replace(/%2C/gi, ',').
    replace(/%20/g, '+').
    replace(/%5B/gi, '[').
    replace(/%5D/gi, ']');
}

/**
 * Build a URL by appending params to the end
 *
 * @param {string} url The base of the url (e.g., http://www.google.com)
 * @param {object} [params] The params to be appended
 * @returns {string} The formatted url
 */
module.exports = function buildURL(url, params, paramsSerializer) {
  /*eslint no-param-reassign:0*/
  if (!params) {
    return url;
  }

  var serializedParams;
  if (paramsSerializer) {
    serializedParams = paramsSerializer(params);
  } else if (utils.isURLSearchParams(params)) {
    serializedParams = params.toString();
  } else {
    var parts = [];

    utils.forEach(params, function serialize(val, key) {
      if (val === null || typeof val === 'undefined') {
        return;
      }

      if (utils.isArray(val)) {
        key = key + '[]';
      }

      if (!utils.isArray(val)) {
        val = [val];
      }

      utils.forEach(val, function parseValue(v) {
        if (utils.isDate(v)) {
          v = v.toISOString();
        } else if (utils.isObject(v)) {
          v = JSON.stringify(v);
        }
        parts.push(encode(key) + '=' + encode(v));
      });
    });

    serializedParams = parts.join('&');
  }

  if (serializedParams) {
    url += (url.indexOf('?') === -1 ? '?' : '&') + serializedParams;
  }

  return url;
};

},{"./../utils":27}],18:[function(require,module,exports){
'use strict';

/**
 * Creates a new URL by combining the specified URLs
 *
 * @param {string} baseURL The base URL
 * @param {string} relativeURL The relative URL
 * @returns {string} The combined URL
 */
module.exports = function combineURLs(baseURL, relativeURL) {
  return baseURL.replace(/\/+$/, '') + '/' + relativeURL.replace(/^\/+/, '');
};

},{}],19:[function(require,module,exports){
'use strict';

var utils = require('./../utils');

module.exports = (
  utils.isStandardBrowserEnv() ?

  // Standard browser envs support document.cookie
  (function standardBrowserEnv() {
    return {
      write: function write(name, value, expires, path, domain, secure) {
        var cookie = [];
        cookie.push(name + '=' + encodeURIComponent(value));

        if (utils.isNumber(expires)) {
          cookie.push('expires=' + new Date(expires).toGMTString());
        }

        if (utils.isString(path)) {
          cookie.push('path=' + path);
        }

        if (utils.isString(domain)) {
          cookie.push('domain=' + domain);
        }

        if (secure === true) {
          cookie.push('secure');
        }

        document.cookie = cookie.join('; ');
      },

      read: function read(name) {
        var match = document.cookie.match(new RegExp('(^|;\\s*)(' + name + ')=([^;]*)'));
        return (match ? decodeURIComponent(match[3]) : null);
      },

      remove: function remove(name) {
        this.write(name, '', Date.now() - 86400000);
      }
    };
  })() :

  // Non standard browser env (web workers, react-native) lack needed support.
  (function nonStandardBrowserEnv() {
    return {
      write: function write() {},
      read: function read() { return null; },
      remove: function remove() {}
    };
  })()
);

},{"./../utils":27}],20:[function(require,module,exports){
'use strict';

/**
 * Determines whether the specified URL is absolute
 *
 * @param {string} url The URL to test
 * @returns {boolean} True if the specified URL is absolute, otherwise false
 */
module.exports = function isAbsoluteURL(url) {
  // A URL is considered absolute if it begins with "<scheme>://" or "//" (protocol-relative URL).
  // RFC 3986 defines scheme name as a sequence of characters beginning with a letter and followed
  // by any combination of letters, digits, plus, period, or hyphen.
  return /^([a-z][a-z\d\+\-\.]*:)?\/\//i.test(url);
};

},{}],21:[function(require,module,exports){
'use strict';

var utils = require('./../utils');

module.exports = (
  utils.isStandardBrowserEnv() ?

  // Standard browser envs have full support of the APIs needed to test
  // whether the request URL is of the same origin as current location.
  (function standardBrowserEnv() {
    var msie = /(msie|trident)/i.test(navigator.userAgent);
    var urlParsingNode = document.createElement('a');
    var originURL;

    /**
    * Parse a URL to discover it's components
    *
    * @param {String} url The URL to be parsed
    * @returns {Object}
    */
    function resolveURL(url) {
      var href = url;

      if (msie) {
        // IE needs attribute set twice to normalize properties
        urlParsingNode.setAttribute('href', href);
        href = urlParsingNode.href;
      }

      urlParsingNode.setAttribute('href', href);

      // urlParsingNode provides the UrlUtils interface - http://url.spec.whatwg.org/#urlutils
      return {
        href: urlParsingNode.href,
        protocol: urlParsingNode.protocol ? urlParsingNode.protocol.replace(/:$/, '') : '',
        host: urlParsingNode.host,
        search: urlParsingNode.search ? urlParsingNode.search.replace(/^\?/, '') : '',
        hash: urlParsingNode.hash ? urlParsingNode.hash.replace(/^#/, '') : '',
        hostname: urlParsingNode.hostname,
        port: urlParsingNode.port,
        pathname: (urlParsingNode.pathname.charAt(0) === '/') ?
                  urlParsingNode.pathname :
                  '/' + urlParsingNode.pathname
      };
    }

    originURL = resolveURL(window.location.href);

    /**
    * Determine if a URL shares the same origin as the current location
    *
    * @param {String} requestURL The URL to test
    * @returns {boolean} True if URL shares the same origin, otherwise false
    */
    return function isURLSameOrigin(requestURL) {
      var parsed = (utils.isString(requestURL)) ? resolveURL(requestURL) : requestURL;
      return (parsed.protocol === originURL.protocol &&
            parsed.host === originURL.host);
    };
  })() :

  // Non standard browser envs (web workers, react-native) lack needed support.
  (function nonStandardBrowserEnv() {
    return function isURLSameOrigin() {
      return true;
    };
  })()
);

},{"./../utils":27}],22:[function(require,module,exports){
'use strict';

var utils = require('../utils');

module.exports = function normalizeHeaderName(headers, normalizedName) {
  utils.forEach(headers, function processHeader(value, name) {
    if (name !== normalizedName && name.toUpperCase() === normalizedName.toUpperCase()) {
      headers[normalizedName] = value;
      delete headers[name];
    }
  });
};

},{"../utils":27}],23:[function(require,module,exports){
'use strict';

var utils = require('./../utils');

/**
 * Parse headers into an object
 *
 * ```
 * Date: Wed, 27 Aug 2014 08:58:49 GMT
 * Content-Type: application/json
 * Connection: keep-alive
 * Transfer-Encoding: chunked
 * ```
 *
 * @param {String} headers Headers needing to be parsed
 * @returns {Object} Headers parsed into an object
 */
module.exports = function parseHeaders(headers) {
  var parsed = {};
  var key;
  var val;
  var i;

  if (!headers) { return parsed; }

  utils.forEach(headers.split('\n'), function parser(line) {
    i = line.indexOf(':');
    key = utils.trim(line.substr(0, i)).toLowerCase();
    val = utils.trim(line.substr(i + 1));

    if (key) {
      parsed[key] = parsed[key] ? parsed[key] + ', ' + val : val;
    }
  });

  return parsed;
};

},{"./../utils":27}],24:[function(require,module,exports){
'use strict';

/**
 * Resolve or reject a Promise based on response status.
 *
 * @param {Function} resolve A function that resolves the promise.
 * @param {Function} reject A function that rejects the promise.
 * @param {object} response The response.
 */
module.exports = function settle(resolve, reject, response) {
  var validateStatus = response.config.validateStatus;
  // Note: status is not exposed by XDomainRequest
  if (!response.status || !validateStatus || validateStatus(response.status)) {
    resolve(response);
  } else {
    reject(response);
  }
};

},{}],25:[function(require,module,exports){
'use strict';

/**
 * Syntactic sugar for invoking a function and expanding an array for arguments.
 *
 * Common use case would be to use `Function.prototype.apply`.
 *
 *  ```js
 *  function f(x, y, z) {}
 *  var args = [1, 2, 3];
 *  f.apply(null, args);
 *  ```
 *
 * With `spread` this example can be re-written.
 *
 *  ```js
 *  spread(function(x, y, z) {})([1, 2, 3]);
 *  ```
 *
 * @param {Function} callback
 * @returns {Function}
 */
module.exports = function spread(callback) {
  return function wrap(arr) {
    return callback.apply(null, arr);
  };
};

},{}],26:[function(require,module,exports){
'use strict';

var utils = require('./../utils');

/**
 * Transform the data for a request or a response
 *
 * @param {Object|String} data The data to be transformed
 * @param {Array} headers The headers for the request or response
 * @param {Array|Function} fns A single function or Array of functions
 * @returns {*} The resulting transformed data
 */
module.exports = function transformData(data, headers, fns) {
  /*eslint no-param-reassign:0*/
  utils.forEach(fns, function transform(fn) {
    data = fn(data, headers);
  });

  return data;
};

},{"./../utils":27}],27:[function(require,module,exports){
'use strict';

/*global toString:true*/

// utils is a library of generic helper functions non-specific to axios

var toString = Object.prototype.toString;

/**
 * Determine if a value is an Array
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is an Array, otherwise false
 */
function isArray(val) {
  return toString.call(val) === '[object Array]';
}

/**
 * Determine if a value is an ArrayBuffer
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is an ArrayBuffer, otherwise false
 */
function isArrayBuffer(val) {
  return toString.call(val) === '[object ArrayBuffer]';
}

/**
 * Determine if a value is a FormData
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is an FormData, otherwise false
 */
function isFormData(val) {
  return (typeof FormData !== 'undefined') && (val instanceof FormData);
}

/**
 * Determine if a value is a view on an ArrayBuffer
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a view on an ArrayBuffer, otherwise false
 */
function isArrayBufferView(val) {
  var result;
  if ((typeof ArrayBuffer !== 'undefined') && (ArrayBuffer.isView)) {
    result = ArrayBuffer.isView(val);
  } else {
    result = (val) && (val.buffer) && (val.buffer instanceof ArrayBuffer);
  }
  return result;
}

/**
 * Determine if a value is a String
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a String, otherwise false
 */
function isString(val) {
  return typeof val === 'string';
}

/**
 * Determine if a value is a Number
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a Number, otherwise false
 */
function isNumber(val) {
  return typeof val === 'number';
}

/**
 * Determine if a value is undefined
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if the value is undefined, otherwise false
 */
function isUndefined(val) {
  return typeof val === 'undefined';
}

/**
 * Determine if a value is an Object
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is an Object, otherwise false
 */
function isObject(val) {
  return val !== null && typeof val === 'object';
}

/**
 * Determine if a value is a Date
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a Date, otherwise false
 */
function isDate(val) {
  return toString.call(val) === '[object Date]';
}

/**
 * Determine if a value is a File
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a File, otherwise false
 */
function isFile(val) {
  return toString.call(val) === '[object File]';
}

/**
 * Determine if a value is a Blob
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a Blob, otherwise false
 */
function isBlob(val) {
  return toString.call(val) === '[object Blob]';
}

/**
 * Determine if a value is a Function
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a Function, otherwise false
 */
function isFunction(val) {
  return toString.call(val) === '[object Function]';
}

/**
 * Determine if a value is a Stream
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a Stream, otherwise false
 */
function isStream(val) {
  return isObject(val) && isFunction(val.pipe);
}

/**
 * Determine if a value is a URLSearchParams object
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a URLSearchParams object, otherwise false
 */
function isURLSearchParams(val) {
  return typeof URLSearchParams !== 'undefined' && val instanceof URLSearchParams;
}

/**
 * Trim excess whitespace off the beginning and end of a string
 *
 * @param {String} str The String to trim
 * @returns {String} The String freed of excess whitespace
 */
function trim(str) {
  return str.replace(/^\s*/, '').replace(/\s*$/, '');
}

/**
 * Determine if we're running in a standard browser environment
 *
 * This allows axios to run in a web worker, and react-native.
 * Both environments support XMLHttpRequest, but not fully standard globals.
 *
 * web workers:
 *  typeof window -> undefined
 *  typeof document -> undefined
 *
 * react-native:
 *  typeof document.createElement -> undefined
 */
function isStandardBrowserEnv() {
  return (
    typeof window !== 'undefined' &&
    typeof document !== 'undefined' &&
    typeof document.createElement === 'function'
  );
}

/**
 * Iterate over an Array or an Object invoking a function for each item.
 *
 * If `obj` is an Array callback will be called passing
 * the value, index, and complete array for each item.
 *
 * If 'obj' is an Object callback will be called passing
 * the value, key, and complete object for each property.
 *
 * @param {Object|Array} obj The object to iterate
 * @param {Function} fn The callback to invoke for each item
 */
function forEach(obj, fn) {
  // Don't bother if no value provided
  if (obj === null || typeof obj === 'undefined') {
    return;
  }

  // Force an array if not already something iterable
  if (typeof obj !== 'object' && !isArray(obj)) {
    /*eslint no-param-reassign:0*/
    obj = [obj];
  }

  if (isArray(obj)) {
    // Iterate over array values
    for (var i = 0, l = obj.length; i < l; i++) {
      fn.call(null, obj[i], i, obj);
    }
  } else {
    // Iterate over object keys
    for (var key in obj) {
      if (obj.hasOwnProperty(key)) {
        fn.call(null, obj[key], key, obj);
      }
    }
  }
}

/**
 * Accepts varargs expecting each argument to be an object, then
 * immutably merges the properties of each object and returns result.
 *
 * When multiple objects contain the same key the later object in
 * the arguments list will take precedence.
 *
 * Example:
 *
 * ```js
 * var result = merge({foo: 123}, {foo: 456});
 * console.log(result.foo); // outputs 456
 * ```
 *
 * @param {Object} obj1 Object to merge
 * @returns {Object} Result of all merge properties
 */
function merge(/* obj1, obj2, obj3, ... */) {
  var result = {};
  function assignValue(val, key) {
    if (typeof result[key] === 'object' && typeof val === 'object') {
      result[key] = merge(result[key], val);
    } else {
      result[key] = val;
    }
  }

  for (var i = 0, l = arguments.length; i < l; i++) {
    forEach(arguments[i], assignValue);
  }
  return result;
}

module.exports = {
  isArray: isArray,
  isArrayBuffer: isArrayBuffer,
  isFormData: isFormData,
  isArrayBufferView: isArrayBufferView,
  isString: isString,
  isNumber: isNumber,
  isObject: isObject,
  isUndefined: isUndefined,
  isDate: isDate,
  isFile: isFile,
  isBlob: isBlob,
  isFunction: isFunction,
  isStream: isStream,
  isURLSearchParams: isURLSearchParams,
  isStandardBrowserEnv: isStandardBrowserEnv,
  forEach: forEach,
  merge: merge,
  trim: trim
};

},{}],28:[function(require,module,exports){
// shim for using process in browser

var process = module.exports = {};
var queue = [];
var draining = false;
var currentQueue;
var queueIndex = -1;

function cleanUpNextTick() {
    if (!draining || !currentQueue) {
        return;
    }
    draining = false;
    if (currentQueue.length) {
        queue = currentQueue.concat(queue);
    } else {
        queueIndex = -1;
    }
    if (queue.length) {
        drainQueue();
    }
}

function drainQueue() {
    if (draining) {
        return;
    }
    var timeout = setTimeout(cleanUpNextTick);
    draining = true;

    var len = queue.length;
    while(len) {
        currentQueue = queue;
        queue = [];
        while (++queueIndex < len) {
            if (currentQueue) {
                currentQueue[queueIndex].run();
            }
        }
        queueIndex = -1;
        len = queue.length;
    }
    currentQueue = null;
    draining = false;
    clearTimeout(timeout);
}

process.nextTick = function (fun) {
    var args = new Array(arguments.length - 1);
    if (arguments.length > 1) {
        for (var i = 1; i < arguments.length; i++) {
            args[i - 1] = arguments[i];
        }
    }
    queue.push(new Item(fun, args));
    if (queue.length === 1 && !draining) {
        setTimeout(drainQueue, 0);
    }
};

// v8 likes predictible objects
function Item(fun, array) {
    this.fun = fun;
    this.array = array;
}
Item.prototype.run = function () {
    this.fun.apply(null, this.array);
};
process.title = 'browser';
process.browser = true;
process.env = {};
process.argv = [];
process.version = ''; // empty string to avoid regexp issues
process.versions = {};

function noop() {}

process.on = noop;
process.addListener = noop;
process.once = noop;
process.off = noop;
process.removeListener = noop;
process.removeAllListeners = noop;
process.emit = noop;

process.binding = function (name) {
    throw new Error('process.binding is not supported');
};

process.cwd = function () { return '/' };
process.chdir = function (dir) {
    throw new Error('process.chdir is not supported');
};
process.umask = function() { return 0; };

},{}]},{},[6])
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJjbGllbnQvc2NyaXB0cy92YW5pbGxhL2Jvb3N0ZXJzLmpzIiwiY2xpZW50L3NjcmlwdHMvdmFuaWxsYS9jb25zb2xlLmpzIiwiY2xpZW50L3NjcmlwdHMvdmFuaWxsYS9kb2MuanMiLCJjbGllbnQvc2NyaXB0cy92YW5pbGxhL2ZlYXR1cmVzLmpzIiwiY2xpZW50L3NjcmlwdHMvdmFuaWxsYS9oZWxwZXJzLmpzIiwiY2xpZW50L3NjcmlwdHMvdmFuaWxsYS9pbmRleC5qcyIsImNsaWVudC9zY3JpcHRzL3ZhbmlsbGEvbmF2LmpzIiwiY2xpZW50L3NjcmlwdHMvdmFuaWxsYS9za3JvbGxyLm1pbi5qcyIsIm5vZGVfbW9kdWxlcy9heGlvcy9pbmRleC5qcyIsIm5vZGVfbW9kdWxlcy9heGlvcy9saWIvYWRhcHRlcnMveGhyLmpzIiwibm9kZV9tb2R1bGVzL2F4aW9zL2xpYi9heGlvcy5qcyIsIm5vZGVfbW9kdWxlcy9heGlvcy9saWIvY29yZS9JbnRlcmNlcHRvck1hbmFnZXIuanMiLCJub2RlX21vZHVsZXMvYXhpb3MvbGliL2NvcmUvZGlzcGF0Y2hSZXF1ZXN0LmpzIiwibm9kZV9tb2R1bGVzL2F4aW9zL2xpYi9kZWZhdWx0cy5qcyIsIm5vZGVfbW9kdWxlcy9heGlvcy9saWIvaGVscGVycy9iaW5kLmpzIiwibm9kZV9tb2R1bGVzL2F4aW9zL2xpYi9oZWxwZXJzL2J0b2EuanMiLCJub2RlX21vZHVsZXMvYXhpb3MvbGliL2hlbHBlcnMvYnVpbGRVUkwuanMiLCJub2RlX21vZHVsZXMvYXhpb3MvbGliL2hlbHBlcnMvY29tYmluZVVSTHMuanMiLCJub2RlX21vZHVsZXMvYXhpb3MvbGliL2hlbHBlcnMvY29va2llcy5qcyIsIm5vZGVfbW9kdWxlcy9heGlvcy9saWIvaGVscGVycy9pc0Fic29sdXRlVVJMLmpzIiwibm9kZV9tb2R1bGVzL2F4aW9zL2xpYi9oZWxwZXJzL2lzVVJMU2FtZU9yaWdpbi5qcyIsIm5vZGVfbW9kdWxlcy9heGlvcy9saWIvaGVscGVycy9ub3JtYWxpemVIZWFkZXJOYW1lLmpzIiwibm9kZV9tb2R1bGVzL2F4aW9zL2xpYi9oZWxwZXJzL3BhcnNlSGVhZGVycy5qcyIsIm5vZGVfbW9kdWxlcy9heGlvcy9saWIvaGVscGVycy9zZXR0bGUuanMiLCJub2RlX21vZHVsZXMvYXhpb3MvbGliL2hlbHBlcnMvc3ByZWFkLmpzIiwibm9kZV9tb2R1bGVzL2F4aW9zL2xpYi9oZWxwZXJzL3RyYW5zZm9ybURhdGEuanMiLCJub2RlX21vZHVsZXMvYXhpb3MvbGliL3V0aWxzLmpzIiwibm9kZV9tb2R1bGVzL3Byb2Nlc3MvYnJvd3Nlci5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTs7Ozs7OztBQ0FBOztBQUVBLElBQU0sV0FBVztBQUNiLFlBQVE7QUFDSixlQUFPO0FBQ0gsc0JBQVUsMEJBQVksU0FBUyxnQkFBVCxDQUEwQixvQkFBMUIsQ0FBWixDQURQO0FBRUgscUJBQVMsU0FBUyxhQUFULENBQXVCLG1CQUF2QixDQUZOO0FBR0gsK0JBQW1CLFNBQVMsYUFBVCxDQUF1QixxQkFBdkIsQ0FIaEI7QUFJSCxtQkFBTyxTQUFTLGdCQUFULENBQTBCLDRCQUExQjtBQUpKLFNBREg7QUFPSixpQ0FBeUIsU0FBUyxhQUFULENBQXVCLHFCQUF2QixJQUFnRCxTQUFTLGFBQVQsQ0FBdUIscUJBQXZCLEVBQThDLFlBQTlGLEdBQTZHO0FBUGxJLEtBREs7O0FBV2IsV0FBTztBQUNILGlCQUFTLFNBRE47QUFFSCxzQkFBYztBQUZYLEtBWE07O0FBZ0JiLFFBaEJhLGtCQWdCTjtBQUNILGlCQUFTLFVBQVQ7QUFDSCxLQWxCWTtBQW9CYixjQXBCYSx3QkFvQkE7OztBQUdULGlCQUFTLE1BQVQsQ0FBZ0IsS0FBaEIsQ0FBc0IsUUFBdEIsQ0FBK0IsT0FBL0IsQ0FBdUMsVUFBQyxPQUFELEVBQVUsS0FBVixFQUFvQjtBQUN2RCxvQkFBUSxnQkFBUixDQUF5QixPQUF6QixFQUFrQyxVQUFDLEtBQUQsRUFBVztBQUN6QyxzQkFBTSxlQUFOO0FBQ0EseUJBQVMsS0FBVCxDQUFlLE9BQWYsR0FBeUIsTUFBTSxhQUEvQjtBQUNBLHlCQUFTLEtBQVQsQ0FBZSxZQUFmLEdBQThCLEtBQTlCO0FBQ0EseUJBQVMsT0FBVCxDQUFpQixJQUFqQjtBQUNBLHlCQUFTLE9BQVQsQ0FBaUIsWUFBakI7QUFDSCxhQU5EO0FBT0gsU0FSRDs7O0FBV0EsaUJBQVMsTUFBVCxDQUFnQixLQUFoQixDQUFzQixLQUF0QixDQUE0QixPQUE1QixDQUFvQyxpQkFBUztBQUN6QyxrQkFBTSxnQkFBTixDQUF1QixPQUF2QixFQUFnQyxVQUFDLEtBQUQsRUFBVztBQUN2QyxzQkFBTSxlQUFOO0FBQ0EseUJBQVMsT0FBVCxDQUFpQixLQUFqQjtBQUNBLHlCQUFTLE9BQVQsQ0FBaUIsWUFBakI7QUFDSCxhQUpEO0FBS0gsU0FORDs7O0FBU0EsaUJBQVMsZ0JBQVQsQ0FBMEIsT0FBMUIsRUFBbUMsWUFBTTtBQUNyQyxxQkFBUyxPQUFULENBQWlCLEtBQWpCO0FBQ0EscUJBQVMsT0FBVCxDQUFpQixZQUFqQjtBQUNILFNBSEQ7OztBQU1BLGlCQUFTLGdCQUFULENBQTBCLFNBQTFCLEVBQXFDLFVBQUMsS0FBRCxFQUFXO0FBQzVDLGdCQUFJLE1BQU0sS0FBTixJQUFlLEVBQW5CLEVBQXVCO0FBQ25CLHlCQUFTLE9BQVQsQ0FBaUIsS0FBakI7QUFDQSx5QkFBUyxPQUFULENBQWlCLFlBQWpCO0FBQ0g7QUFDSixTQUxEO0FBTUgsS0F2RFk7OztBQXlEYixhQUFTO0FBQ0wsWUFESyxrQkFDRTtBQUNILGdCQUFJLGFBQWMsU0FBUyxLQUFULENBQWUsWUFBZixHQUE4QixDQUEvQixHQUFvQyxDQUFyRDtBQUNBLHFCQUFTLEtBQVQsQ0FBZSxPQUFmLENBQXVCLFNBQXZCLENBQWlDLEdBQWpDLENBQXFDLGdCQUFyQyxFQUF1RCxhQUF2RCxzQkFBd0YsVUFBeEY7QUFDQSxxQkFBUyxNQUFULENBQWdCLEtBQWhCLENBQXNCLFFBQXRCLENBQStCLE9BQS9CLENBQXVDLFVBQUMsT0FBRCxFQUFVLEtBQVYsRUFBb0I7QUFDdkQsb0JBQUksU0FBUyxTQUFTLEtBQVQsQ0FBZSxZQUE1QixFQUEwQztBQUN0Qyw0QkFBUSxTQUFSLENBQWtCLEdBQWxCLENBQXNCLGtCQUF0QjtBQUNIO0FBQ0osYUFKRDtBQUtILFNBVEk7QUFXTCxhQVhLLG1CQVdHO0FBQ0oscUJBQVMsTUFBVCxDQUFnQixLQUFoQixDQUFzQixRQUF0QixDQUErQixPQUEvQixDQUF1QyxVQUFDLE9BQUQsRUFBVSxLQUFWLEVBQW9CO0FBQ3ZELHdCQUFRLFNBQVIsQ0FBa0IsTUFBbEIsQ0FBeUIsa0JBQXpCLEVBQTRDLGdCQUE1QyxFQUE4RCxrQkFBOUQsRUFBa0Ysa0JBQWxGLEVBQXNHLGtCQUF0RyxFQUEwSCxrQkFBMUg7QUFDQSxvQkFBSSxTQUFTLFNBQVMsS0FBVCxDQUFlLFlBQTVCLEVBQTBDO0FBQ3RDLDRCQUFRLFNBQVIsQ0FBa0IsTUFBbEIsQ0FBeUIsYUFBekI7QUFDSDtBQUNKLGFBTEQ7QUFNQSx1QkFBVyxZQUFNO0FBQ2IseUJBQVMsS0FBVCxDQUFlLE9BQWYsQ0FBdUIsU0FBdkIsQ0FBaUMsTUFBakMsQ0FBd0MsYUFBeEM7QUFDSCxhQUZELEVBRUcsR0FGSDtBQUdILFNBckJJO0FBdUJMLG9CQXZCSywwQkF1QlU7QUFDWCxnQkFBSSxNQUFNLEtBQUssS0FBTCxDQUFXLENBQUMsU0FBUyxLQUFULENBQWUsWUFBZixHQUE4QixDQUEvQixJQUFvQyxDQUEvQyxDQUFWO0FBQ0EsZ0JBQUksZ0JBQWdCLFNBQVMsTUFBVCxDQUFnQixLQUFoQixDQUFzQixPQUF0QixDQUE4QixZQUFsRDtBQUNBLGdCQUFJLDBCQUEwQixTQUFTLE1BQVQsQ0FBZ0IsS0FBaEIsQ0FBc0IsaUJBQXRCLENBQXdDLFlBQXRFO0FBQ0EsZ0JBQUksWUFBWSxTQUFTLEtBQVQsQ0FBZSxPQUFmLENBQXVCLFVBQXZCLENBQWtDLENBQWxDLEVBQXFDLFVBQXJDLENBQWdELENBQWhELEVBQW1ELFlBQW5ELEdBQWtFLHVCQUFsRjtBQUNBLGdCQUFJLGNBQWMsWUFBWSwwQkFBMEIsSUFBeEQ7O0FBRUEsZ0JBQUksU0FBUyxLQUFULENBQWUsT0FBZixDQUF1QixTQUF2QixDQUFpQyxRQUFqQyxDQUEwQyxnQkFBMUMsQ0FBSixFQUFpRTtBQUM3RCxvQkFBSSxZQUFZLENBQWhCLEVBQW1CO0FBQ2YsNkJBQVMsTUFBVCxDQUFnQixLQUFoQixDQUFzQixpQkFBdEIsQ0FBd0MsS0FBeEMsQ0FBOEMsTUFBOUMsR0FBdUQsY0FBYyxJQUFyRTtBQUNIO0FBQ0osYUFKRCxNQUtLO0FBQ0QseUJBQVMsTUFBVCxDQUFnQixLQUFoQixDQUFzQixpQkFBdEIsQ0FBd0MsS0FBeEMsQ0FBOEMsTUFBOUMsR0FBdUQsU0FBUyxNQUFULENBQWdCLHVCQUFoQixHQUEwQyxJQUFqRztBQUNIO0FBQ0o7QUF0Q0k7QUF6REksQ0FBakI7O2tCQW1HZSxROzs7Ozs7Ozs7QUNyR2Y7Ozs7QUFFQSxJQUFNLFVBQVU7QUFDWixZQUFRO0FBQ0osZ0JBQVEsU0FESjtBQUVKLGVBQU8sQ0FBQyxRQUFELEVBQVcsTUFBWCxFQUFtQixRQUFuQixFQUE2QixhQUE3QixDQUZIO0FBR0osZUFBTztBQUNILHdCQUFZLDBCQUFZLFNBQVMsZ0JBQVQsY0FBWixDQURUO0FBRUgscUJBQVMsMEJBQVksU0FBUyxnQkFBVCxDQUEwQiwwQkFBMUIsQ0FBWixDQUZOO0FBR0gsa0JBQU0sMEJBQVksU0FBUyxnQkFBVCxTQUFaO0FBSEgsU0FISDtBQVFKLGVBQU87QUFDSCwyQkFBZTtBQURaO0FBUkgsS0FESTs7QUFjWixRQWRZLGtCQWNMO0FBQ0gsZ0JBQVEsVUFBUjtBQUNBLGlCQUFTLGFBQVQsQ0FBdUIsbUJBQXZCLEVBQTRDLEtBQTVDO0FBQ0gsS0FqQlc7QUFtQlosY0FuQlksd0JBbUJDOztBQUVULGdCQUFRLE1BQVIsQ0FBZSxLQUFmLENBQXFCLFVBQXJCLENBQWdDLE9BQWhDLENBQXdDLHFCQUFhO0FBQ2pELHNCQUFVLGdCQUFWLENBQTJCLE9BQTNCLEVBQW9DLFVBQVUsS0FBVixFQUFpQjtBQUNqRCxzQkFBTSxlQUFOO0FBQ0Esd0JBQVEsT0FBUixDQUFnQixNQUFoQixDQUF1QixJQUF2QixDQUE0QixNQUFNLGFBQWxDO0FBQ0gsYUFIRDtBQUlILFNBTEQ7OztBQVFBLGlCQUFTLGdCQUFULENBQTBCLFFBQTFCLEVBQW9DLFlBQU07QUFDdEMsb0JBQVEsT0FBUixDQUFnQixLQUFoQjtBQUNILFNBRkQ7QUFHSCxLQWhDVzs7O0FBa0NaLGFBQVM7QUFDTCxnQkFBUTtBQUNKLGdCQURJLGdCQUNDLFNBREQsRUFDWSxPQURaLEVBQ3FCO0FBQ3JCLG9CQUFJLENBQUMsT0FBTCxFQUFjO0FBQ1Ysa0NBQWMsUUFBUSxNQUFSLENBQWUsS0FBZixDQUFxQixhQUFuQztBQUNIO0FBQ0Qsd0JBQVEsT0FBUixDQUFnQixNQUFoQixDQUF1QixRQUF2QixDQUFnQyxTQUFoQztBQUNBLHdCQUFRLE9BQVIsQ0FBZ0IsTUFBaEIsQ0FBdUIsU0FBdkI7QUFDQSx3QkFBUSxPQUFSLENBQWdCLE1BQWhCLENBQXVCLFlBQXZCO0FBQ0Esd0JBQVEsT0FBUixDQUFnQixNQUFoQixDQUF1QixPQUF2QjtBQUNBLHdCQUFRLE9BQVIsQ0FBZ0IsTUFBaEIsQ0FBdUIsU0FBdkI7QUFDQSx3QkFBUSxPQUFSLENBQWdCLE1BQWhCLENBQXVCLElBQXZCO0FBQ0Esd0JBQVEsT0FBUixDQUFnQixNQUFoQixDQUF1QixJQUF2QjtBQUNILGFBWkc7QUFjSixvQkFkSSxvQkFjSyxTQWRMLEVBY2dCO0FBQ2hCLG9CQUFJLE9BQU8sVUFBVSxTQUFWLENBQW9CLENBQXBCLEVBQXVCLEtBQXZCLENBQTZCLEdBQTdCLEVBQWtDLENBQWxDLENBQVg7QUFDQSxvQkFBSSxZQUFZLFNBQVMsYUFBVCxpQkFBcUMsSUFBckMsQ0FBaEI7QUFDQSx3QkFBUSxNQUFSLENBQWUsTUFBZixHQUF3QixFQUFFLG9CQUFGLEVBQWEsVUFBYixFQUF4QjtBQUNILGFBbEJHO0FBb0JKLHFCQXBCSSx1QkFvQlE7QUFDUixvQkFBSSwwQ0FBaUIsUUFBUSxNQUFSLENBQWUsS0FBaEMsRUFBSjtBQUNBLG9CQUFJLFFBQVEsV0FBVyxPQUFYLENBQW1CLFFBQVEsTUFBUixDQUFlLE1BQWYsQ0FBc0IsSUFBekMsSUFBaUQsQ0FBN0Q7QUFDQSxvQkFBSSxTQUFTLFdBQVcsTUFBWCxHQUFvQixLQUFqQztBQUNBLG9CQUFJLFVBQVUsNkJBQUksVUFBSixHQUFnQixNQUFoQixDQUF1QixDQUF2QixFQUEwQixLQUExQixDQUFkO0FBQ0Esb0JBQUksWUFBWSw2QkFBSSxVQUFKLEdBQWdCLE1BQWhCLENBQXVCLENBQUMsTUFBeEIsRUFBZ0MsTUFBaEMsQ0FBaEI7QUFDQSwwQkFBVSxPQUFWLENBQWtCLHFCQUFhO0FBQzNCLDZCQUFTLGFBQVQsaUJBQXFDLFNBQXJDLEVBQWtELFlBQWxELENBQStELE9BQS9ELDJCQUErRixTQUEvRjtBQUNILGlCQUZEO0FBR0Esd0JBQVEsT0FBUixDQUFnQixxQkFBYTtBQUN6Qiw2QkFBUyxhQUFULGlCQUFxQyxTQUFyQyxFQUFrRCxZQUFsRCxDQUErRCxPQUEvRCwyQkFBK0YsU0FBL0YsZUFBa0gsU0FBbEg7QUFDSCxpQkFGRDtBQUdBLDJCQUFXLE9BQVgsQ0FBbUIscUJBQWE7QUFDNUIsNkJBQVMsYUFBVCxpQkFBcUMsU0FBckMsRUFBa0QsU0FBbEQsQ0FBNEQsTUFBNUQsWUFBNEUsU0FBNUU7QUFDSCxpQkFGRDtBQUdBLHdCQUFRLE1BQVIsQ0FBZSxNQUFmLENBQXNCLFNBQXRCLENBQWdDLFNBQWhDLENBQTBDLEdBQTFDLFlBQXVELFFBQVEsTUFBUixDQUFlLE1BQWYsQ0FBc0IsSUFBN0U7QUFDSCxhQXBDRztBQXNDSix3QkF0Q0ksMEJBc0NXO0FBQ1gsb0JBQUksT0FBTywwQkFBWSxTQUFTLGdCQUFULENBQTBCLE1BQTFCLENBQVosQ0FBWDtBQUNBLHFCQUFLLE9BQUwsQ0FBYSxlQUFPO0FBQ2hCLHdCQUFJLFVBQVUsSUFBSSxTQUFKLENBQWMsQ0FBZCxFQUFpQixLQUFqQixDQUF1QixHQUF2QixFQUE0QixDQUE1QixDQUFkO0FBQ0Esd0JBQUksV0FBVyxRQUFRLE1BQVIsQ0FBZSxNQUFmLENBQXNCLElBQXJDLEVBQTJDO0FBQ3ZDLDRCQUFJLFlBQUosQ0FBaUIsT0FBakIsZUFBcUMsT0FBckM7QUFDSCxxQkFGRCxNQUdLO0FBQ0QsNEJBQUksWUFBSixDQUFpQixPQUFqQixlQUFxQyxPQUFyQztBQUNIO0FBQ0osaUJBUkQ7QUFTSCxhQWpERztBQW1ESixtQkFuREkscUJBbURNO0FBQ04sd0JBQVEsTUFBUixDQUFlLEtBQWYsQ0FBcUIsT0FBckIsQ0FBNkIsT0FBN0IsQ0FBcUMsa0JBQVU7QUFDM0Msd0JBQUksZUFBZSxPQUFPLFNBQVAsQ0FBaUIsQ0FBakIsQ0FBbkI7QUFDQSwyQkFBTyxZQUFQLENBQW9CLE9BQXBCLGNBQXVDLFlBQXZDO0FBQ0EsMkJBQU8sU0FBUCxDQUFpQixHQUFqQixhQUErQixRQUFRLE1BQVIsQ0FBZSxNQUFmLENBQXNCLElBQXJEO0FBQ0gsaUJBSkQ7QUFLSCxhQXpERztBQTJESixxQkEzREksdUJBMkRRO0FBQ1Isd0JBQVEsTUFBUixDQUFlLEtBQWYsQ0FBcUIsVUFBckIsQ0FBZ0MsT0FBaEMsQ0FBd0MscUJBQWE7QUFDakQsOEJBQVUsU0FBVixDQUFvQixNQUFwQixDQUEyQixpQkFBM0I7QUFDSCxpQkFGRDtBQUdBLHlCQUFTLGFBQVQsaUJBQXFDLFFBQVEsTUFBUixDQUFlLE1BQWYsQ0FBc0IsSUFBM0QsRUFBbUUsU0FBbkUsQ0FBNkUsR0FBN0UsQ0FBaUYsaUJBQWpGO0FBQ0gsYUFoRUc7QUFrRUosZ0JBbEVJLGtCQWtFRztBQUNILG9CQUFJLFNBQVMsU0FBUyxhQUFULENBQXVCLGNBQXZCLENBQWI7QUFDQSxvQkFBSSxlQUFlLE9BQU8sU0FBUCxDQUFpQixDQUFqQixDQUFuQjtBQUNBLHVCQUFPLFlBQVAsQ0FBb0IsT0FBcEIsRUFBNkIsWUFBN0I7QUFDQSx1QkFBTyxTQUFQLENBQWlCLEdBQWpCLFdBQTZCLFFBQVEsTUFBUixDQUFlLE1BQWYsQ0FBc0IsSUFBbkQ7QUFDSCxhQXZFRztBQXlFSixnQkF6RUksa0JBeUVHO0FBQ0gsd0JBQVEsTUFBUixDQUFlLEtBQWYsQ0FBcUIsSUFBckIsQ0FBMEIsT0FBMUIsQ0FBa0MsZ0JBQVE7QUFDdEMsd0JBQUksZUFBZSxLQUFLLFNBQUwsQ0FBZSxDQUFmLENBQW5CO0FBQ0EseUJBQUssWUFBTCxDQUFrQixPQUFsQixZQUFtQyxZQUFuQztBQUNILGlCQUhEOztBQUtBLG9CQUFJLFlBQVksMEJBQVksU0FBUyxnQkFBVCxZQUFtQyxRQUFRLE1BQVIsQ0FBZSxNQUFmLENBQXNCLElBQXpELENBQVosQ0FBaEI7QUFDQSwwQkFBVSxPQUFWLENBQWtCLG9CQUFZO0FBQzFCLDZCQUFTLFNBQVQsQ0FBbUIsR0FBbkIsWUFBZ0MsUUFBUSxNQUFSLENBQWUsTUFBZixDQUFzQixJQUF0RDtBQUNILGlCQUZEO0FBR0g7QUFuRkcsU0FESDs7QUF1RkwsYUF2RkssbUJBdUZHO0FBQ0osZ0JBQUksWUFBWSxTQUFTLGVBQVQsQ0FBeUIsU0FBekIsR0FBcUMsU0FBUyxlQUFULENBQXlCLFNBQTlELEdBQTBFLFNBQVMsSUFBVCxDQUFjLFNBQXhHO0FBQ0EsZ0JBQUksVUFBVSxTQUFTLGFBQVQsQ0FBdUIsZ0NBQXZCLEVBQXlELFlBQXpELEdBQXdFLFNBQVMsYUFBVCxDQUF1QixpQ0FBdkIsRUFBMEQsWUFBbEksR0FBaUosU0FBUyxhQUFULENBQXVCLGtDQUF2QixFQUEyRCxZQUExTjtBQUNBLGdCQUFJLGFBQWEsT0FBYixJQUF3QixRQUFRLE1BQVIsQ0FBZSxLQUFmLENBQXFCLGFBQXJCLElBQXNDLFNBQWxFLEVBQTZFO0FBQ3pFLHdCQUFRLE1BQVIsQ0FBZSxLQUFmLENBQXFCLGFBQXJCLEdBQXFDLFlBQVksWUFBTTtBQUNuRCx3QkFBSSxVQUFVLFFBQVEsTUFBUixDQUFlLEtBQWYsQ0FBcUIsT0FBckIsQ0FBNkIsUUFBUSxNQUFSLENBQWUsTUFBZixDQUFzQixJQUFuRCxDQUFkO0FBQ0Esd0JBQUksT0FBTyxVQUFVLENBQVYsR0FBYyxRQUFRLE1BQVIsQ0FBZSxLQUFmLENBQXFCLFVBQVUsQ0FBL0IsQ0FBZCxHQUFrRCxRQUE3RDtBQUNBLHdCQUFJLFlBQVksU0FBUyxhQUFULGlCQUFxQyxJQUFyQyxDQUFoQjtBQUNBLDRCQUFRLE9BQVIsQ0FBZ0IsTUFBaEIsQ0FBdUIsSUFBdkIsQ0FBNEIsU0FBUyxhQUFULGlCQUFxQyxJQUFyQyxDQUE1QixFQUEwRSxDQUExRTtBQUNILGlCQUxvQyxFQUtsQyxJQUxrQyxDQUFyQztBQU1IO0FBQ0o7QUFsR0k7QUFsQ0csQ0FBaEI7O2tCQXdJZSxPOzs7Ozs7Ozs7QUMxSWY7O0FBRUEsSUFBTSxPQUFPO0FBQ1QsWUFBUTtBQUNKLGVBQU87QUFDSCxvQkFBUTtBQUNKLHlCQUFTLENBQ0wsZUFESyxFQUVMLHNCQUZLLEVBR0wsUUFISyxFQUlMLFNBSkssRUFLTCxnQkFMSyxFQU1MLGNBTkssRUFPTCxtQkFQSyxFQVFMLE1BUkssRUFTTCxRQVRLLEVBVUwsb0JBVkssRUFXTCxlQVhLLEVBWUwsV0FaSyxFQWFMLFNBYkssRUFjTCxnQkFkSyxFQWVMLFdBZkssRUFnQkwsUUFoQkssRUFpQkwsTUFqQkssRUFrQkwsS0FsQkssRUFtQkwsbUJBbkJLLEVBb0JMLGtCQXBCSyxFQXFCTCxTQXJCSyxDQURMO0FBd0JKLDJCQUFXLENBQ1AsZUFETyxFQUVQLHNCQUZPLEVBR1AsUUFITyxFQUlQLFNBSk8sRUFLUCxnQkFMTyxFQU1QLGNBTk8sRUFPUCxtQkFQTyxFQVFQLE1BUk8sRUFTUCxRQVRPLEVBVVAsb0JBVk8sRUFXUCxlQVhPLEVBWVAsV0FaTyxFQWFQLFNBYk8sRUFjUCxnQkFkTyxFQWVQLFdBZk8sRUFnQlAsUUFoQk8sRUFpQlAsTUFqQk8sRUFrQlAsS0FsQk8sRUFtQlAsbUJBbkJPLEVBb0JQLGtCQXBCTyxFQXFCUCxTQXJCTztBQXhCUCxhQURMO0FBaURILGtCQUFNO0FBQ0YseUJBQVMsQ0FBQyxlQUFELEVBQWtCLFNBQWxCLEVBQTZCLGdCQUE3QixFQUErQyxvQkFBL0MsRUFBcUUsc0JBQXJFLEVBQTZGLFFBQTdGLEVBQXVHLFFBQXZHLEVBQWlILGNBQWpILEVBQWlJLGdCQUFqSSxFQUFtSixrQkFBbkosRUFBdUssZUFBdkssRUFBd0wsbUJBQXhMLEVBQTZNLE1BQTdNLEVBQXFOLEtBQXJOLEVBQTROLFFBQTVOLEVBQXNPLFFBQXRPLEVBQWdQLGVBQWhQLEVBQWlRLE1BQWpRLEVBQXlRLG1CQUF6USxDQURQO0FBRUYsMkJBQVcsQ0FBQyxlQUFELEVBQWtCLFNBQWxCLEVBQTZCLGdCQUE3QixFQUErQyxvQkFBL0M7QUFGVCxhQWpESDtBQXFESCxvQkFBUTtBQUNKLHlCQUFTLENBQUMsUUFBRCxFQUFXLGdCQUFYLEVBQTZCLG9CQUE3QixFQUFtRCxlQUFuRCxFQUFvRSxzQkFBcEUsRUFBNEYsU0FBNUYsRUFBdUcsa0JBQXZHLENBREw7QUFFSiwyQkFBVyxDQUFDLFFBQUQ7QUFGUCxhQXJETDtBQXlESCxvQkFBUTtBQUNKLHlCQUFTLENBQUMsUUFBRCxFQUFXLGVBQVgsRUFBNEIsU0FBNUIsRUFBdUMsZ0JBQXZDLEVBQXlELG9CQUF6RCxFQUErRSxzQkFBL0UsRUFBdUcsTUFBdkcsRUFBK0csa0JBQS9HLENBREw7QUFFSiwyQkFBVyxDQUFDLFFBQUQ7QUFGUCxhQXpETDtBQTZESCw0QkFBZ0I7QUFDWix5QkFBUyxDQUFDLFNBQUQsRUFBWSxvQkFBWixFQUFrQyxnQkFBbEMsRUFBb0QsU0FBcEQsRUFBK0QsZUFBL0QsRUFBZ0Ysc0JBQWhGLEVBQXdHLGtCQUF4RyxDQURHO0FBRVosMkJBQVcsQ0FBQyxTQUFEO0FBRkMsYUE3RGI7QUFpRUgsOEJBQWtCO0FBQ2QseUJBQVMsQ0FBQyxRQUFELEVBQVcsZ0JBQVgsRUFBNkIsb0JBQTdCLEVBQW1ELFNBQW5ELEVBQThELGVBQTlELEVBQStFLHNCQUEvRSxFQUF1RyxrQkFBdkcsQ0FESztBQUVkLDJCQUFXLENBQUMsUUFBRDtBQUZHLGFBakVmO0FBcUVILCtCQUFtQjtBQUNmLHlCQUFTLENBQUMsU0FBRCxFQUFZLGdCQUFaLEVBQThCLG9CQUE5QixFQUFvRCxTQUFwRCxFQUErRCxlQUEvRCxFQUFnRixzQkFBaEYsRUFBd0csa0JBQXhHLENBRE07QUFFZiwyQkFBVyxDQUFDLFNBQUQ7QUFGSSxhQXJFaEI7QUF5RUgsb0JBQVE7QUFDSix5QkFBUyxDQUFDLGtCQUFELEVBQXFCLFNBQXJCLEVBQWdDLGdCQUFoQyxFQUFrRCxvQkFBbEQsRUFBd0UsZUFBeEUsRUFBeUYsZ0JBQXpGLENBREw7QUFFSiwyQkFBVztBQUZQLGFBekVMO0FBNkVILG9CQUFRO0FBQ0oseUJBQVMsQ0FBQyxRQUFELEVBQVcsa0JBQVgsRUFBK0IsZUFBL0IsQ0FETDtBQUVKLDJCQUFXLENBQUMsUUFBRDtBQUZQLGFBN0VMO0FBaUZILDhCQUFrQjtBQUNkLHlCQUFTLENBQUMsU0FBRCxFQUFZLGtCQUFaLENBREs7QUFFZCwyQkFBVyxDQUFDLFNBQUQ7QUFGRyxhQWpGZjtBQXFGSCw2QkFBaUI7QUFDYix5QkFBUyxDQUFDLE9BQUQsQ0FESTtBQUViLDJCQUFXLENBQUMsT0FBRDtBQUZFLGFBckZkO0FBeUZILGlCQUFLO0FBQ0QseUJBQVMsQ0FBQyxLQUFELEVBQVEsZUFBUixFQUF5QixrQkFBekIsQ0FEUjtBQUVELDJCQUFXLENBQUMsS0FBRDtBQUZWO0FBekZGLFNBREg7QUErRkosaUJBQVM7QUFDTCw2QkFBaUI7QUFDYix1QkFBTyxDQUFDLGVBQUQsQ0FETTtBQUViLDJCQUFXLENBQUMsZUFBRCxDQUZFO0FBR2Isc0JBQU07QUFDRiw0QkFBUSxrQ0FETjtBQUVGLDJCQUFPLENBQUMsZ0NBQUQ7QUFGTDtBQUhPLGFBRFo7QUFTTCxvQ0FBd0I7QUFDcEIsdUJBQU8sQ0FBQyxzQkFBRCxDQURhO0FBRXBCLDJCQUFXLENBQUMsc0JBQUQsQ0FGUztBQUdwQixzQkFBTTtBQUNGLDRCQUFRLHlDQUROO0FBRUYsMkJBQU8sQ0FBQyx1Q0FBRDtBQUZMO0FBSGMsYUFUbkI7QUFpQkwsb0JBQVE7QUFDSix1QkFBTyxDQUFDLFFBQUQsRUFBVyxXQUFYLEVBQXdCLEtBQXhCLEVBQStCLGdCQUEvQixFQUFpRCxNQUFqRCxFQUF5RCxNQUF6RCxDQURIO0FBRUosMkJBQVcsQ0FBQyxRQUFELEVBQVcsV0FBWCxFQUF3QixLQUF4QixDQUZQO0FBR0osc0JBQU07QUFDRiw0QkFBUSwyQkFETjtBQUVGLDJCQUFPLENBQ0gseUJBREcsRUFFSCw0QkFGRyxFQUdILHNCQUhHLEVBSUgsaUNBSkcsRUFLSCxpREFMRyxFQU1ILHVCQU5HO0FBRkw7QUFIRixhQWpCSDtBQWdDTCxxQkFBUztBQUNMLHVCQUFPLENBQUMsTUFBRCxFQUFTLGFBQVQsRUFBd0IsTUFBeEIsRUFBZ0MsS0FBaEMsRUFBdUMsVUFBdkMsRUFBbUQsY0FBbkQsRUFBbUUsT0FBbkUsQ0FERjtBQUVMLDJCQUFXLENBQUMsTUFBRCxFQUFTLGFBQVQsRUFBd0IsTUFBeEIsRUFBZ0MsS0FBaEMsRUFBdUMsVUFBdkMsQ0FGTjtBQUdMLHNCQUFNO0FBQ0YsNEJBQVEsNEJBRE47QUFFRiwyQkFBTyxDQUNILDJKQURHLEVBRUgsa0tBRkcsRUFHSCwySkFIRyxFQUlILDBKQUpHLEVBS0gsK0pBTEcsRUFNSCxtS0FORyxFQU9ILDRKQVBHO0FBRkw7QUFIRCxhQWhDSjtBQWdETCw4QkFBa0I7QUFDZCx1QkFBTyxDQUFDLFVBQUQsRUFBYSxXQUFiLEVBQTBCLFdBQTFCLENBRE87QUFFZCwyQkFBVyxDQUFDLFVBQUQsRUFBYSxXQUFiLEVBQTBCLFdBQTFCLENBRkc7QUFHZCxzQkFBTTtBQUNGLDRCQUFRLG1DQUROO0FBRUYsMkJBQU8sQ0FBQywyQkFBRCxFQUE4Qiw0QkFBOUIsRUFBNEQsNEJBQTVEO0FBRkw7QUFIUSxhQWhEYjtBQXdETCw0QkFBZ0I7QUFDWix1QkFBTyxDQUFDLE1BQUQsRUFBUyxLQUFULEVBQWdCLE9BQWhCLEVBQXlCLFVBQXpCLENBREs7QUFFWiwyQkFBVyxDQUFDLE1BQUQsRUFBUyxLQUFULEVBQWdCLE9BQWhCLEVBQXlCLFVBQXpCLENBRkM7QUFHWixzQkFBTTtBQUNGLDRCQUFRLGlDQUROO0FBRUYsMkJBQU8sQ0FDSCx1QkFERyxFQUVILHNCQUZHLEVBR0gsd0JBSEcsRUFJSCwyQkFKRztBQUZMO0FBSE0sYUF4RFg7QUFxRUwsaUNBQXFCO0FBQ2pCLHVCQUFPLENBQUMsT0FBRCxDQURVO0FBRWpCLDJCQUFXLENBQUMsT0FBRCxDQUZNO0FBR2pCLHNCQUFNO0FBQ0YsNEJBQVEsc0NBRE47QUFFRiwyQkFBTyxDQUFDLHdCQUFEO0FBRkw7QUFIVyxhQXJFaEI7QUE2RUwsa0JBQU07QUFDRix1QkFBTyxDQUFDLE1BQUQsQ0FETDtBQUVGLDJCQUFXLENBQUMsTUFBRCxDQUZUO0FBR0Ysc0JBQU07QUFDRiw0QkFBUSx5QkFETjtBQUVGLDJCQUFPLENBQUMsdUJBQUQ7QUFGTDtBQUhKLGFBN0VEO0FBcUZMLG9CQUFRO0FBQ0osdUJBQU8sQ0FBQyxPQUFELENBREg7QUFFSiwyQkFBVyxDQUFDLE9BQUQsQ0FGUDtBQUdKLHNCQUFNO0FBQ0YsNEJBQVEsMkJBRE47QUFFRiwyQkFBTyxDQUFDLHdCQUFEO0FBRkw7QUFIRixhQXJGSDtBQTZGTCxrQ0FBc0I7QUFDbEIsdUJBQU8sQ0FBQyxpQkFBRCxFQUFvQixpQkFBcEIsRUFBdUMsZ0JBQXZDLENBRFc7QUFFbEIsMkJBQVcsQ0FBQyxpQkFBRCxFQUFvQixpQkFBcEIsRUFBdUMsZ0JBQXZDLENBRk87QUFHbEIsc0JBQU07QUFDRiw0QkFBUSx1Q0FETjtBQUVGLDJCQUFPLENBQ0gsa0NBREcsRUFFSCxrQ0FGRyxFQUdILGlDQUhHO0FBRkw7QUFIWSxhQTdGakI7QUF5R0wsNkJBQWlCO0FBQ2IsdUJBQU8sQ0FBQyxLQUFELENBRE07QUFFYiwyQkFBVyxDQUFDLEtBQUQsQ0FGRTtBQUdiLHNCQUFNO0FBQ0YsNEJBQVEsa0NBRE47QUFFRiwyQkFBTyxDQUFDLHNCQUFEO0FBRkw7QUFITyxhQXpHWjtBQWlITCx5QkFBYTtBQUNULHVCQUFPLENBQUMsS0FBRCxDQURFO0FBRVQsMkJBQVcsQ0FBQyxLQUFELENBRkY7QUFHVCxzQkFBTTtBQUNGLDRCQUFRLDhCQUROO0FBRUYsMkJBQU8sQ0FBQyxzQkFBRDtBQUZMO0FBSEcsYUFqSFI7QUF5SEwscUJBQVM7QUFDTCx1QkFBTyxDQUFDLFNBQUQsRUFBWSxRQUFaLEVBQXNCLE1BQXRCLEVBQThCLFNBQTlCLENBREY7QUFFTCwyQkFBVyxDQUFDLFNBQUQsQ0FGTjtBQUdMLHNCQUFNO0FBQ0YsNEJBQVEsNEJBRE47QUFFRiwyQkFBTyxDQUNILDBCQURHLEVBRUgseUJBRkcsRUFHSCx1QkFIRyxFQUlILDBCQUpHO0FBRkw7QUFIRCxhQXpISjtBQXNJTCw4QkFBa0I7QUFDZCx1QkFBTyxDQUFDLFNBQUQsRUFBWSxNQUFaLEVBQW9CLE1BQXBCLENBRE87QUFFZCwyQkFBVyxDQUFDLFNBQUQsQ0FGRztBQUdkLHNCQUFNO0FBQ0YsNEJBQVEsbUNBRE47QUFFRiwyQkFBTyxDQUNILDBCQURHLEVBRUgsdUJBRkcsRUFHSCx1QkFIRztBQUZMO0FBSFEsYUF0SWI7QUFrSkwsdUJBQVc7QUFDUCx1QkFBTyxDQUFDLGtCQUFELEVBQXFCLG1CQUFyQixDQURBO0FBRVAsMkJBQVcsQ0FBQyxrQkFBRCxFQUFxQixtQkFBckIsQ0FGSjtBQUdQLHNCQUFNO0FBQ0YsNEJBQVEsOEJBRE47QUFFRiwyQkFBTyxDQUNILG1DQURHLEVBRUgsb0NBRkc7QUFGTDtBQUhDLGFBbEpOO0FBNkpMLG9CQUFRO0FBQ0osdUJBQU8sQ0FBQyxlQUFELEVBQWtCLGNBQWxCLENBREg7QUFFSiwyQkFBVyxDQUFDLGVBQUQsRUFBa0IsY0FBbEIsQ0FGUDtBQUdKLHNCQUFNO0FBQ0YsNEJBQVEsMkJBRE47QUFFRiwyQkFBTyxDQUNILGdDQURHLEVBRUgsK0JBRkc7QUFGTDtBQUhGLGFBN0pIO0FBd0tMLGtCQUFNO0FBQ0YsdUJBQU8sQ0FBQyxLQUFELENBREw7QUFFRiwyQkFBVyxDQUFDLEtBQUQsQ0FGVDtBQUdGLHNCQUFNO0FBQ0YsNEJBQVEseUJBRE47QUFFRiwyQkFBTyxDQUFDLHNCQUFEO0FBRkw7QUFISixhQXhLRDtBQWdMTCxpQkFBSztBQUNELHVCQUFPLENBQUMsTUFBRCxFQUFTLE1BQVQsQ0FETjtBQUVELDJCQUFXLENBQUMsTUFBRCxFQUFTLE1BQVQsQ0FGVjtBQUdELHNCQUFNO0FBQ0YsNEJBQVEsd0JBRE47QUFFRiwyQkFBTyxDQUNILHNCQURHLEVBRUgsdUJBRkc7QUFGTDtBQUhMLGFBaExBO0FBMkxMLGlDQUFxQjtBQUNqQix1QkFBTyxDQUFDLFdBQUQsQ0FEVTtBQUVqQiwyQkFBVyxDQUFDLFdBQUQsQ0FGTTtBQUdqQixzQkFBTTtBQUNGLDRCQUFRLHNDQUROO0FBRUYsMkJBQU8sQ0FBQyw0QkFBRDtBQUZMO0FBSFcsYUEzTGhCO0FBbU1MLGdDQUFvQjtBQUNoQix1QkFBTyxDQUFDLEtBQUQsQ0FEUztBQUVoQiwyQkFBVyxDQUFDLEtBQUQsQ0FGSztBQUdoQixzQkFBTTtBQUNGLDRCQUFRLHFDQUROO0FBRUYsMkJBQU8sQ0FBQyxzQkFBRDtBQUZMO0FBSFUsYUFuTWY7QUEyTUwscUJBQVM7QUFDTCx1QkFBTyxDQUFDLEtBQUQsQ0FERjtBQUVMLDJCQUFXLENBQUMsS0FBRCxDQUZOO0FBR0wsc0JBQU07QUFDRiw0QkFBUSw0QkFETjtBQUVGLDJCQUFPLENBQUMsc0JBQUQ7QUFGTDtBQUhEO0FBM01KLFNBL0ZMO0FBbVRKLGVBQU87QUFDSCx3QkFBWSxTQURUO0FBRUgsbUJBQU8sQ0FGSjtBQUdILGtCQUFNLFNBSEg7QUFJSCxrQkFBTTtBQUpILFNBblRIO0FBeVRKLGVBQU87QUFDSCxzQkFBVSxTQUFTLGFBQVQsQ0FBdUIsY0FBdkIsQ0FEUDtBQUVILDBCQUFjLFNBQVMsYUFBVCxDQUF1QixZQUF2QixDQUZYO0FBR0gsK0JBQW1CLFNBSGhCO0FBSUgsa0JBQU0sU0FBUyxhQUFULENBQXVCLG9CQUF2QixDQUpIO0FBS0gsMEJBQWMsU0FBUyxhQUFULENBQXVCLGtCQUF2QixDQUxYO0FBTUgsa0JBQU0sU0FOSDtBQU9ILHlCQUFhLFNBUFY7QUFRSCwyQkFBZSxTQUFTLGFBQVQsQ0FBdUIsaUJBQXZCO0FBUlosU0F6VEg7QUFtVUosZUFBTyxDQUFDLFFBQUQsRUFBVyxNQUFYLEVBQW1CLFFBQW5CLEVBQTZCLGFBQTdCO0FBblVILEtBREM7O0FBdVVULGNBQVU7QUFFTixZQUZNLGtCQUVDO0FBQ0gsaUJBQUssUUFBTCxDQUFjLFFBQWQ7QUFDQSxpQkFBSyxNQUFMLENBQVksS0FBWixDQUFrQixpQkFBbEIsR0FBc0MsMEJBQVksU0FBUyxnQkFBVCxDQUEwQixlQUExQixDQUFaLENBQXRDO0FBQ0EsaUJBQUssUUFBTCxDQUFjLFVBQWQ7O0FBRUEscUJBQVMsYUFBVCxDQUF1Qiw0QkFBdkIsRUFBcUQsS0FBckQ7QUFDSCxTQVJLO0FBVU4sa0JBVk0sd0JBVU87O0FBRVQscUJBQVMsZ0JBQVQsQ0FBMEIsT0FBMUIsRUFBbUMsWUFBTTtBQUNyQyxxQkFBSyxRQUFMLENBQWMsUUFBZDtBQUNILGFBRkQ7OztBQUtBLHFCQUFTLGdCQUFULENBQTBCLFNBQTFCLEVBQXFDLFVBQUMsS0FBRCxFQUFXO0FBQzVDLG9CQUFJLE1BQU0sS0FBTixJQUFlLEVBQW5CLEVBQXVCO0FBQ25CLHlCQUFLLFFBQUwsQ0FBYyxRQUFkO0FBQ0g7QUFDSixhQUpEOzs7QUFPQSxpQkFBSyxNQUFMLENBQVksS0FBWixDQUFrQixRQUFsQixDQUEyQixnQkFBM0IsQ0FBNEMsT0FBNUMsRUFBcUQsVUFBQyxLQUFELEVBQVc7QUFDNUQsc0JBQU0sZUFBTjtBQUNBLHFCQUFLLFFBQUwsQ0FBYyxNQUFkO0FBQ0gsYUFIRDs7O0FBTUEsaUJBQUssTUFBTCxDQUFZLEtBQVosQ0FBa0IsaUJBQWxCLENBQW9DLE9BQXBDLENBQTRDLGdCQUFRO0FBQ2hELHFCQUFLLGdCQUFMLENBQXNCLE9BQXRCLEVBQStCLFVBQUMsS0FBRCxFQUFXO0FBQ3RDLDBCQUFNLGVBQU47QUFDQSx5QkFBSyxRQUFMLENBQWMsTUFBZCxDQUFxQixNQUFNLGFBQTNCO0FBQ0EseUJBQUssUUFBTCxDQUFjLFFBQWQ7QUFDQSx5QkFBSyxRQUFMLENBQWMsTUFBZDtBQUNBLHlCQUFLLEdBQUwsQ0FBUyxNQUFUO0FBRUgsaUJBUEQ7QUFRSCxhQVREOzs7QUFZQSxpQkFBSyxNQUFMLENBQVksS0FBWixDQUFrQixZQUFsQixDQUErQixnQkFBL0IsQ0FBZ0QsT0FBaEQsRUFBeUQsVUFBQyxLQUFELEVBQVc7QUFDaEUsc0JBQU0sZUFBTjtBQUNBLHFCQUFLLEdBQUwsQ0FBUyxPQUFULENBQWlCLFVBQWpCO0FBQ0gsYUFIRDtBQUlILFNBOUNLO0FBZ0ROLGdCQWhETSxzQkFnREs7QUFDUCxtQkFBTyxJQUFQLENBQVksS0FBSyxNQUFMLENBQVksS0FBeEIsRUFBK0IsT0FBL0IsQ0FBdUMsZ0JBQVE7QUFDM0Msb0JBQUksT0FBTyxTQUFTLGFBQVQsQ0FBdUIsSUFBdkIsQ0FBWDtBQUNBLG9CQUFJLE9BQU8sU0FBUyxjQUFULENBQXdCLElBQXhCLENBQVg7QUFDQSxxQkFBSyxXQUFMLENBQWlCLElBQWpCO0FBQ0EscUJBQUssTUFBTCxDQUFZLEtBQVosQ0FBa0IsWUFBbEIsQ0FBK0IsV0FBL0IsQ0FBMkMsSUFBM0M7QUFDSCxhQUxEO0FBTUgsU0F2REs7QUF5RE4sZ0JBekRNLHNCQXlESztBQUNQLGlCQUFLLE1BQUwsQ0FBWSxLQUFaLENBQWtCLFFBQWxCLENBQTJCLFNBQTNCLENBQXFDLE1BQXJDLENBQTRDLGVBQTVDO0FBQ0gsU0EzREs7QUE2RE4sY0E3RE0sb0JBNkRHO0FBQ0wsaUJBQUssTUFBTCxDQUFZLEtBQVosQ0FBa0IsUUFBbEIsQ0FBMkIsU0FBM0IsQ0FBcUMsTUFBckMsQ0FBNEMsZUFBNUM7QUFDSCxTQS9ESztBQWlFTixjQWpFTSxrQkFpRUMsSUFqRUQsRUFpRU87QUFDVCxpQkFBSyxNQUFMLENBQVksS0FBWixDQUFrQixJQUFsQixDQUF1QixTQUF2QixHQUFtQyxLQUFLLE1BQUwsQ0FBWSxLQUFaLENBQWtCLElBQWxCLEdBQXlCLEtBQUssU0FBakU7QUFDSCxTQW5FSztBQXFFTixjQXJFTSxvQkFxRUc7QUFDTCxnQkFBSSxVQUFVLFNBQVMsU0FBUyxhQUFULENBQXVCLG1CQUF2QixFQUE0QyxZQUFyRCxDQUFkO0FBQ0EsZ0JBQUksZ0JBQWdCLFNBQVMsU0FBUyxhQUFULENBQXVCLHdCQUF2QixFQUFpRCxZQUExRCxJQUEwRSxTQUFTLE9BQU8sZ0JBQVAsQ0FBd0IsU0FBUyxhQUFULENBQXVCLHdCQUF2QixDQUF4QixFQUEwRSxZQUExRSxDQUFULENBQTFFLEdBQThLLFNBQVMsT0FBTyxnQkFBUCxDQUF3QixTQUFTLGFBQVQsQ0FBdUIsd0JBQXZCLENBQXhCLEVBQTBFLGVBQTFFLENBQVQsSUFBdUcsSUFBelM7QUFDQSxnQkFBSSxVQUFVLFNBQVMsU0FBUyxhQUFULENBQXVCLGtCQUF2QixFQUEyQyxZQUFwRCxDQUFkO0FBQ0EsZ0JBQUksU0FBUyxTQUFTLFNBQVMsYUFBVCxDQUF1QixpQkFBdkIsRUFBMEMsWUFBbkQsQ0FBYjtBQUNBLGdCQUFJLE1BQU0sU0FBUyxTQUFTLGFBQVQsQ0FBdUIsV0FBdkIsRUFBb0MsWUFBN0MsQ0FBVjtBQUNBLGdCQUFJLFdBQVcsU0FBUyxTQUFTLGFBQVQsQ0FBdUIsbUJBQXZCLEVBQTRDLFlBQXJELENBQWY7QUFDQSxnQkFBSSxTQUFTLFVBQVUsYUFBVixHQUEwQixNQUExQixHQUFtQyxHQUFuQyxHQUF5QyxPQUF6QyxHQUFtRCxRQUFoRTtBQUNBLGdCQUFJLEtBQUssTUFBTCxDQUFZLEtBQVosQ0FBa0IsSUFBbEIsSUFBMEIsU0FBOUIsRUFBeUM7QUFDckMsd0NBQVUsTUFBVixFQUFrQixHQUFsQixFQUF1QixnQkFBdkI7QUFDSDtBQUNELGlCQUFLLE1BQUwsQ0FBWSxLQUFaLENBQWtCLElBQWxCLEdBQXlCLENBQXpCO0FBQ0g7QUFqRkssS0F2VUQ7O0FBMlpULFNBQUs7QUFDRCxZQURDLGtCQUNNO0FBQ0gsaUJBQUssR0FBTCxDQUFTLE1BQVQsQ0FBZ0IsSUFBaEI7QUFDQSxpQkFBSyxNQUFMLENBQVksS0FBWixDQUFrQixJQUFsQixHQUF5QiwwQkFBWSxTQUFTLGdCQUFULENBQTBCLGNBQTFCLENBQVosQ0FBekI7QUFDQSxpQkFBSyxNQUFMLENBQVksS0FBWixDQUFrQixXQUFsQixHQUFnQyxLQUFLLE1BQUwsQ0FBWSxLQUFaLENBQWtCLElBQWxEO0FBQ0EsaUJBQUssR0FBTCxDQUFTLE9BQVQsQ0FBaUIsS0FBakI7QUFDQSxpQkFBSyxHQUFMLENBQVMsVUFBVDtBQUNILFNBUEE7QUFTRCxrQkFUQyx3QkFTWTs7O0FBR1QsaUJBQUssTUFBTCxDQUFZLEtBQVosQ0FBa0IsSUFBbEIsQ0FBdUIsT0FBdkIsQ0FBK0IsVUFBQyxHQUFELEVBQU0sS0FBTixFQUFnQjtBQUMzQyxvQkFBSSxnQkFBSixDQUFxQixPQUFyQixFQUE4QixVQUFDLEtBQUQsRUFBVztBQUNyQywwQkFBTSxlQUFOO0FBQ0Esd0JBQUksT0FBTyxVQUFQLEdBQW9CLEdBQXhCLEVBQTZCO0FBQ3pCLDZCQUFLLEdBQUwsQ0FBUyxPQUFULENBQWlCLE1BQWpCLENBQXdCLE1BQU0sYUFBOUI7QUFDQSw2QkFBSyxHQUFMLENBQVMsT0FBVCxDQUFpQixxQkFBakIsQ0FBdUMsTUFBTSxhQUE3QztBQUNILHFCQUhELE1BSUs7QUFDRCw2QkFBSyxHQUFMLENBQVMsT0FBVCxDQUFpQixZQUFqQixDQUE4QixNQUFNLGFBQXBDO0FBQ0EsNkJBQUssR0FBTCxDQUFTLE9BQVQsQ0FBaUIsMkJBQWpCLENBQTZDLE1BQU0sYUFBbkQ7QUFDSDtBQUNKLGlCQVZEO0FBV0gsYUFaRDs7O0FBZUEscUJBQVMsZ0JBQVQsQ0FBMEIsU0FBMUIsRUFBcUMsVUFBQyxLQUFELEVBQVc7QUFDNUMsb0JBQUksTUFBTSxLQUFOLElBQWUsRUFBbkIsRUFBdUI7QUFDbkIseUJBQUssR0FBTCxDQUFTLE9BQVQsQ0FBaUIsUUFBakI7QUFDSDtBQUNKLGFBSkQ7OztBQU9BLHFCQUFTLGdCQUFULENBQTBCLE9BQTFCLEVBQW1DLFlBQU07QUFDckMscUJBQUssR0FBTCxDQUFTLE9BQVQsQ0FBaUIsUUFBakI7QUFDSCxhQUZEO0FBR0gsU0FyQ0E7OztBQXVDRCxnQkFBUTtBQUNKLGdCQURJLGtCQUNHO0FBQ0gscUJBQUssTUFBTCxDQUFZLEtBQVosQ0FBa0IsYUFBbEIsQ0FBZ0MsU0FBaEMsR0FBNEMsRUFBNUM7QUFDQSxxQkFBSyxHQUFMLENBQVMsTUFBVCxDQUFnQixPQUFoQixHQUEwQixPQUExQixDQUFrQyxVQUFDLE9BQUQsRUFBVSxLQUFWLEVBQW9CO0FBQ2xELDRCQUFRLFlBQVIsQ0FBcUIsT0FBckIsRUFBOEIsS0FBOUI7QUFDQSx5QkFBSyxNQUFMLENBQVksS0FBWixDQUFrQixhQUFsQixDQUFnQyxXQUFoQyxDQUE0QyxPQUE1QztBQUNILGlCQUhEO0FBSUgsYUFQRztBQVNKLG1CQVRJLHFCQVNNO0FBQ04sb0JBQUksT0FBTyxLQUFLLE1BQUwsQ0FBWSxLQUFaLENBQWtCLElBQTdCO0FBQ0Esb0JBQUksUUFBUSxLQUFLLE1BQUwsQ0FBWSxLQUF4QjtBQUNBLHVCQUFPLE1BQU0sSUFBTixFQUFZLE9BQVosQ0FBb0IsR0FBcEIsQ0FBd0I7QUFBQSx3SEFFRixNQUZFO0FBQUEsaUJBQXhCLENBQVA7QUFLSCxhQWpCRztBQW1CSixpQkFuQkksbUJBbUJJO0FBQ0osb0JBQUksT0FBTyxLQUFLLE1BQUwsQ0FBWSxLQUFaLENBQWtCLElBQTdCO0FBQ0Esb0JBQUksUUFBUSxLQUFLLE1BQUwsQ0FBWSxLQUF4QjtBQUNBLHVCQUFPLE1BQU0sSUFBTixFQUFZLE9BQVosQ0FBb0IsR0FBcEIsQ0FBd0Isa0JBQVU7QUFDckMsMkJBQU8sS0FBSyxNQUFMLENBQVksT0FBWixDQUFvQixNQUFwQixFQUE0QixLQUE1QixDQUFrQyxHQUFsQyxDQUFzQyxnQkFBUTtBQUNqRCw0RUFBa0QsSUFBbEQ7QUFDSCxxQkFGTSxDQUFQO0FBR0gsaUJBSk0sQ0FBUDtBQUtILGFBM0JHO0FBNkJKLG9CQTdCSSxzQkE2Qk87QUFDUCxvQkFBSSxVQUFVLEtBQUssR0FBTCxDQUFTLE1BQVQsQ0FBZ0IsT0FBaEIsRUFBZDtBQUNBLG9CQUFJLFFBQVEsS0FBSyxHQUFMLENBQVMsTUFBVCxDQUFnQixLQUFoQixFQUFaO0FBQ0Esb0JBQUksV0FBVyxNQUFNLEdBQU4sQ0FBVSxVQUFDLEtBQUQsRUFBUSxLQUFSLEVBQWtCO0FBQ3ZDLHdCQUFJLFdBQVcsRUFBZjtBQUNBLDBCQUFNLEdBQU4sQ0FBVSxnQkFBUTtBQUNkLG1DQUFXLFdBQVcsSUFBdEI7QUFDSCxxQkFGRDtBQUdBLDJCQUFPLFFBQVEsS0FBUixJQUFpQixRQUF4QjtBQUNILGlCQU5jLENBQWY7QUFPQSx1QkFBTyxTQUFTLEdBQVQsQ0FBYTtBQUFBLHNEQUFnQyxHQUFoQztBQUFBLGlCQUFiLENBQVA7QUFDSCxhQXhDRztBQTBDSix5QkExQ0ksMkJBMENZO0FBQ1osb0JBQUksT0FBTyxLQUFLLE1BQUwsQ0FBWSxLQUFaLENBQWtCLElBQTdCO0FBQ0Esb0JBQUksUUFBUSxLQUFLLE1BQUwsQ0FBWSxLQUF4QjtBQUNBLHVCQUFPLE1BQU0sSUFBTixFQUFZLE9BQVosQ0FBb0IsR0FBcEIsQ0FBd0Isa0JBQVU7QUFDckMsd0JBQUksU0FBUyxLQUFLLE1BQUwsQ0FBWSxPQUFaLENBQW9CLE1BQXBCLEVBQTRCLElBQTVCLENBQWlDLE1BQTlDO0FBQ0EsaUVBQTJDLE1BQTNDO0FBQ0gsaUJBSE0sQ0FBUDtBQUlILGFBakRHO0FBbURKLHVCQW5ESSx5QkFtRFU7QUFDVixvQkFBSSxPQUFPLEtBQUssTUFBTCxDQUFZLEtBQVosQ0FBa0IsSUFBN0I7QUFDQSxvQkFBSSxRQUFRLEtBQUssTUFBTCxDQUFZLEtBQXhCO0FBQ0EsdUJBQU8sTUFBTSxJQUFOLEVBQVksT0FBWixDQUFvQixHQUFwQixDQUF3QixrQkFBVTtBQUNyQyx3QkFBSSxZQUFZLEVBQWhCO0FBQ0EseUJBQUssTUFBTCxDQUFZLE9BQVosQ0FBb0IsTUFBcEIsRUFBNEIsSUFBNUIsQ0FBaUMsS0FBakMsQ0FBdUMsR0FBdkMsQ0FBMkMsZ0JBQVE7QUFDL0Msb0NBQVksaURBQThDLElBQTlDLG1CQUFaO0FBQ0gscUJBRkQ7QUFHQSwyQkFBTyxTQUFQO0FBQ0gsaUJBTk0sQ0FBUDtBQU9ILGFBN0RHO0FBK0RKLG1CQS9ESSxxQkErRE07QUFDTix1QkFBTyxLQUFLLEdBQUwsQ0FBUyxNQUFULENBQWdCLGFBQWhCLEdBQWdDLEdBQWhDLENBQW9DLFVBQUMsTUFBRCxFQUFTLEtBQVQsRUFBbUI7QUFDMUQscURBQThCLFNBQVMsS0FBSyxHQUFMLENBQVMsTUFBVCxDQUFnQixXQUFoQixHQUE4QixLQUE5QixDQUF2QztBQUNILGlCQUZNLENBQVA7QUFHSCxhQW5FRztBQXFFSixlQXJFSSxpQkFxRUU7QUFDRixvQkFBSSxVQUFVLEtBQUssR0FBTCxDQUFTLE1BQVQsQ0FBZ0IsT0FBaEIsRUFBZDtBQUNBLHVCQUFPLEtBQUssR0FBTCxDQUFTLE1BQVQsQ0FBZ0IsUUFBaEIsR0FBMkIsR0FBM0IsQ0FBK0IsVUFBQyxRQUFELEVBQVcsS0FBWCxFQUFxQjtBQUN2RCw4SkFHVSxRQUhWLHNDQUlVLFFBQVEsS0FBUixDQUpWO0FBaUJGLGlCQWxCSyxDQUFQO0FBbUJILGFBMUZHO0FBNEZKLG1CQTVGSSxxQkE0Rk07QUFDTix1QkFBTyxLQUFLLEdBQUwsQ0FBUyxNQUFULENBQWdCLEdBQWhCLEdBQXNCLEdBQXRCLENBQTBCLFVBQUMsR0FBRCxFQUFNLEtBQU4sRUFBZ0I7QUFDN0Msd0JBQUksU0FBUyxLQUFLLE1BQUwsQ0FBWSxLQUFaLENBQWtCLEtBQUssTUFBTCxDQUFZLEtBQVosQ0FBa0IsSUFBcEMsRUFBMEMsT0FBMUMsQ0FBa0QsS0FBbEQsQ0FBYjtBQUNBLHdCQUFJLFVBQVUsU0FBUyxhQUFULENBQXVCLEtBQXZCLENBQWQ7QUFDQSw0QkFBUSxTQUFSLEdBQW9CLEdBQXBCO0FBQ0EsNEJBQVEsU0FBUixHQUFvQixhQUFwQjtBQUNBLDRCQUFRLFlBQVIsQ0FBcUIsUUFBckIsRUFBK0IsTUFBL0I7QUFDQSwyQkFBTyxPQUFQO0FBQ0gsaUJBUE0sQ0FBUDtBQVFIO0FBckdHLFNBdkNQOztBQStJRCxjQS9JQyxvQkErSVE7QUFDTCxpQkFBSyxHQUFMLENBQVMsT0FBVCxDQUFpQixNQUFqQjtBQUNBLGlCQUFLLE1BQUwsQ0FBWSxLQUFaLENBQWtCLFdBQWxCLEdBQWdDLDBCQUFZLFNBQVMsZ0JBQVQsQ0FBMEIsMkJBQTFCLENBQVosQ0FBaEM7QUFDQSxpQkFBSyxHQUFMLENBQVMsT0FBVCxDQUFpQixlQUFqQixDQUFpQyxNQUFNLGFBQXZDO0FBQ0EsaUJBQUssR0FBTCxDQUFTLE9BQVQsQ0FBaUIsY0FBakI7QUFDQSxpQkFBSyxNQUFMLENBQVksS0FBWixDQUFrQixXQUFsQixHQUFnQywwQkFBWSxTQUFTLGdCQUFULENBQTBCLDJCQUExQixDQUFaLENBQWhDO0FBQ0EsaUJBQUssR0FBTCxDQUFTLE9BQVQsQ0FBaUIsS0FBakI7QUFDQSxpQkFBSyxHQUFMLENBQVMsT0FBVCxDQUFpQixxQkFBakIsQ0FBdUMsU0FBUyxhQUFULENBQXVCLDBDQUF2QixDQUF2QztBQUNILFNBdkpBOzs7QUF5SkQsaUJBQVM7QUFFTCxpQkFGSyxtQkFFRztBQUNKLHFCQUFLLE1BQUwsQ0FBWSxLQUFaLENBQWtCLFdBQWxCLENBQThCLE9BQTlCLENBQXNDLFVBQUMsR0FBRCxFQUFNLEtBQU4sRUFBZ0I7QUFDbEQsd0JBQUksWUFBSixDQUFpQixPQUFqQixFQUEwQixLQUExQjtBQUNBLHdCQUFJLGVBQWUsSUFBSSxTQUFKLENBQWMsQ0FBZCxDQUFuQjtBQUNBLHdCQUFJLFlBQUosQ0FBaUIsT0FBakIsRUFBMEIsWUFBMUI7QUFDQSw0QkFBUSxRQUFRLENBQWhCO0FBQ0ksNkJBQUssQ0FBTDtBQUNJLGdDQUFJLFNBQUosQ0FBYyxHQUFkLENBQWtCLGtCQUFsQjtBQUNBO0FBQ0osNkJBQUssQ0FBTDtBQUNJLGdDQUFJLFNBQUosQ0FBYyxHQUFkLENBQWtCLG9CQUFsQjtBQUNBO0FBQ0osNkJBQUssQ0FBTDtBQUNJLGdDQUFJLFNBQUosQ0FBYyxHQUFkLENBQWtCLG1CQUFsQjtBQUNBO0FBVFI7QUFXSCxpQkFmRDtBQWdCSCxhQW5CSTtBQXFCTCxrQkFyQkssa0JBcUJFLE9BckJGLEVBcUJXO0FBQ1oscUJBQUssTUFBTCxDQUFZLEtBQVosQ0FBa0IsSUFBbEIsQ0FBdUIsT0FBdkIsQ0FBK0IsZUFBTztBQUNsQyx3QkFBSSxJQUFJLFlBQUosQ0FBaUIsT0FBakIsS0FBNkIsUUFBUSxZQUFSLENBQXFCLE9BQXJCLENBQWpDLEVBQWdFO0FBQzVELDZCQUFLLEdBQUwsQ0FBUyxPQUFULENBQWlCLFFBQWpCLENBQTBCLEdBQTFCO0FBQ0gscUJBRkQsTUFHSztBQUNELDRCQUFJLElBQUksU0FBSixDQUFjLFFBQWQsQ0FBdUIsb0JBQXZCLENBQUosRUFBa0Q7QUFDOUMsaUNBQUssR0FBTCxDQUFTLE9BQVQsQ0FBaUIsVUFBakI7QUFDQSxnQ0FBSSxTQUFKLENBQWMsR0FBZCxDQUFrQixZQUFsQjtBQUNBLGlDQUFLLE1BQUwsQ0FBWSxLQUFaLENBQWtCLEtBQWxCO0FBQ0EseUNBQWEsS0FBSyxNQUFMLENBQVksS0FBWixDQUFrQixVQUEvQjtBQUNBLG9DQUFRLEtBQVIsQ0FBYyxNQUFkLEdBQXVCLEtBQUssTUFBTCxDQUFZLEtBQVosQ0FBa0IsS0FBekM7QUFDSDtBQUNKO0FBQ0osaUJBYkQ7QUFjQSxvQkFBSSxRQUFRLFFBQVEsWUFBUixDQUFxQixPQUFyQixJQUFnQyxDQUE1QztBQUNBLG9CQUFJLE1BQU0sUUFBUSxTQUFsQjtBQUNBLG9CQUFJLGVBQUo7O0FBRUEsd0JBQVEsS0FBUjtBQUNJLHlCQUFLLENBQUw7QUFDSSxpQ0FBUyxpQkFBVDtBQUNBO0FBQ0oseUJBQUssQ0FBTDtBQUNJLGlDQUFTLG1CQUFUO0FBQ0E7QUFDSix5QkFBSyxDQUFMO0FBQ0ksaUNBQVMsa0JBQVQ7QUFDQTtBQVRSOztBQVlBLG9CQUFJLENBQUMsSUFBSSxRQUFKLENBQWEsWUFBYixDQUFMLEVBQWlDO0FBQzdCLHlCQUFLLE1BQUwsQ0FBWSxLQUFaLENBQWtCLEtBQWxCO0FBQ0EsaUNBQWEsS0FBSyxNQUFMLENBQVksS0FBWixDQUFrQixVQUEvQjtBQUNBLHdCQUFJLEdBQUosQ0FBUSxZQUFSO0FBQ0EsNEJBQVEsS0FBUixDQUFjLE1BQWQsR0FBdUIsS0FBSyxNQUFMLENBQVksS0FBWixDQUFrQixLQUF6QztBQUNILGlCQUxELE1BTUs7QUFDRCx3QkFBSSxDQUFDLElBQUksUUFBSixDQUFhLE1BQWIsQ0FBTCxFQUEyQjtBQUN2Qiw0QkFBSSxHQUFKLENBQVEsTUFBUjtBQUNILHFCQUZELE1BR0s7QUFDRCw0QkFBSSxNQUFKLENBQVcsTUFBWCxFQUFtQixZQUFuQjtBQUNBLDZCQUFLLE1BQUwsQ0FBWSxLQUFaLENBQWtCLFVBQWxCLEdBQStCLFdBQVcsWUFBTTtBQUM1QyxvQ0FBUSxLQUFSLENBQWMsTUFBZCxHQUF1QixHQUF2QjtBQUNILHlCQUY4QixFQUU1QixJQUY0QixDQUEvQjtBQUdIO0FBQ0o7QUFDSixhQXJFSTtBQXVFTCxzQkF2RUssd0JBdUVRO0FBQ1QscUJBQUssTUFBTCxDQUFZLEtBQVosQ0FBa0IsV0FBbEIsQ0FBOEIsT0FBOUIsQ0FBc0MsZUFBTztBQUN6Qyx3QkFBSSxDQUFDLElBQUksU0FBSixDQUFjLFFBQWQsQ0FBdUIsb0JBQXZCLENBQUwsRUFBbUQ7QUFDL0MsNEJBQUksU0FBSixDQUFjLEdBQWQsQ0FBa0Isb0JBQWxCLEVBQXdDLFlBQXhDO0FBQ0EsNkJBQUssTUFBTCxDQUFZLEtBQVosQ0FBa0IsWUFBbEIsQ0FBK0IsU0FBL0IsQ0FBeUMsR0FBekMsQ0FBNkMsaUJBQTdDO0FBQ0gscUJBSEQsTUFJSztBQUNELDRCQUFJLFNBQUosQ0FBYyxNQUFkLENBQXFCLG9CQUFyQixFQUEyQyxZQUEzQztBQUNBLDZCQUFLLE1BQUwsQ0FBWSxLQUFaLENBQWtCLFlBQWxCLENBQStCLFNBQS9CLENBQXlDLE1BQXpDLENBQWdELGlCQUFoRDtBQUNIO0FBQ0osaUJBVEQ7QUFVSCxhQWxGSTtBQW9GTCx3QkFwRkssd0JBb0ZRLE9BcEZSLEVBb0ZpQjtBQUNsQixxQkFBSyxNQUFMLENBQVksS0FBWixDQUFrQixJQUFsQixDQUF1QixPQUF2QixDQUErQixlQUFPO0FBQ2xDLHdCQUFJLElBQUksWUFBSixDQUFpQixPQUFqQixLQUE2QixRQUFRLFlBQVIsQ0FBcUIsT0FBckIsQ0FBakMsRUFBZ0U7QUFDNUQsNEJBQUksU0FBSixDQUFjLE1BQWQsQ0FBcUIsWUFBckI7QUFDQSxtQ0FBVyxZQUFNO0FBQ2IsZ0NBQUksS0FBSixDQUFVLE1BQVYsR0FBbUIsQ0FBbkI7QUFDSCx5QkFGRCxFQUVHLEdBRkg7QUFHSCxxQkFMRCxNQU1LO0FBQ0QsZ0NBQVEsU0FBUixDQUFrQixNQUFsQixDQUF5QixZQUF6QjtBQUNBLDZCQUFLLE1BQUwsQ0FBWSxLQUFaLENBQWtCLEtBQWxCO0FBQ0EsZ0NBQVEsS0FBUixDQUFjLE1BQWQsR0FBdUIsS0FBSyxNQUFMLENBQVksS0FBWixDQUFrQixLQUF6QztBQUNIO0FBQ0osaUJBWkQ7QUFhSCxhQWxHSTtBQW9HTCxvQkFwR0ssb0JBb0dJLEdBcEdKLEVBb0dTO0FBQ1Ysb0JBQUksR0FBSixFQUFTO0FBQ0wsd0JBQUksU0FBSixDQUFjLE1BQWQsQ0FBcUIsaUJBQXJCLEVBQXdDLG1CQUF4QyxFQUE2RCxrQkFBN0QsRUFBaUYsWUFBakY7QUFDQSwrQkFBVyxZQUFNO0FBQ2IsNEJBQUksS0FBSixDQUFVLE1BQVYsR0FBbUIsQ0FBbkI7QUFDSCxxQkFGRCxFQUVHLEdBRkg7QUFHSCxpQkFMRCxNQU1LO0FBQ0QseUJBQUssTUFBTCxDQUFZLEtBQVosQ0FBa0IsSUFBbEIsQ0FBdUIsT0FBdkIsQ0FBK0IsZUFBTztBQUNsQyw0QkFBSSxTQUFKLENBQWMsTUFBZCxDQUFxQixpQkFBckIsRUFBd0MsbUJBQXhDLEVBQTZELGtCQUE3RCxFQUFpRixZQUFqRixFQUErRixvQkFBL0Y7QUFDQSxtQ0FBVyxZQUFNO0FBQ2IsZ0NBQUksS0FBSixDQUFVLE1BQVYsR0FBbUIsQ0FBbkI7QUFDSCx5QkFGRCxFQUVHLEdBRkg7QUFHSCxxQkFMRDtBQU1IO0FBQ0osYUFuSEk7QUFxSEwsaUNBckhLLGlDQXFIaUIsR0FySGpCLEVBcUhzQjtBQUN2QixvQkFBSSxHQUFKLEVBQVM7QUFBQTtBQUFBLDRCQVdJLEtBWEosR0FXTCxTQUFTLEtBQVQsR0FBaUI7QUFDYixpQ0FBSyxNQUFMLENBQVksS0FBWixDQUFrQixhQUFsQixDQUFnQyxLQUFoQyxDQUFzQyxlQUF0QyxHQUF3RCxNQUF4RDtBQUNBLGlDQUFLLE1BQUwsQ0FBWSxLQUFaLENBQWtCLGFBQWxCLENBQWdDLEtBQWhDLENBQXNDLE1BQXRDLEdBQWdELGdCQUFnQixJQUFqQixHQUF5QixJQUF4RTtBQUNILHlCQWRJOztBQUNMLDRCQUFJLGdCQUFnQixTQUFTLE9BQU8sZ0JBQVAsQ0FBd0IsR0FBeEIsRUFBNkIsWUFBN0IsQ0FBVCxDQUFwQjtBQUNBLDRCQUFJLGdCQUFpQixTQUFTLE9BQU8sZ0JBQVAsQ0FBd0IsR0FBeEIsRUFBNkIsV0FBN0IsQ0FBVCxJQUFzRCxHQUF2RCxHQUE4RCxhQUFsRjtBQUNBLDRCQUFJLFlBQVksZ0JBQWdCLENBQWhDO0FBQ0EsNEJBQUksT0FBTyxLQUFLLElBQUwsQ0FBVyxLQUFLLE1BQUwsQ0FBWSxLQUFaLENBQWtCLFdBQWxCLENBQThCLE1BQTlCLENBQXFDO0FBQUEsbUNBQU8sT0FBUSxHQUFSLElBQWdCLFFBQXZCO0FBQUEseUJBQXJDLENBQUQsQ0FBd0UsTUFBeEUsR0FBaUYsQ0FBM0YsQ0FBWDtBQUNBLDRCQUFJLGVBQWUsT0FBTyxDQUExQjtBQUNBLDRCQUFJLGtCQUFrQixnQkFBZ0IsSUFBdEM7QUFDQSw0QkFBSSxhQUFhLEtBQUssSUFBTCxDQUFVLENBQUMsU0FBUyxJQUFJLFlBQUosQ0FBaUIsT0FBakIsQ0FBVCxJQUFzQyxDQUF2QyxJQUE0QyxDQUF0RCxDQUFqQjtBQUNBLHVDQUFlLGVBQWUsQ0FBZixHQUFtQixDQUFuQixHQUF1QixZQUF0QztBQUNBLDRCQUFJLGNBQWMsT0FBTyxZQUF6Qjs7QUFPQSw0QkFBSSxJQUFJLFNBQUosQ0FBYyxRQUFkLENBQXVCLFlBQXZCLENBQUosRUFBMEM7QUFDdEMsZ0NBQUksYUFBYSxZQUFqQixFQUErQjtBQUMzQixvQ0FBSSxXQUFXLGVBQWUsZUFBZSxVQUE5QixJQUE0QyxDQUEzRDtBQUNBLG9DQUFJLFdBQVcsbUJBQW1CLFlBQWEsQ0FBQyxnQkFBZ0IsYUFBakIsSUFBa0MsUUFBL0MsR0FBNEQsZ0JBQWdCLFFBQS9GLENBQWY7QUFDQSxxQ0FBSyxNQUFMLENBQVksS0FBWixDQUFrQixhQUFsQixDQUFnQyxLQUFoQyxDQUFzQyxlQUF0QyxHQUF3RCxJQUF4RDtBQUNBLHFDQUFLLE1BQUwsQ0FBWSxLQUFaLENBQWtCLGFBQWxCLENBQWdDLEtBQWhDLENBQXNDLE1BQXRDLEdBQStDLFdBQVcsSUFBMUQ7QUFDSCw2QkFMRCxNQU1LO0FBQ0Q7QUFDSDtBQUNKLHlCQVZELE1BV0s7QUFDRDtBQUNIO0FBN0JJO0FBOEJSO0FBQ0osYUFySkk7QUF1SkwsdUNBdkpLLHVDQXVKdUIsR0F2SnZCLEVBdUo0QjtBQUM3QixvQkFBSSxHQUFKLEVBQVM7QUFBQTtBQUFBLDRCQVdJLEtBWEosR0FXTCxTQUFTLEtBQVQsR0FBaUI7QUFDYixpQ0FBSyxNQUFMLENBQVksS0FBWixDQUFrQixhQUFsQixDQUFnQyxLQUFoQyxDQUFzQyxlQUF0QyxHQUF3RCxNQUF4RDtBQUNBLGlDQUFLLE1BQUwsQ0FBWSxLQUFaLENBQWtCLGFBQWxCLENBQWdDLEtBQWhDLENBQXNDLE1BQXRDLEdBQWdELGdCQUFnQixJQUFqQixHQUF5QixJQUF4RTtBQUNILHlCQWRJOztBQUNMLDRCQUFJLGdCQUFnQixTQUFTLE9BQU8sZ0JBQVAsQ0FBd0IsR0FBeEIsRUFBNkIsWUFBN0IsQ0FBVCxDQUFwQjtBQUNBLDRCQUFJLGdCQUFpQixTQUFTLE9BQU8sZ0JBQVAsQ0FBd0IsR0FBeEIsRUFBNkIsV0FBN0IsQ0FBVCxJQUFzRCxFQUF2RCxHQUE2RCxhQUFqRjtBQUNBLDRCQUFJLFlBQVksZ0JBQWdCLENBQWhDO0FBQ0EsNEJBQUksT0FBTyxLQUFLLElBQUwsQ0FBVyxLQUFLLE1BQUwsQ0FBWSxLQUFaLENBQWtCLFdBQWxCLENBQThCLE1BQTlCLENBQXFDO0FBQUEsbUNBQU8sT0FBUSxHQUFSLElBQWdCLFFBQXZCO0FBQUEseUJBQXJDLENBQUQsQ0FBd0UsTUFBeEUsR0FBaUYsQ0FBM0YsQ0FBWDtBQUNBLDRCQUFJLGVBQWUsT0FBTyxDQUExQjtBQUNBLDRCQUFJLGtCQUFrQixnQkFBZ0IsSUFBdEM7QUFDQSw0QkFBSSxhQUFhLEtBQUssSUFBTCxDQUFVLENBQUMsU0FBUyxJQUFJLFlBQUosQ0FBaUIsT0FBakIsQ0FBVCxJQUFzQyxDQUF2QyxJQUE0QyxDQUF0RCxDQUFqQjtBQUNBLHVDQUFlLGVBQWUsQ0FBZixHQUFtQixDQUFuQixHQUF1QixZQUF0QztBQUNBLDRCQUFJLGNBQWMsT0FBTyxZQUF6Qjs7QUFPQSw0QkFBSSxJQUFJLFNBQUosQ0FBYyxRQUFkLENBQXVCLFlBQXZCLENBQUosRUFBMEM7QUFDdEMsZ0NBQUksYUFBYSxZQUFqQixFQUErQjtBQUMzQixvQ0FBSSxXQUFXLGVBQWUsZUFBZSxVQUE5QixJQUE0QyxDQUEzRDtBQUNBLG9DQUFJLFdBQVcsbUJBQW1CLFlBQWEsQ0FBQyxnQkFBZ0IsYUFBakIsSUFBa0MsUUFBL0MsR0FBNEQsZ0JBQWdCLFFBQS9GLENBQWY7QUFDQSxxQ0FBSyxNQUFMLENBQVksS0FBWixDQUFrQixhQUFsQixDQUFnQyxLQUFoQyxDQUFzQyxlQUF0QyxHQUF3RCxJQUF4RDtBQUNBLHFDQUFLLE1BQUwsQ0FBWSxLQUFaLENBQWtCLGFBQWxCLENBQWdDLEtBQWhDLENBQXNDLE1BQXRDLEdBQStDLFdBQVcsSUFBMUQ7QUFDSCw2QkFMRCxNQU1LO0FBQ0Q7QUFDSDtBQUNKLHlCQVZELE1BV0s7QUFDRDtBQUNIO0FBN0JJO0FBOEJSO0FBQ0osYUF2TEk7QUF5TEwsa0JBekxLLG9CQXlMSTtBQUNMLG9CQUFJLEtBQUssTUFBTCxDQUFZLEtBQVosQ0FBa0IsSUFBbEIsSUFBMEIsU0FBOUIsRUFBeUM7QUFBQTtBQUNyQyw0QkFBSSxnQkFBZ0IsS0FBSyxNQUFMLENBQVksS0FBWixDQUFrQixLQUFLLE1BQUwsQ0FBWSxLQUFaLENBQWtCLElBQXBDLEVBQTBDLE9BQTlEO0FBQ0EsNkJBQUssTUFBTCxDQUFZLEtBQVosQ0FBa0IsSUFBbEIsQ0FBdUIsT0FBdkIsQ0FBK0IsZUFBTztBQUNsQyxnQ0FBSSxTQUFTLElBQUksWUFBSixDQUFpQixRQUFqQixDQUFiO0FBQ0EsZ0NBQUksVUFBVSxjQUFjLE9BQWQsQ0FBc0IsTUFBdEIsQ0FBZDtBQUNBLGdDQUFJLFVBQVUsQ0FBZCxFQUFpQjtBQUNiLG9DQUFJLFNBQUosQ0FBYyxHQUFkLENBQWtCLFVBQWxCO0FBQ0Esb0NBQUksU0FBSixDQUFjLE1BQWQsQ0FBcUIsVUFBckI7QUFDSCw2QkFIRCxNQUlLO0FBQ0Qsb0NBQUksU0FBSixDQUFjLEdBQWQsQ0FBa0IsVUFBbEI7QUFDQSxvQ0FBSSxTQUFKLENBQWMsTUFBZCxDQUFxQixVQUFyQjtBQUNIO0FBQ0oseUJBWEQ7QUFGcUM7QUFjeEM7QUFDSixhQXpNSTtBQTJNTCwyQkEzTUssMkJBMk1XLElBM01YLEVBMk1pQjtBQUNsQixvQkFBSSxZQUFZLEtBQUssTUFBTCxDQUFZLEtBQVosQ0FBa0IsS0FBSyxNQUFMLENBQVksS0FBWixDQUFrQixJQUFwQyxFQUEwQyxTQUExRDtBQUNBLHFCQUFLLE1BQUwsQ0FBWSxLQUFaLENBQWtCLFdBQWxCLENBQThCLE9BQTlCLENBQXNDLGVBQU87QUFDekMsd0JBQUksU0FBUyxJQUFJLFlBQUosQ0FBaUIsUUFBakIsQ0FBYjtBQUNBLHdCQUFJLFVBQVUsVUFBVSxPQUFWLENBQWtCLE1BQWxCLENBQWQ7QUFDQSx3QkFBSSxVQUFVLENBQWQsRUFBaUI7QUFDYiw0QkFBSSxVQUFKLENBQWUsQ0FBZixFQUFrQixTQUFsQixDQUE0QixNQUE1QixDQUFtQyxlQUFuQztBQUNBLDRCQUFJLFVBQUosQ0FBZSxDQUFmLEVBQWtCLFNBQWxCLENBQTRCLEdBQTVCLENBQWdDLGNBQWhDO0FBQ0gscUJBSEQsTUFJSztBQUNELDRCQUFJLFVBQUosQ0FBZSxDQUFmLEVBQWtCLFNBQWxCLENBQTRCLEdBQTVCLENBQWdDLGVBQWhDO0FBQ0EsNEJBQUksVUFBSixDQUFlLENBQWYsRUFBa0IsU0FBbEIsQ0FBNEIsTUFBNUIsQ0FBbUMsY0FBbkM7QUFDSDtBQUNKLGlCQVhEO0FBWUgsYUF6Tkk7QUEyTkwsMEJBM05LLDRCQTJOWTtBQUNiLG9CQUFJLEtBQUssTUFBTCxDQUFZLEtBQVosQ0FBa0IsSUFBbEIsSUFBMEIsUUFBOUIsRUFBd0M7QUFBQTtBQUNwQyw0QkFBSSxRQUFRLENBQVo7QUFDQSw2QkFBSyxNQUFMLENBQVksS0FBWixDQUFrQixXQUFsQixDQUE4QixPQUE5QixDQUFzQyxlQUFPO0FBQ3pDLGdDQUFJLElBQUksVUFBSixDQUFlLENBQWYsRUFBa0IsU0FBbEIsQ0FBNEIsUUFBNUIsQ0FBcUMsZUFBckMsQ0FBSixFQUEyRDtBQUN2RCxvQ0FBSSxVQUFVLEdBQWQ7QUFDQSxvQ0FBSSxNQUFKO0FBQ0EscUNBQUssTUFBTCxDQUFZLEtBQVosQ0FBa0IsYUFBbEIsQ0FBZ0MsWUFBaEMsQ0FBNkMsT0FBN0MsRUFBc0QsS0FBSyxNQUFMLENBQVksS0FBWixDQUFrQixhQUFsQixDQUFnQyxVQUFoQyxDQUEyQyxLQUEzQyxDQUF0RDtBQUNBO0FBQ0g7QUFDSix5QkFQRDtBQUZvQztBQVV2QztBQUNKO0FBdk9JO0FBekpSLEtBM1pJOztBQSt4QlQsUUEveEJTLGtCQSt4QkY7QUFDSCxhQUFLLFFBQUwsQ0FBYyxJQUFkO0FBQ0EsYUFBSyxHQUFMLENBQVMsSUFBVDtBQUNIO0FBbHlCUSxDQUFiOztrQkFxeUJlLEk7Ozs7Ozs7OztBQ3J5QmY7O0FBQ0E7Ozs7Ozs7O0FBRUEsSUFBSSxXQUFXO0FBQ1gsWUFBUTtBQUNKLGVBQU87QUFDSCxvQkFBUSxTQUFTLGFBQVQsQ0FBdUIsZ0JBQXZCLENBREw7QUFFSCxtQkFBTyxTQUFTLGFBQVQsQ0FBdUIsZUFBdkIsQ0FGSjtBQUdILHNCQUhHLHNCQUdRLE9BSFIsRUFHaUI7QUFDaEIsdUJBQU8sU0FBUyxhQUFULHlCQUE2QyxPQUE3QyxDQUFQO0FBQ0gsYUFMRTs7QUFNSCxzQkFBVTtBQUNOLG1CQUFHO0FBQ0MsNEJBQVEsU0FBUyxhQUFULENBQXVCLDRCQUF2QixDQURUO0FBRUMsMkJBQU8sU0FBUyxhQUFULENBQXVCLG1CQUF2QjtBQUZSLGlCQURHO0FBS04sbUJBQUc7QUFDQyx5QkFBSyxTQUFTLGFBQVQsQ0FBdUIsaUJBQXZCLENBRE47QUFFQyw0QkFBUSxTQUFTLGFBQVQsQ0FBdUIsc0JBQXZCLENBRlQ7QUFHQywyQkFBTyxTQUFTLGFBQVQsQ0FBdUIsbUJBQXZCO0FBSFIsaUJBTEc7QUFVTixtQkFBRztBQUNDLDJCQUFPLFNBQVMsYUFBVCxDQUF1QixvQkFBdkIsQ0FEUjtBQUVDLDJCQUFPLFNBQVMsYUFBVCxDQUF1QixvQkFBdkIsQ0FGUjtBQUdDLDJCQUFPLFNBQVMsYUFBVCxDQUF1QixvQkFBdkIsQ0FIUjtBQUlDLDJCQUFPLFNBQVMsYUFBVCxDQUF1QixvQkFBdkIsQ0FKUjtBQUtDLDJCQUFPLFNBQVMsYUFBVCxDQUF1QixvQkFBdkIsQ0FMUjtBQU1DLDJCQUFPLFNBQVMsYUFBVCxDQUF1QixvQkFBdkI7QUFOUixpQkFWRztBQWtCTixtQkFBRztBQUNDLDJCQUFPLFNBQVMsYUFBVCxDQUF1QixtQkFBdkIsQ0FEUjtBQUVDLHlCQUFLLFNBQVMsYUFBVCxDQUF1QixpQkFBdkI7QUFGTixpQkFsQkc7QUFzQk4sbUJBQUc7QUFDQywyQkFBTyxTQUFTLGFBQVQsQ0FBdUIsb0JBQXZCLENBRFI7QUFFQywyQkFBTyxTQUFTLGFBQVQsQ0FBdUIsNkJBQXZCO0FBRlI7QUF0Qkc7QUFOUCxTQURIO0FBbUNKLHFCQUFhO0FBbkNULEtBREc7O0FBdUNYLGFBQVM7QUFDTCxzQkFESyw0QkFDWTtBQUNiLGdCQUFJLGVBQWUsU0FBUyxPQUFPLGdCQUFQLENBQXdCLFNBQVMsTUFBVCxDQUFnQixLQUFoQixDQUFzQixNQUE5QyxFQUFzRCxJQUF0RCxFQUE0RCxNQUFyRSxDQUFuQjtBQUNBLGdCQUFJLGdCQUFnQixTQUFTLE9BQU8sZ0JBQVAsQ0FBd0IsU0FBUyxNQUFULENBQWdCLEtBQWhCLENBQXNCLE1BQTlDLEVBQXNELElBQXRELEVBQTRELFFBQXJFLElBQWlGLEVBQXJHO0FBQ0EsZ0JBQUksY0FBYyxTQUFTLE9BQU8sZ0JBQVAsQ0FBd0IsU0FBUyxNQUFULENBQWdCLEtBQWhCLENBQXNCLEtBQTlDLEVBQXFELElBQXJELEVBQTJELE1BQXBFLENBQWxCO0FBQ0EsbUJBQU8sY0FBYyxZQUFkLEdBQTZCLGFBQXBDO0FBQ0gsU0FOSTtBQVFMLHdCQVJLLDRCQVFZLE9BUlosRUFRcUI7QUFDdEIsZ0JBQUksV0FBVyxDQUFmLEVBQWtCO0FBQ2QsdUJBQU8sQ0FBUDtBQUNIO0FBQ0QsbUJBQU8sU0FBUyxPQUFPLGdCQUFQLENBQXdCLFNBQVMsTUFBVCxDQUFnQixLQUFoQixDQUFzQixVQUF0QixDQUFpQyxPQUFqQyxDQUF4QixFQUFtRSxJQUFuRSxFQUF5RSxNQUFsRixJQUE0RixTQUFTLE9BQU8sZ0JBQVAsQ0FBd0IsU0FBUyxNQUFULENBQWdCLEtBQWhCLENBQXNCLFVBQXRCLENBQWlDLE9BQWpDLENBQXhCLEVBQW1FLElBQW5FLEVBQXlFLGFBQWxGLENBQW5HO0FBQ0gsU0FiSTtBQWVMLHNCQWZLLDRCQWVZO0FBQ2IscUJBQVMsTUFBVCxDQUFnQixXQUFoQixHQUE4QixPQUFPLFVBQXJDO0FBQ0gsU0FqQkk7QUFtQkwsbUJBbkJLLHVCQW1CTyxRQW5CUCxFQW1CaUI7O0FBRWxCLGdCQUFJLGFBQWEsU0FBakI7O0FBRUEsZ0JBQUksT0FBTyxVQUFQLEdBQW9CLElBQXhCLEVBQThCO0FBQzFCLDZCQUFhLENBQWI7QUFDSCxhQUZELE1BR0ssSUFBSSxPQUFPLFVBQVAsR0FBb0IsSUFBcEIsSUFBNEIsT0FBTyxVQUFQLElBQXFCLElBQXJELEVBQTJEO0FBQzVELDZCQUFhLEdBQWI7QUFDSCxhQUZJLE1BR0EsSUFBSSxPQUFPLFVBQVAsSUFBcUIsSUFBckIsSUFBNkIsT0FBTyxVQUFQLEdBQW9CLEdBQXJELEVBQTBEO0FBQzNELDZCQUFhLElBQWI7QUFDSCxhQUZJLE1BR0EsSUFBSSxPQUFPLFVBQVAsSUFBcUIsR0FBckIsSUFBNEIsT0FBTyxVQUFQLEdBQW9CLEdBQXBELEVBQXlEO0FBQzFELDZCQUFhLEdBQWI7QUFDSDs7QUFFRCxtQkFBTyxhQUFhLFFBQXBCO0FBQ0gsU0FyQ0k7QUF1Q0wsZ0JBdkNLLHNCQXVDTTtBQUNQLGdCQUFJLFFBQVEsMEJBQVksU0FBUyxNQUFULENBQWdCLEtBQWhCLENBQXNCLFFBQWxDLEVBQTRDLEdBQTVDLENBQWdELFVBQUMsT0FBRCxFQUFVLEtBQVYsRUFBb0I7QUFDNUUsdUJBQU8sMEJBQVksU0FBUyxNQUFULENBQWdCLEtBQWhCLENBQXNCLFFBQXRCLENBQStCLFFBQVEsQ0FBdkMsQ0FBWixFQUF1RCxHQUF2RCxDQUEyRCxnQkFBUTtBQUN0RSwyQkFBTyxJQUFQO0FBQ0gsaUJBRk0sQ0FBUDtBQUdILGFBSlcsQ0FBWjs7QUFNQSxvQkFBUSxNQUFNLE1BQU4sQ0FBYSxVQUFDLElBQUQsRUFBTyxJQUFQLEVBQWdCO0FBQ2pDLHVCQUFPLEtBQUssTUFBTCxDQUFZLElBQVosQ0FBUDtBQUNILGFBRk8sQ0FBUjs7QUFJQSxtQkFBTyxLQUFQO0FBQ0gsU0FuREk7QUFxREwsb0JBckRLLHdCQXFEUSxJQXJEUixFQXFEYztBQUNmLHNDQUFZLEtBQUssVUFBakIsRUFBNkIsT0FBN0IsQ0FBcUMscUJBQWE7QUFDOUMsb0JBQUksVUFBVSxJQUFWLENBQWUsS0FBZixDQUFxQixHQUFyQixFQUEwQixDQUExQixLQUFnQyxNQUFwQyxFQUE0QztBQUN4Qyx5QkFBSyxlQUFMLENBQXFCLFVBQVUsSUFBL0I7QUFDSDtBQUNKLGFBSkQ7QUFLSDtBQTNESSxLQXZDRTs7QUFxR1gsYUFBUztBQUNMLFlBREssa0JBQ0U7QUFDSCxxQkFBUyxPQUFULENBQWlCLFdBQWpCO0FBQ0EscUJBQVMsT0FBVCxDQUFpQixXQUFqQjtBQUNBLHFCQUFTLE9BQVQsQ0FBaUIsV0FBakI7QUFDQSxxQkFBUyxPQUFULENBQWlCLFdBQWpCO0FBQ0EscUJBQVMsT0FBVCxDQUFpQixXQUFqQjtBQUNBLGlDQUFRLElBQVIsQ0FBYSxFQUFFLGFBQWEsS0FBZixFQUFiO0FBQ0gsU0FSSTtBQVVMLG1CQVZLLHlCQVVTO0FBQ1YsZ0JBQUksY0FBYyxLQUFLLEtBQUwsQ0FBVyxTQUFTLE9BQVQsQ0FBaUIsY0FBakIsS0FBb0MsU0FBUyxPQUFULENBQWlCLFdBQWpCLENBQTZCLElBQTdCLENBQS9DLENBQWxCO0FBQ0EsZ0JBQUksYUFBYSxLQUFLLEtBQUwsQ0FBVyxTQUFTLE9BQVQsQ0FBaUIsY0FBakIsS0FBb0MsU0FBUyxPQUFULENBQWlCLFdBQWpCLENBQTZCLElBQTdCLENBQS9DLENBQWpCO0FBQ0EsZ0JBQUksWUFBWSxLQUFLLEtBQUwsQ0FBVyxTQUFTLE9BQVQsQ0FBaUIsY0FBakIsS0FBb0MsU0FBUyxPQUFULENBQWlCLFdBQWpCLENBQTZCLElBQTdCLENBQS9DLENBQWhCOztBQUVBLHFCQUFTLE1BQVQsQ0FBZ0IsS0FBaEIsQ0FBc0IsUUFBdEIsQ0FBK0IsQ0FBL0IsRUFBa0MsTUFBbEMsQ0FBeUMsWUFBekMsV0FBOEQsV0FBOUQsRUFBNkUsYUFBN0U7QUFDQSxxQkFBUyxNQUFULENBQWdCLEtBQWhCLENBQXNCLFFBQXRCLENBQStCLENBQS9CLEVBQWtDLE1BQWxDLENBQXlDLFlBQXpDLFdBQThELFNBQTlELEVBQTJFLGNBQTNFO0FBQ0EscUJBQVMsTUFBVCxDQUFnQixLQUFoQixDQUFzQixRQUF0QixDQUErQixDQUEvQixFQUFrQyxLQUFsQyxDQUF3QyxZQUF4QyxXQUE2RCxXQUE3RCxFQUE0RSxhQUE1RTtBQUNBLHFCQUFTLE1BQVQsQ0FBZ0IsS0FBaEIsQ0FBc0IsUUFBdEIsQ0FBK0IsQ0FBL0IsRUFBa0MsS0FBbEMsQ0FBd0MsWUFBeEMsV0FBNkQsVUFBN0QsRUFBMkUsdUJBQTNFO0FBQ0EscUJBQVMsTUFBVCxDQUFnQixLQUFoQixDQUFzQixRQUF0QixDQUErQixDQUEvQixFQUFrQyxLQUFsQyxDQUF3QyxZQUF4QyxXQUE2RCxTQUE3RCxFQUEwRSxnQkFBMUU7QUFDSCxTQXBCSTtBQXNCTCxtQkF0QksseUJBc0JTO0FBQ1YsZ0JBQUksWUFBWSxLQUFLLEtBQUwsQ0FBVyxTQUFTLE9BQVQsQ0FBaUIsY0FBakIsS0FBb0MsU0FBUyxPQUFULENBQWlCLGdCQUFqQixDQUFrQyxDQUFsQyxJQUF1QyxTQUFTLE9BQVQsQ0FBaUIsV0FBakIsQ0FBNkIsR0FBN0IsQ0FBdEYsQ0FBaEI7QUFDQSxnQkFBSSxVQUFVLEtBQUssS0FBTCxDQUFXLFNBQVMsT0FBVCxDQUFpQixjQUFqQixLQUFvQyxTQUFTLE9BQVQsQ0FBaUIsZ0JBQWpCLENBQWtDLENBQWxDLElBQXVDLFNBQVMsT0FBVCxDQUFpQixXQUFqQixDQUE2QixHQUE3QixDQUF0RixDQUFkO0FBQ0EsZ0JBQUksYUFBYSxLQUFLLEtBQUwsQ0FBVyxTQUFTLE9BQVQsQ0FBaUIsY0FBakIsS0FBb0MsU0FBUyxPQUFULENBQWlCLGdCQUFqQixDQUFrQyxDQUFsQyxJQUF1QyxTQUFTLE9BQVQsQ0FBaUIsV0FBakIsQ0FBNkIsSUFBN0IsQ0FBM0UsR0FBZ0gsT0FBM0gsQ0FBakI7QUFDQSxnQkFBSSxZQUFZLGFBQWEsQ0FBN0I7O0FBRUEsZ0JBQUksY0FBYyxVQUFVLFNBQTVCO0FBQ0EsZ0JBQUksY0FBYyxjQUFjLFNBQWhDO0FBQ0EsZ0JBQUksY0FBYyxjQUFjLFNBQWhDO0FBQ0EsZ0JBQUksY0FBYyxjQUFjLFNBQWhDO0FBQ0EsZ0JBQUksY0FBYyxjQUFjLFNBQWhDO0FBQ0EsZ0JBQUksY0FBYyxjQUFjLFNBQWhDO0FBQ0EsZ0JBQUksY0FBYyxjQUFjLFNBQWhDOztBQUVBLHFCQUFTLE1BQVQsQ0FBZ0IsS0FBaEIsQ0FBc0IsUUFBdEIsQ0FBK0IsQ0FBL0IsRUFBa0MsR0FBbEMsQ0FBc0MsWUFBdEMsV0FBMkQsU0FBM0QsRUFBd0UsY0FBeEU7QUFDQSxxQkFBUyxNQUFULENBQWdCLEtBQWhCLENBQXNCLFFBQXRCLENBQStCLENBQS9CLEVBQWtDLEdBQWxDLENBQXNDLFlBQXRDLFdBQTJELE9BQTNELEVBQXNFLGdCQUF0RTs7QUFFQSxxQkFBUyxNQUFULENBQWdCLEtBQWhCLENBQXNCLFFBQXRCLENBQStCLENBQS9CLEVBQWtDLEtBQWxDLENBQXdDLFlBQXhDLFdBQTZELFdBQTdELEVBQTRFLGNBQTVFO0FBQ0EscUJBQVMsTUFBVCxDQUFnQixLQUFoQixDQUFzQixRQUF0QixDQUErQixDQUEvQixFQUFrQyxLQUFsQyxDQUF3QyxZQUF4QyxXQUE2RCxXQUE3RCxFQUE0RSxjQUE1RTtBQUNBLHFCQUFTLE1BQVQsQ0FBZ0IsS0FBaEIsQ0FBc0IsUUFBdEIsQ0FBK0IsQ0FBL0IsRUFBa0MsS0FBbEMsQ0FBd0MsWUFBeEMsV0FBNkQsV0FBN0QsRUFBNEUsY0FBNUU7QUFDQSxxQkFBUyxNQUFULENBQWdCLEtBQWhCLENBQXNCLFFBQXRCLENBQStCLENBQS9CLEVBQWtDLEtBQWxDLENBQXdDLFlBQXhDLFdBQTZELFdBQTdELEVBQTRFLGNBQTVFO0FBQ0EscUJBQVMsTUFBVCxDQUFnQixLQUFoQixDQUFzQixRQUF0QixDQUErQixDQUEvQixFQUFrQyxLQUFsQyxDQUF3QyxZQUF4QyxXQUE2RCxXQUE3RCxFQUE0RSxjQUE1RTtBQUNBLHFCQUFTLE1BQVQsQ0FBZ0IsS0FBaEIsQ0FBc0IsUUFBdEIsQ0FBK0IsQ0FBL0IsRUFBa0MsS0FBbEMsQ0FBd0MsWUFBeEMsV0FBNkQsV0FBN0QsRUFBNEUsY0FBNUU7QUFDQSxxQkFBUyxNQUFULENBQWdCLEtBQWhCLENBQXNCLFFBQXRCLENBQStCLENBQS9CLEVBQWtDLEtBQWxDLENBQXdDLFlBQXhDLFdBQTZELFdBQTdELEVBQTRFLGNBQTVFOztBQUVBLHFCQUFTLE1BQVQsQ0FBZ0IsS0FBaEIsQ0FBc0IsUUFBdEIsQ0FBK0IsQ0FBL0IsRUFBa0MsTUFBbEMsQ0FBeUMsWUFBekMsV0FBOEQsU0FBOUQsRUFBMkUsY0FBM0U7QUFDQSxxQkFBUyxNQUFULENBQWdCLEtBQWhCLENBQXNCLFFBQXRCLENBQStCLENBQS9CLEVBQWtDLE1BQWxDLENBQXlDLFlBQXpDLFdBQThELFdBQTlELEVBQTZFLGNBQTdFO0FBQ0gsU0FqREk7QUFtREwsbUJBbkRLLHlCQW1EUztBQUNWLGdCQUFJLFlBQVksS0FBSyxLQUFMLENBQVcsU0FBUyxPQUFULENBQWlCLGNBQWpCLEtBQW9DLFNBQVMsT0FBVCxDQUFpQixnQkFBakIsQ0FBa0MsQ0FBbEMsSUFBdUMsU0FBUyxPQUFULENBQWlCLFdBQWpCLENBQTZCLEdBQTdCLENBQXRGLENBQWhCO0FBQ0EsZ0JBQUksVUFBVSxLQUFLLEtBQUwsQ0FBVyxTQUFTLE9BQVQsQ0FBaUIsY0FBakIsS0FBb0MsU0FBUyxPQUFULENBQWlCLGdCQUFqQixDQUFrQyxDQUFsQyxDQUFwQyxHQUEyRSxTQUFTLE9BQVQsQ0FBaUIsZ0JBQWpCLENBQWtDLENBQWxDLElBQXVDLFNBQVMsT0FBVCxDQUFpQixXQUFqQixDQUE2QixHQUE3QixDQUE3SCxDQUFkO0FBQ0EsZ0JBQUksWUFBWSxVQUFVLFNBQTFCO0FBQ0EsZ0JBQUksV0FBVyxZQUFZLENBQTNCO0FBQ0EsZ0JBQUksWUFBWSxZQUFZLFFBQTVCO0FBQ0EsZ0JBQUksWUFBWSxZQUFZLFFBQTVCOztBQUVBLHFCQUFTLE1BQVQsQ0FBZ0IsS0FBaEIsQ0FBc0IsUUFBdEIsQ0FBK0IsQ0FBL0IsRUFBa0MsS0FBbEMsQ0FBd0MsWUFBeEMsWUFBNkQsWUFBWSxHQUF6RSxHQUFnRixZQUFoRjtBQUNBLHFCQUFTLE1BQVQsQ0FBZ0IsS0FBaEIsQ0FBc0IsUUFBdEIsQ0FBK0IsQ0FBL0IsRUFBa0MsS0FBbEMsQ0FBd0MsWUFBeEMsV0FBNkQsU0FBN0QsRUFBMEUsVUFBMUU7O0FBRUEscUJBQVMsTUFBVCxDQUFnQixLQUFoQixDQUFzQixRQUF0QixDQUErQixDQUEvQixFQUFrQyxLQUFsQyxDQUF3QyxZQUF4QyxXQUE2RCxTQUE3RCxFQUEwRSxZQUExRTtBQUNBLHFCQUFTLE1BQVQsQ0FBZ0IsS0FBaEIsQ0FBc0IsUUFBdEIsQ0FBK0IsQ0FBL0IsRUFBa0MsS0FBbEMsQ0FBd0MsWUFBeEMsV0FBNkQsU0FBN0QsRUFBMEUsVUFBMUU7O0FBRUEscUJBQVMsTUFBVCxDQUFnQixLQUFoQixDQUFzQixRQUF0QixDQUErQixDQUEvQixFQUFrQyxLQUFsQyxDQUF3QyxZQUF4QyxZQUE2RCxZQUFZLEdBQXpFLEdBQWdGLFlBQWhGO0FBQ0EscUJBQVMsTUFBVCxDQUFnQixLQUFoQixDQUFzQixRQUF0QixDQUErQixDQUEvQixFQUFrQyxLQUFsQyxDQUF3QyxZQUF4QyxXQUE2RCxTQUE3RCxFQUEwRSxZQUExRTs7QUFFQSxxQkFBUyxNQUFULENBQWdCLEtBQWhCLENBQXNCLFFBQXRCLENBQStCLENBQS9CLEVBQWtDLEtBQWxDLENBQXdDLFlBQXhDLFdBQTZELFNBQTdELEVBQTBFLFlBQTFFO0FBQ0EscUJBQVMsTUFBVCxDQUFnQixLQUFoQixDQUFzQixRQUF0QixDQUErQixDQUEvQixFQUFrQyxLQUFsQyxDQUF3QyxZQUF4QyxXQUE2RCxTQUE3RCxFQUEwRSxVQUExRTs7QUFFQSxxQkFBUyxNQUFULENBQWdCLEtBQWhCLENBQXNCLFFBQXRCLENBQStCLENBQS9CLEVBQWtDLEtBQWxDLENBQXdDLFlBQXhDLFdBQTZELFNBQTdELEVBQTBFLFlBQTFFO0FBQ0EscUJBQVMsTUFBVCxDQUFnQixLQUFoQixDQUFzQixRQUF0QixDQUErQixDQUEvQixFQUFrQyxLQUFsQyxDQUF3QyxZQUF4QyxXQUE2RCxTQUE3RCxFQUEwRSxVQUExRTs7QUFFQSxxQkFBUyxNQUFULENBQWdCLEtBQWhCLENBQXNCLFFBQXRCLENBQStCLENBQS9CLEVBQWtDLEtBQWxDLENBQXdDLFlBQXhDLFdBQTZELFNBQTdELEVBQTBFLFlBQTFFO0FBQ0EscUJBQVMsTUFBVCxDQUFnQixLQUFoQixDQUFzQixRQUF0QixDQUErQixDQUEvQixFQUFrQyxLQUFsQyxDQUF3QyxZQUF4QyxZQUE2RCxZQUFZLEdBQXpFLEdBQWdGLFlBQWhGOztBQUVBLHFCQUFTLE1BQVQsQ0FBZ0IsS0FBaEIsQ0FBc0IsUUFBdEIsQ0FBK0IsQ0FBL0IsRUFBa0MsS0FBbEMsQ0FBd0MsWUFBeEMsV0FBNkQsU0FBN0QsRUFBMEUsWUFBMUU7QUFDQSxxQkFBUyxNQUFULENBQWdCLEtBQWhCLENBQXNCLFFBQXRCLENBQStCLENBQS9CLEVBQWtDLEtBQWxDLENBQXdDLFlBQXhDLFlBQTZELFlBQVksR0FBekUsR0FBZ0YsVUFBaEY7QUFDSCxTQS9FSTtBQWlGTCxtQkFqRksseUJBaUZTO0FBQ1YsZ0JBQUksYUFBYSxLQUFLLEtBQUwsQ0FBVyxTQUFTLE9BQVQsQ0FBaUIsY0FBakIsS0FBb0MsU0FBUyxPQUFULENBQWlCLGdCQUFqQixDQUFrQyxDQUFsQyxDQUFwQyxHQUEyRSxTQUFTLE9BQVQsQ0FBaUIsZ0JBQWpCLENBQWtDLENBQWxDLENBQTNFLEdBQWtILFNBQVMsT0FBVCxDQUFpQixnQkFBakIsQ0FBa0MsQ0FBbEMsSUFBdUMsU0FBUyxPQUFULENBQWlCLFdBQWpCLENBQTZCLElBQTdCLENBQXBLLENBQWpCO0FBQ0EsZ0JBQUksU0FBUyxLQUFLLEtBQUwsQ0FBVyxTQUFTLE9BQVQsQ0FBaUIsY0FBakIsS0FBb0MsU0FBUyxPQUFULENBQWlCLGdCQUFqQixDQUFrQyxDQUFsQyxDQUFwQyxHQUEyRSxTQUFTLE9BQVQsQ0FBaUIsZ0JBQWpCLENBQWtDLENBQWxDLENBQTNFLEdBQWtILFNBQVMsT0FBVCxDQUFpQixnQkFBakIsQ0FBa0MsQ0FBbEMsSUFBdUMsU0FBUyxPQUFULENBQWlCLFdBQWpCLENBQTZCLElBQTdCLENBQXBLLENBQWI7QUFDQSxnQkFBSSxXQUFXLEtBQUssS0FBTCxDQUFXLFNBQVMsT0FBVCxDQUFpQixjQUFqQixLQUFvQyxTQUFTLE9BQVQsQ0FBaUIsZ0JBQWpCLENBQWtDLENBQWxDLENBQXBDLEdBQTJFLFNBQVMsT0FBVCxDQUFpQixnQkFBakIsQ0FBa0MsQ0FBbEMsQ0FBM0UsR0FBa0gsU0FBUyxPQUFULENBQWlCLGdCQUFqQixDQUFrQyxDQUFsQyxDQUE3SCxDQUFmOztBQUVBLHFCQUFTLE1BQVQsQ0FBZ0IsS0FBaEIsQ0FBc0IsUUFBdEIsQ0FBK0IsQ0FBL0IsRUFBa0MsS0FBbEMsQ0FBd0MsWUFBeEMsV0FBNkQsVUFBN0QsRUFBMkUsZ0JBQTNFO0FBQ0EscUJBQVMsTUFBVCxDQUFnQixLQUFoQixDQUFzQixRQUF0QixDQUErQixDQUEvQixFQUFrQyxLQUFsQyxDQUF3QyxZQUF4QyxXQUE2RCxNQUE3RCxFQUF1RSxhQUF2RTtBQUNBLHFCQUFTLE1BQVQsQ0FBZ0IsS0FBaEIsQ0FBc0IsUUFBdEIsQ0FBK0IsQ0FBL0IsRUFBa0MsS0FBbEMsQ0FBd0MsWUFBeEMsV0FBNkQsUUFBN0QsRUFBeUUsZUFBekU7QUFDQSxxQkFBUyxNQUFULENBQWdCLEtBQWhCLENBQXNCLFFBQXRCLENBQStCLENBQS9CLEVBQWtDLEdBQWxDLENBQXNDLFlBQXRDLFdBQTJELFVBQTNELEVBQXlFLGdCQUF6RTtBQUNBLHFCQUFTLE1BQVQsQ0FBZ0IsS0FBaEIsQ0FBc0IsUUFBdEIsQ0FBK0IsQ0FBL0IsRUFBa0MsR0FBbEMsQ0FBc0MsWUFBdEMsV0FBMkQsTUFBM0QsRUFBcUUsaUJBQXJFO0FBRUgsU0E1Rkk7QUE4RkwsbUJBOUZLLHlCQThGUztBQUNWLGdCQUFJLFlBQVksS0FBSyxLQUFMLENBQVcsU0FBUyxPQUFULENBQWlCLGNBQWpCLEtBQW9DLFNBQVMsT0FBVCxDQUFpQixnQkFBakIsQ0FBa0MsQ0FBbEMsQ0FBcEMsR0FBMkUsU0FBUyxPQUFULENBQWlCLGdCQUFqQixDQUFrQyxDQUFsQyxDQUEzRSxHQUFrSCxTQUFTLE9BQVQsQ0FBaUIsZ0JBQWpCLENBQWtDLENBQWxDLENBQWxILEdBQXlKLFNBQVMsT0FBVCxDQUFpQixnQkFBakIsQ0FBa0MsQ0FBbEMsSUFBdUMsU0FBUyxPQUFULENBQWlCLFdBQWpCLENBQTZCLEdBQTdCLENBQTNNLENBQWhCO0FBQ0EsZ0JBQUksY0FBYyxZQUFZLEtBQUssS0FBTCxDQUFXLFNBQVMsT0FBVCxDQUFpQixnQkFBakIsQ0FBa0MsQ0FBbEMsSUFBdUMsU0FBUyxPQUFULENBQWlCLFdBQWpCLENBQTZCLElBQTdCLENBQWxELENBQTlCO0FBQ0EsZ0JBQUksVUFBVSxZQUFZLEtBQUssS0FBTCxDQUFXLFNBQVMsT0FBVCxDQUFpQixnQkFBakIsQ0FBa0MsQ0FBbEMsSUFBdUMsU0FBUyxPQUFULENBQWlCLFdBQWpCLENBQTZCLEdBQTdCLENBQWxELENBQTFCOztBQUVBLHFCQUFTLE1BQVQsQ0FBZ0IsS0FBaEIsQ0FBc0IsUUFBdEIsQ0FBK0IsQ0FBL0IsRUFBa0MsS0FBbEMsQ0FBd0MsWUFBeEMsV0FBNkQsU0FBN0QsRUFBMEUsWUFBMUU7QUFDQSxxQkFBUyxNQUFULENBQWdCLEtBQWhCLENBQXNCLFFBQXRCLENBQStCLENBQS9CLEVBQWtDLEtBQWxDLENBQXdDLFlBQXhDLFdBQTZELFNBQTdELEVBQTBFLFlBQTFFO0FBQ0EscUJBQVMsTUFBVCxDQUFnQixLQUFoQixDQUFzQixRQUF0QixDQUErQixDQUEvQixFQUFrQyxLQUFsQyxDQUF3QyxZQUF4QyxXQUE2RCxPQUE3RCxFQUF3RSxVQUF4RTtBQUNBLHFCQUFTLE1BQVQsQ0FBZ0IsS0FBaEIsQ0FBc0IsUUFBdEIsQ0FBK0IsQ0FBL0IsRUFBa0MsS0FBbEMsQ0FBd0MsWUFBeEMsV0FBNkQsV0FBN0QsRUFBNEUsWUFBNUU7QUFDSDtBQXZHSSxLQXJHRTs7QUErTVgsY0EvTVcsd0JBK01FOztBQUVULGVBQU8sZ0JBQVAsQ0FBd0IsUUFBeEIsRUFBa0MsWUFBTTtBQUNwQyxxQkFBUyxPQUFULENBQWlCLGNBQWpCO0FBQ0EscUJBQVMsT0FBVCxDQUFpQixRQUFqQixHQUE0QixPQUE1QixDQUFvQztBQUFBLHVCQUFRLFNBQVMsT0FBVCxDQUFpQixZQUFqQixDQUE4QixJQUE5QixDQUFSO0FBQUEsYUFBcEM7QUFDQSxxQkFBUyxPQUFULENBQWlCLElBQWpCO0FBQ0gsU0FKRDtBQUtILEtBdE5VO0FBd05YLFFBeE5XLGtCQXdOSjtBQUNILGlCQUFTLFVBQVQ7QUFDQSxpQkFBUyxPQUFULENBQWlCLGNBQWpCO0FBQ0EsaUJBQVMsT0FBVCxDQUFpQixJQUFqQjtBQUNIO0FBNU5VLENBQWY7O2tCQStOZSxROzs7Ozs7OztBQ3BPZixJQUFJLGNBQWMsU0FBZCxXQUFjLENBQUMsTUFBRCxFQUFZO0FBQzFCLFFBQUksT0FBTyxPQUFPLElBQVAsQ0FBWSxNQUFaLENBQVg7QUFDQSxXQUFPLEtBQUssR0FBTCxDQUFTO0FBQUEsZUFBTyxPQUFPLEdBQVAsQ0FBUDtBQUFBLEtBQVQsRUFBNkIsTUFBN0IsQ0FBb0M7QUFBQSxlQUFRLE9BQVEsSUFBUixJQUFpQixRQUF6QjtBQUFBLEtBQXBDLENBQVA7QUFDSCxDQUhEOztBQUtBLE9BQU8sZ0JBQVAsR0FBMkIsWUFBWTtBQUNuQyxXQUFPLE9BQU8scUJBQVAsSUFDSCxPQUFPLDJCQURKLElBRUgsT0FBTyx3QkFGSixJQUdILFVBQVUsUUFBVixFQUFvQjtBQUNoQixlQUFPLFVBQVAsQ0FBa0IsUUFBbEIsRUFBNEIsT0FBTyxFQUFuQztBQUNILEtBTEw7QUFNSCxDQVB5QixFQUExQjs7QUFTQSxTQUFTLFNBQVQsQ0FBbUIsYUFBbkIsRUFBa0MsS0FBbEMsRUFBeUMsTUFBekMsRUFBaUQ7Ozs7O0FBSzdDLFFBQUksVUFBVSxPQUFPLE9BQXJCO1FBQ0ksZ0JBQWdCLGlCQUFpQixDQURyQztRQUVJLFFBQVEsU0FBUyxJQUZyQjtRQUdJLFNBQVMsVUFBVSxhQUh2QjtRQUlJLGNBQWMsQ0FKbEI7OztBQU9BLFFBQUksT0FBTyxLQUFLLEdBQUwsQ0FBUyxFQUFULEVBQWEsS0FBSyxHQUFMLENBQVMsS0FBSyxHQUFMLENBQVMsVUFBVSxhQUFuQixJQUFvQyxLQUE3QyxFQUFvRCxFQUFwRCxDQUFiLENBQVg7OztBQUdBLFFBQUksUUFBUSxLQUFLLEVBQUwsR0FBVSxDQUF0QjtRQUNJLGtCQUFrQjtBQUNkLHFCQUFhLHFCQUFVLEdBQVYsRUFBZTtBQUN4QixtQkFBTyxLQUFLLEdBQUwsQ0FBUyxPQUFPLEtBQUssRUFBTCxHQUFVLENBQWpCLENBQVQsQ0FBUDtBQUNILFNBSGE7QUFJZCx1QkFBZSx1QkFBVSxHQUFWLEVBQWU7QUFDMUIsbUJBQVEsQ0FBQyxHQUFELElBQVEsS0FBSyxHQUFMLENBQVMsS0FBSyxFQUFMLEdBQVUsR0FBbkIsSUFBMEIsQ0FBbEMsQ0FBUjtBQUNILFNBTmE7QUFPZCx3QkFBZ0Isd0JBQVUsR0FBVixFQUFlO0FBQzNCLGdCQUFJLENBQUMsT0FBTyxHQUFSLElBQWUsQ0FBbkIsRUFBc0I7QUFDbEIsdUJBQU8sTUFBTSxLQUFLLEdBQUwsQ0FBUyxHQUFULEVBQWMsQ0FBZCxDQUFiO0FBQ0g7QUFDRCxtQkFBTyxPQUFPLEtBQUssR0FBTCxDQUFVLE1BQU0sQ0FBaEIsRUFBb0IsQ0FBcEIsSUFBeUIsQ0FBaEMsQ0FBUDtBQUNIO0FBWmEsS0FEdEI7OztBQWlCQSxhQUFTLElBQVQsR0FBZ0I7QUFDWix1QkFBZSxJQUFJLEVBQW5COztBQUVBLFlBQUksSUFBSSxjQUFjLElBQXRCO0FBQ0EsWUFBSSxJQUFJLGdCQUFnQixNQUFoQixFQUF3QixDQUF4QixDQUFSOztBQUVBLFlBQUksSUFBSSxDQUFSLEVBQVc7QUFDUCw2QkFBaUIsSUFBakI7O0FBRUEsbUJBQU8sUUFBUCxDQUFnQixDQUFoQixFQUFtQixVQUFXLENBQUMsZ0JBQWdCLE9BQWpCLElBQTRCLENBQTFEO0FBQ0gsU0FKRCxNQUlPO0FBQ0gsbUJBQU8sUUFBUCxDQUFnQixDQUFoQixFQUFtQixhQUFuQjtBQUNIO0FBQ0o7OztBQUdEO0FBQ0g7O1FBRU8sVyxHQUFBLFc7UUFBYSxTLEdBQUEsUzs7Ozs7QUNqRXJCOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFFQSxJQUFJLE9BQU8sUUFBUCxDQUFnQixRQUFoQixJQUE0QixNQUE1QixJQUFzQyxPQUFPLFFBQVAsQ0FBZ0IsUUFBaEIsSUFBNEIsR0FBdEUsRUFBMkU7QUFBQSxRQXdFbkUsYUF4RW1FOztBQUFBO0FBQUEsWUFFOUQsY0FGOEQsR0FFdkUsU0FBUyxjQUFULEdBQTBCO0FBQ3RCLGdCQUFJLFNBQVMsU0FBUyxhQUFULENBQXVCLGdCQUF2QixDQUFiO0FBQ0EsZ0JBQUksU0FBUyxTQUFTLE9BQU8sZ0JBQVAsQ0FBd0IsTUFBeEIsRUFBZ0MsSUFBaEMsRUFBc0MsTUFBL0MsQ0FBYjtBQUNBLGdCQUFJLFVBQVUsU0FBUyxPQUFPLGdCQUFQLENBQXdCLE1BQXhCLEVBQWdDLElBQWhDLEVBQXNDLFFBQS9DLElBQTJELEVBQXpFO0FBQ0EsbUJBQU8sU0FBUyxhQUFULENBQXVCLGVBQXZCLEVBQXdDLFlBQXhDLEdBQXVELE1BQXZELEdBQWdFLE9BQXZFO0FBQ0gsU0FQc0U7O0FBQUEsWUFTOUQsZ0JBVDhELEdBU3ZFLFNBQVMsZ0JBQVQsQ0FBMEIsT0FBMUIsRUFBbUM7QUFDL0IsZ0JBQUksV0FBVyxDQUFmLEVBQWtCO0FBQ2QsdUJBQU8sU0FBUyxhQUFULHlCQUE2QyxPQUE3QyxFQUF3RCxZQUEvRDtBQUNILGFBRkQsTUFHSztBQUNELHVCQUFPLENBQVA7QUFDSDtBQUNKLFNBaEJzRTs7OztBQUFBLFlBd0I5RCxpQkF4QjhELEdBd0J2RSxTQUFTLGlCQUFULENBQTJCLE9BQTNCLEVBQW9DO0FBQ2hDLGdCQUFJLFNBQVMsQ0FBYjtBQUNBLGlCQUFLLElBQUksSUFBSSxDQUFiLEVBQWdCLEtBQUssT0FBckIsRUFBOEIsR0FBOUIsRUFBbUM7QUFDL0IseUJBQVMsU0FBUyxpQkFBaUIsSUFBSSxDQUFyQixDQUFsQjtBQUNIO0FBQ0QsbUJBQU8sU0FBUyxnQkFBaEI7QUFDSCxTQTlCc0U7O0FBQUEsWUFrRDlELGNBbEQ4RCxHQWtEdkUsU0FBUyxjQUFULENBQXdCLE1BQXhCLEVBQWdDO0FBQzVCLGdCQUFJLE9BQU8sU0FBUyxhQUFULDZCQUFpRCxNQUFqRCxPQUFYO0FBQ0EsbUJBQU8sT0FBTyxnQkFBUCxDQUF3QixJQUF4QixFQUE4QixJQUE5QixFQUFvQyxLQUEzQztBQUNILFNBckRzRTs7QUFBQSxZQXVEOUQsYUF2RDhELEdBdUR2RSxTQUFTLGFBQVQsQ0FBdUIsTUFBdkIsRUFBK0I7QUFDM0IsZ0JBQUksUUFBUSwwQkFBWSxTQUFTLGdCQUFULGdCQUFaLENBQVo7QUFDQSxnQkFBSSxPQUFPLE1BQU0sR0FBTixDQUFVLFVBQUMsSUFBRCxFQUFPLEtBQVAsRUFBaUI7QUFDbEMsb0JBQUksUUFBUSxTQUFTLENBQXJCLEVBQXdCO0FBQ3BCLHdCQUFJLFFBQVEsU0FBUyxPQUFPLGdCQUFQLENBQXdCLElBQXhCLEVBQThCLElBQTlCLEVBQW9DLEtBQTdDLENBQVo7QUFDQSx3QkFBSSxTQUFTLFNBQVMsT0FBTyxnQkFBUCxDQUF3QixJQUF4QixFQUE4QixJQUE5QixFQUFvQyxXQUE3QyxDQUFiO0FBQ0EsMkJBQU8sUUFBUSxNQUFmO0FBQ0g7QUFDRCx1QkFBTyxDQUFQO0FBQ0gsYUFQVSxDQUFYO0FBUUEsbUJBQU8sS0FBSyxNQUFMLENBQVksVUFBQyxHQUFELEVBQU0sSUFBTixFQUFlO0FBQzlCLHVCQUFPLE1BQU0sSUFBYjtBQUNILGFBRk0sQ0FBUDs7QUFJQSxtQkFBTyxPQUFPLElBQWQ7QUFDSCxTQXRFc0U7O0FBb0J2RSxZQUFJLE1BQU0sU0FBUyxhQUFULENBQXVCLFlBQXZCLENBQVY7QUFDQSxZQUFJLE1BQU0sU0FBUyxhQUFULENBQXVCLGFBQXZCLENBQVY7QUFDQSxZQUFJLFFBQVEsMEJBQVksU0FBUyxnQkFBVCxDQUEwQixnQkFBMUIsQ0FBWixDQUFaOztBQVVBLFlBQUksZ0JBQUosQ0FBcUIsT0FBckIsRUFBOEIsWUFBWTtBQUN0QyxnQkFBSSxTQUFKLENBQWMsTUFBZCxDQUFxQixpQkFBckI7QUFDSCxTQUZEOztBQUlBLGNBQU0sTUFBTixDQUFhO0FBQUEsbUJBQVEsT0FBUSxJQUFSLElBQWlCLFFBQXpCO0FBQUEsU0FBYixFQUFnRCxPQUFoRCxDQUF3RCxnQkFBUTtBQUM1RCxpQkFBSyxnQkFBTCxDQUFzQixPQUF0QixFQUErQixZQUFZO0FBQ3ZDLG9CQUFJLFNBQUosQ0FBYyxNQUFkLENBQXFCLGlCQUFyQjtBQUNBLG9CQUFJLFVBQVUsS0FBSyxZQUFMLENBQWtCLGFBQWxCLENBQWQ7QUFDQSxvQkFBSSxXQUFXLENBQWYsRUFBa0I7QUFDZCw0Q0FBVSxnQkFBZ0IsR0FBMUIsRUFBK0IsSUFBL0IsRUFBcUMsZ0JBQXJDO0FBQ0gsaUJBRkQsTUFHSztBQUNELDRDQUFVLGtCQUFrQixPQUFsQixDQUFWLEVBQXNDLElBQXRDLEVBQTRDLGdCQUE1QztBQUNIO0FBRUosYUFWRDtBQVdILFNBWkQ7O0FBb0NJLHdCQUFnQixTQUFTLE9BQU8sZ0JBQVAsQ0FBd0IsU0FBUyxhQUFULENBQXVCLE1BQXZCLENBQXhCLEVBQXdELElBQXhELEVBQThELE1BQXZFLElBQWlGLE9BQU8sV0F4RXJDOzs7QUEwRXZFLFlBQUksU0FBUyxTQUFTLGFBQVQsQ0FBdUIsc0JBQXZCLENBQWI7QUFDQSxlQUFPLFlBQVA7QUFDQSxlQUFPLFlBQVAsV0FBNEIsa0JBQWtCLENBQWxCLENBQTVCLGNBQThELGVBQWUsQ0FBZixDQUE5RCxnQkFBMEYsY0FBYyxDQUFkLENBQTFGO0FBQ0EsZUFBTyxZQUFQLFdBQTRCLGtCQUFrQixDQUFsQixDQUE1QixjQUE4RCxlQUFlLENBQWYsQ0FBOUQsZ0JBQTBGLGNBQWMsQ0FBZCxDQUExRjtBQUNBLGVBQU8sWUFBUCxXQUE0QixrQkFBa0IsQ0FBbEIsQ0FBNUIsY0FBOEQsZUFBZSxDQUFmLENBQTlELGdCQUEwRixjQUFjLENBQWQsQ0FBMUY7QUFDQSxlQUFPLFlBQVAsV0FBNEIsa0JBQWtCLENBQWxCLENBQTVCLGNBQThELGVBQWUsQ0FBZixDQUE5RCxnQkFBMEYsY0FBYyxDQUFkLENBQTFGO0FBQ0EsZUFBTyxZQUFQLFdBQTRCLGtCQUFrQixDQUFsQixDQUE1QixjQUE4RCxlQUFlLENBQWYsQ0FBOUQsZ0JBQTBGLGNBQWMsQ0FBZCxDQUExRjtBQUNBLGVBQU8sWUFBUCxXQUE0QixhQUE1QixjQUF1RCxlQUFlLENBQWYsQ0FBdkQsZ0JBQW1GLGNBQWMsQ0FBZCxDQUFuRjs7QUFFQSxrQ0FBWSxTQUFTLGdCQUFULGdCQUFaLEVBQXVELE9BQXZELENBQStELFVBQUMsSUFBRCxFQUFPLEtBQVAsRUFBaUI7QUFDNUUsaUJBQUssZ0JBQUwsQ0FBc0IsT0FBdEIsRUFBK0IsWUFBTTtBQUNqQyxvQkFBSSxRQUFRLENBQVIsSUFBYSxDQUFqQixFQUFvQjtBQUNoQiw0Q0FBVSxnQkFBZ0IsR0FBMUIsRUFBK0IsSUFBL0IsRUFBcUMsZ0JBQXJDO0FBQ0gsaUJBRkQsTUFHSztBQUNELDRDQUFVLGtCQUFrQixRQUFRLENBQTFCLENBQVYsRUFBd0MsSUFBeEMsRUFBOEMsZ0JBQTlDO0FBQ0g7QUFDSixhQVBEO0FBUUgsU0FURDs7OztBQWFBLFlBQUksT0FBTyxTQUFTLGFBQVQsQ0FBdUIsc0JBQXZCLENBQVg7QUFDQSxZQUFJLFVBQVUsU0FBUyxhQUFULENBQXVCLDBCQUF2QixDQUFkO0FBQ0EsWUFBSSxXQUFXLGtFQUFmO0FBQ0EsWUFBSSxNQUFNLHdCQUFWOztBQUVBLGFBQUssZ0JBQUwsQ0FBc0IsUUFBdEIsRUFBZ0MsVUFBQyxLQUFELEVBQVc7QUFDdkMsa0JBQU0sY0FBTjtBQUNBLGdCQUFJLE9BQU8sS0FBSyxVQUFMLENBQWdCLENBQWhCLEVBQW1CLFVBQW5CLENBQThCLENBQTlCLEVBQWlDLEtBQTVDO0FBQ0EsZ0JBQUksUUFBUSxLQUFLLFVBQUwsQ0FBZ0IsQ0FBaEIsRUFBbUIsVUFBbkIsQ0FBOEIsQ0FBOUIsRUFBaUMsS0FBN0M7QUFDQSxnQkFBSSxVQUFVLEtBQUssVUFBTCxDQUFnQixDQUFoQixFQUFtQixVQUFuQixDQUE4QixDQUE5QixFQUFpQyxLQUEvQztBQUNBLDRCQUFNLElBQU4sQ0FBVyxvREFBWCxFQUFpRTtBQUM3RCx1QkFBTyxHQURzRDtBQUU3RCwyQkFBVztBQUNQLGtDQUFjLG9CQURQO0FBRVAsaUNBQWEsSUFGTjtBQUdQLCtCQUFXLCtCQUhKO0FBSVAsNkNBQXVCLElBQXZCLHdCQUE4QyxLQUE5QyxnQ0FBOEUsT0FBOUUsU0FKTztBQUtQLDBCQUFNLENBQ0Y7QUFDSSxpQ0FBUyx1QkFEYjtBQUVJLGdDQUFRO0FBRloscUJBREU7QUFMQztBQUZrRCxhQUFqRSxFQWVLLElBZkwsQ0FlVSxVQUFVLFFBQVYsRUFBb0I7QUFDdEIsd0JBQVEsR0FBUixDQUFZLFNBQVMsSUFBckI7QUFDQSx3QkFBUSxTQUFSLENBQWtCLEdBQWxCLENBQXNCLGNBQXRCO0FBQ0EsMkJBQVcsWUFBTTtBQUNiLHlCQUFLLEtBQUw7QUFDQSw0QkFBUSxTQUFSLENBQWtCLE1BQWxCLENBQXlCLGNBQXpCO0FBQ0gsaUJBSEQsRUFHRyxJQUhIO0FBSUgsYUF0Qkw7QUF1QkgsU0E1QkQ7QUFyR3VFO0FBa0kxRTs7QUFFRCxJQUFJLE9BQU8sUUFBUCxDQUFnQixRQUFoQixJQUE0QixNQUFoQyxFQUF3QztBQUNwQyxrQkFBSSxJQUFKO0FBQ0Esc0JBQVEsSUFBUjtBQUNBLGtCQUFLLElBQUw7QUFDQSx1QkFBUyxJQUFUO0FBQ0g7O0FBRUQsSUFBSSxDQUFDLE9BQU8sUUFBUCxDQUFnQixRQUFoQixJQUE0QixNQUE1QixJQUFxQyxPQUFPLFFBQVAsQ0FBZ0IsUUFBaEIsSUFBNEIsR0FBbEUsS0FBMEUsT0FBTyxVQUFQLEdBQW9CLEdBQWxHLEVBQXVHO0FBQ25HLHVCQUFTLElBQVQ7QUFDSDs7Ozs7Ozs7O0FDdEpEOztBQUVBLElBQUksTUFBTTtBQUNOLFlBQVE7QUFDSixlQUFPO0FBQ0gsaUJBQUssU0FBUyxhQUFULENBQXVCLHFCQUF2QixDQURGO0FBRUgsbUJBQU8sMEJBQVksU0FBUyxnQkFBVCxvQ0FBWixDQUZKO0FBR0gsb0JBQVEsU0FBUyxhQUFULENBQXVCLGFBQXZCLENBSEw7QUFJSCx1QkFBVyxTQUFTLGFBQVQsQ0FBdUIsd0JBQXZCLENBSlI7QUFLSCx5QkFBYSwwQkFBWSxTQUFTLGdCQUFULENBQTBCLHVCQUExQixDQUFaO0FBTFYsU0FESDtBQVFKLGtCQUFVO0FBQ04sbUJBQU8sU0FERDtBQUVOLGtCQUFNLFNBRkE7QUFHTixzQkFBVSxTQUhKO0FBSU4sdUJBQVc7QUFKTCxTQVJOO0FBY0osYUFBSztBQWRELEtBREY7O0FBa0JOLFFBbEJNLGtCQWtCQztBQUNILFlBQUksT0FBTyxRQUFQLENBQWdCLFFBQWhCLElBQTRCLE1BQWhDLEVBQXdDO0FBQ3BDLGdCQUFJLE9BQUosQ0FBWSxXQUFaO0FBQ0EsZ0JBQUksVUFBSjtBQUNIO0FBQ0osS0F2Qks7QUF5Qk4sY0F6Qk0sd0JBeUJPOzs7QUFHVCxpQkFBUyxnQkFBVCxDQUEwQixRQUExQixFQUFvQyxZQUFNO0FBQ3RDLGdCQUFJLE9BQUosQ0FBWSxNQUFaO0FBQ0EsZ0JBQUksT0FBSixDQUFZLFdBQVo7QUFDQSxnQkFBSSxPQUFKLENBQVksS0FBWjtBQUNBLGdCQUFJLE9BQUosQ0FBWSxTQUFaO0FBQ0gsU0FMRDs7O0FBUUEsWUFBSSxNQUFKLENBQVcsS0FBWCxDQUFpQixLQUFqQixDQUF1QixPQUF2QixDQUErQixVQUFDLElBQUQsRUFBTyxLQUFQLEVBQWlCO0FBQzVDLGlCQUFLLGdCQUFMLENBQXNCLE9BQXRCLEVBQStCLFlBQU07QUFDakMsb0JBQUksT0FBSixDQUFZLE1BQVosQ0FBbUIsUUFBUSxDQUEzQjtBQUNILGFBRkQ7QUFHSCxTQUpEOzs7QUFPQSxZQUFJLE1BQUosQ0FBVyxLQUFYLENBQWlCLE1BQWpCLENBQXdCLGdCQUF4QixDQUF5QyxPQUF6QyxFQUFrRCxVQUFDLEtBQUQsRUFBVztBQUN6RCxnQkFBSSxNQUFKLENBQVcsS0FBWCxDQUFpQixNQUFqQixDQUF3QixTQUF4QixDQUFrQyxNQUFsQyxDQUF5QyxpQkFBekM7QUFDSCxTQUZEOzs7QUFLQSxZQUFJLE1BQUosQ0FBVyxLQUFYLENBQWlCLFdBQWpCLENBQTZCLE1BQTdCLENBQW9DO0FBQUEsbUJBQVEsT0FBUSxJQUFSLElBQWlCLFFBQXpCO0FBQUEsU0FBcEMsRUFBdUUsT0FBdkUsQ0FBK0UsVUFBQyxJQUFELEVBQU8sS0FBUCxFQUFpQjtBQUM1RixpQkFBSyxnQkFBTCxDQUFzQixPQUF0QixFQUErQixZQUFNO0FBQ2pDLG9CQUFJLE9BQUosQ0FBWSxNQUFaLENBQW1CLFFBQVEsQ0FBM0I7QUFDSCxhQUZEO0FBR0gsU0FKRDtBQUtILEtBckRLOzs7QUF1RE4sYUFBUztBQUVMLGNBRkssb0JBRUk7QUFDTCxnQkFBSSxPQUFPLFdBQVAsR0FBcUIsQ0FBekIsRUFBNEI7QUFDeEIsb0JBQUksTUFBSixDQUFXLEdBQVgsR0FBaUIsT0FBTyxXQUF4QjtBQUNILGFBRkQsTUFHSyxJQUFJLFNBQVMsZUFBYixFQUE4QjtBQUMvQixvQkFBSSxNQUFKLENBQVcsR0FBWCxHQUFpQixTQUFTLGVBQVQsQ0FBeUIsU0FBMUM7QUFDSCxhQUZJLE1BR0EsSUFBSSxTQUFTLElBQWIsRUFBbUI7QUFDcEIsb0JBQUksTUFBSixDQUFXLEdBQVgsR0FBaUIsU0FBUyxJQUFULENBQWMsU0FBL0I7QUFDSCxhQUZJLE1BR0EsSUFBSSxTQUFTLElBQVQsQ0FBYyxVQUFsQixFQUE4QjtBQUMvQixvQkFBSSxNQUFKLENBQVcsR0FBWCxHQUFpQixTQUFTLElBQVQsQ0FBYyxVQUFkLENBQXlCLFNBQTFDO0FBQ0g7QUFDSixTQWZJO0FBaUJMLG1CQWpCSyx5QkFpQlM7QUFDVixnQkFBSSxNQUFKLENBQVcsUUFBWCxDQUFvQixLQUFwQixHQUE0QixTQUFTLGFBQVQsQ0FBdUIsd0JBQXZCLEVBQWlELFlBQWpELEdBQWdFLEdBQTVGLEVBQ0ksSUFBSSxNQUFKLENBQVcsUUFBWCxDQUFvQixJQUFwQixHQUEyQixTQUFTLGFBQVQsQ0FBdUIsZ0NBQXZCLEVBQXlELFlBRHhGLEVBRUksSUFBSSxNQUFKLENBQVcsUUFBWCxDQUFvQixRQUFwQixHQUErQixJQUFJLE1BQUosQ0FBVyxRQUFYLENBQW9CLElBQXBCLEdBQTJCLFNBQVMsYUFBVCxDQUF1QixpQ0FBdkIsRUFBMEQsWUFGeEgsRUFHSSxJQUFJLE1BQUosQ0FBVyxRQUFYLENBQW9CLFNBQXBCLEdBQWdDLElBQUksTUFBSixDQUFXLFFBQVgsQ0FBb0IsUUFBcEIsR0FBK0IsU0FBUyxhQUFULENBQXVCLGtDQUF2QixFQUEyRCxZQUg5SDtBQUlILFNBdEJJO0FBd0JMLGFBeEJLLG1CQXdCRztBQUNKLGdCQUFJLElBQUksTUFBSixDQUFXLEdBQVgsR0FBaUIsSUFBSSxNQUFKLENBQVcsUUFBWCxDQUFvQixLQUF6QyxFQUFnRDtBQUM1QyxvQkFBSSxDQUFDLElBQUksTUFBSixDQUFXLEtBQVgsQ0FBaUIsR0FBakIsQ0FBcUIsU0FBckIsQ0FBK0IsUUFBL0IsQ0FBd0MsV0FBeEMsQ0FBTCxFQUEyRDtBQUN2RCx3QkFBSSxNQUFKLENBQVcsS0FBWCxDQUFpQixHQUFqQixDQUFxQixTQUFyQixDQUErQixHQUEvQixDQUFtQyxXQUFuQztBQUNIO0FBQ0osYUFKRCxNQUtLO0FBQ0Qsb0JBQUksTUFBSixDQUFXLEtBQVgsQ0FBaUIsR0FBakIsQ0FBcUIsU0FBckIsQ0FBK0IsTUFBL0IsQ0FBc0MsV0FBdEM7QUFDSDtBQUNKLFNBakNJO0FBbUNMLGlCQW5DSyx1QkFtQ087O0FBRVIsZ0JBQUksSUFBSSxNQUFKLENBQVcsR0FBWCxHQUFpQixJQUFJLE1BQUosQ0FBVyxRQUFYLENBQW9CLElBQXpDLEVBQStDO0FBQzNDLHlCQUFTLENBQVQ7QUFDSCxhQUZELE1BR0ssSUFBSSxJQUFJLE1BQUosQ0FBVyxHQUFYLElBQWtCLElBQUksTUFBSixDQUFXLFFBQVgsQ0FBb0IsSUFBdEMsSUFBOEMsSUFBSSxNQUFKLENBQVcsR0FBWCxHQUFpQixJQUFJLE1BQUosQ0FBVyxRQUFYLENBQW9CLFFBQXZGLEVBQWlHO0FBQ2xHLHlCQUFTLENBQVQ7QUFDSCxhQUZJLE1BR0EsSUFBSSxJQUFJLE1BQUosQ0FBVyxHQUFYLElBQWtCLElBQUksTUFBSixDQUFXLFFBQVgsQ0FBb0IsUUFBdEMsSUFBa0QsSUFBSSxNQUFKLENBQVcsR0FBWCxHQUFpQixJQUFJLE1BQUosQ0FBVyxRQUFYLENBQW9CLFNBQTNGLEVBQXNHO0FBQ3ZHLHlCQUFTLENBQVQ7QUFDSCxhQUZJLE1BR0EsSUFBSSxJQUFJLE1BQUosQ0FBVyxHQUFYLElBQWtCLElBQUksTUFBSixDQUFXLFFBQVgsQ0FBb0IsU0FBMUMsRUFBcUQ7QUFDdEQseUJBQVMsQ0FBVDtBQUNIOztBQUVELHFCQUFTLFFBQVQsQ0FBa0IsS0FBbEIsRUFBeUI7QUFDckIsb0JBQUksTUFBSixDQUFXLEtBQVgsQ0FBaUIsS0FBakIsQ0FBdUIsT0FBdkIsQ0FBK0IsZ0JBQVE7QUFDbkMseUJBQUssU0FBTCxDQUFlLE1BQWYsQ0FBc0IsUUFBdEI7QUFDSCxpQkFGRDtBQUdBLHlCQUFTLGFBQVQsaURBQXFFLEtBQXJFLFFBQStFLFNBQS9FLENBQXlGLEdBQXpGLENBQTZGLFFBQTdGO0FBQ0g7QUFDSixTQXhESTtBQTBETCxjQTFESyxrQkEwREUsS0ExREYsRUEwRFM7QUFDVixvQkFBUSxLQUFSO0FBQ0kscUJBQUssQ0FBTDtBQUNJLDJCQUFPLENBQVA7QUFDQTtBQUNKLHFCQUFLLENBQUw7QUFDSSwyQkFBTyxJQUFJLE1BQUosQ0FBVyxRQUFYLENBQW9CLElBQTNCO0FBQ0E7QUFDSixxQkFBSyxDQUFMO0FBQ0ksMkJBQU8sSUFBSSxNQUFKLENBQVcsUUFBWCxDQUFvQixRQUEzQjtBQUNBO0FBQ0oscUJBQUssQ0FBTDtBQUNJLDJCQUFPLElBQUksTUFBSixDQUFXLFFBQVgsQ0FBb0IsU0FBM0I7QUFDQTtBQVpSOztBQWVBLHFCQUFTLE1BQVQsQ0FBZ0IsR0FBaEIsRUFBcUI7QUFDakIsd0NBQVUsR0FBVixFQUFlLElBQWYsRUFBcUIsZ0JBQXJCO0FBQ0g7QUFDSjtBQTdFSTtBQXZESCxDQUFWOztrQkF3SWUsRzs7Ozs7O0FDeklmLENBQUMsVUFBUyxDQUFULEVBQVcsQ0FBWCxFQUFhLENBQWIsRUFBZTtBQUFDO0FBQWEsV0FBUyxDQUFULENBQVcsQ0FBWCxFQUFhO0FBQUMsUUFBRyxJQUFFLEVBQUUsZUFBSixFQUFvQixJQUFFLEVBQUUsSUFBeEIsRUFBNkIsR0FBN0IsRUFBaUMsS0FBRyxJQUFwQyxFQUF5QyxJQUFFLEtBQUcsRUFBOUMsRUFBaUQsS0FBRyxFQUFFLFNBQUYsSUFBYSxFQUFqRSxFQUFvRSxFQUFFLE1BQXpFLEVBQWdGLEtBQUksSUFBSSxDQUFSLElBQWEsRUFBRSxNQUFmO0FBQXNCLFFBQUUsQ0FBRixJQUFLLEVBQUUsTUFBRixDQUFTLENBQVQsQ0FBTDtBQUF0QixLQUF1QyxLQUFHLEVBQUUsWUFBRixJQUFnQixLQUFuQixFQUF5QixLQUFHLEVBQUMsY0FBYSxFQUFFLFlBQWhCLEVBQTZCLFFBQU8sRUFBRSxNQUF0QyxFQUE2QyxVQUFTLEVBQUUsUUFBeEQsRUFBNUIsRUFBOEYsS0FBRyxFQUFFLFdBQUYsS0FBZ0IsQ0FBQyxDQUFsSCxFQUFvSCxPQUFLLEtBQUcsRUFBRSxLQUFGLElBQVMsQ0FBakIsQ0FBcEgsRUFBd0ksS0FBRyxFQUFFLGtCQUFGLElBQXNCLENBQWpLLEVBQW1LLEtBQUcsRUFBRSxlQUFGLEtBQW9CLENBQUMsQ0FBM0wsRUFBNkwsS0FBRyxFQUFFLHVCQUFGLElBQTJCLENBQTNOLEVBQTZOLEtBQUcsRUFBQyxXQUFVLEdBQUcsWUFBSCxFQUFYLEVBQWhPLEVBQThQLEtBQUcsQ0FBQyxFQUFFLFdBQUYsSUFBZSxZQUFVO0FBQUMsYUFBTSx3Q0FBdUMsSUFBdkMsQ0FBNEMsVUFBVSxTQUFWLElBQXFCLFVBQVUsTUFBL0IsSUFBdUMsRUFBRSxLQUFyRjtBQUFOO0FBQWtHLEtBQTdILEdBQWpRLEVBQWtZLE1BQUksS0FBRyxFQUFFLGNBQUYsQ0FBaUIsRUFBRSxXQUFGLElBQWUsQ0FBaEMsQ0FBSCxFQUFzQyxNQUFJLElBQTFDLEVBQStDLEdBQS9DLEVBQW1ELEdBQUcsQ0FBSCxFQUFLLENBQUMsQ0FBRCxFQUFHLENBQUgsQ0FBTCxFQUFXLENBQUMsQ0FBRCxDQUFYLENBQXZELElBQXdFLEdBQUcsQ0FBSCxFQUFLLENBQUMsQ0FBRCxFQUFHLENBQUgsQ0FBTCxFQUFXLENBQUMsQ0FBRCxDQUFYLENBQTFjLEVBQTBkLEdBQUcsT0FBSCxFQUExZCxFQUF1ZSxHQUFHLENBQUgsRUFBSywwQkFBTCxFQUFnQyxZQUFVO0FBQUMsVUFBSSxJQUFFLEVBQUUsV0FBUjtVQUFvQixJQUFFLEVBQUUsWUFBeEIsQ0FBcUMsQ0FBQyxNQUFJLEVBQUosSUFBUSxNQUFJLEVBQWIsTUFBbUIsS0FBRyxDQUFILEVBQUssS0FBRyxDQUFSLEVBQVUsS0FBRyxDQUFDLENBQWpDO0FBQW9DLEtBQXBILENBQXZlLENBQTZsQixJQUFJLElBQUUsR0FBTixDQUFVLE9BQU8sU0FBUyxDQUFULEdBQVk7QUFBQyxXQUFJLEtBQUcsRUFBRSxDQUFGLENBQVA7QUFBWSxLQUF6QixJQUE0QixFQUFuQztBQUFzQyxPQUFJLENBQUo7TUFBTSxDQUFOO01BQVEsSUFBRSxFQUFDLEtBQUksZUFBVTtBQUFDLGFBQU8sRUFBUDtBQUFVLEtBQTFCLEVBQTJCLE1BQUssY0FBUyxDQUFULEVBQVc7QUFBQyxhQUFPLE1BQUksSUFBSSxDQUFKLENBQU0sQ0FBTixDQUFYO0FBQW9CLEtBQWhFLEVBQWlFLFNBQVEsUUFBekUsRUFBVjtNQUE2RixJQUFFLE9BQU8sU0FBUCxDQUFpQixjQUFoSDtNQUErSCxJQUFFLEVBQUUsSUFBbkk7TUFBd0ksSUFBRSxFQUFFLGdCQUE1STtNQUE2SixJQUFFLFlBQS9KO01BQTRLLElBQUUsV0FBOUs7TUFBMEwsSUFBRSxhQUE1TDtNQUEwTSxJQUFFLFVBQTVNO01BQXVOLElBQUUsWUFBek47TUFBc08sSUFBRSxJQUFFLFNBQTFPO01BQW9QLElBQUUsSUFBRSxVQUF4UDtNQUFtUSxJQUFFLElBQUUsUUFBdlE7TUFBZ1IsSUFBRSxTQUFsUjtNQUE0UixJQUFFLFFBQU0sQ0FBcFM7TUFBc1MsSUFBRSxJQUFFLFVBQTFTO01BQXFULElBQUUsSUFBRSxTQUF6VDtNQUFtVSxJQUFFLFFBQXJVO01BQThVLElBQUUsR0FBaFY7TUFBb1YsSUFBRSxJQUF0VjtNQUEyVixJQUFFLGNBQTdWO01BQTRXLElBQUUsR0FBOVc7TUFBa1gsSUFBRSxPQUFwWDtNQUE0WCxJQUFFLEtBQTlYO01BQW9ZLElBQUUsUUFBdFk7TUFBK1ksSUFBRSxRQUFqWjtNQUEwWixJQUFFLGtCQUE1WjtNQUErYSxJQUFFLHFDQUFqYjtNQUF1ZCxJQUFFLFlBQXpkO01BQXNlLElBQUUsd0dBQXhlO01BQWlsQixJQUFFLDRDQUFubEI7TUFBZ29CLElBQUUseUJBQWxvQjtNQUE0cEIsSUFBRSxlQUE5cEI7TUFBOHFCLElBQUUsU0FBRixDQUFFLENBQVMsQ0FBVCxFQUFXLENBQVgsRUFBYTtBQUFDLFdBQU8sRUFBRSxXQUFGLEVBQVA7QUFBdUIsR0FBcnRCO01BQXN0QixJQUFFLHNCQUF4dEI7TUFBK3VCLElBQUUsU0FBanZCO01BQTJ2QixJQUFFLDBDQUE3dkI7TUFBd3lCLElBQUUsb0JBQTF5QjtNQUErekIsSUFBRSxFQUFqMEI7TUFBbzBCLElBQUUsRUFBdDBCO01BQXkwQixJQUFFLFNBQUYsQ0FBRSxHQUFVO0FBQUMsUUFBSSxJQUFFLGdEQUFOLENBQXVELElBQUcsQ0FBSCxFQUFLO0FBQUMsVUFBSSxJQUFFLEVBQUUsQ0FBRixFQUFJLElBQUosQ0FBTixDQUFnQixLQUFJLElBQUksQ0FBUixJQUFhLENBQWI7QUFBZSxZQUFHLElBQUUsRUFBRSxLQUFGLENBQVEsQ0FBUixLQUFZLENBQUMsQ0FBRCxJQUFJLENBQUosSUFBTyxFQUFFLENBQUYsRUFBSyxLQUFMLENBQVcsQ0FBWCxDQUF4QixFQUFzQztBQUFyRCxPQUEyRCxJQUFHLENBQUMsQ0FBSixFQUFNLE9BQU8sTUFBSyxJQUFFLElBQUUsRUFBVCxDQUFQLENBQW9CLElBQUUsRUFBRSxDQUFGLENBQUYsRUFBTyxRQUFNLEVBQUUsS0FBRixDQUFRLENBQVIsRUFBVSxDQUFWLENBQU4sSUFBb0IsSUFBRSxDQUFGLEVBQUksSUFBRSxFQUFDLFlBQVcsUUFBWixFQUFxQixTQUFRLEtBQTdCLEVBQW1DLFFBQU8sSUFBMUMsRUFBK0MsT0FBTSxHQUFyRCxHQUEwRCxDQUExRCxDQUExQixJQUF3RixJQUFFLE1BQUksRUFBRSxXQUFGLEVBQUosR0FBb0IsR0FBckg7QUFBeUg7QUFBQyxHQUFsbkM7TUFBbW5DLElBQUUsU0FBRixDQUFFLEdBQVU7QUFBQyxRQUFJLElBQUUsRUFBRSxxQkFBRixJQUF5QixFQUFFLEVBQUUsV0FBRixLQUFnQix1QkFBbEIsQ0FBL0I7UUFBMEUsSUFBRSxJQUE1RSxDQUFpRixPQUFNLENBQUMsTUFBSSxDQUFDLENBQU4sTUFBVyxJQUFFLFdBQVMsRUFBVCxFQUFXO0FBQUMsVUFBSSxJQUFFLE9BQUssQ0FBWDtVQUFhLElBQUUsRUFBRSxHQUFGLENBQU0sQ0FBTixFQUFRLE1BQUksRUFBSixHQUFPLENBQWYsQ0FBZixDQUFpQyxPQUFPLEVBQUUsVUFBRixDQUFhLFlBQVU7QUFBQyxZQUFFLElBQUYsRUFBTyxJQUFQO0FBQVcsT0FBbkMsRUFBb0MsQ0FBcEMsQ0FBUDtBQUE4QyxLQUF4RyxHQUEwRyxDQUFoSDtBQUFrSCxHQUFuMEM7TUFBbzBDLElBQUUsU0FBRixDQUFFLEdBQVU7QUFBQyxRQUFJLElBQUUsRUFBRSxvQkFBRixJQUF3QixFQUFFLEVBQUUsV0FBRixLQUFnQixzQkFBbEIsQ0FBOUIsQ0FBd0UsT0FBTSxDQUFDLE1BQUksQ0FBQyxDQUFOLE1BQVcsSUFBRSxXQUFTLEdBQVQsRUFBVztBQUFDLGFBQU8sRUFBRSxZQUFGLENBQWUsR0FBZixDQUFQO0FBQXlCLEtBQWxELEdBQW9ELENBQTFEO0FBQTRELEdBQXI5QztNQUFzOUMsSUFBRSxFQUFDLE9BQU0saUJBQVU7QUFBQyxhQUFPLENBQVA7QUFBUyxLQUEzQixFQUE0QixLQUFJLGVBQVU7QUFBQyxhQUFPLENBQVA7QUFBUyxLQUFwRCxFQUFxRCxRQUFPLGdCQUFTLENBQVQsRUFBVztBQUFDLGFBQU8sQ0FBUDtBQUFTLEtBQWpGLEVBQWtGLFdBQVUsbUJBQVMsQ0FBVCxFQUFXO0FBQUMsYUFBTyxJQUFFLENBQVQ7QUFBVyxLQUFuSCxFQUFvSCxPQUFNLGVBQVMsQ0FBVCxFQUFXO0FBQUMsYUFBTyxJQUFFLENBQUYsR0FBSSxDQUFYO0FBQWEsS0FBbkosRUFBb0osT0FBTSxlQUFTLENBQVQsRUFBVztBQUFDLGFBQU0sQ0FBQyxFQUFFLEdBQUYsQ0FBTSxJQUFFLEVBQUUsRUFBVixDQUFELEdBQWUsQ0FBZixHQUFpQixFQUF2QjtBQUEwQixLQUFoTSxFQUFpTSxNQUFLLGNBQVMsQ0FBVCxFQUFXO0FBQUMsYUFBTyxFQUFFLElBQUYsQ0FBTyxDQUFQLENBQVA7QUFBaUIsS0FBbk8sRUFBb08sVUFBUyxrQkFBUyxDQUFULEVBQVc7QUFBQyxhQUFPLEVBQUUsR0FBRixDQUFNLElBQUUsQ0FBUixFQUFVLENBQVYsSUFBYSxDQUFwQjtBQUFzQixLQUEvUSxFQUFnUixRQUFPLGdCQUFTLENBQVQsRUFBVztBQUFDLFVBQUksQ0FBSixDQUFNLElBQUcsU0FBTyxDQUFWLEVBQVksSUFBRSxDQUFGLENBQVosS0FBcUIsSUFBRyxTQUFPLENBQVYsRUFBWSxJQUFFLENBQUYsQ0FBWixLQUFxQixJQUFHLFVBQVEsQ0FBWCxFQUFhLElBQUUsRUFBRixDQUFiLEtBQXNCO0FBQUMsWUFBRyxFQUFFLFVBQVEsQ0FBVixDQUFILEVBQWdCLE9BQU8sQ0FBUCxDQUFTLElBQUUsRUFBRjtBQUFLLGNBQU8sSUFBRSxFQUFFLEdBQUYsQ0FBTSxJQUFFLEVBQUUsR0FBRixDQUFNLElBQUUsQ0FBRixHQUFJLEtBQVYsQ0FBRixHQUFtQixDQUF6QixDQUFUO0FBQXFDLEtBQTdhLEVBQXg5QyxDQUF1NEQsRUFBRSxTQUFGLENBQVksT0FBWixHQUFvQixVQUFTLENBQVQsRUFBVztBQUFDLFFBQUksQ0FBSjtRQUFNLENBQU47UUFBUSxJQUFFLENBQUMsQ0FBWCxDQUFhLEtBQUksTUFBSSxDQUFKLElBQU8sSUFBRSxDQUFDLENBQUgsRUFBSyxLQUFHLEVBQVIsRUFBVyxLQUFHLENBQWQsRUFBZ0IsSUFBRSxFQUFFLG9CQUFGLENBQXVCLEdBQXZCLENBQXpCLElBQXNELEVBQUUsTUFBRixLQUFXLENBQVgsS0FBZSxJQUFFLENBQUMsQ0FBRCxDQUFqQixDQUF0RCxFQUE0RSxJQUFFLENBQTlFLEVBQWdGLElBQUUsRUFBRSxNQUF4RixFQUErRixJQUFFLENBQWpHLEVBQW1HLEdBQW5HLEVBQXVHO0FBQUMsVUFBSSxJQUFFLEVBQUUsQ0FBRixDQUFOO1VBQVcsSUFBRSxDQUFiO1VBQWUsSUFBRSxFQUFqQjtVQUFvQixJQUFFLEVBQXRCO1VBQXlCLElBQUUsRUFBM0I7VUFBOEIsSUFBRSxDQUFDLENBQWpDLENBQW1DLElBQUcsS0FBRyxLQUFLLENBQVIsSUFBVyxPQUFPLEVBQUUsQ0FBRixDQUFsQixFQUF1QixFQUFFLFVBQTVCLEVBQXVDO0FBQUMsYUFBSSxJQUFJLElBQUUsQ0FBTixFQUFRLElBQUUsRUFBRSxVQUFGLENBQWEsTUFBM0IsRUFBa0MsSUFBRSxDQUFwQyxFQUFzQyxHQUF0QyxFQUEwQztBQUFDLGNBQUksSUFBRSxFQUFFLFVBQUYsQ0FBYSxDQUFiLENBQU4sQ0FBc0IsSUFBRyx5QkFBdUIsRUFBRSxJQUE1QjtBQUFpQyxnQkFBRyw0QkFBMEIsRUFBRSxJQUEvQjtBQUFvQyxrQkFBRyx5QkFBdUIsRUFBRSxJQUE1QjtBQUFpQyxvQkFBRyx1QkFBcUIsRUFBRSxJQUExQixFQUErQjtBQUFDLHNCQUFJLElBQUUsRUFBRSxJQUFGLENBQU8sS0FBUCxDQUFhLENBQWIsQ0FBTixDQUFzQixJQUFHLFNBQU8sQ0FBVixFQUFZO0FBQUMsd0JBQUksSUFBRSxFQUFDLE9BQU0sRUFBRSxLQUFULEVBQWUsU0FBUSxDQUF2QixFQUF5QixXQUFVLEVBQUUsSUFBRixDQUFPLE9BQVAsQ0FBZSxDQUFmLEVBQWlCLENBQWpCLENBQW5DLEVBQU4sQ0FBOEQsRUFBRSxJQUFGLENBQU8sQ0FBUCxFQUFVLElBQUksSUFBRSxFQUFFLENBQUYsQ0FBTixDQUFXLE1BQUksRUFBRSxRQUFGLEdBQVcsRUFBRSxNQUFGLENBQVMsQ0FBVCxDQUFmLEVBQTRCLElBQUksSUFBRSxFQUFFLENBQUYsQ0FBTixDQUFXLEtBQUssSUFBTCxDQUFVLENBQVYsS0FBYyxFQUFFLFlBQUYsR0FBZSxDQUFDLENBQWhCLEVBQWtCLEVBQUUsTUFBRixHQUFTLENBQUMsSUFBRSxFQUFFLEtBQUYsQ0FBUSxDQUFSLEVBQVUsQ0FBQyxDQUFYLENBQUgsSUFBa0IsR0FBM0QsSUFBZ0UsRUFBRSxNQUFGLEdBQVMsSUFBRSxDQUEzRSxDQUE2RSxJQUFJLElBQUUsRUFBRSxDQUFGLENBQU47d0JBQVcsSUFBRSxFQUFFLENBQUYsS0FBTSxDQUFuQixDQUFxQixLQUFHLE1BQUksQ0FBUCxJQUFVLE1BQUksQ0FBZCxJQUFpQixFQUFFLElBQUYsR0FBTyxVQUFQLEVBQWtCLEVBQUUsT0FBRixHQUFVLENBQUMsQ0FBRCxFQUFHLENBQUgsQ0FBN0MsS0FBcUQsRUFBRSxJQUFGLEdBQU8sVUFBUCxFQUFrQixNQUFJLENBQUosR0FBTSxFQUFFLEtBQUYsR0FBUSxDQUFDLENBQWYsR0FBaUIsRUFBRSxZQUFGLEtBQWlCLEVBQUUsTUFBRixHQUFTLEVBQUUsTUFBRixHQUFTLEVBQW5DLENBQXhGO0FBQWdJO0FBQUMsaUJBQWhhLE1BQXFhLElBQUUsQ0FBQyxDQUFIO0FBQXRjLHFCQUFnZCxJQUFFLEVBQUUsS0FBSjtBQUFwZixtQkFBbWdCLElBQUUsVUFBUSxFQUFFLEtBQVo7QUFBcGlCLGlCQUEyakIsSUFBRyxJQUFFLEVBQUUsYUFBRixDQUFnQixFQUFFLEtBQWxCLENBQUYsRUFBMkIsU0FBTyxDQUFyQyxFQUF1QyxNQUFLLG1DQUFpQyxFQUFFLEtBQW5DLEdBQXlDLEdBQTlDO0FBQWtELGFBQUcsRUFBRSxNQUFMLEVBQVk7QUFBQyxjQUFJLENBQUosRUFBTSxDQUFOLEVBQVEsQ0FBUixDQUFVLENBQUMsQ0FBRCxJQUFJLEtBQUssQ0FBVCxJQUFZLElBQUUsRUFBRSxDQUFGLENBQUYsRUFBTyxJQUFFLEdBQUcsQ0FBSCxFQUFNLFNBQWYsRUFBeUIsSUFBRSxHQUFHLENBQUgsRUFBTSxTQUE3QyxLQUF5RCxJQUFFLEVBQUUsQ0FBRixJQUFLLElBQVAsRUFBWSxJQUFFLEVBQUUsS0FBRixDQUFRLE9BQXRCLEVBQThCLElBQUUsR0FBRyxDQUFILENBQXpGLEdBQWdHLEdBQUcsQ0FBSCxJQUFNLEVBQUMsU0FBUSxDQUFULEVBQVcsV0FBVSxDQUFyQixFQUF1QixXQUFVLENBQWpDLEVBQW1DLGNBQWEsQ0FBaEQsRUFBa0QsV0FBVSxDQUE1RCxFQUE4RCxpQkFBZ0IsQ0FBOUUsRUFBZ0YsY0FBYSxDQUE3RixFQUErRixZQUFXLENBQTFHLEVBQTRHLGdCQUFlLENBQUMsQ0FBNUgsRUFBdEcsRUFBcU8sR0FBRyxDQUFILEVBQUssQ0FBQyxDQUFELENBQUwsRUFBUyxFQUFULENBQXJPO0FBQWtQO0FBQUM7QUFBQyxVQUFJLE1BQUssSUFBRSxDQUFQLEVBQVMsSUFBRSxFQUFFLE1BQWpCLEVBQXdCLElBQUUsQ0FBMUIsRUFBNEIsR0FBNUIsRUFBZ0M7QUFBQyxVQUFJLElBQUUsR0FBRyxFQUFFLENBQUYsRUFBSyxDQUFMLENBQUgsQ0FBTixDQUFrQixNQUFJLENBQUosS0FBUSxFQUFFLENBQUYsR0FBSyxHQUFHLENBQUgsQ0FBYjtBQUFvQixZQUFPLEVBQVA7QUFBVSxHQUFqeEMsRUFBa3hDLEVBQUUsU0FBRixDQUFZLGtCQUFaLEdBQStCLFVBQVMsQ0FBVCxFQUFXLENBQVgsRUFBYSxDQUFiLEVBQWU7QUFBQyxRQUFJLElBQUUsRUFBRSxZQUFSO1FBQXFCLElBQUUsRUFBRSxxQkFBRixFQUF2QjtRQUFpRCxJQUFFLEVBQUUsR0FBckQ7UUFBeUQsSUFBRSxFQUFFLE1BQUYsR0FBUyxFQUFFLEdBQXRFLENBQTBFLE9BQU8sTUFBSSxDQUFKLEdBQU0sS0FBRyxDQUFULEdBQVcsTUFBSSxDQUFKLEtBQVEsS0FBRyxJQUFFLENBQWIsQ0FBWCxFQUEyQixNQUFJLENBQUosR0FBTSxLQUFHLENBQVQsR0FBVyxNQUFJLENBQUosS0FBUSxLQUFHLElBQUUsQ0FBYixDQUF0QyxFQUFzRCxLQUFHLEdBQUcsWUFBSCxFQUF6RCxFQUEyRSxJQUFFLEVBQUYsR0FBSyxDQUF2RjtBQUF5RixHQUFwK0MsRUFBcStDLEVBQUUsU0FBRixDQUFZLFNBQVosR0FBc0IsVUFBUyxDQUFULEVBQVcsQ0FBWCxFQUFhO0FBQUMsUUFBRSxLQUFHLEVBQUwsQ0FBUSxJQUFJLElBQUUsSUFBTjtRQUFXLElBQUUsR0FBRyxZQUFILEVBQWI7UUFBK0IsSUFBRSxFQUFFLFFBQUYsS0FBYSxDQUFiLEdBQWUsQ0FBZixHQUFpQixFQUFFLFFBQXBELENBQTZELE9BQU8sS0FBRyxFQUFDLFVBQVMsQ0FBVixFQUFZLFNBQVEsSUFBRSxDQUF0QixFQUF3QixXQUFVLENBQWxDLEVBQW9DLFVBQVMsQ0FBN0MsRUFBK0MsV0FBVSxDQUF6RCxFQUEyRCxTQUFRLElBQUUsQ0FBckUsRUFBdUUsUUFBTyxFQUFFLEVBQUUsTUFBRixJQUFVLENBQVosQ0FBOUUsRUFBNkYsTUFBSyxFQUFFLElBQXBHLEVBQUgsRUFBNkcsR0FBRyxPQUFILEtBQWEsR0FBRyxJQUFILElBQVMsR0FBRyxJQUFILENBQVEsSUFBUixDQUFhLEVBQWIsRUFBZ0IsQ0FBQyxDQUFqQixDQUFULEVBQTZCLEtBQUcsQ0FBN0MsQ0FBN0csRUFBNkosRUFBcEs7QUFBdUssR0FBcnZELEVBQXN2RCxFQUFFLFNBQUYsQ0FBWSxhQUFaLEdBQTBCLFlBQVU7QUFBQyxVQUFJLEdBQUcsSUFBUCxJQUFhLEdBQUcsSUFBSCxDQUFRLElBQVIsQ0FBYSxFQUFiLEVBQWdCLENBQUMsQ0FBakIsQ0FBYixFQUFpQyxLQUFHLENBQXBDO0FBQXNDLEdBQWowRCxFQUFrMEQsRUFBRSxTQUFGLENBQVksYUFBWixHQUEwQixZQUFVO0FBQUMsV0FBTSxDQUFDLENBQUMsRUFBUjtBQUFXLEdBQWwzRCxFQUFtM0QsRUFBRSxTQUFGLENBQVksUUFBWixHQUFxQixZQUFVO0FBQUMsV0FBTyxFQUFQO0FBQVUsR0FBNzVELEVBQTg1RCxFQUFFLFNBQUYsQ0FBWSxZQUFaLEdBQXlCLFVBQVMsQ0FBVCxFQUFXLENBQVgsRUFBYTtBQUFDLFdBQU8sS0FBRyxNQUFJLENBQUMsQ0FBUixFQUFVLEtBQUcsS0FBRyxFQUFFLEdBQUYsQ0FBTSxFQUFFLEdBQUYsQ0FBTSxDQUFOLEVBQVEsQ0FBUixDQUFOLEVBQWlCLEVBQWpCLENBQU4sR0FBMkIsRUFBRSxRQUFGLENBQVcsQ0FBWCxFQUFhLENBQWIsQ0FBckMsRUFBcUQsRUFBNUQ7QUFBK0QsR0FBcGdFLEVBQXFnRSxFQUFFLFNBQUYsQ0FBWSxZQUFaLEdBQXlCLFlBQVU7QUFBQyxXQUFPLEtBQUcsRUFBSCxHQUFNLEVBQUUsV0FBRixJQUFlLEVBQUUsU0FBakIsSUFBNEIsRUFBRSxTQUE5QixJQUF5QyxDQUF0RDtBQUF3RCxHQUFqbUUsRUFBa21FLEVBQUUsU0FBRixDQUFZLGVBQVosR0FBNEIsWUFBVTtBQUFDLFdBQU8sRUFBUDtBQUFVLEdBQW5wRSxFQUFvcEUsRUFBRSxTQUFGLENBQVksRUFBWixHQUFlLFVBQVMsQ0FBVCxFQUFXLENBQVgsRUFBYTtBQUFDLFdBQU8sR0FBRyxDQUFILElBQU0sQ0FBTixFQUFRLEVBQWY7QUFBa0IsR0FBbnNFLEVBQW9zRSxFQUFFLFNBQUYsQ0FBWSxHQUFaLEdBQWdCLFVBQVMsQ0FBVCxFQUFXO0FBQUMsV0FBTyxPQUFPLEdBQUcsQ0FBSCxDQUFQLEVBQWEsRUFBcEI7QUFBdUIsR0FBdnZFLEVBQXd2RSxFQUFFLFNBQUYsQ0FBWSxPQUFaLEdBQW9CLFlBQVU7QUFBQyxRQUFJLElBQUUsR0FBTixDQUFVLEVBQUUsRUFBRixHQUFNLElBQU4sRUFBVyxHQUFHLENBQUgsRUFBSyxDQUFDLENBQUQsQ0FBTCxFQUFTLENBQUMsQ0FBRCxFQUFHLENBQUgsRUFBSyxDQUFMLENBQVQsQ0FBWCxDQUE2QixLQUFJLElBQUksSUFBRSxDQUFOLEVBQVEsSUFBRSxHQUFHLE1BQWpCLEVBQXdCLElBQUUsQ0FBMUIsRUFBNEIsR0FBNUI7QUFBZ0MsU0FBRyxHQUFHLENBQUgsRUFBTSxPQUFUO0FBQWhDLEtBQWtELEVBQUUsS0FBRixDQUFRLFFBQVIsR0FBaUIsRUFBRSxLQUFGLENBQVEsUUFBUixHQUFpQixFQUFsQyxFQUFxQyxFQUFFLEtBQUYsQ0FBUSxNQUFSLEdBQWUsRUFBRSxLQUFGLENBQVEsTUFBUixHQUFlLEVBQW5FLEVBQXNFLE1BQUksRUFBRSxRQUFGLENBQVcsRUFBWCxFQUFjLFdBQWQsRUFBMEIsTUFBMUIsQ0FBMUUsRUFBNEcsS0FBRyxDQUEvRyxFQUFpSCxLQUFHLENBQXBILEVBQXNILEtBQUcsQ0FBekgsRUFBMkgsS0FBRyxDQUE5SCxFQUFnSSxLQUFHLENBQW5JLEVBQXFJLEtBQUcsQ0FBeEksRUFBMEksS0FBRyxDQUE3SSxFQUErSSxLQUFHLENBQWxKLEVBQW9KLEtBQUcsTUFBdkosRUFBOEosS0FBRyxDQUFDLENBQWxLLEVBQW9LLEtBQUcsQ0FBdkssRUFBeUssS0FBRyxDQUE1SyxFQUE4SyxLQUFHLENBQUMsQ0FBbEwsRUFBb0wsS0FBRyxDQUF2TCxFQUF5TCxLQUFHLENBQTVMLEVBQThMLEtBQUcsQ0FBak0sRUFBbU0sS0FBRyxDQUF0TSxFQUF3TSxLQUFHLENBQTNNLEVBQTZNLEtBQUcsQ0FBaE4sRUFBa04sS0FBRyxDQUFyTixFQUF1TixLQUFHLENBQUMsQ0FBM04sRUFBNk4sS0FBRyxDQUFoTyxFQUFrTyxLQUFHLENBQXJPO0FBQXVPLEdBQXZsRixDQUF3bEYsSUFBSSxJQUFFLFNBQUYsQ0FBRSxHQUFVO0FBQUMsUUFBSSxDQUFKLEVBQU0sQ0FBTixFQUFRLENBQVIsRUFBVSxDQUFWLEVBQVksQ0FBWixFQUFjLENBQWQsRUFBZ0IsQ0FBaEIsRUFBa0IsQ0FBbEIsRUFBb0IsQ0FBcEIsRUFBc0IsQ0FBdEIsRUFBd0IsQ0FBeEIsRUFBMEIsQ0FBMUIsQ0FBNEIsR0FBRyxDQUFILEVBQUssQ0FBQyxDQUFELEVBQUcsQ0FBSCxFQUFLLENBQUwsRUFBTyxDQUFQLEVBQVUsSUFBVixDQUFlLEdBQWYsQ0FBTCxFQUF5QixVQUFTLENBQVQsRUFBVztBQUFDLFVBQUksSUFBRSxFQUFFLGNBQUYsQ0FBaUIsQ0FBakIsQ0FBTixDQUEwQixLQUFJLElBQUUsRUFBRSxNQUFSLEVBQWUsTUFBSSxFQUFFLFFBQXJCO0FBQStCLFlBQUUsRUFBRSxVQUFKO0FBQS9CLE9BQThDLFFBQU8sSUFBRSxFQUFFLE9BQUosRUFBWSxJQUFFLEVBQUUsT0FBaEIsRUFBd0IsSUFBRSxFQUFFLFNBQTVCLEVBQXNDLEVBQUUsSUFBRixDQUFPLEVBQUUsT0FBVCxLQUFtQixFQUFFLGNBQUYsRUFBekQsRUFBNEUsRUFBRSxJQUFyRixHQUEyRixLQUFLLENBQUw7QUFBTyxlQUFHLEVBQUUsSUFBRixFQUFILEVBQVksR0FBRyxhQUFILEVBQVosRUFBK0IsSUFBRSxDQUFqQyxFQUFtQyxJQUFFLElBQUUsQ0FBdkMsRUFBeUMsSUFBRSxDQUEzQyxFQUE2QyxJQUFFLENBQS9DLENBQWlELE1BQU0sS0FBSyxDQUFMO0FBQU8sWUFBRSxJQUFGLENBQU8sRUFBRSxPQUFULEtBQW1CLEVBQUUsYUFBRixLQUFrQixDQUFyQyxJQUF3QyxFQUFFLGNBQUYsRUFBeEMsRUFBMkQsSUFBRSxJQUFFLENBQS9ELEVBQWlFLElBQUUsSUFBRSxDQUFyRSxFQUF1RSxHQUFHLFlBQUgsQ0FBZ0IsS0FBRyxDQUFuQixFQUFxQixDQUFDLENBQXRCLENBQXZFLEVBQWdHLElBQUUsQ0FBbEcsRUFBb0csSUFBRSxDQUF0RyxDQUF3RyxNQUFNLFFBQVEsS0FBSyxDQUFMLENBQU8sS0FBSyxDQUFMO0FBQU8sY0FBSSxJQUFFLElBQUUsQ0FBUjtjQUFVLElBQUUsSUFBRSxDQUFkO2NBQWdCLElBQUUsSUFBRSxDQUFGLEdBQUksSUFBRSxDQUF4QixDQUEwQixJQUFHLEtBQUcsQ0FBTixFQUFRO0FBQUMsZ0JBQUcsQ0FBQyxFQUFFLElBQUYsQ0FBTyxFQUFFLE9BQVQsQ0FBSixFQUFzQjtBQUFDLGdCQUFFLEtBQUYsR0FBVSxJQUFJLElBQUUsRUFBRSxXQUFGLENBQWMsYUFBZCxDQUFOLENBQW1DLEVBQUUsY0FBRixDQUFpQixPQUFqQixFQUF5QixDQUFDLENBQTFCLEVBQTRCLENBQUMsQ0FBN0IsRUFBK0IsRUFBRSxJQUFqQyxFQUFzQyxDQUF0QyxFQUF3QyxFQUFFLE9BQTFDLEVBQWtELEVBQUUsT0FBcEQsRUFBNEQsRUFBRSxPQUE5RCxFQUFzRSxFQUFFLE9BQXhFLEVBQWdGLEVBQUUsT0FBbEYsRUFBMEYsRUFBRSxNQUE1RixFQUFtRyxFQUFFLFFBQXJHLEVBQThHLEVBQUUsT0FBaEgsRUFBd0gsQ0FBeEgsRUFBMEgsSUFBMUgsR0FBZ0ksRUFBRSxhQUFGLENBQWdCLENBQWhCLENBQWhJO0FBQW1KO0FBQU8sZUFBRSxDQUFGLENBQUksSUFBSSxJQUFFLElBQUUsQ0FBUixDQUFVLElBQUUsRUFBRSxHQUFGLENBQU0sRUFBRSxHQUFGLENBQU0sQ0FBTixFQUFRLENBQVIsQ0FBTixFQUFpQixDQUFDLENBQWxCLENBQUYsQ0FBdUIsSUFBSSxJQUFFLEVBQUUsR0FBRixDQUFNLElBQUUsRUFBUixDQUFOO2NBQWtCLElBQUUsSUFBRSxDQUFGLEdBQUksS0FBRyxFQUFILEdBQU0sQ0FBTixHQUFRLENBQWhDO2NBQWtDLElBQUUsR0FBRyxZQUFILEtBQWtCLENBQXREO2NBQXdELElBQUUsQ0FBMUQsQ0FBNEQsSUFBRSxFQUFGLElBQU0sSUFBRSxDQUFDLEtBQUcsQ0FBSixJQUFPLENBQVQsRUFBVyxJQUFFLEVBQW5CLElBQXVCLElBQUUsQ0FBRixLQUFNLElBQUUsQ0FBQyxDQUFELEdBQUcsQ0FBTCxFQUFPLElBQUUsQ0FBZixDQUF2QixFQUF5QyxLQUFHLElBQUUsQ0FBOUMsRUFBZ0QsR0FBRyxTQUFILENBQWEsSUFBRSxFQUFGLEdBQUssQ0FBbEIsRUFBb0IsRUFBQyxRQUFPLFVBQVIsRUFBbUIsVUFBUyxDQUE1QixFQUFwQixDQUFoRCxDQUF0b0I7QUFBMnVCLEtBQXgxQixHQUEwMUIsRUFBRSxRQUFGLENBQVcsQ0FBWCxFQUFhLENBQWIsQ0FBMTFCLEVBQTAyQixFQUFFLEtBQUYsQ0FBUSxRQUFSLEdBQWlCLEVBQUUsS0FBRixDQUFRLFFBQVIsR0FBaUIsUUFBNTRCO0FBQXE1QixHQUFsOEI7TUFBbThCLElBQUUsU0FBRixDQUFFLEdBQVU7QUFBQyxRQUFJLENBQUo7UUFBTSxDQUFOO1FBQVEsQ0FBUjtRQUFVLENBQVY7UUFBWSxDQUFaO1FBQWMsQ0FBZDtRQUFnQixDQUFoQjtRQUFrQixDQUFsQjtRQUFvQixDQUFwQjtRQUFzQixDQUF0QjtRQUF3QixDQUF4QjtRQUEwQixJQUFFLEVBQUUsWUFBOUI7UUFBMkMsSUFBRSxJQUE3QyxDQUFrRCxLQUFJLElBQUUsQ0FBRixFQUFJLElBQUUsR0FBRyxNQUFiLEVBQW9CLElBQUUsQ0FBdEIsRUFBd0IsR0FBeEI7QUFBNEIsV0FBSSxJQUFFLEdBQUcsQ0FBSCxDQUFGLEVBQVEsSUFBRSxFQUFFLE9BQVosRUFBb0IsSUFBRSxFQUFFLFlBQXhCLEVBQXFDLElBQUUsRUFBRSxTQUF6QyxFQUFtRCxJQUFFLENBQXJELEVBQXVELElBQUUsRUFBRSxNQUEvRCxFQUFzRSxJQUFFLENBQXhFLEVBQTBFLEdBQTFFO0FBQThFLFlBQUUsRUFBRSxDQUFGLENBQUYsRUFBTyxJQUFFLEVBQUUsTUFBWCxFQUFrQixJQUFFLEVBQUUsRUFBRSxRQUFKLEtBQWUsQ0FBbkMsRUFBcUMsRUFBRSxLQUFGLEdBQVEsQ0FBN0MsRUFBK0MsRUFBRSxZQUFGLEtBQWlCLEtBQUcsQ0FBSCxFQUFLLEVBQUUsS0FBRixHQUFRLENBQTlCLENBQS9DLEVBQWdGLGVBQWEsRUFBRSxJQUFmLEtBQXNCLEdBQUcsQ0FBSCxHQUFNLEVBQUUsS0FBRixHQUFRLEdBQUcsa0JBQUgsQ0FBc0IsQ0FBdEIsRUFBd0IsRUFBRSxPQUFGLENBQVUsQ0FBVixDQUF4QixFQUFxQyxFQUFFLE9BQUYsQ0FBVSxDQUFWLENBQXJDLElBQW1ELENBQWpFLEVBQW1FLEdBQUcsQ0FBSCxFQUFLLENBQUMsQ0FBTixDQUF6RixDQUFoRixFQUFtTCxFQUFFLEtBQUYsSUFBUyxDQUE1TCxFQUE4TCxNQUFJLENBQUMsRUFBRSxLQUFQLElBQWMsRUFBRSxLQUFGLEdBQVEsRUFBdEIsS0FBMkIsS0FBRyxFQUFFLEtBQWhDLENBQTlMO0FBQTlFO0FBQTVCLEtBQStVLEtBQUksS0FBRyxFQUFFLEdBQUYsQ0FBTSxFQUFOLEVBQVMsSUFBVCxDQUFILEVBQWtCLElBQUUsQ0FBcEIsRUFBc0IsSUFBRSxHQUFHLE1BQS9CLEVBQXNDLElBQUUsQ0FBeEMsRUFBMEMsR0FBMUMsRUFBOEM7QUFBQyxXQUFJLElBQUUsR0FBRyxDQUFILENBQUYsRUFBUSxJQUFFLEVBQUUsU0FBWixFQUFzQixJQUFFLENBQXhCLEVBQTBCLElBQUUsRUFBRSxNQUFsQyxFQUF5QyxJQUFFLENBQTNDLEVBQTZDLEdBQTdDO0FBQWlELFlBQUUsRUFBRSxDQUFGLENBQUYsRUFBTyxJQUFFLEVBQUUsRUFBRSxRQUFKLEtBQWUsQ0FBeEIsRUFBMEIsRUFBRSxLQUFGLEtBQVUsRUFBRSxLQUFGLEdBQVEsS0FBRyxFQUFFLE1BQUwsR0FBWSxDQUE5QixDQUExQjtBQUFqRCxPQUE0RyxFQUFFLFNBQUYsQ0FBWSxJQUFaLENBQWlCLEVBQWpCO0FBQXFCO0FBQUMsR0FBbGdEO01BQW1nRCxJQUFFLFNBQUYsQ0FBRSxDQUFTLENBQVQsRUFBVyxDQUFYLEVBQWE7QUFBQyxTQUFJLElBQUksSUFBRSxDQUFOLEVBQVEsSUFBRSxHQUFHLE1BQWpCLEVBQXdCLElBQUUsQ0FBMUIsRUFBNEIsR0FBNUIsRUFBZ0M7QUFBQyxVQUFJLENBQUo7VUFBTSxDQUFOO1VBQVEsSUFBRSxHQUFHLENBQUgsQ0FBVjtVQUFnQixJQUFFLEVBQUUsT0FBcEI7VUFBNEIsSUFBRSxFQUFFLGVBQUYsR0FBa0IsQ0FBbEIsR0FBb0IsQ0FBbEQ7VUFBb0QsSUFBRSxFQUFFLFNBQXhEO1VBQWtFLElBQUUsRUFBRSxNQUF0RTtVQUE2RSxJQUFFLEVBQUUsQ0FBRixDQUEvRTtVQUFvRixJQUFFLEVBQUUsRUFBRSxNQUFGLEdBQVMsQ0FBWCxDQUF0RjtVQUFvRyxJQUFFLElBQUUsRUFBRSxLQUExRztVQUFnSCxJQUFFLElBQUUsRUFBRSxLQUF0SDtVQUE0SCxJQUFFLElBQUUsQ0FBRixHQUFJLENBQWxJO1VBQW9JLElBQUUsRUFBRSxVQUF4STtVQUFtSixJQUFFLEVBQUUsY0FBdkosQ0FBc0ssSUFBRyxLQUFHLENBQU4sRUFBUTtBQUFDLFlBQUcsS0FBRyxDQUFDLENBQUQsS0FBSyxFQUFFLElBQVYsSUFBZ0IsS0FBRyxNQUFJLEVBQUUsSUFBNUIsRUFBaUMsU0FBUyxRQUFPLEtBQUcsR0FBRyxDQUFILEVBQUssQ0FBQyxDQUFELENBQUwsRUFBUyxDQUFDLENBQUQsRUFBRyxDQUFILENBQVQsR0FBZ0IsS0FBRyxJQUFFLENBQUMsQ0FBTixLQUFVLEdBQUcsQ0FBSCxFQUFLLEVBQUUsU0FBUCxFQUFpQixFQUFqQixHQUFxQixFQUFFLGNBQUYsR0FBaUIsQ0FBQyxDQUFqRCxDQUFuQixLQUF5RSxHQUFHLENBQUgsRUFBSyxDQUFDLENBQUQsQ0FBTCxFQUFTLENBQUMsQ0FBRCxFQUFHLENBQUgsQ0FBVCxHQUFnQixLQUFHLElBQUUsQ0FBTCxLQUFTLEdBQUcsQ0FBSCxFQUFLLEVBQUUsU0FBUCxFQUFpQixFQUFqQixHQUFxQixFQUFFLGNBQUYsR0FBaUIsQ0FBL0MsQ0FBekYsR0FBNEksRUFBRSxJQUFGLEdBQU8sSUFBRSxDQUFDLENBQUgsR0FBSyxDQUF4SixFQUEwSixFQUFFLFlBQW5LLEdBQWlMLEtBQUksT0FBSjtBQUFZLGVBQUcsQ0FBSCxFQUFNLFNBQVMsS0FBSSxNQUFKO0FBQVcsZ0JBQUUsRUFBRSxLQUFKLENBQVUsTUFBTSxRQUFRLEtBQUksS0FBSjtBQUFVLGdCQUFJLElBQUUsRUFBRSxLQUFSLENBQWMsS0FBSSxDQUFKLElBQVMsQ0FBVDtBQUFXLGdCQUFFLElBQUYsQ0FBTyxDQUFQLEVBQVMsQ0FBVCxNQUFjLElBQUUsR0FBRyxFQUFFLENBQUYsRUFBSyxLQUFSLENBQUYsRUFBaUIsTUFBSSxFQUFFLE9BQUYsQ0FBVSxHQUFWLENBQUosR0FBbUIsRUFBRSxZQUFGLENBQWUsRUFBRSxNQUFGLENBQVMsQ0FBVCxDQUFmLEVBQTJCLENBQTNCLENBQW5CLEdBQWlELEVBQUUsUUFBRixDQUFXLENBQVgsRUFBYSxDQUFiLEVBQWUsQ0FBZixDQUFoRjtBQUFYLGFBQThHLFNBQXJYO0FBQStYLE9BQWxiLE1BQXViLE1BQUksRUFBRSxJQUFOLEtBQWEsR0FBRyxDQUFILEVBQUssQ0FBQyxDQUFELEVBQUcsQ0FBSCxDQUFMLEVBQVcsQ0FBQyxDQUFELEVBQUcsQ0FBSCxDQUFYLEdBQWtCLEVBQUUsSUFBRixHQUFPLENBQXRDLEVBQXlDLEtBQUksSUFBSSxJQUFFLENBQVYsRUFBWSxJQUFFLENBQUYsR0FBSSxDQUFoQixFQUFrQixHQUFsQjtBQUFzQixZQUFHLEtBQUcsRUFBRSxDQUFGLEVBQUssS0FBUixJQUFlLEtBQUcsRUFBRSxJQUFFLENBQUosRUFBTyxLQUE1QixFQUFrQztBQUFDLGNBQUksSUFBRSxFQUFFLENBQUYsQ0FBTjtjQUFXLElBQUUsRUFBRSxJQUFFLENBQUosQ0FBYixDQUFvQixLQUFJLENBQUosSUFBUyxFQUFFLEtBQVg7QUFBaUIsZ0JBQUcsRUFBRSxJQUFGLENBQU8sRUFBRSxLQUFULEVBQWUsQ0FBZixDQUFILEVBQXFCO0FBQUMsa0JBQUksSUFBRSxDQUFDLElBQUUsRUFBRSxLQUFMLEtBQWEsRUFBRSxLQUFGLEdBQVEsRUFBRSxLQUF2QixDQUFOLENBQW9DLElBQUUsRUFBRSxLQUFGLENBQVEsQ0FBUixFQUFXLE1BQVgsQ0FBa0IsQ0FBbEIsQ0FBRixFQUF1QixJQUFFLEdBQUcsRUFBRSxLQUFGLENBQVEsQ0FBUixFQUFXLEtBQWQsRUFBb0IsRUFBRSxLQUFGLENBQVEsQ0FBUixFQUFXLEtBQS9CLEVBQXFDLENBQXJDLENBQXpCLEVBQWlFLElBQUUsR0FBRyxDQUFILENBQW5FLEVBQXlFLE1BQUksRUFBRSxPQUFGLENBQVUsR0FBVixDQUFKLEdBQW1CLEVBQUUsWUFBRixDQUFlLEVBQUUsTUFBRixDQUFTLENBQVQsQ0FBZixFQUEyQixDQUEzQixDQUFuQixHQUFpRCxFQUFFLFFBQUYsQ0FBVyxDQUFYLEVBQWEsQ0FBYixFQUFlLENBQWYsQ0FBMUg7QUFBNEk7QUFBdk4sV0FBdU4sS0FBRyxNQUFJLENBQVAsS0FBVyxXQUFTLEVBQVQsR0FBWSxHQUFHLENBQUgsRUFBSyxFQUFFLFNBQVAsRUFBaUIsRUFBakIsQ0FBWixHQUFpQyxHQUFHLENBQUgsRUFBSyxFQUFFLFNBQVAsRUFBaUIsRUFBakIsQ0FBakMsRUFBc0QsRUFBRSxjQUFGLEdBQWlCLENBQWxGLEVBQXFGO0FBQU07QUFBL1g7QUFBZ1k7QUFBQyxHQUEzakY7TUFBNGpGLElBQUUsU0FBRixDQUFFLEdBQVU7QUFBQyxXQUFLLEtBQUcsQ0FBQyxDQUFKLEVBQU0sSUFBWCxFQUFpQixJQUFJLENBQUo7UUFBTSxDQUFOO1FBQVEsSUFBRSxHQUFHLFlBQUgsRUFBVjtRQUE0QixJQUFFLElBQTlCLENBQW1DLElBQUcsRUFBSCxFQUFNLEtBQUcsR0FBRyxPQUFOLElBQWUsSUFBRSxHQUFHLFNBQUwsRUFBZSxJQUFFLEdBQUcsSUFBcEIsRUFBeUIsS0FBRyxDQUEzQyxLQUErQyxJQUFFLEdBQUcsTUFBSCxDQUFVLENBQUMsSUFBRSxHQUFHLFNBQU4sSUFBaUIsR0FBRyxRQUE5QixDQUFGLEVBQTBDLElBQUUsR0FBRyxRQUFILEdBQVksSUFBRSxHQUFHLE9BQWpCLEdBQXlCLENBQXBILEdBQXVILEdBQUcsWUFBSCxDQUFnQixDQUFoQixFQUFrQixDQUFDLENBQW5CLENBQXZILENBQU4sS0FBd0osSUFBRyxDQUFDLEVBQUosRUFBTztBQUFDLFVBQUksSUFBRSxHQUFHLFNBQUgsR0FBYSxDQUFuQixDQUFxQixNQUFJLEtBQUcsRUFBQyxVQUFTLEVBQVYsRUFBYSxTQUFRLElBQUUsRUFBdkIsRUFBMEIsV0FBVSxDQUFwQyxFQUFzQyxXQUFVLEVBQWhELEVBQW1ELFNBQVEsS0FBRyxFQUE5RCxFQUFQLEdBQTBFLEtBQUcsR0FBRyxPQUFOLEtBQWdCLElBQUUsRUFBRSxJQUFGLENBQU8sQ0FBQyxJQUFFLEdBQUcsU0FBTixJQUFpQixFQUF4QixDQUFGLEVBQThCLElBQUUsR0FBRyxRQUFILEdBQVksSUFBRSxHQUFHLE9BQWpCLEdBQXlCLENBQXpFLENBQTFFO0FBQXNKLFNBQUcsTUFBSSxPQUFLLENBQVosRUFBYztBQUFDLFdBQUcsSUFBRSxFQUFGLEdBQUssTUFBTCxHQUFZLEtBQUcsQ0FBSCxHQUFLLElBQUwsR0FBVSxFQUF6QixFQUE0QixLQUFHLENBQUMsQ0FBaEMsQ0FBa0MsSUFBSSxJQUFFLEVBQUMsUUFBTyxDQUFSLEVBQVUsU0FBUSxFQUFsQixFQUFxQixRQUFPLEVBQTVCLEVBQStCLFdBQVUsRUFBekMsRUFBTjtVQUFtRCxJQUFFLEdBQUcsWUFBSCxJQUFpQixHQUFHLFlBQUgsQ0FBZ0IsSUFBaEIsQ0FBcUIsRUFBckIsRUFBd0IsQ0FBeEIsQ0FBdEUsQ0FBaUcsTUFBSSxDQUFDLENBQUwsS0FBUyxFQUFFLENBQUYsRUFBSSxHQUFHLFlBQUgsRUFBSixHQUF1QixNQUFJLEVBQUosSUFBUSxFQUFFLFFBQUYsQ0FBVyxFQUFYLEVBQWMsV0FBZCxFQUEwQixrQkFBZ0IsQ0FBQyxFQUFqQixHQUFvQixNQUFwQixHQUEyQixFQUFyRCxDQUEvQixFQUF3RixLQUFHLENBQTNGLEVBQTZGLEdBQUcsTUFBSCxJQUFXLEdBQUcsTUFBSCxDQUFVLElBQVYsQ0FBZSxFQUFmLEVBQWtCLENBQWxCLENBQWpILEdBQXVJLEtBQUcsRUFBRSxJQUFGLENBQU8sRUFBUCxFQUFVLENBQUMsQ0FBWCxDQUExSTtBQUF3SixVQUFHLENBQUg7QUFBSyxHQUF2dkc7TUFBd3ZHLElBQUUsU0FBRixDQUFFLENBQVMsQ0FBVCxFQUFXO0FBQUMsU0FBSSxJQUFJLElBQUUsQ0FBTixFQUFRLElBQUUsRUFBRSxTQUFGLENBQVksTUFBMUIsRUFBaUMsSUFBRSxDQUFuQyxFQUFxQyxHQUFyQyxFQUF5QztBQUFDLFdBQUksSUFBSSxDQUFKLEVBQU0sQ0FBTixFQUFRLENBQVIsRUFBVSxDQUFWLEVBQVksSUFBRSxFQUFFLFNBQUYsQ0FBWSxDQUFaLENBQWQsRUFBNkIsSUFBRSxFQUFuQyxFQUFzQyxVQUFRLElBQUUsRUFBRSxJQUFGLENBQU8sRUFBRSxLQUFULENBQVYsQ0FBdEM7QUFBa0UsWUFBRSxFQUFFLENBQUYsQ0FBRixFQUFPLElBQUUsRUFBRSxDQUFGLENBQVQsRUFBYyxJQUFFLEVBQUUsS0FBRixDQUFRLENBQVIsQ0FBaEIsRUFBMkIsU0FBTyxDQUFQLElBQVUsSUFBRSxFQUFFLENBQUYsQ0FBRixFQUFPLElBQUUsRUFBRSxDQUFGLENBQW5CLElBQXlCLElBQUUsQ0FBdEQsRUFBd0QsSUFBRSxFQUFFLE9BQUYsQ0FBVSxHQUFWLElBQWUsR0FBRyxDQUFILENBQWYsR0FBcUIsQ0FBQyxFQUFFLEtBQUYsQ0FBUSxDQUFSLENBQUQsQ0FBL0UsRUFBNEYsRUFBRSxDQUFGLElBQUssRUFBQyxPQUFNLENBQVAsRUFBUyxRQUFPLEVBQUUsQ0FBRixDQUFoQixFQUFqRztBQUFsRSxPQUF5TCxFQUFFLEtBQUYsR0FBUSxDQUFSO0FBQVU7QUFBQyxHQUFwL0c7TUFBcS9HLEtBQUcsU0FBSCxFQUFHLENBQVMsQ0FBVCxFQUFXO0FBQUMsUUFBSSxJQUFFLEVBQU4sQ0FBUyxPQUFPLEVBQUUsU0FBRixHQUFZLENBQVosRUFBYyxJQUFFLEVBQUUsT0FBRixDQUFVLENBQVYsRUFBWSxVQUFTLENBQVQsRUFBVztBQUFDLGFBQU8sRUFBRSxPQUFGLENBQVUsQ0FBVixFQUFZLFVBQVMsQ0FBVCxFQUFXO0FBQUMsZUFBTyxJQUFFLEdBQUYsR0FBTSxHQUFOLEdBQVUsR0FBakI7QUFBcUIsT0FBN0MsQ0FBUDtBQUFzRCxLQUE5RSxDQUFoQixFQUFnRyxNQUFJLEVBQUUsU0FBRixHQUFZLENBQVosRUFBYyxJQUFFLEVBQUUsT0FBRixDQUFVLENBQVYsRUFBWSxVQUFTLENBQVQsRUFBVztBQUFDLGFBQU8sSUFBRSxDQUFUO0FBQVcsS0FBbkMsQ0FBcEIsQ0FBaEcsRUFBMEosSUFBRSxFQUFFLE9BQUYsQ0FBVSxDQUFWLEVBQVksVUFBUyxDQUFULEVBQVc7QUFBQyxhQUFPLEVBQUUsSUFBRixDQUFPLENBQUMsQ0FBUixHQUFXLEtBQWxCO0FBQXdCLEtBQWhELENBQTVKLEVBQThNLEVBQUUsT0FBRixDQUFVLENBQVYsQ0FBOU0sRUFBMk4sQ0FBbE87QUFBb08sR0FBanZIO01BQWt2SCxLQUFHLFNBQUgsRUFBRyxDQUFTLENBQVQsRUFBVztBQUFDLFFBQUksQ0FBSjtRQUFNLENBQU47UUFBUSxJQUFFLEVBQVYsQ0FBYSxLQUFJLElBQUUsQ0FBRixFQUFJLElBQUUsRUFBRSxTQUFGLENBQVksTUFBdEIsRUFBNkIsSUFBRSxDQUEvQixFQUFpQyxHQUFqQztBQUFxQyxTQUFHLEVBQUUsU0FBRixDQUFZLENBQVosQ0FBSCxFQUFrQixDQUFsQjtBQUFyQyxLQUEwRCxLQUFJLElBQUUsRUFBRixFQUFLLElBQUUsRUFBRSxTQUFGLENBQVksTUFBWixHQUFtQixDQUE5QixFQUFnQyxLQUFHLENBQW5DLEVBQXFDLEdBQXJDO0FBQXlDLFNBQUcsRUFBRSxTQUFGLENBQVksQ0FBWixDQUFILEVBQWtCLENBQWxCO0FBQXpDO0FBQThELEdBQXQ0SDtNQUF1NEgsS0FBRyxTQUFILEVBQUcsQ0FBUyxDQUFULEVBQVcsQ0FBWCxFQUFhO0FBQUMsUUFBSSxDQUFKLENBQU0sS0FBSSxDQUFKLElBQVMsQ0FBVDtBQUFXLFFBQUUsSUFBRixDQUFPLEVBQUUsS0FBVCxFQUFlLENBQWYsTUFBb0IsRUFBRSxLQUFGLENBQVEsQ0FBUixJQUFXLEVBQUUsQ0FBRixDQUEvQjtBQUFYLEtBQWdELEtBQUksQ0FBSixJQUFTLEVBQUUsS0FBWDtBQUFpQixRQUFFLENBQUYsSUFBSyxFQUFFLEtBQUYsQ0FBUSxDQUFSLENBQUw7QUFBakI7QUFBaUMsR0FBLytIO01BQWcvSCxLQUFHLFNBQUgsRUFBRyxDQUFTLENBQVQsRUFBVyxDQUFYLEVBQWEsQ0FBYixFQUFlO0FBQUMsUUFBSSxDQUFKO1FBQU0sSUFBRSxFQUFFLE1BQVYsQ0FBaUIsSUFBRyxNQUFJLEVBQUUsTUFBVCxFQUFnQixNQUFLLGlDQUErQixFQUFFLENBQUYsQ0FBL0IsR0FBb0MsU0FBcEMsR0FBOEMsRUFBRSxDQUFGLENBQTlDLEdBQW1ELEdBQXhELENBQTRELElBQUksSUFBRSxDQUFDLEVBQUUsQ0FBRixDQUFELENBQU4sQ0FBYSxLQUFJLElBQUUsQ0FBTixFQUFRLElBQUUsQ0FBVixFQUFZLEdBQVo7QUFBZ0IsUUFBRSxDQUFGLElBQUssRUFBRSxDQUFGLElBQUssQ0FBQyxFQUFFLENBQUYsSUFBSyxFQUFFLENBQUYsQ0FBTixJQUFZLENBQXRCO0FBQWhCLEtBQXdDLE9BQU8sQ0FBUDtBQUFTLEdBQTlwSTtNQUErcEksS0FBRyxTQUFILEVBQUcsQ0FBUyxDQUFULEVBQVc7QUFBQyxRQUFJLElBQUUsQ0FBTixDQUFRLE9BQU8sRUFBRSxTQUFGLEdBQVksQ0FBWixFQUFjLEVBQUUsQ0FBRixFQUFLLE9BQUwsQ0FBYSxDQUFiLEVBQWUsWUFBVTtBQUFDLGFBQU8sRUFBRSxHQUFGLENBQVA7QUFBYyxLQUF4QyxDQUFyQjtBQUErRCxHQUFydkk7TUFBc3ZJLEtBQUcsU0FBSCxFQUFHLENBQVMsQ0FBVCxFQUFXLENBQVgsRUFBYTtBQUFDLFFBQUUsR0FBRyxNQUFILENBQVUsQ0FBVixDQUFGLENBQWUsS0FBSSxJQUFJLENBQUosRUFBTSxDQUFOLEVBQVEsSUFBRSxDQUFWLEVBQVksSUFBRSxFQUFFLE1BQXBCLEVBQTJCLElBQUUsQ0FBN0IsRUFBK0IsR0FBL0I7QUFBbUMsVUFBRSxFQUFFLENBQUYsQ0FBRixFQUFPLElBQUUsR0FBRyxFQUFFLENBQUYsQ0FBSCxDQUFULEVBQWtCLE1BQUksS0FBRyxFQUFFLEtBQUYsQ0FBUSxPQUFSLEdBQWdCLEVBQUUsY0FBbEIsRUFBaUMsR0FBRyxDQUFILEVBQUssRUFBRSxjQUFQLENBQXBDLEtBQTZELEVBQUUsY0FBRixHQUFpQixFQUFFLEtBQUYsQ0FBUSxPQUF6QixFQUFpQyxFQUFFLGNBQUYsR0FBaUIsR0FBRyxDQUFILENBQWxELEVBQXdELEVBQUUsS0FBRixDQUFRLE9BQVIsR0FBZ0IsRUFBRSxTQUExRSxFQUFvRixHQUFHLENBQUgsRUFBSyxFQUFFLFNBQVAsQ0FBakosQ0FBSixDQUFsQjtBQUFuQztBQUE4TixHQUFwL0k7TUFBcS9JLEtBQUcsU0FBSCxFQUFHLEdBQVU7QUFBQyxTQUFHLGVBQUgsRUFBbUIsRUFBRSxRQUFGLENBQVcsRUFBWCxFQUFjLFdBQWQsRUFBMEIsRUFBMUIsQ0FBbkIsQ0FBaUQsSUFBSSxJQUFFLEVBQUUsRUFBRixDQUFOO1FBQVksSUFBRSxFQUFFLGdCQUFGLENBQW1CLFdBQW5CLENBQWQ7UUFBOEMsSUFBRSxFQUFFLGdCQUFGLENBQW1CLElBQUUsV0FBckIsQ0FBaEQ7UUFBa0YsSUFBRSxLQUFHLFdBQVMsQ0FBWixJQUFlLEtBQUcsV0FBUyxDQUEvRyxDQUFpSCxNQUFJLEtBQUcsRUFBUDtBQUFXLEdBQWhySixDQUFpckosRUFBRSxRQUFGLEdBQVcsVUFBUyxDQUFULEVBQVcsQ0FBWCxFQUFhLENBQWIsRUFBZTtBQUFDLFFBQUksSUFBRSxFQUFFLEtBQVIsQ0FBYyxJQUFHLElBQUUsRUFBRSxPQUFGLENBQVUsQ0FBVixFQUFZLENBQVosRUFBZSxPQUFmLENBQXVCLEdBQXZCLEVBQTJCLEVBQTNCLENBQUYsRUFBaUMsYUFBVyxDQUEvQyxFQUFpRCxNQUFNLENBQU4sSUFBUyxFQUFFLENBQUYsSUFBSyxDQUFkLEdBQWdCLEVBQUUsQ0FBRixJQUFLLE1BQUksSUFBRSxDQUFOLENBQXJCLENBQWpELEtBQW9GLElBQUcsWUFBVSxDQUFiLEVBQWUsRUFBRSxVQUFGLEdBQWEsRUFBRSxRQUFGLEdBQVcsQ0FBeEIsQ0FBZixLQUE4QyxJQUFHO0FBQUMsWUFBSSxFQUFFLElBQUUsRUFBRSxLQUFGLENBQVEsQ0FBUixFQUFVLENBQVYsRUFBYSxXQUFiLEVBQUYsR0FBNkIsRUFBRSxLQUFGLENBQVEsQ0FBUixDQUEvQixJQUEyQyxDQUEvQyxHQUFrRCxFQUFFLENBQUYsSUFBSyxDQUF2RDtBQUF5RCxLQUE3RCxDQUE2RCxPQUFNLENBQU4sRUFBUSxDQUFFO0FBQUMsR0FBblAsQ0FBb1AsSUFBSSxFQUFKO01BQU8sRUFBUDtNQUFVLEVBQVY7TUFBYSxFQUFiO01BQWdCLEVBQWhCO01BQW1CLEVBQW5CO01BQXNCLEVBQXRCO01BQXlCLEVBQXpCO01BQTRCLEVBQTVCO01BQStCLEVBQS9CO01BQWtDLEVBQWxDO01BQXFDLEVBQXJDO01BQXdDLEVBQXhDO01BQTJDLEVBQTNDO01BQThDLEVBQTlDO01BQWlELEtBQUcsRUFBRSxRQUFGLEdBQVcsVUFBUyxDQUFULEVBQVcsQ0FBWCxFQUFhLENBQWIsRUFBZTtBQUFDLFFBQUksSUFBRSxTQUFGLENBQUUsQ0FBUyxDQUFULEVBQVc7QUFBQyxhQUFPLElBQUUsS0FBRyxFQUFFLEtBQVAsRUFBYSxFQUFFLE1BQUYsS0FBVyxFQUFFLE1BQUYsR0FBUyxFQUFFLFVBQXRCLENBQWIsRUFBK0MsRUFBRSxjQUFGLEtBQW1CLEVBQUUsY0FBRixHQUFpQixZQUFVO0FBQUMsVUFBRSxXQUFGLEdBQWMsQ0FBQyxDQUFmLEVBQWlCLEVBQUUsZ0JBQUYsR0FBbUIsQ0FBQyxDQUFyQztBQUF1QyxPQUF0RixDQUEvQyxFQUF1SSxFQUFFLElBQUYsQ0FBTyxJQUFQLEVBQVksQ0FBWixDQUE5STtBQUE2SixLQUEvSyxDQUFnTCxJQUFFLEVBQUUsS0FBRixDQUFRLEdBQVIsQ0FBRixDQUFlLEtBQUksSUFBSSxDQUFKLEVBQU0sSUFBRSxDQUFSLEVBQVUsSUFBRSxFQUFFLE1BQWxCLEVBQXlCLElBQUUsQ0FBM0IsRUFBNkIsR0FBN0I7QUFBaUMsVUFBRSxFQUFFLENBQUYsQ0FBRixFQUFPLEVBQUUsZ0JBQUYsR0FBbUIsRUFBRSxnQkFBRixDQUFtQixDQUFuQixFQUFxQixDQUFyQixFQUF1QixDQUFDLENBQXhCLENBQW5CLEdBQThDLEVBQUUsV0FBRixDQUFjLE9BQUssQ0FBbkIsRUFBcUIsQ0FBckIsQ0FBckQsRUFBNkUsR0FBRyxJQUFILENBQVEsRUFBQyxTQUFRLENBQVQsRUFBVyxNQUFLLENBQWhCLEVBQWtCLFVBQVMsQ0FBM0IsRUFBUixDQUE3RTtBQUFqQztBQUFxSixHQUFuYTtNQUFvYSxLQUFHLEVBQUUsV0FBRixHQUFjLFVBQVMsQ0FBVCxFQUFXLENBQVgsRUFBYSxDQUFiLEVBQWU7QUFBQyxRQUFFLEVBQUUsS0FBRixDQUFRLEdBQVIsQ0FBRixDQUFlLEtBQUksSUFBSSxJQUFFLENBQU4sRUFBUSxJQUFFLEVBQUUsTUFBaEIsRUFBdUIsSUFBRSxDQUF6QixFQUEyQixHQUEzQjtBQUErQixRQUFFLG1CQUFGLEdBQXNCLEVBQUUsbUJBQUYsQ0FBc0IsRUFBRSxDQUFGLENBQXRCLEVBQTJCLENBQTNCLEVBQTZCLENBQUMsQ0FBOUIsQ0FBdEIsR0FBdUQsRUFBRSxXQUFGLENBQWMsT0FBSyxFQUFFLENBQUYsQ0FBbkIsRUFBd0IsQ0FBeEIsQ0FBdkQ7QUFBL0I7QUFBaUgsR0FBcmtCO01BQXNrQixLQUFHLFNBQUgsRUFBRyxHQUFVO0FBQUMsU0FBSSxJQUFJLENBQUosRUFBTSxJQUFFLENBQVIsRUFBVSxJQUFFLEdBQUcsTUFBbkIsRUFBMEIsSUFBRSxDQUE1QixFQUE4QixHQUE5QjtBQUFrQyxVQUFFLEdBQUcsQ0FBSCxDQUFGLEVBQVEsR0FBRyxFQUFFLE9BQUwsRUFBYSxFQUFFLElBQWYsRUFBb0IsRUFBRSxRQUF0QixDQUFSO0FBQWxDLEtBQTBFLEtBQUcsRUFBSDtBQUFNLEdBQXBxQjtNQUFxcUIsS0FBRyxTQUFILEVBQUcsQ0FBUyxDQUFULEVBQVcsQ0FBWCxFQUFhLENBQWIsRUFBZTtBQUFDLE9BQUcsUUFBSCxJQUFhLEdBQUcsUUFBSCxDQUFZLElBQVosQ0FBaUIsRUFBakIsRUFBb0IsQ0FBcEIsRUFBc0IsQ0FBdEIsRUFBd0IsQ0FBeEIsQ0FBYjtBQUF3QyxHQUFodUI7TUFBaXVCLEtBQUcsU0FBSCxFQUFHLEdBQVU7QUFBQyxRQUFJLElBQUUsR0FBRyxZQUFILEVBQU4sQ0FBd0IsS0FBRyxDQUFILEVBQUssTUFBSSxDQUFDLEVBQUwsS0FBVSxFQUFFLEtBQUYsQ0FBUSxNQUFSLEdBQWUsRUFBekIsQ0FBTCxFQUFrQyxHQUFsQyxFQUFzQyxNQUFJLENBQUMsRUFBTCxLQUFVLEVBQUUsS0FBRixDQUFRLE1BQVIsR0FBZSxLQUFHLEVBQUUsWUFBTCxHQUFrQixJQUEzQyxDQUF0QyxFQUF1RixLQUFHLEdBQUcsWUFBSCxDQUFnQixFQUFFLEdBQUYsQ0FBTSxHQUFHLFlBQUgsRUFBTixFQUF3QixFQUF4QixDQUFoQixDQUFILEdBQWdELEdBQUcsWUFBSCxDQUFnQixDQUFoQixFQUFrQixDQUFDLENBQW5CLENBQXZJLEVBQTZKLEtBQUcsQ0FBQyxDQUFqSztBQUFtSyxHQUExNkI7TUFBMjZCLEtBQUcsU0FBSCxFQUFHLEdBQVU7QUFBQyxRQUFJLENBQUo7UUFBTSxDQUFOO1FBQVEsSUFBRSxFQUFFLFlBQVo7UUFBeUIsSUFBRSxFQUEzQixDQUE4QixLQUFJLENBQUosSUFBUyxFQUFUO0FBQVksVUFBRSxHQUFHLENBQUgsQ0FBRixFQUFRLGNBQVksT0FBTyxDQUFuQixHQUFxQixJQUFFLEVBQUUsSUFBRixDQUFPLEVBQVAsQ0FBdkIsR0FBa0MsS0FBSyxJQUFMLENBQVUsQ0FBVixNQUFlLElBQUUsRUFBRSxLQUFGLENBQVEsQ0FBUixFQUFVLENBQUMsQ0FBWCxJQUFjLEdBQWQsR0FBa0IsQ0FBbkMsQ0FBMUMsRUFBZ0YsRUFBRSxDQUFGLElBQUssQ0FBckY7QUFBWixLQUFtRyxPQUFPLENBQVA7QUFBUyxHQUFua0M7TUFBb2tDLEtBQUcsU0FBSCxFQUFHLEdBQVU7QUFBQyxRQUFJLENBQUo7UUFBTSxJQUFFLENBQVIsQ0FBVSxPQUFPLE9BQUssSUFBRSxFQUFFLEdBQUYsQ0FBTSxHQUFHLFlBQVQsRUFBc0IsR0FBRyxZQUF6QixDQUFQLEdBQStDLElBQUUsRUFBRSxHQUFGLENBQU0sQ0FBTixFQUFRLEVBQUUsWUFBVixFQUF1QixFQUFFLFlBQXpCLEVBQXNDLEVBQUUsWUFBeEMsRUFBcUQsRUFBRSxZQUF2RCxFQUFvRSxFQUFFLFlBQXRFLENBQWpELEVBQXFJLElBQUUsRUFBRSxZQUFoSjtBQUE2SixHQUF6dkM7TUFBMHZDLEtBQUcsU0FBSCxFQUFHLENBQVMsQ0FBVCxFQUFXO0FBQUMsUUFBSSxJQUFFLFdBQU4sQ0FBa0IsT0FBTyxFQUFFLFVBQUYsSUFBYyxhQUFhLEVBQUUsVUFBN0IsS0FBMEMsSUFBRSxFQUFFLENBQUYsQ0FBRixFQUFPLElBQUUsU0FBbkQsR0FBOEQsRUFBRSxDQUFGLENBQXJFO0FBQTBFLEdBQXIyQztNQUFzMkMsS0FBRyxTQUFILEVBQUcsQ0FBUyxDQUFULEVBQVcsQ0FBWCxFQUFhLENBQWIsRUFBZTtBQUFDLFFBQUksSUFBRSxXQUFOLENBQWtCLElBQUcsRUFBRSxVQUFGLElBQWMsYUFBYSxFQUFFLFVBQTdCLEtBQTBDLElBQUUsRUFBRSxDQUFGLENBQUYsRUFBTyxJQUFFLFNBQW5ELEdBQThELE1BQUksQ0FBckUsRUFBdUUsT0FBTyxNQUFLLEVBQUUsQ0FBRixJQUFLLENBQVYsQ0FBUCxDQUFvQixLQUFJLElBQUksSUFBRSxFQUFFLENBQUYsQ0FBTixFQUFXLElBQUUsQ0FBYixFQUFlLElBQUUsRUFBRSxNQUF2QixFQUE4QixJQUFFLENBQWhDLEVBQWtDLEdBQWxDO0FBQXNDLFVBQUUsR0FBRyxDQUFILEVBQU0sT0FBTixDQUFjLEdBQUcsRUFBRSxDQUFGLENBQUgsQ0FBZCxFQUF1QixHQUF2QixDQUFGO0FBQXRDLEtBQW9FLElBQUUsR0FBRyxDQUFILENBQUYsQ0FBUSxLQUFJLElBQUksSUFBRSxDQUFOLEVBQVEsSUFBRSxFQUFFLE1BQWhCLEVBQXVCLElBQUUsQ0FBekIsRUFBMkIsR0FBM0I7QUFBK0IsT0FBQyxDQUFELEtBQUssR0FBRyxDQUFILEVBQU0sT0FBTixDQUFjLEdBQUcsRUFBRSxDQUFGLENBQUgsQ0FBZCxDQUFMLEtBQStCLEtBQUcsTUFBSSxFQUFFLENBQUYsQ0FBdEM7QUFBL0IsS0FBMkUsRUFBRSxDQUFGLElBQUssR0FBRyxDQUFILENBQUw7QUFBVyxHQUF4b0Q7TUFBeW9ELEtBQUcsU0FBSCxFQUFHLENBQVMsQ0FBVCxFQUFXO0FBQUMsV0FBTyxFQUFFLE9BQUYsQ0FBVSxDQUFWLEVBQVksRUFBWixDQUFQO0FBQXVCLEdBQS9xRDtNQUFnckQsS0FBRyxTQUFILEVBQUcsQ0FBUyxDQUFULEVBQVc7QUFBQyxXQUFNLE1BQUksQ0FBSixHQUFNLEdBQVo7QUFBZ0IsR0FBL3NEO01BQWd0RCxLQUFHLEtBQUssR0FBTCxJQUFVLFlBQVU7QUFBQyxXQUFNLENBQUMsSUFBSSxJQUFKLEVBQVA7QUFBZ0IsR0FBeHZEO01BQXl2RCxLQUFHLFNBQUgsRUFBRyxDQUFTLENBQVQsRUFBVyxDQUFYLEVBQWE7QUFBQyxXQUFPLEVBQUUsS0FBRixHQUFRLEVBQUUsS0FBakI7QUFBdUIsR0FBanlEO01BQWt5RCxLQUFHLENBQXJ5RDtNQUF1eUQsS0FBRyxDQUExeUQ7TUFBNHlELEtBQUcsTUFBL3lEO01BQXN6RCxLQUFHLENBQUMsQ0FBMXpEO01BQTR6RCxLQUFHLElBQS96RDtNQUFvMEQsS0FBRyxDQUF2MEQ7TUFBeTBELEtBQUcsQ0FBNTBEO01BQTgwRCxLQUFHLENBQUMsQ0FBbDFEO01BQW8xRCxLQUFHLENBQXYxRDtNQUF5MUQsS0FBRyxDQUFDLENBQTcxRDtNQUErMUQsS0FBRyxDQUFsMkQ7TUFBbzJELEtBQUcsRUFBdjJELENBQTAyRCxjQUFZLE9BQU8sTUFBbkIsSUFBMkIsT0FBTyxHQUFsQyxHQUFzQyxPQUFPLEVBQVAsRUFBVSxZQUFVO0FBQUMsV0FBTyxDQUFQO0FBQVMsR0FBOUIsQ0FBdEMsR0FBc0UsZUFBYSxPQUFPLE1BQXBCLElBQTRCLE9BQU8sT0FBbkMsR0FBMkMsT0FBTyxPQUFQLEdBQWUsQ0FBMUQsR0FBNEQsRUFBRSxPQUFGLEdBQVUsQ0FBNUk7QUFBOEksQ0FBM3FZLENBQTRxWSxNQUE1cVksRUFBbXJZLFFBQW5yWSxDQUFEOzs7QUNEQTs7O0FDQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7QUNsS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDM0hBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQ3BEQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7O0FDbENBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ3hFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDWEE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDcENBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNwRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDWkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ3JEQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDZEE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ3BFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNaQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ3JDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNsQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDM0JBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNwQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNyUkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsImZpbGUiOiJnZW5lcmF0ZWQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlc0NvbnRlbnQiOlsiKGZ1bmN0aW9uIGUodCxuLHIpe2Z1bmN0aW9uIHMobyx1KXtpZighbltvXSl7aWYoIXRbb10pe3ZhciBhPXR5cGVvZiByZXF1aXJlPT1cImZ1bmN0aW9uXCImJnJlcXVpcmU7aWYoIXUmJmEpcmV0dXJuIGEobywhMCk7aWYoaSlyZXR1cm4gaShvLCEwKTt2YXIgZj1uZXcgRXJyb3IoXCJDYW5ub3QgZmluZCBtb2R1bGUgJ1wiK28rXCInXCIpO3Rocm93IGYuY29kZT1cIk1PRFVMRV9OT1RfRk9VTkRcIixmfXZhciBsPW5bb109e2V4cG9ydHM6e319O3Rbb11bMF0uY2FsbChsLmV4cG9ydHMsZnVuY3Rpb24oZSl7dmFyIG49dFtvXVsxXVtlXTtyZXR1cm4gcyhuP246ZSl9LGwsbC5leHBvcnRzLGUsdCxuLHIpfXJldHVybiBuW29dLmV4cG9ydHN9dmFyIGk9dHlwZW9mIHJlcXVpcmU9PVwiZnVuY3Rpb25cIiYmcmVxdWlyZTtmb3IodmFyIG89MDtvPHIubGVuZ3RoO28rKylzKHJbb10pO3JldHVybiBzfSkiLCJpbXBvcnQgeyByZXR1cm5BcnJheSB9IGZyb20gJy4vaGVscGVycy5qcydcblxuY29uc3QgYm9vc3RlcnMgPSB7XG4gICAgY29uZmlnOiB7XG4gICAgICAgIG5vZGVzOiB7XG4gICAgICAgICAgICBib29zdGVyczogcmV0dXJuQXJyYXkoZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbCgnLmNvbnRhaW5lci1ib29zdGVyJykpLFxuICAgICAgICAgICAgc2VjdGlvbjogZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLnNlY3Rpb24tYm9vc3RlcnMnKSxcbiAgICAgICAgICAgIGNvbnRhaW5lckJvb3N0ZXJzOiBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcuY29udGFpbmVyLWJvb3N0ZXJzJyksXG4gICAgICAgICAgICBjbG9zZTogZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbCgnLmNvbnRhaW5lci1ib29zdGVycyAuY2xvc2UnKVxuICAgICAgICB9LFxuICAgICAgICBjb250YWluZXJCb29zdGVyc0hlaWdodDogZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLmNvbnRhaW5lci1ib29zdGVycycpID8gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLmNvbnRhaW5lci1ib29zdGVycycpLm9mZnNldEhlaWdodCA6IHVuZGVmaW5lZFxuICAgIH0sXG5cbiAgICBzdGF0ZToge1xuICAgICAgICBjdXJyZW50OiB1bmRlZmluZWQsXG4gICAgICAgIGN1cnJlbnRJbmRleDogdW5kZWZpbmVkXG4gICAgfSxcblxuICAgIGluaXQoKSB7XG4gICAgICAgIGJvb3N0ZXJzLmJpbmRFdmVudHMoKVxuICAgIH0sXG5cbiAgICBiaW5kRXZlbnRzKCkge1xuXG4gICAgICAgIC8vIFdoZW4gYSBib29zdGVyIGdldHMgY2xpY2tlZFxuICAgICAgICBib29zdGVycy5jb25maWcubm9kZXMuYm9vc3RlcnMuZm9yRWFjaCgoYm9vc3RlciwgaW5kZXgpID0+IHtcbiAgICAgICAgICAgIGJvb3N0ZXIuYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCAoZXZlbnQpID0+IHtcbiAgICAgICAgICAgICAgICBldmVudC5zdG9wUHJvcGFnYXRpb24oKVxuICAgICAgICAgICAgICAgIGJvb3N0ZXJzLnN0YXRlLmN1cnJlbnQgPSBldmVudC5jdXJyZW50VGFyZ2V0XG4gICAgICAgICAgICAgICAgYm9vc3RlcnMuc3RhdGUuY3VycmVudEluZGV4ID0gaW5kZXhcbiAgICAgICAgICAgICAgICBib29zdGVycy5tZXRob2RzLm9wZW4oKVxuICAgICAgICAgICAgICAgIGJvb3N0ZXJzLm1ldGhvZHMuYWRqdXN0SGVpZ2h0KClcbiAgICAgICAgICAgIH0pXG4gICAgICAgIH0pXG5cbiAgICAgICAgLy9DbG9zZSBidXR0b24gZ2V0cyBjbGlja2VkXG4gICAgICAgIGJvb3N0ZXJzLmNvbmZpZy5ub2Rlcy5jbG9zZS5mb3JFYWNoKGNsb3NlID0+IHtcbiAgICAgICAgICAgIGNsb3NlLmFkZEV2ZW50TGlzdGVuZXIoJ2NsaWNrJywgKGV2ZW50KSA9PiB7XG4gICAgICAgICAgICAgICAgZXZlbnQuc3RvcFByb3BhZ2F0aW9uKClcbiAgICAgICAgICAgICAgICBib29zdGVycy5tZXRob2RzLmNsb3NlKClcbiAgICAgICAgICAgICAgICBib29zdGVycy5tZXRob2RzLmFkanVzdEhlaWdodCgpXG4gICAgICAgICAgICB9KVxuICAgICAgICB9KVxuXG4gICAgICAgIC8vQ2xpY2sgYW55d2hlcmUgb24gdGhlIGRvY3VtZW50XG4gICAgICAgIGRvY3VtZW50LmFkZEV2ZW50TGlzdGVuZXIoJ2NsaWNrJywgKCkgPT4ge1xuICAgICAgICAgICAgYm9vc3RlcnMubWV0aG9kcy5jbG9zZSgpXG4gICAgICAgICAgICBib29zdGVycy5tZXRob2RzLmFkanVzdEhlaWdodCgpXG4gICAgICAgIH0pXG5cbiAgICAgICAgLy9QcmVzcyBlc2NhcGVcbiAgICAgICAgZG9jdW1lbnQuYWRkRXZlbnRMaXN0ZW5lcigna2V5ZG93bicsIChldmVudCkgPT4ge1xuICAgICAgICAgICAgaWYgKGV2ZW50LndoaWNoID09IDI3KSB7XG4gICAgICAgICAgICAgICAgYm9vc3RlcnMubWV0aG9kcy5jbG9zZSgpXG4gICAgICAgICAgICAgICAgYm9vc3RlcnMubWV0aG9kcy5hZGp1c3RIZWlnaHQoKVxuICAgICAgICAgICAgfVxuICAgICAgICB9KVxuICAgIH0sXG5cbiAgICBtZXRob2RzOiB7XG4gICAgICAgIG9wZW4oKSB7XG4gICAgICAgICAgICBsZXQgYWxpZ25JbmRleCA9IChib29zdGVycy5zdGF0ZS5jdXJyZW50SW5kZXggJSA0KSArIDFcbiAgICAgICAgICAgIGJvb3N0ZXJzLnN0YXRlLmN1cnJlbnQuY2xhc3NMaXN0LmFkZCgnYm9vc3Rlci1leHBhbmQnLCAnYm9vc3Rlci10b3AnLCBgYm9vc3Rlci1leHBhbmQtJHthbGlnbkluZGV4fWApXG4gICAgICAgICAgICBib29zdGVycy5jb25maWcubm9kZXMuYm9vc3RlcnMuZm9yRWFjaCgoYm9vc3RlciwgaW5kZXgpID0+IHtcbiAgICAgICAgICAgICAgICBpZiAoaW5kZXggIT0gYm9vc3RlcnMuc3RhdGUuY3VycmVudEluZGV4KSB7XG4gICAgICAgICAgICAgICAgICAgIGJvb3N0ZXIuY2xhc3NMaXN0LmFkZCgnYm9vc3Rlci1uby1jbGljaycpXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSlcbiAgICAgICAgfSxcblxuICAgICAgICBjbG9zZSgpIHtcbiAgICAgICAgICAgIGJvb3N0ZXJzLmNvbmZpZy5ub2Rlcy5ib29zdGVycy5mb3JFYWNoKChib29zdGVyLCBpbmRleCkgPT4ge1xuICAgICAgICAgICAgICAgIGJvb3N0ZXIuY2xhc3NMaXN0LnJlbW92ZSgnYm9vc3Rlci1uby1jbGljaycsJ2Jvb3N0ZXItZXhwYW5kJywgJ2Jvb3N0ZXItZXhwYW5kLTEnLCAnYm9vc3Rlci1leHBhbmQtMicsICdib29zdGVyLWV4cGFuZC0zJywgJ2Jvb3N0ZXItZXhwYW5kLTQnKVxuICAgICAgICAgICAgICAgIGlmIChpbmRleCAhPSBib29zdGVycy5zdGF0ZS5jdXJyZW50SW5kZXgpIHtcbiAgICAgICAgICAgICAgICAgICAgYm9vc3Rlci5jbGFzc0xpc3QucmVtb3ZlKCdib29zdGVyLXRvcCcpXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIHNldFRpbWVvdXQoKCkgPT4ge1xuICAgICAgICAgICAgICAgIGJvb3N0ZXJzLnN0YXRlLmN1cnJlbnQuY2xhc3NMaXN0LnJlbW92ZSgnYm9vc3Rlci10b3AnKVxuICAgICAgICAgICAgfSwgMzAwKVxuICAgICAgICB9LFxuXG4gICAgICAgIGFkanVzdEhlaWdodCgpIHtcbiAgICAgICAgICAgIGxldCByb3cgPSBNYXRoLmZsb29yKChib29zdGVycy5zdGF0ZS5jdXJyZW50SW5kZXggKyAxKSAlIDQpXG4gICAgICAgICAgICBsZXQgc2VjdGlvbkhlaWdodCA9IGJvb3N0ZXJzLmNvbmZpZy5ub2Rlcy5zZWN0aW9uLm9mZnNldEhlaWdodFxuICAgICAgICAgICAgbGV0IGNvbnRhaW5lckJvb3N0ZXJzSGVpZ2h0ID0gYm9vc3RlcnMuY29uZmlnLm5vZGVzLmNvbnRhaW5lckJvb3N0ZXJzLm9mZnNldEhlaWdodFxuICAgICAgICAgICAgbGV0IGluY3JlbWVudCA9IGJvb3N0ZXJzLnN0YXRlLmN1cnJlbnQuY2hpbGROb2Rlc1sxXS5jaGlsZE5vZGVzWzNdLm9mZnNldEhlaWdodCAtIGNvbnRhaW5lckJvb3N0ZXJzSGVpZ2h0XG4gICAgICAgICAgICBsZXQgaW5jcmVtZW50ZWQgPSBpbmNyZW1lbnQgKyBjb250YWluZXJCb29zdGVyc0hlaWdodCAqIDEuMDhcblxuICAgICAgICAgICAgaWYgKGJvb3N0ZXJzLnN0YXRlLmN1cnJlbnQuY2xhc3NMaXN0LmNvbnRhaW5zKCdib29zdGVyLWV4cGFuZCcpKSB7XG4gICAgICAgICAgICAgICAgaWYgKGluY3JlbWVudCA+IDApIHtcbiAgICAgICAgICAgICAgICAgICAgYm9vc3RlcnMuY29uZmlnLm5vZGVzLmNvbnRhaW5lckJvb3N0ZXJzLnN0eWxlLmhlaWdodCA9IGluY3JlbWVudGVkICsgJ3B4J1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgIGJvb3N0ZXJzLmNvbmZpZy5ub2Rlcy5jb250YWluZXJCb29zdGVycy5zdHlsZS5oZWlnaHQgPSBib29zdGVycy5jb25maWcuY29udGFpbmVyQm9vc3RlcnNIZWlnaHQgKyAncHgnXG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IGJvb3N0ZXJzIiwiaW1wb3J0IHsgcmV0dXJuQXJyYXkgfSBmcm9tICcuL2hlbHBlcnMuanMnXG5cbmNvbnN0IENvbnNvbGUgPSB7XG4gICAgY29uZmlnOiB7XG4gICAgICAgIGFjdGl2ZTogdW5kZWZpbmVkLFxuICAgICAgICBvcmRlcjogWydlbnRpdHknLCAndmlldycsICd3aWRnZXQnLCAnd2lkZ2V0UHJvcHMnXSxcbiAgICAgICAgbm9kZXM6IHtcbiAgICAgICAgICAgIGluZGljYXRvcnM6IHJldHVybkFycmF5KGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoYC5pbmRpY2F0b3JgKSksXG4gICAgICAgICAgICBtYXJrZXJzOiByZXR1cm5BcnJheShkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKCcuc2VjdGlvbi1jb25zb2xlIC5tYXJrZXInKSksXG4gICAgICAgICAgICBjb2RlOiByZXR1cm5BcnJheShkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKGAuY29kZWApKVxuICAgICAgICB9LFxuICAgICAgICBzdGF0ZToge1xuICAgICAgICAgICAgY3ljbGVJbnRlcnZhbDogdW5kZWZpbmVkXG4gICAgICAgIH1cbiAgICB9LFxuXG4gICAgaW5pdCgpIHtcbiAgICAgICAgQ29uc29sZS5iaW5kRXZlbnRzKClcbiAgICAgICAgZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLmluZGljYXRvci1lbnRpdHknKS5jbGljaygpXG4gICAgfSxcblxuICAgIGJpbmRFdmVudHMoKSB7XG4gICAgICAgIC8vT24gY2xpY2tpbmcgYW4gaW5kaWNhdG9yXG4gICAgICAgIENvbnNvbGUuY29uZmlnLm5vZGVzLmluZGljYXRvcnMuZm9yRWFjaChpbmRpY2F0b3IgPT4ge1xuICAgICAgICAgICAgaW5kaWNhdG9yLmFkZEV2ZW50TGlzdGVuZXIoJ2NsaWNrJywgZnVuY3Rpb24gKGV2ZW50KSB7XG4gICAgICAgICAgICAgICAgZXZlbnQuc3RvcFByb3BhZ2F0aW9uKClcbiAgICAgICAgICAgICAgICBDb25zb2xlLm1ldGhvZHMudG9nZ2xlLmluaXQoZXZlbnQuY3VycmVudFRhcmdldClcbiAgICAgICAgICAgIH0pXG4gICAgICAgIH0pXG5cbiAgICAgICAgLy9TdGFydCBjeWNsaW5nIHdoZW4gc2Nyb2xsZWQgdG8gc2VjdGlvblxuICAgICAgICBkb2N1bWVudC5hZGRFdmVudExpc3RlbmVyKCdzY3JvbGwnLCAoKSA9PiB7XG4gICAgICAgICAgICBDb25zb2xlLm1ldGhvZHMuY3ljbGUoKVxuICAgICAgICB9KVxuICAgIH0sXG5cbiAgICBtZXRob2RzOiB7XG4gICAgICAgIHRvZ2dsZToge1xuICAgICAgICAgICAgaW5pdChpbmRpY2F0b3IsIG5vY3ljbGUpIHtcbiAgICAgICAgICAgICAgICBpZiAoIW5vY3ljbGUpIHtcbiAgICAgICAgICAgICAgICAgICAgY2xlYXJJbnRlcnZhbChDb25zb2xlLmNvbmZpZy5zdGF0ZS5jeWNsZUludGVydmFsKVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBDb25zb2xlLm1ldGhvZHMudG9nZ2xlLmFjdGl2YXRlKGluZGljYXRvcilcbiAgICAgICAgICAgICAgICBDb25zb2xlLm1ldGhvZHMudG9nZ2xlLmNvbXBvbmVudCgpXG4gICAgICAgICAgICAgICAgQ29uc29sZS5tZXRob2RzLnRvZ2dsZS5leHBsYWluYXRpb24oKVxuICAgICAgICAgICAgICAgIENvbnNvbGUubWV0aG9kcy50b2dnbGUubWFya2VycygpXG4gICAgICAgICAgICAgICAgQ29uc29sZS5tZXRob2RzLnRvZ2dsZS5pbmRpY2F0b3IoKVxuICAgICAgICAgICAgICAgIENvbnNvbGUubWV0aG9kcy50b2dnbGUubmV4dCgpXG4gICAgICAgICAgICAgICAgQ29uc29sZS5tZXRob2RzLnRvZ2dsZS5jb2RlKClcbiAgICAgICAgICAgIH0sXG5cbiAgICAgICAgICAgIGFjdGl2YXRlKGluZGljYXRvcikge1xuICAgICAgICAgICAgICAgIGxldCBuYW1lID0gaW5kaWNhdG9yLmNsYXNzTGlzdFsxXS5zcGxpdCgnLScpWzFdXG4gICAgICAgICAgICAgICAgbGV0IGNvbXBvbmVudCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoYC5jb21wb25lbnQtJHtuYW1lfWApXG4gICAgICAgICAgICAgICAgQ29uc29sZS5jb25maWcuYWN0aXZlID0geyBjb21wb25lbnQsIG5hbWUgfVxuICAgICAgICAgICAgfSxcblxuICAgICAgICAgICAgY29tcG9uZW50KCkge1xuICAgICAgICAgICAgICAgIGxldCBjb21wb25lbnRzID0gWy4uLkNvbnNvbGUuY29uZmlnLm9yZGVyXVxuICAgICAgICAgICAgICAgIGxldCBpbmRleCA9IGNvbXBvbmVudHMuaW5kZXhPZihDb25zb2xlLmNvbmZpZy5hY3RpdmUubmFtZSkgKyAxXG4gICAgICAgICAgICAgICAgbGV0IG51bWJlciA9IGNvbXBvbmVudHMubGVuZ3RoIC0gaW5kZXhcbiAgICAgICAgICAgICAgICBsZXQgdG9Gb2N1cyA9IFsuLi5jb21wb25lbnRzXS5zcGxpY2UoMCwgaW5kZXgpXG4gICAgICAgICAgICAgICAgbGV0IHRvVW5mb2N1cyA9IFsuLi5jb21wb25lbnRzXS5zcGxpY2UoLW51bWJlciwgbnVtYmVyKVxuICAgICAgICAgICAgICAgIHRvVW5mb2N1cy5mb3JFYWNoKGNvbXBvbmVudCA9PiB7XG4gICAgICAgICAgICAgICAgICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoYC5jb21wb25lbnQtJHtjb21wb25lbnR9YCkuc2V0QXR0cmlidXRlKCdjbGFzcycsIGBjb21wb25lbnQgY29tcG9uZW50LSR7Y29tcG9uZW50fWApXG4gICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICB0b0ZvY3VzLmZvckVhY2goY29tcG9uZW50ID0+IHtcbiAgICAgICAgICAgICAgICAgICAgZG9jdW1lbnQucXVlcnlTZWxlY3RvcihgLmNvbXBvbmVudC0ke2NvbXBvbmVudH1gKS5zZXRBdHRyaWJ1dGUoJ2NsYXNzJywgYGNvbXBvbmVudCBjb21wb25lbnQtJHtjb21wb25lbnR9IGZvY3VzLSR7Y29tcG9uZW50fWApXG4gICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICBjb21wb25lbnRzLmZvckVhY2goY29tcG9uZW50ID0+IHtcbiAgICAgICAgICAgICAgICAgICAgZG9jdW1lbnQucXVlcnlTZWxlY3RvcihgLmNvbXBvbmVudC0ke2NvbXBvbmVudH1gKS5jbGFzc0xpc3QucmVtb3ZlKGBjb2xvci0ke2NvbXBvbmVudH1gKVxuICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgQ29uc29sZS5jb25maWcuYWN0aXZlLmNvbXBvbmVudC5jbGFzc0xpc3QuYWRkKGBjb2xvci0ke0NvbnNvbGUuY29uZmlnLmFjdGl2ZS5uYW1lfWApXG4gICAgICAgICAgICB9LFxuXG4gICAgICAgICAgICBleHBsYWluYXRpb24oKSB7XG4gICAgICAgICAgICAgICAgbGV0IGV4cHMgPSByZXR1cm5BcnJheShkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKCcuZXhwJykpXG4gICAgICAgICAgICAgICAgZXhwcy5mb3JFYWNoKGV4cCA9PiB7XG4gICAgICAgICAgICAgICAgICAgIGxldCBjdXJyZW50ID0gZXhwLmNsYXNzTGlzdFsxXS5zcGxpdCgnLScpWzFdXG4gICAgICAgICAgICAgICAgICAgIGlmIChjdXJyZW50ICE9IENvbnNvbGUuY29uZmlnLmFjdGl2ZS5uYW1lKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBleHAuc2V0QXR0cmlidXRlKCdjbGFzcycsIGBleHAgZXhwLSR7Y3VycmVudH1gKVxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgZXhwLnNldEF0dHJpYnV0ZSgnY2xhc3MnLCBgZXhwIGV4cC0ke2N1cnJlbnR9IGV4cC1mb2N1c2ApXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgfSxcblxuICAgICAgICAgICAgbWFya2VycygpIHtcbiAgICAgICAgICAgICAgICBDb25zb2xlLmNvbmZpZy5ub2Rlcy5tYXJrZXJzLmZvckVhY2gobWFya2VyID0+IHtcbiAgICAgICAgICAgICAgICAgICAgbGV0IGRlZmF1bHRDbGFzcyA9IG1hcmtlci5jbGFzc0xpc3RbMV1cbiAgICAgICAgICAgICAgICAgICAgbWFya2VyLnNldEF0dHJpYnV0ZSgnY2xhc3MnLCBgbWFya2VyICR7ZGVmYXVsdENsYXNzfWApXG4gICAgICAgICAgICAgICAgICAgIG1hcmtlci5jbGFzc0xpc3QuYWRkKGBtYXJrZXItJHtDb25zb2xlLmNvbmZpZy5hY3RpdmUubmFtZX1gKVxuICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICB9LFxuXG4gICAgICAgICAgICBpbmRpY2F0b3IoKSB7XG4gICAgICAgICAgICAgICAgQ29uc29sZS5jb25maWcubm9kZXMuaW5kaWNhdG9ycy5mb3JFYWNoKGluZGljYXRvciA9PiB7XG4gICAgICAgICAgICAgICAgICAgIGluZGljYXRvci5jbGFzc0xpc3QucmVtb3ZlKCdpbmRpY2F0b3ItZm9jdXMnKVxuICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgZG9jdW1lbnQucXVlcnlTZWxlY3RvcihgLmluZGljYXRvci0ke0NvbnNvbGUuY29uZmlnLmFjdGl2ZS5uYW1lfWApLmNsYXNzTGlzdC5hZGQoJ2luZGljYXRvci1mb2N1cycpXG4gICAgICAgICAgICB9LFxuXG4gICAgICAgICAgICBuZXh0KCkge1xuICAgICAgICAgICAgICAgIGxldCBidXR0b24gPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcubmV4dC1idXR0b24nKVxuICAgICAgICAgICAgICAgIGxldCBkZWZhdWx0Q2xhc3MgPSBidXR0b24uY2xhc3NMaXN0WzBdXG4gICAgICAgICAgICAgICAgYnV0dG9uLnNldEF0dHJpYnV0ZSgnY2xhc3MnLCBkZWZhdWx0Q2xhc3MpXG4gICAgICAgICAgICAgICAgYnV0dG9uLmNsYXNzTGlzdC5hZGQoYG5leHQtJHtDb25zb2xlLmNvbmZpZy5hY3RpdmUubmFtZX1gKVxuICAgICAgICAgICAgfSxcblxuICAgICAgICAgICAgY29kZSgpIHtcbiAgICAgICAgICAgICAgICBDb25zb2xlLmNvbmZpZy5ub2Rlcy5jb2RlLmZvckVhY2gobGluZSA9PiB7XG4gICAgICAgICAgICAgICAgICAgIGxldCBkZWZhdWx0Q2xhc3MgPSBsaW5lLmNsYXNzTGlzdFsxXVxuICAgICAgICAgICAgICAgICAgICBsaW5lLnNldEF0dHJpYnV0ZSgnY2xhc3MnLCBgY29kZSAke2RlZmF1bHRDbGFzc31gKVxuICAgICAgICAgICAgICAgIH0pXG5cbiAgICAgICAgICAgICAgICBsZXQgc2VsZWN0aW9uID0gcmV0dXJuQXJyYXkoZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbChgLmNvZGUtJHtDb25zb2xlLmNvbmZpZy5hY3RpdmUubmFtZX1gKSlcbiAgICAgICAgICAgICAgICBzZWxlY3Rpb24uZm9yRWFjaChzZWxlY3RlZCA9PiB7XG4gICAgICAgICAgICAgICAgICAgIHNlbGVjdGVkLmNsYXNzTGlzdC5hZGQoYGZvY3VzLSR7Q29uc29sZS5jb25maWcuYWN0aXZlLm5hbWV9YClcbiAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIGN5Y2xlKCkge1xuICAgICAgICAgICAgbGV0IHNjcm9sbFBvcyA9IGRvY3VtZW50LmRvY3VtZW50RWxlbWVudC5zY3JvbGxUb3AgPyBkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQuc2Nyb2xsVG9wIDogZG9jdW1lbnQuYm9keS5zY3JvbGxUb3BcbiAgICAgICAgICAgIGxldCB0cmlnZ2VyID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLnNlY3Rpb24taW50cm8gLndyYXBwZXItYmFubmVyJykuY2xpZW50SGVpZ2h0ICsgZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLnNlY3Rpb24taW50cm8gLndyYXBwZXItY29udGVudCcpLmNsaWVudEhlaWdodCArIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5zZWN0aW9uLWludHJvIC53cmFwcGVyLWZlYXR1cmVzJykuY2xpZW50SGVpZ2h0XG4gICAgICAgICAgICBpZiAoc2Nyb2xsUG9zID49IHRyaWdnZXIgJiYgQ29uc29sZS5jb25maWcuc3RhdGUuY3ljbGVJbnRlcnZhbCA9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgICAgICBDb25zb2xlLmNvbmZpZy5zdGF0ZS5jeWNsZUludGVydmFsID0gc2V0SW50ZXJ2YWwoKCkgPT4ge1xuICAgICAgICAgICAgICAgICAgICBsZXQgY3VycmVudCA9IENvbnNvbGUuY29uZmlnLm9yZGVyLmluZGV4T2YoQ29uc29sZS5jb25maWcuYWN0aXZlLm5hbWUpXG4gICAgICAgICAgICAgICAgICAgIGxldCBuZXh0ID0gY3VycmVudCA8IDMgPyBDb25zb2xlLmNvbmZpZy5vcmRlcltjdXJyZW50ICsgMV0gOiAnZW50aXR5J1xuICAgICAgICAgICAgICAgICAgICBsZXQgY29tcG9uZW50ID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihgLmNvbXBvbmVudC0ke25leHR9YClcbiAgICAgICAgICAgICAgICAgICAgQ29uc29sZS5tZXRob2RzLnRvZ2dsZS5pbml0KGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoYC5pbmRpY2F0b3ItJHtuZXh0fWApLCAxKVxuICAgICAgICAgICAgICAgIH0sIDUwMDApXG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IENvbnNvbGUiLCJpbXBvcnQgeyByZXR1cm5BcnJheSwgc2Nyb2xsVG9ZIH0gZnJvbSAnLi9oZWxwZXJzLmpzJ1xuXG5jb25zdCBEb2NzID0ge1xuICAgIGNvbmZpZzoge1xuICAgICAgICB2aWV3czoge1xuICAgICAgICAgICAgbWFzdGVyOiB7XG4gICAgICAgICAgICAgICAgd2lkZ2V0czogW1xuICAgICAgICAgICAgICAgICAgICAnQnVzaW5lc3MtTmFtZScsXG4gICAgICAgICAgICAgICAgICAgICdCdXNpbmVzcy1EZXNjcmlwdGlvbicsXG4gICAgICAgICAgICAgICAgICAgICdVcGRhdGUnLFxuICAgICAgICAgICAgICAgICAgICAnUHJvZHVjdCcsXG4gICAgICAgICAgICAgICAgICAgICdCdXNpbmVzcy1Ib3VycycsXG4gICAgICAgICAgICAgICAgICAgICdBbGwtUHJvZHVjdHMnLFxuICAgICAgICAgICAgICAgICAgICAnU3Vic2NyaWJlcnMtQ291bnQnLFxuICAgICAgICAgICAgICAgICAgICAnVGFncycsXG4gICAgICAgICAgICAgICAgICAgICdWaXNpdHMnLFxuICAgICAgICAgICAgICAgICAgICAnQnVzaW5lc3MtRW5xdWlyaWVzJyxcbiAgICAgICAgICAgICAgICAgICAgJ1ZpZGVvLUdhbGxlcnknLFxuICAgICAgICAgICAgICAgICAgICAnU2l0ZS1MaW5rJyxcbiAgICAgICAgICAgICAgICAgICAgJ0FkZHJlc3MnLFxuICAgICAgICAgICAgICAgICAgICAnTW9iaWxlLU51bWJlcnMnLFxuICAgICAgICAgICAgICAgICAgICAnU3Vic2NyaWJlJyxcbiAgICAgICAgICAgICAgICAgICAgJ1NlYXJjaCcsXG4gICAgICAgICAgICAgICAgICAgICdMb2dvJyxcbiAgICAgICAgICAgICAgICAgICAgJ01hcCcsXG4gICAgICAgICAgICAgICAgICAgICdGYWNlYm9vay1MaWtlLUJveCcsXG4gICAgICAgICAgICAgICAgICAgICdCYWNrZ3JvdW5kLUltYWdlJyxcbiAgICAgICAgICAgICAgICAgICAgJ0Zhdmljb24nXG4gICAgICAgICAgICAgICAgXSxcbiAgICAgICAgICAgICAgICBtYW5kYXRvcnk6IFtcbiAgICAgICAgICAgICAgICAgICAgJ0J1c2luZXNzLU5hbWUnLFxuICAgICAgICAgICAgICAgICAgICAnQnVzaW5lc3MtRGVzY3JpcHRpb24nLFxuICAgICAgICAgICAgICAgICAgICAnVXBkYXRlJyxcbiAgICAgICAgICAgICAgICAgICAgJ1Byb2R1Y3QnLFxuICAgICAgICAgICAgICAgICAgICAnQnVzaW5lc3MtSG91cnMnLFxuICAgICAgICAgICAgICAgICAgICAnQWxsLVByb2R1Y3RzJyxcbiAgICAgICAgICAgICAgICAgICAgJ1N1YnNjcmliZXJzLUNvdW50JyxcbiAgICAgICAgICAgICAgICAgICAgJ1RhZ3MnLFxuICAgICAgICAgICAgICAgICAgICAnVmlzaXRzJyxcbiAgICAgICAgICAgICAgICAgICAgJ0J1c2luZXNzLUVucXVpcmllcycsXG4gICAgICAgICAgICAgICAgICAgICdWaWRlby1HYWxsZXJ5JyxcbiAgICAgICAgICAgICAgICAgICAgJ1NpdGUtTGluaycsXG4gICAgICAgICAgICAgICAgICAgICdBZGRyZXNzJyxcbiAgICAgICAgICAgICAgICAgICAgJ01vYmlsZS1OdW1iZXJzJyxcbiAgICAgICAgICAgICAgICAgICAgJ1N1YnNjcmliZScsXG4gICAgICAgICAgICAgICAgICAgICdTZWFyY2gnLFxuICAgICAgICAgICAgICAgICAgICAnTG9nbycsXG4gICAgICAgICAgICAgICAgICAgICdNYXAnLFxuICAgICAgICAgICAgICAgICAgICAnRmFjZWJvb2stTGlrZS1Cb3gnLFxuICAgICAgICAgICAgICAgICAgICAnQmFja2dyb3VuZC1JbWFnZScsXG4gICAgICAgICAgICAgICAgICAgICdGYXZpY29uJ1xuICAgICAgICAgICAgICAgIF0sXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgSG9tZToge1xuICAgICAgICAgICAgICAgIHdpZGdldHM6IFsnQnVzaW5lc3MtTmFtZScsICdBZGRyZXNzJywgJ01vYmlsZS1OdW1iZXJzJywgJ0J1c2luZXNzLUVucXVpcmllcycsICdCdXNpbmVzcy1EZXNjcmlwdGlvbicsICdVcGRhdGUnLCAnT2ZmZXJzJywgJ0FsbC1Qcm9kdWN0cycsICdCdXNpbmVzcy1Ib3VycycsICdCYWNrZ3JvdW5kLUltYWdlJywgJ0ltYWdlLUdhbGxlcnknLCAnU3Vic2NyaWJlcnMtQ291bnQnLCAnVGFncycsICdNYXAnLCAnVmlzaXRzJywgJ0N1c3RvbScsICdWaWRlby1HYWxsZXJ5JywgJ0xvZ28nLCAnRmFjZWJvb2stTGlrZS1Cb3gnXSxcbiAgICAgICAgICAgICAgICBtYW5kYXRvcnk6IFsnQnVzaW5lc3MtTmFtZScsICdBZGRyZXNzJywgJ01vYmlsZS1OdW1iZXJzJywgJ0J1c2luZXNzLUVucXVpcmllcyddLFxuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIE9mZmVyczoge1xuICAgICAgICAgICAgICAgIHdpZGdldHM6IFsnVXBkYXRlJywgJ01vYmlsZS1OdW1iZXJzJywgJ0J1c2luZXNzLUVucXVpcmllcycsICdCdXNpbmVzcy1OYW1lJywgJ0J1c2luZXNzLURlc2NyaXB0aW9uJywgJ0FkZHJlc3MnLCAnQmFja2dyb3VuZC1JbWFnZSddLFxuICAgICAgICAgICAgICAgIG1hbmRhdG9yeTogWydVcGRhdGUnXSxcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBVcGRhdGU6IHtcbiAgICAgICAgICAgICAgICB3aWRnZXRzOiBbJ1VwZGF0ZScsICdCdXNpbmVzcy1OYW1lJywgJ0FkZHJlc3MnLCAnTW9iaWxlLU51bWJlcnMnLCAnQnVzaW5lc3MtRW5xdWlyaWVzJywgJ0J1c2luZXNzLURlc2NyaXB0aW9uJywgJ1RhZ3MnLCAnQmFja2dyb3VuZC1JbWFnZSddLFxuICAgICAgICAgICAgICAgIG1hbmRhdG9yeTogWydVcGRhdGUnXSxcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAnQWxsLVByb2R1Y3RzJzoge1xuICAgICAgICAgICAgICAgIHdpZGdldHM6IFsnUHJvZHVjdCcsICdCdXNpbmVzcy1FbnF1aXJpZXMnLCAnTW9iaWxlLU51bWJlcnMnLCAnQWRkcmVzcycsICdCdXNpbmVzcy1OYW1lJywgJ0J1c2luZXNzLURlc2NyaXB0aW9uJywgJ0JhY2tncm91bmQtSW1hZ2UnXSxcbiAgICAgICAgICAgICAgICBtYW5kYXRvcnk6IFsnUHJvZHVjdCddLFxuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICdVcGRhdGUtRGV0YWlscyc6IHtcbiAgICAgICAgICAgICAgICB3aWRnZXRzOiBbJ1VwZGF0ZScsICdNb2JpbGUtTnVtYmVycycsICdCdXNpbmVzcy1FbnF1aXJpZXMnLCAnQWRkcmVzcycsICdCdXNpbmVzcy1OYW1lJywgJ0J1c2luZXNzLURlc2NyaXB0aW9uJywgJ0JhY2tncm91bmQtSW1hZ2UnXSxcbiAgICAgICAgICAgICAgICBtYW5kYXRvcnk6IFsnVXBkYXRlJ10sXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgJ1Byb2R1Y3QtRGV0YWlscyc6IHtcbiAgICAgICAgICAgICAgICB3aWRnZXRzOiBbJ1Byb2R1Y3QnLCAnTW9iaWxlLU51bWJlcnMnLCAnQnVzaW5lc3MtRW5xdWlyaWVzJywgJ0FkZHJlc3MnLCAnQnVzaW5lc3MtTmFtZScsICdCdXNpbmVzcy1EZXNjcmlwdGlvbicsICdCYWNrZ3JvdW5kLUltYWdlJ10sXG4gICAgICAgICAgICAgICAgbWFuZGF0b3J5OiBbJ1Byb2R1Y3QnXSxcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBDdXN0b206IHtcbiAgICAgICAgICAgICAgICB3aWRnZXRzOiBbJ0JhY2tncm91bmQtSW1hZ2UnLCAnQWRkcmVzcycsICdNb2JpbGUtTnVtYmVycycsICdCdXNpbmVzcy1FbnF1aXJpZXMnLCAnQnVzaW5lc3MtTmFtZScsICdCdXNpbmVzcy1Ib3VycyddLFxuICAgICAgICAgICAgICAgIG1hbmRhdG9yeTogW10sXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgU2VhcmNoOiB7XG4gICAgICAgICAgICAgICAgd2lkZ2V0czogWydVcGRhdGUnLCAnQmFja2dyb3VuZC1JbWFnZScsICdCdXNpbmVzcy1OYW1lJ10sXG4gICAgICAgICAgICAgICAgbWFuZGF0b3J5OiBbJ1VwZGF0ZSddLFxuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICdQcm9kdWN0LVNlYXJjaCc6IHtcbiAgICAgICAgICAgICAgICB3aWRnZXRzOiBbJ1Byb2R1Y3QnLCAnQmFja2dyb3VuZC1JbWFnZSddLFxuICAgICAgICAgICAgICAgIG1hbmRhdG9yeTogWydQcm9kdWN0J10sXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgJ0ltYWdlLUdhbGxlcnknOiB7XG4gICAgICAgICAgICAgICAgd2lkZ2V0czogWydJbWFnZSddLFxuICAgICAgICAgICAgICAgIG1hbmRhdG9yeTogWydJbWFnZSddLFxuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIE1hcDoge1xuICAgICAgICAgICAgICAgIHdpZGdldHM6IFsnTWFwJywgJ0J1c2luZXNzLU5hbWUnLCAnQmFja2dyb3VuZC1JbWFnZSddLFxuICAgICAgICAgICAgICAgIG1hbmRhdG9yeTogWydNYXAnXSxcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSxcbiAgICAgICAgd2lkZ2V0czoge1xuICAgICAgICAgICAgJ0J1c2luZXNzLU5hbWUnOiB7XG4gICAgICAgICAgICAgICAgcHJvcHM6IFsnQnVzaW5lc3MtTmFtZSddLFxuICAgICAgICAgICAgICAgIG1hbmRhdG9yeTogWydCdXNpbmVzcy1OYW1lJ10sXG4gICAgICAgICAgICAgICAgY29weToge1xuICAgICAgICAgICAgICAgICAgICB3aWRnZXQ6ICdUaGlzIGlzIHRoZSBCdXNpbmVzcy1OYW1lIHdpZGdldCcsXG4gICAgICAgICAgICAgICAgICAgIHByb3BzOiBbJ1RoaXMgaXMgdGhlIEJ1c2luZXNzLU5hbWUgcHJvcCddXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICdCdXNpbmVzcy1EZXNjcmlwdGlvbic6IHtcbiAgICAgICAgICAgICAgICBwcm9wczogWydCdXNpbmVzcy1EZXNjcmlwdGlvbiddLFxuICAgICAgICAgICAgICAgIG1hbmRhdG9yeTogWydCdXNpbmVzcy1EZXNjcmlwdGlvbiddLFxuICAgICAgICAgICAgICAgIGNvcHk6IHtcbiAgICAgICAgICAgICAgICAgICAgd2lkZ2V0OiAnVGhpcyBpcyB0aGUgQnVzaW5lc3MtRGVzY3JpcHRpb24gd2lkZ2V0JyxcbiAgICAgICAgICAgICAgICAgICAgcHJvcHM6IFsnVGhpcyBpcyB0aGUgQnVzaW5lc3MtRGVzY3JpcHRpb24gcHJvcCddXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIFVwZGF0ZToge1xuICAgICAgICAgICAgICAgIHByb3BzOiBbJ1VwZGF0ZScsICdUaW1lU3RhbXAnLCAnVXJsJywgJ0J1c2luZXNzLUltYWdlJywgJ1R5cGUnLCAnVGFncyddLFxuICAgICAgICAgICAgICAgIG1hbmRhdG9yeTogWydVcGRhdGUnLCAnVGltZVN0YW1wJywgJ1VybCddLFxuICAgICAgICAgICAgICAgIGNvcHk6IHtcbiAgICAgICAgICAgICAgICAgICAgd2lkZ2V0OiAnVGhpcyBpcyB0aGUgVXBkYXRlIHdpZGdldCcsXG4gICAgICAgICAgICAgICAgICAgIHByb3BzOiBbXG4gICAgICAgICAgICAgICAgICAgICAgICAnVGhpcyBpcyB0aGUgVXBkYXRlIHByb3AnLFxuICAgICAgICAgICAgICAgICAgICAgICAgJ1RoaXMgaXMgdGhlIFRpbWVTdGFtcCBwcm9wJyxcbiAgICAgICAgICAgICAgICAgICAgICAgICdUaGlzIGlzIHRoZSBVcmwgcHJvcCcsXG4gICAgICAgICAgICAgICAgICAgICAgICAnVGhpcyBpcyB0aGUgQnVzaW5lc3MtSW1hZ2UgcHJvcCcsXG4gICAgICAgICAgICAgICAgICAgICAgICAnVGhpcyBpcyB0aGUgVHlwZSAoTEFURVNUL0ZFQVRVUkVEL1BPUFVMQVIpIHByb3AnLFxuICAgICAgICAgICAgICAgICAgICAgICAgJ1RoaXMgaXMgdGhlIFRhZ3MgcHJvcCdcbiAgICAgICAgICAgICAgICAgICAgXVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBQcm9kdWN0OiB7XG4gICAgICAgICAgICAgICAgcHJvcHM6IFsnTmFtZScsICdEZXNjcmlwdGlvbicsICdDb3N0JywgJ1VybCcsICdDdXJyZW5jeScsICdBdmFpbGFiaWxpdHknLCAnSW1hZ2UnXSxcbiAgICAgICAgICAgICAgICBtYW5kYXRvcnk6IFsnTmFtZScsICdEZXNjcmlwdGlvbicsICdDb3N0JywgJ1VybCcsICdDdXJyZW5jeSddLFxuICAgICAgICAgICAgICAgIGNvcHk6IHtcbiAgICAgICAgICAgICAgICAgICAgd2lkZ2V0OiAnVGhpcyBpcyB0aGUgUHJvZHVjdCB3aWRnZXQnLFxuICAgICAgICAgICAgICAgICAgICBwcm9wczogW1xuICAgICAgICAgICAgICAgICAgICAgICAgJ1RoaXMgaXMgdGhlIE5hbWUgcHJvcCBUaGlzIGlzIHRoZSBOYW1lIHByb3AgVGhpcyBpcyB0aGUgTmFtZSBwcm9wIFRoaXMgaXMgdGhlIE5hbWUgcHJvcCBUaGlzIGlzIHRoZSBOYW1lIHByb3AgVGhpcyBpcyB0aGUgTmFtZSBwcm9wIFRoaXMgaXMgdGhlIE5hbWUgcHJvcCcsXG4gICAgICAgICAgICAgICAgICAgICAgICAnVGhpcyBpcyB0aGUgRGVzY3JpcHRpb24gcHJvcCBUaGlzIGlzIHRoZSBOYW1lIHByb3AgVGhpcyBpcyB0aGUgTmFtZSBwcm9wIFRoaXMgaXMgdGhlIE5hbWUgcHJvcCBUaGlzIGlzIHRoZSBOYW1lIHByb3AgVGhpcyBpcyB0aGUgTmFtZSBwcm9wIFRoaXMgaXMgdGhlIE5hbWUgcHJvcCcsXG4gICAgICAgICAgICAgICAgICAgICAgICAnVGhpcyBpcyB0aGUgQ29zdCBwcm9wIFRoaXMgaXMgdGhlIE5hbWUgcHJvcCBUaGlzIGlzIHRoZSBOYW1lIHByb3AgVGhpcyBpcyB0aGUgTmFtZSBwcm9wIFRoaXMgaXMgdGhlIE5hbWUgcHJvcCBUaGlzIGlzIHRoZSBOYW1lIHByb3AgVGhpcyBpcyB0aGUgTmFtZSBwcm9wJyxcbiAgICAgICAgICAgICAgICAgICAgICAgICdUaGlzIGlzIHRoZSBVcmwgcHJvcCBUaGlzIGlzIHRoZSBOYW1lIHByb3AgVGhpcyBpcyB0aGUgTmFtZSBwcm9wIFRoaXMgaXMgdGhlIE5hbWUgcHJvcCBUaGlzIGlzIHRoZSBOYW1lIHByb3AgVGhpcyBpcyB0aGUgTmFtZSBwcm9wIFRoaXMgaXMgdGhlIE5hbWUgcHJvcCcsXG4gICAgICAgICAgICAgICAgICAgICAgICAnVGhpcyBpcyB0aGUgQ3VycmVuY3kgcHJvcCBUaGlzIGlzIHRoZSBOYW1lIHByb3AgVGhpcyBpcyB0aGUgTmFtZSBwcm9wIFRoaXMgaXMgdGhlIE5hbWUgcHJvcCBUaGlzIGlzIHRoZSBOYW1lIHByb3AgVGhpcyBpcyB0aGUgTmFtZSBwcm9wIFRoaXMgaXMgdGhlIE5hbWUgcHJvcCcsXG4gICAgICAgICAgICAgICAgICAgICAgICAnVGhpcyBpcyB0aGUgQXZhaWxhYmlsaXR5IHByb3AgVGhpcyBpcyB0aGUgTmFtZSBwcm9wIFRoaXMgaXMgdGhlIE5hbWUgcHJvcCBUaGlzIGlzIHRoZSBOYW1lIHByb3AgVGhpcyBpcyB0aGUgTmFtZSBwcm9wIFRoaXMgaXMgdGhlIE5hbWUgcHJvcCBUaGlzIGlzIHRoZSBOYW1lIHByb3AnLFxuICAgICAgICAgICAgICAgICAgICAgICAgJ1RoaXMgaXMgdGhlIEltYWdlIHByb3AgVGhpcyBpcyB0aGUgTmFtZSBwcm9wIFRoaXMgaXMgdGhlIE5hbWUgcHJvcCBUaGlzIGlzIHRoZSBOYW1lIHByb3AgVGhpcyBpcyB0aGUgTmFtZSBwcm9wIFRoaXMgaXMgdGhlIE5hbWUgcHJvcCBUaGlzIGlzIHRoZSBOYW1lIHByb3AnXG4gICAgICAgICAgICAgICAgICAgIF1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgJ0J1c2luZXNzLUhvdXJzJzoge1xuICAgICAgICAgICAgICAgIHByb3BzOiBbJ09wZW4tTm93JywgJ0RheXMtT3BlbicsICdUaW1lLU9wZW4nXSxcbiAgICAgICAgICAgICAgICBtYW5kYXRvcnk6IFsnT3Blbi1Ob3cnLCAnRGF5cy1PcGVuJywgJ1RpbWUtT3BlbiddLFxuICAgICAgICAgICAgICAgIGNvcHk6IHtcbiAgICAgICAgICAgICAgICAgICAgd2lkZ2V0OiAnVGhpcyBpcyB0aGUgQnVzaW5lc3MtSG91cnMgd2lkZ2V0JyxcbiAgICAgICAgICAgICAgICAgICAgcHJvcHM6IFsnVGhpcyBpcyB0aGUgT3Blbi1Ob3cgcHJvcCcsICdUaGlzIGlzIHRoZSBEYXlzLU9wZW4gcHJvcCcsICdUaGlzIGlzIHRoZSBUaW1lLU9wZW4gcHJvcCddXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICdBbGwtUHJvZHVjdHMnOiB7XG4gICAgICAgICAgICAgICAgcHJvcHM6IFsnTmFtZScsICdVcmwnLCAnUHJpY2UnLCAnQ3VycmVuY3knXSxcbiAgICAgICAgICAgICAgICBtYW5kYXRvcnk6IFsnTmFtZScsICdVcmwnLCAnUHJpY2UnLCAnQ3VycmVuY3knXSxcbiAgICAgICAgICAgICAgICBjb3B5OiB7XG4gICAgICAgICAgICAgICAgICAgIHdpZGdldDogJ1RoaXMgaXMgdGhlIEFsbC1Qcm9kdWN0cyB3aWRnZXQnLFxuICAgICAgICAgICAgICAgICAgICBwcm9wczogW1xuICAgICAgICAgICAgICAgICAgICAgICAgJ1RoaXMgaXMgdGhlIE5hbWUgcHJvcCcsXG4gICAgICAgICAgICAgICAgICAgICAgICAnVGhpcyBpcyB0aGUgVXJsIHByb3AnLFxuICAgICAgICAgICAgICAgICAgICAgICAgJ1RoaXMgaXMgdGhlIFByaWNlIHByb3AnLFxuICAgICAgICAgICAgICAgICAgICAgICAgJ1RoaXMgaXMgdGhlIEN1cnJlbmN5IHByb3AnLFxuICAgICAgICAgICAgICAgICAgICBdXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICdTdWJzY3JpYmVycy1Db3VudCc6IHtcbiAgICAgICAgICAgICAgICBwcm9wczogWydDb3VudCddLFxuICAgICAgICAgICAgICAgIG1hbmRhdG9yeTogWydDb3VudCddLFxuICAgICAgICAgICAgICAgIGNvcHk6IHtcbiAgICAgICAgICAgICAgICAgICAgd2lkZ2V0OiAnVGhpcyBpcyB0aGUgU3Vic2NyaWJlcnMtQ291bnQgd2lkZ2V0JyxcbiAgICAgICAgICAgICAgICAgICAgcHJvcHM6IFsnVGhpcyBpcyB0aGUgQ291bnQgcHJvcCddXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIFRhZ3M6IHtcbiAgICAgICAgICAgICAgICBwcm9wczogWydUYWdzJ10sXG4gICAgICAgICAgICAgICAgbWFuZGF0b3J5OiBbJ1RhZ3MnXSxcbiAgICAgICAgICAgICAgICBjb3B5OiB7XG4gICAgICAgICAgICAgICAgICAgIHdpZGdldDogJ1RoaXMgaXMgdGhlIFRhZ3Mgd2lkZ2V0JyxcbiAgICAgICAgICAgICAgICAgICAgcHJvcHM6IFsnVGhpcyBpcyB0aGUgVGFncyBwcm9wJ11cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgVmlzaXRzOiB7XG4gICAgICAgICAgICAgICAgcHJvcHM6IFsnQ291bnQnXSxcbiAgICAgICAgICAgICAgICBtYW5kYXRvcnk6IFsnQ291bnQnXSxcbiAgICAgICAgICAgICAgICBjb3B5OiB7XG4gICAgICAgICAgICAgICAgICAgIHdpZGdldDogJ1RoaXMgaXMgdGhlIFZpc2l0cyB3aWRnZXQnLFxuICAgICAgICAgICAgICAgICAgICBwcm9wczogWydUaGlzIGlzIHRoZSBDb3VudCBwcm9wJ11cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgJ0J1c2luZXNzLUVucXVpcmllcyc6IHtcbiAgICAgICAgICAgICAgICBwcm9wczogWydFbnF1aXJ5LUNvbnRhY3QnLCAnRW5xdWlyeS1NZXNzYWdlJywgJ0VucXVpcnktQnV0dG9uJ10sXG4gICAgICAgICAgICAgICAgbWFuZGF0b3J5OiBbJ0VucXVpcnktQ29udGFjdCcsICdFbnF1aXJ5LU1lc3NhZ2UnLCAnRW5xdWlyeS1CdXR0b24nXSxcbiAgICAgICAgICAgICAgICBjb3B5OiB7XG4gICAgICAgICAgICAgICAgICAgIHdpZGdldDogJ1RoaXMgaXMgdGhlIEJ1c2luZXNzLUVucXVpcmllcyB3aWRnZXQnLFxuICAgICAgICAgICAgICAgICAgICBwcm9wczogW1xuICAgICAgICAgICAgICAgICAgICAgICAgJ1RoaXMgaXMgdGhlIEVucXVpcnktQ29udGFjdCBwcm9wJyxcbiAgICAgICAgICAgICAgICAgICAgICAgICdUaGlzIGlzIHRoZSBFbnF1aXJ5LU1lc3NhZ2UgcHJvcCcsXG4gICAgICAgICAgICAgICAgICAgICAgICAnVGhpcyBpcyB0aGUgRW5xdWlyeS1CdXR0b24gcHJvcCdcbiAgICAgICAgICAgICAgICAgICAgXVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAnVmlkZW8tR2FsbGVyeSc6IHtcbiAgICAgICAgICAgICAgICBwcm9wczogWydVcmwnXSxcbiAgICAgICAgICAgICAgICBtYW5kYXRvcnk6IFsnVXJsJ10sXG4gICAgICAgICAgICAgICAgY29weToge1xuICAgICAgICAgICAgICAgICAgICB3aWRnZXQ6ICdUaGlzIGlzIHRoZSBWaWRlby1HYWxsZXJ5IHdpZGdldCcsXG4gICAgICAgICAgICAgICAgICAgIHByb3BzOiBbJ1RoaXMgaXMgdGhlIFVybCBwcm9wJ11cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgJ1NpdGUtTGluayc6IHtcbiAgICAgICAgICAgICAgICBwcm9wczogWydVcmwnXSxcbiAgICAgICAgICAgICAgICBtYW5kYXRvcnk6IFsnVXJsJ10sXG4gICAgICAgICAgICAgICAgY29weToge1xuICAgICAgICAgICAgICAgICAgICB3aWRnZXQ6ICdUaGlzIGlzIHRoZSBTaXRlLUxpbmsgd2lkZ2V0JyxcbiAgICAgICAgICAgICAgICAgICAgcHJvcHM6IFsnVGhpcyBpcyB0aGUgVXJsIHByb3AnXVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBBZGRyZXNzOiB7XG4gICAgICAgICAgICAgICAgcHJvcHM6IFsnQ291bnRyeScsICdMaW5lLTEnLCAnQ2l0eScsICdQaW5jb2RlJ10sXG4gICAgICAgICAgICAgICAgbWFuZGF0b3J5OiBbJ0NvdW50cnknXSxcbiAgICAgICAgICAgICAgICBjb3B5OiB7XG4gICAgICAgICAgICAgICAgICAgIHdpZGdldDogJ1RoaXMgaXMgdGhlIEFkZHJlc3Mgd2lkZ2V0JyxcbiAgICAgICAgICAgICAgICAgICAgcHJvcHM6IFtcbiAgICAgICAgICAgICAgICAgICAgICAgICdUaGlzIGlzIHRoZSBDb3VudHJ5IHByb3AnLFxuICAgICAgICAgICAgICAgICAgICAgICAgJ1RoaXMgaXMgdGhlIExpbmUtMSBwcm9wJyxcbiAgICAgICAgICAgICAgICAgICAgICAgICdUaGlzIGlzIHRoZSBDaXR5IHByb3AnLFxuICAgICAgICAgICAgICAgICAgICAgICAgJ1RoaXMgaXMgdGhlIFBpbmNvZGUgcHJvcCdcbiAgICAgICAgICAgICAgICAgICAgXVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAnTW9iaWxlLU51bWJlcnMnOiB7XG4gICAgICAgICAgICAgICAgcHJvcHM6IFsnUHJpbWFyeScsICdXb3JrJywgJ0hvbWUnXSxcbiAgICAgICAgICAgICAgICBtYW5kYXRvcnk6IFsnUHJpbWFyeSddLFxuICAgICAgICAgICAgICAgIGNvcHk6IHtcbiAgICAgICAgICAgICAgICAgICAgd2lkZ2V0OiAnVGhpcyBpcyB0aGUgTW9iaWxlLU51bWJlcnMgd2lkZ2V0JyxcbiAgICAgICAgICAgICAgICAgICAgcHJvcHM6IFtcbiAgICAgICAgICAgICAgICAgICAgICAgICdUaGlzIGlzIHRoZSBQcmltYXJ5IHByb3AnLFxuICAgICAgICAgICAgICAgICAgICAgICAgJ1RoaXMgaXMgdGhlIFdvcmsgcHJvcCcsXG4gICAgICAgICAgICAgICAgICAgICAgICAnVGhpcyBpcyB0aGUgSG9tZSBwcm9wJ1xuICAgICAgICAgICAgICAgICAgICBdXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIFN1YnNjcmliZToge1xuICAgICAgICAgICAgICAgIHByb3BzOiBbJ1N1YnNjcmliZS1CdXR0b24nLCAnU3Vic2NyaWJlLUNvbnRhY3QnXSxcbiAgICAgICAgICAgICAgICBtYW5kYXRvcnk6IFsnU3Vic2NyaWJlLUJ1dHRvbicsICdTdWJzY3JpYmUtQ29udGFjdCddLFxuICAgICAgICAgICAgICAgIGNvcHk6IHtcbiAgICAgICAgICAgICAgICAgICAgd2lkZ2V0OiAnVGhpcyBpcyB0aGUgU3Vic2NyaWJlIHdpZGdldCcsXG4gICAgICAgICAgICAgICAgICAgIHByb3BzOiBbXG4gICAgICAgICAgICAgICAgICAgICAgICAnVGhpcyBpcyB0aGUgU3Vic2NyaWJlLUJ1dHRvbiBwcm9wJyxcbiAgICAgICAgICAgICAgICAgICAgICAgICdUaGlzIGlzIHRoZSBTdWJzY3JpYmUtQ29udGFjdCBwcm9wJ1xuICAgICAgICAgICAgICAgICAgICBdXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIFNlYXJjaDoge1xuICAgICAgICAgICAgICAgIHByb3BzOiBbJ1NlYXJjaC1CdXR0b24nLCAnU2VhcmNoLVF1ZXJ5J10sXG4gICAgICAgICAgICAgICAgbWFuZGF0b3J5OiBbJ1NlYXJjaC1CdXR0b24nLCAnU2VhcmNoLVF1ZXJ5J10sXG4gICAgICAgICAgICAgICAgY29weToge1xuICAgICAgICAgICAgICAgICAgICB3aWRnZXQ6ICdUaGlzIGlzIHRoZSBTZWFyY2ggd2lkZ2V0JyxcbiAgICAgICAgICAgICAgICAgICAgcHJvcHM6IFtcbiAgICAgICAgICAgICAgICAgICAgICAgICdUaGlzIGlzIHRoZSBTZWFyY2gtQnV0dG9uIHByb3AnLFxuICAgICAgICAgICAgICAgICAgICAgICAgJ1RoaXMgaXMgdGhlIFNlYXJjaC1RdWVyeSBwcm9wJ1xuICAgICAgICAgICAgICAgICAgICBdXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIExvZ286IHtcbiAgICAgICAgICAgICAgICBwcm9wczogWydVcmwnXSxcbiAgICAgICAgICAgICAgICBtYW5kYXRvcnk6IFsnVXJsJ10sXG4gICAgICAgICAgICAgICAgY29weToge1xuICAgICAgICAgICAgICAgICAgICB3aWRnZXQ6ICdUaGlzIGlzIHRoZSBMb2dvIHdpZGdldCcsXG4gICAgICAgICAgICAgICAgICAgIHByb3BzOiBbJ1RoaXMgaXMgdGhlIFVybCBwcm9wJ11cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgTWFwOiB7XG4gICAgICAgICAgICAgICAgcHJvcHM6IFsnTGF0ICcsICdMb25nJ10sXG4gICAgICAgICAgICAgICAgbWFuZGF0b3J5OiBbJ0xhdCAnLCAnTG9uZyddLFxuICAgICAgICAgICAgICAgIGNvcHk6IHtcbiAgICAgICAgICAgICAgICAgICAgd2lkZ2V0OiAnVGhpcyBpcyB0aGUgTWFwIHdpZGdldCcsXG4gICAgICAgICAgICAgICAgICAgIHByb3BzOiBbXG4gICAgICAgICAgICAgICAgICAgICAgICAnVGhpcyBpcyB0aGUgTGF0IHByb3AnLFxuICAgICAgICAgICAgICAgICAgICAgICAgJ1RoaXMgaXMgdGhlIExvbmcgcHJvcCdcbiAgICAgICAgICAgICAgICAgICAgXVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAnRmFjZWJvb2stTGlrZS1Cb3gnOiB7XG4gICAgICAgICAgICAgICAgcHJvcHM6IFsnUGFnZS1OYW1lJ10sXG4gICAgICAgICAgICAgICAgbWFuZGF0b3J5OiBbJ1BhZ2UtTmFtZSddLFxuICAgICAgICAgICAgICAgIGNvcHk6IHtcbiAgICAgICAgICAgICAgICAgICAgd2lkZ2V0OiAnVGhpcyBpcyB0aGUgRmFjZWJvb2stTGlrZS1Cb3ggd2lkZ2V0JyxcbiAgICAgICAgICAgICAgICAgICAgcHJvcHM6IFsnVGhpcyBpcyB0aGUgUGFnZS1OYW1lIHByb3AnXVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAnQmFja2dyb3VuZC1JbWFnZSc6IHtcbiAgICAgICAgICAgICAgICBwcm9wczogWydVcmwnXSxcbiAgICAgICAgICAgICAgICBtYW5kYXRvcnk6IFsnVXJsJ10sXG4gICAgICAgICAgICAgICAgY29weToge1xuICAgICAgICAgICAgICAgICAgICB3aWRnZXQ6ICdUaGlzIGlzIHRoZSBCYWNrZ3JvdW5kLUltYWdlIHdpZGdldCcsXG4gICAgICAgICAgICAgICAgICAgIHByb3BzOiBbJ1RoaXMgaXMgdGhlIFVybCBwcm9wJ11cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgRmF2aWNvbjoge1xuICAgICAgICAgICAgICAgIHByb3BzOiBbJ1VybCddLFxuICAgICAgICAgICAgICAgIG1hbmRhdG9yeTogWydVcmwnXSxcbiAgICAgICAgICAgICAgICBjb3B5OiB7XG4gICAgICAgICAgICAgICAgICAgIHdpZGdldDogJ1RoaXMgaXMgdGhlIEZhdmljb24gd2lkZ2V0JyxcbiAgICAgICAgICAgICAgICAgICAgcHJvcHM6IFsnVGhpcyBpcyB0aGUgVXJsIHByb3AnXVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfSxcbiAgICAgICAgc3RhdGU6IHtcbiAgICAgICAgICAgIHRpbWVvdXRUb3A6IHVuZGVmaW5lZCxcbiAgICAgICAgICAgIGluZGV4OiAxLFxuICAgICAgICAgICAgdmlldzogdW5kZWZpbmVkLFxuICAgICAgICAgICAgaW5pdDogdW5kZWZpbmVkXG4gICAgICAgIH0sXG4gICAgICAgIG5vZGVzOiB7XG4gICAgICAgICAgICBkcm9wRG93bjogZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLnNlbGVjdC12aWV3JyksXG4gICAgICAgICAgICBkcm9wRG93bkxpc3Q6IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy52aWV3LWxpc3QnKSxcbiAgICAgICAgICAgIGRyb3BEb3duTGlzdEl0ZW1zOiB1bmRlZmluZWQsXG4gICAgICAgICAgICB2aWV3OiBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcuc2VsZWN0LXZpZXcgLnZpZXcnKSxcbiAgICAgICAgICAgIGJ1dHRvbkV4cGFuZDogZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLmJhci12aWV3IGJ1dHRvbicpLFxuICAgICAgICAgICAgZG9jczogdW5kZWZpbmVkLFxuICAgICAgICAgICAgZG9jc1Zpc2libGU6IHVuZGVmaW5lZCxcbiAgICAgICAgICAgIGRvY3NDb250YWluZXI6IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5jb250YWluZXItZG9jcycpXG4gICAgICAgIH0sXG4gICAgICAgIG9yZGVyOiBbJ2VudGl0eScsICd2aWV3JywgJ3dpZGdldCcsICd3aWRnZXRQcm9wcyddXG4gICAgfSxcblxuICAgIGRyb3BEb3duOiB7XG5cbiAgICAgICAgaW5pdCgpIHtcbiAgICAgICAgICAgIERvY3MuZHJvcERvd24ucG9wdWxhdGUoKVxuICAgICAgICAgICAgRG9jcy5jb25maWcubm9kZXMuZHJvcERvd25MaXN0SXRlbXMgPSByZXR1cm5BcnJheShkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKCcudmlldy1saXN0IGxpJykpXG4gICAgICAgICAgICBEb2NzLmRyb3BEb3duLmJpbmRFdmVudHMoKVxuICAgICAgICAgICAgLy8gU2VsZWN0IE1hc3RlciBWaWV3XG4gICAgICAgICAgICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcudmlldy1saXN0IGxpOm50aC1jaGlsZCgyKScpLmNsaWNrKClcbiAgICAgICAgfSxcblxuICAgICAgICBiaW5kRXZlbnRzKCkge1xuICAgICAgICAgICAgLy9PbiBjbGlja2luZyBkb2N1bWVudFxuICAgICAgICAgICAgZG9jdW1lbnQuYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCAoKSA9PiB7XG4gICAgICAgICAgICAgICAgRG9jcy5kcm9wRG93bi5jb2xsYXBzZSgpXG4gICAgICAgICAgICB9KVxuXG4gICAgICAgICAgICAvL09uIHByZXNzaW5nIGVzY2FwZVxuICAgICAgICAgICAgZG9jdW1lbnQuYWRkRXZlbnRMaXN0ZW5lcigna2V5ZG93bicsIChldmVudCkgPT4ge1xuICAgICAgICAgICAgICAgIGlmIChldmVudC53aGljaCA9PSAyNykge1xuICAgICAgICAgICAgICAgICAgICBEb2NzLmRyb3BEb3duLmNvbGxhcHNlKClcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KVxuXG4gICAgICAgICAgICAvL09uIGNsaWNraW5nIGRyb3Bkb3duXG4gICAgICAgICAgICBEb2NzLmNvbmZpZy5ub2Rlcy5kcm9wRG93bi5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIChldmVudCkgPT4ge1xuICAgICAgICAgICAgICAgIGV2ZW50LnN0b3BQcm9wYWdhdGlvbigpXG4gICAgICAgICAgICAgICAgRG9jcy5kcm9wRG93bi50b2dnbGUoKVxuICAgICAgICAgICAgfSlcblxuICAgICAgICAgICAgLy9PbiBzZWxlY3RpbmcgZnJvbSBkcm9wZG93blxuICAgICAgICAgICAgRG9jcy5jb25maWcubm9kZXMuZHJvcERvd25MaXN0SXRlbXMuZm9yRWFjaCh2aWV3ID0+IHtcbiAgICAgICAgICAgICAgICB2aWV3LmFkZEV2ZW50TGlzdGVuZXIoJ2NsaWNrJywgKGV2ZW50KSA9PiB7XG4gICAgICAgICAgICAgICAgICAgIGV2ZW50LnN0b3BQcm9wYWdhdGlvbigpXG4gICAgICAgICAgICAgICAgICAgIERvY3MuZHJvcERvd24uc2VsZWN0KGV2ZW50LmN1cnJlbnRUYXJnZXQpXG4gICAgICAgICAgICAgICAgICAgIERvY3MuZHJvcERvd24uY29sbGFwc2UoKVxuICAgICAgICAgICAgICAgICAgICBEb2NzLmRyb3BEb3duLnNjcm9sbCgpXG4gICAgICAgICAgICAgICAgICAgIERvY3MuZG9jLnNlbGVjdCgpXG5cbiAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgfSlcblxuICAgICAgICAgICAgLy9PbiBjbGlja2luZyBleHBhbmRcbiAgICAgICAgICAgIERvY3MuY29uZmlnLm5vZGVzLmJ1dHRvbkV4cGFuZC5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIChldmVudCkgPT4ge1xuICAgICAgICAgICAgICAgIGV2ZW50LnN0b3BQcm9wYWdhdGlvbigpXG4gICAgICAgICAgICAgICAgRG9jcy5kb2MubWV0aG9kcy50b2dnbGVTZW1pKClcbiAgICAgICAgICAgIH0pXG4gICAgICAgIH0sXG5cbiAgICAgICAgcG9wdWxhdGUoKSB7XG4gICAgICAgICAgICBPYmplY3Qua2V5cyhEb2NzLmNvbmZpZy52aWV3cykuZm9yRWFjaCh2aWV3ID0+IHtcbiAgICAgICAgICAgICAgICBsZXQgaXRlbSA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2xpJylcbiAgICAgICAgICAgICAgICBsZXQgbmFtZSA9IGRvY3VtZW50LmNyZWF0ZVRleHROb2RlKHZpZXcpXG4gICAgICAgICAgICAgICAgaXRlbS5hcHBlbmRDaGlsZChuYW1lKVxuICAgICAgICAgICAgICAgIERvY3MuY29uZmlnLm5vZGVzLmRyb3BEb3duTGlzdC5hcHBlbmRDaGlsZChpdGVtKVxuICAgICAgICAgICAgfSlcbiAgICAgICAgfSxcblxuICAgICAgICBjb2xsYXBzZSgpIHtcbiAgICAgICAgICAgIERvY3MuY29uZmlnLm5vZGVzLmRyb3BEb3duLmNsYXNzTGlzdC5yZW1vdmUoJ3NlbGVjdC1leHBhbmQnKVxuICAgICAgICB9LFxuXG4gICAgICAgIHRvZ2dsZSgpIHtcbiAgICAgICAgICAgIERvY3MuY29uZmlnLm5vZGVzLmRyb3BEb3duLmNsYXNzTGlzdC50b2dnbGUoJ3NlbGVjdC1leHBhbmQnKVxuICAgICAgICB9LFxuXG4gICAgICAgIHNlbGVjdCh2aWV3KSB7XG4gICAgICAgICAgICBEb2NzLmNvbmZpZy5ub2Rlcy52aWV3LmlubmVySFRNTCA9IERvY3MuY29uZmlnLnN0YXRlLnZpZXcgPSB2aWV3LmlubmVySFRNTFxuICAgICAgICB9LFxuXG4gICAgICAgIHNjcm9sbCgpIHtcbiAgICAgICAgICAgIGxldCB3cmFwcGVyID0gcGFyc2VJbnQoZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLndyYXBwZXItc2VjdGlvbnMnKS5jbGllbnRIZWlnaHQpXG4gICAgICAgICAgICBsZXQgZG9jdW1lbnRhdGlvbiA9IHBhcnNlSW50KGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5zZWN0aW9uLWRvY3VtZW50YXRpb24nKS5jbGllbnRIZWlnaHQpICsgcGFyc2VJbnQod2luZG93LmdldENvbXB1dGVkU3R5bGUoZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLnNlY3Rpb24tZG9jdW1lbnRhdGlvbicpKVsnbWFyZ2luLXRvcCddKSAtIHBhcnNlSW50KHdpbmRvdy5nZXRDb21wdXRlZFN0eWxlKGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5zZWN0aW9uLWRvY3VtZW50YXRpb24nKSlbJ21hcmdpbi1ib3R0b20nXSkgLyAxLjM1XG4gICAgICAgICAgICBsZXQgZW5xdWlyeSA9IHBhcnNlSW50KGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5zZWN0aW9uLWVucXVpcnknKS5jbGllbnRIZWlnaHQpXG4gICAgICAgICAgICBsZXQgZm9vdGVyID0gcGFyc2VJbnQoZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLnNlY3Rpb24tZm9vdGVyJykuY2xpZW50SGVpZ2h0KVxuICAgICAgICAgICAgbGV0IGJhciA9IHBhcnNlSW50KGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5iYXItdmlldycpLmNsaWVudEhlaWdodClcbiAgICAgICAgICAgIGxldCBib29zdGVycyA9IHBhcnNlSW50KGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5zZWN0aW9uLWJvb3N0ZXJzJykub2Zmc2V0SGVpZ2h0KVxuICAgICAgICAgICAgbGV0IHNjcm9sbCA9IHdyYXBwZXIgLSBkb2N1bWVudGF0aW9uIC0gZm9vdGVyIC0gYmFyIC0gZW5xdWlyeSAtIGJvb3N0ZXJzXG4gICAgICAgICAgICBpZiAoRG9jcy5jb25maWcuc3RhdGUuaW5pdCAhPSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgICAgICBzY3JvbGxUb1koc2Nyb2xsLCA1MDAsICdlYXNlSW5PdXRRdWludCcpXG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBEb2NzLmNvbmZpZy5zdGF0ZS5pbml0ID0gMVxuICAgICAgICB9XG4gICAgfSxcblxuICAgIGRvYzoge1xuICAgICAgICBpbml0KCkge1xuICAgICAgICAgICAgRG9jcy5kb2MuY3JlYXRlLmluaXQoKVxuICAgICAgICAgICAgRG9jcy5jb25maWcubm9kZXMuZG9jcyA9IHJldHVybkFycmF5KGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoJy53cmFwcGVyLWRvYycpKVxuICAgICAgICAgICAgRG9jcy5jb25maWcubm9kZXMuZG9jc1Zpc2libGUgPSBEb2NzLmNvbmZpZy5ub2Rlcy5kb2NzXG4gICAgICAgICAgICBEb2NzLmRvYy5tZXRob2RzLmFsaWduKClcbiAgICAgICAgICAgIERvY3MuZG9jLmJpbmRFdmVudHMoKVxuICAgICAgICB9LFxuXG4gICAgICAgIGJpbmRFdmVudHMoKSB7XG5cbiAgICAgICAgICAgIC8vIFdoZW4gYSBkb2MgaXMgY2xpY2tlZFxuICAgICAgICAgICAgRG9jcy5jb25maWcubm9kZXMuZG9jcy5mb3JFYWNoKChkb2MsIGluZGV4KSA9PiB7XG4gICAgICAgICAgICAgICAgZG9jLmFkZEV2ZW50TGlzdGVuZXIoJ2NsaWNrJywgKGV2ZW50KSA9PiB7XG4gICAgICAgICAgICAgICAgICAgIGV2ZW50LnN0b3BQcm9wYWdhdGlvbigpXG4gICAgICAgICAgICAgICAgICAgIGlmICh3aW5kb3cuaW5uZXJXaWR0aCA+IDY0MCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgRG9jcy5kb2MubWV0aG9kcy50b2dnbGUoZXZlbnQuY3VycmVudFRhcmdldClcbiAgICAgICAgICAgICAgICAgICAgICAgIERvY3MuZG9jLm1ldGhvZHMuYWRqdXN0Q29udGFpbmVySGVpZ2h0KGV2ZW50LmN1cnJlbnRUYXJnZXQpXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBEb2NzLmRvYy5tZXRob2RzLnRvZ2dsZU1vYmlsZShldmVudC5jdXJyZW50VGFyZ2V0KVxuICAgICAgICAgICAgICAgICAgICAgICAgRG9jcy5kb2MubWV0aG9kcy5hZGp1c3RDb250YWluZXJIZWlnaHRNb2JpbGUoZXZlbnQuY3VycmVudFRhcmdldClcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICB9KVxuXG4gICAgICAgICAgICAvL1doZW4gZXNjYXBlIGlzIHByZXNzZWRcbiAgICAgICAgICAgIGRvY3VtZW50LmFkZEV2ZW50TGlzdGVuZXIoJ2tleWRvd24nLCAoZXZlbnQpID0+IHtcbiAgICAgICAgICAgICAgICBpZiAoZXZlbnQud2hpY2ggPT0gMjcpIHtcbiAgICAgICAgICAgICAgICAgICAgRG9jcy5kb2MubWV0aG9kcy5jb250cmFjdCgpXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSlcblxuICAgICAgICAgICAgLy9PbiBjbGlja2luZyBhbnl3aGVyZSBvbiB0aGUgZG9jdW1lbnRcbiAgICAgICAgICAgIGRvY3VtZW50LmFkZEV2ZW50TGlzdGVuZXIoJ2NsaWNrJywgKCkgPT4ge1xuICAgICAgICAgICAgICAgIERvY3MuZG9jLm1ldGhvZHMuY29udHJhY3QoKVxuICAgICAgICAgICAgfSlcbiAgICAgICAgfSxcblxuICAgICAgICBjcmVhdGU6IHtcbiAgICAgICAgICAgIGluaXQoKSB7XG4gICAgICAgICAgICAgICAgRG9jcy5jb25maWcubm9kZXMuZG9jc0NvbnRhaW5lci5pbm5lckhUTUwgPSAnJ1xuICAgICAgICAgICAgICAgIERvY3MuZG9jLmNyZWF0ZS53cmFwcGVyKCkuZm9yRWFjaCgod3JhcHBlciwgaW5kZXgpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgd3JhcHBlci5zZXRBdHRyaWJ1dGUoJ2luZGV4JywgaW5kZXgpXG4gICAgICAgICAgICAgICAgICAgIERvY3MuY29uZmlnLm5vZGVzLmRvY3NDb250YWluZXIuYXBwZW5kQ2hpbGQod3JhcHBlcilcbiAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgfSxcblxuICAgICAgICAgICAgd2lkZ2V0cygpIHtcbiAgICAgICAgICAgICAgICBsZXQgdmlldyA9IERvY3MuY29uZmlnLnN0YXRlLnZpZXdcbiAgICAgICAgICAgICAgICBsZXQgdmlld3MgPSBEb2NzLmNvbmZpZy52aWV3c1xuICAgICAgICAgICAgICAgIHJldHVybiB2aWV3c1t2aWV3XS53aWRnZXRzLm1hcCh3aWRnZXQgPT4gYFxuICAgICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzcz1cIndpZGdldFwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgPHNwYW4gY2xhc3M9XCJuYW1lXCI+JHt3aWRnZXR9PC9zcGFuPlxuICAgICAgICAgICAgICAgICAgICA8L3NwYW4+XG4gICAgICAgICAgICAgICAgYClcbiAgICAgICAgICAgIH0sXG5cbiAgICAgICAgICAgIHByb3BzKCkge1xuICAgICAgICAgICAgICAgIGxldCB2aWV3ID0gRG9jcy5jb25maWcuc3RhdGUudmlld1xuICAgICAgICAgICAgICAgIGxldCB2aWV3cyA9IERvY3MuY29uZmlnLnZpZXdzXG4gICAgICAgICAgICAgICAgcmV0dXJuIHZpZXdzW3ZpZXddLndpZGdldHMubWFwKHdpZGdldCA9PiB7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBEb2NzLmNvbmZpZy53aWRnZXRzW3dpZGdldF0ucHJvcHMubWFwKHByb3AgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGA8c3BhbiBjbGFzcz1cInByb3AgcHJvcC1tYW5kYXRvcnlcIj48c3Bhbj4ke3Byb3B9PC9zcGFuPjwvc3Bhbj5gXG4gICAgICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIH0sXG5cbiAgICAgICAgICAgIG92ZXJ2aWV3KCkge1xuICAgICAgICAgICAgICAgIGxldCB3aWRnZXRzID0gRG9jcy5kb2MuY3JlYXRlLndpZGdldHMoKVxuICAgICAgICAgICAgICAgIGxldCBQcm9wcyA9IERvY3MuZG9jLmNyZWF0ZS5wcm9wcygpXG4gICAgICAgICAgICAgICAgbGV0IG92ZXJ2aWV3ID0gUHJvcHMubWFwKChwcm9wcywgaW5kZXgpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgbGV0IHByb3BOb2RlID0gJydcbiAgICAgICAgICAgICAgICAgICAgcHJvcHMubWFwKHByb3AgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgcHJvcE5vZGUgPSBwcm9wTm9kZSArIHByb3BcbiAgICAgICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHdpZGdldHNbaW5kZXhdICsgcHJvcE5vZGVcbiAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgIHJldHVybiBvdmVydmlldy5tYXAodmFsID0+IGA8ZGl2IGNsYXNzPVwib3ZlcnZpZXdcIj4ke3ZhbH08L2Rpdj5gKVxuICAgICAgICAgICAgfSxcblxuICAgICAgICAgICAgd2lkZ2V0RGV0YWlscygpIHtcbiAgICAgICAgICAgICAgICBsZXQgdmlldyA9IERvY3MuY29uZmlnLnN0YXRlLnZpZXdcbiAgICAgICAgICAgICAgICBsZXQgdmlld3MgPSBEb2NzLmNvbmZpZy52aWV3c1xuICAgICAgICAgICAgICAgIHJldHVybiB2aWV3c1t2aWV3XS53aWRnZXRzLm1hcCh3aWRnZXQgPT4ge1xuICAgICAgICAgICAgICAgICAgICBsZXQgZGV0YWlsID0gRG9jcy5jb25maWcud2lkZ2V0c1t3aWRnZXRdLmNvcHkud2lkZ2V0XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBgPGRpdiBjbGFzcz1cImRldGFpbC13aWRnZXRcIj48c3Bhbj4ke2RldGFpbH08L3NwYW4+PC9kaXY+YFxuICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICB9LFxuXG4gICAgICAgICAgICBwcm9wRGV0YWlscygpIHtcbiAgICAgICAgICAgICAgICBsZXQgdmlldyA9IERvY3MuY29uZmlnLnN0YXRlLnZpZXdcbiAgICAgICAgICAgICAgICBsZXQgdmlld3MgPSBEb2NzLmNvbmZpZy52aWV3c1xuICAgICAgICAgICAgICAgIHJldHVybiB2aWV3c1t2aWV3XS53aWRnZXRzLm1hcCh3aWRnZXQgPT4ge1xuICAgICAgICAgICAgICAgICAgICBsZXQgcHJvcE5vZGVzID0gJydcbiAgICAgICAgICAgICAgICAgICAgRG9jcy5jb25maWcud2lkZ2V0c1t3aWRnZXRdLmNvcHkucHJvcHMubWFwKHByb3AgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgcHJvcE5vZGVzID0gcHJvcE5vZGVzICsgYDxkaXYgY2xhc3M9XCJkZXRhaWwtcHJvcFwiPjxzcGFuPiR7cHJvcH08L3NwYW4+PC9kaXY+YFxuICAgICAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gcHJvcE5vZGVzXG4gICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIH0sXG5cbiAgICAgICAgICAgIGRldGFpbHMoKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIERvY3MuZG9jLmNyZWF0ZS53aWRnZXREZXRhaWxzKCkubWFwKCh3aWRnZXQsIGluZGV4KSA9PiB7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBgPGRpdiBjbGFzcz1cImRldGFpbFwiPiR7d2lkZ2V0ICsgRG9jcy5kb2MuY3JlYXRlLnByb3BEZXRhaWxzKClbaW5kZXhdfTwvZGl2PmBcbiAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgfSxcblxuICAgICAgICAgICAgZG9jKCkge1xuICAgICAgICAgICAgICAgIGxldCBkZXRhaWxzID0gRG9jcy5kb2MuY3JlYXRlLmRldGFpbHMoKVxuICAgICAgICAgICAgICAgIHJldHVybiBEb2NzLmRvYy5jcmVhdGUub3ZlcnZpZXcoKS5tYXAoKG92ZXJ2aWV3LCBpbmRleCkgPT4ge1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gYFxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiZG9jXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiaW5uZXItd3JhcHBlci1kb2NcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAke292ZXJ2aWV3fVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICR7ZGV0YWlsc1tpbmRleF19XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImxhYmVsXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzPVwid2lkZ2V0XCI+d2lkZ2V0PC9zcGFuPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzcz1cIndpZGdldFByb3BzXCI+d2lkZ2V0IHByb3BzPC9zcGFuPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJidXR0b24tZXhwYW5kXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzPVwiZXhwYW5kXCI+Kzwvc3Bhbj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHNwYW4gY2xhc3M9XCJjb2xsYXBzZVwiPiZuZGFzaDs8L3NwYW4+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJ1bmRlcmxheS1ib3JkZXJcIj48L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJ1bmRlcmxheS1jb2xvclwiPjwvZGl2PlxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICBgfSlcbiAgICAgICAgICAgIH0sXG5cbiAgICAgICAgICAgIHdyYXBwZXIoKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIERvY3MuZG9jLmNyZWF0ZS5kb2MoKS5tYXAoKGRvYywgaW5kZXgpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgbGV0IHdpZGdldCA9IERvY3MuY29uZmlnLnZpZXdzW0RvY3MuY29uZmlnLnN0YXRlLnZpZXddLndpZGdldHNbaW5kZXhdXG4gICAgICAgICAgICAgICAgICAgIGxldCB3cmFwcGVyID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnZGl2JylcbiAgICAgICAgICAgICAgICAgICAgd3JhcHBlci5pbm5lckhUTUwgPSBkb2NcbiAgICAgICAgICAgICAgICAgICAgd3JhcHBlci5jbGFzc05hbWUgPSAnd3JhcHBlci1kb2MnXG4gICAgICAgICAgICAgICAgICAgIHdyYXBwZXIuc2V0QXR0cmlidXRlKCd3aWRnZXQnLCB3aWRnZXQpXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiB3cmFwcGVyXG4gICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICBzZWxlY3QoKSB7XG4gICAgICAgICAgICBEb2NzLmRvYy5tZXRob2RzLmZpbHRlcigpXG4gICAgICAgICAgICBEb2NzLmNvbmZpZy5ub2Rlcy5kb2NzVmlzaWJsZSA9IHJldHVybkFycmF5KGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoJy5jb250YWluZXItZG9jcyAuZG9jLXNob3cnKSlcbiAgICAgICAgICAgIERvY3MuZG9jLm1ldGhvZHMudG9nZ2xlTWFuZGF0b3J5KGV2ZW50LmN1cnJlbnRUYXJnZXQpXG4gICAgICAgICAgICBEb2NzLmRvYy5tZXRob2RzLm1hbmRhdG9yeUZpcnN0KClcbiAgICAgICAgICAgIERvY3MuY29uZmlnLm5vZGVzLmRvY3NWaXNpYmxlID0gcmV0dXJuQXJyYXkoZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbCgnLmNvbnRhaW5lci1kb2NzIC5kb2Mtc2hvdycpKVxuICAgICAgICAgICAgRG9jcy5kb2MubWV0aG9kcy5hbGlnbigpXG4gICAgICAgICAgICBEb2NzLmRvYy5tZXRob2RzLmFkanVzdENvbnRhaW5lckhlaWdodChkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcuY29udGFpbmVyLWRvY3MgLndyYXBwZXItZG9jOmZpcnN0LWNoaWxkJykpXG4gICAgICAgIH0sXG5cbiAgICAgICAgbWV0aG9kczoge1xuXG4gICAgICAgICAgICBhbGlnbigpIHtcbiAgICAgICAgICAgICAgICBEb2NzLmNvbmZpZy5ub2Rlcy5kb2NzVmlzaWJsZS5mb3JFYWNoKChkb2MsIGluZGV4KSA9PiB7XG4gICAgICAgICAgICAgICAgICAgIGRvYy5zZXRBdHRyaWJ1dGUoJ2luZGV4JywgaW5kZXgpXG4gICAgICAgICAgICAgICAgICAgIGxldCBkZWZhdWx0Q2xhc3MgPSBkb2MuY2xhc3NMaXN0WzBdXG4gICAgICAgICAgICAgICAgICAgIGRvYy5zZXRBdHRyaWJ1dGUoJ2NsYXNzJywgZGVmYXVsdENsYXNzKVxuICAgICAgICAgICAgICAgICAgICBzd2l0Y2ggKGluZGV4ICUgMykge1xuICAgICAgICAgICAgICAgICAgICAgICAgY2FzZSAwOlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRvYy5jbGFzc0xpc3QuYWRkKCd3cmFwcGVyLWRvYy1sZWZ0JylcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBicmVha1xuICAgICAgICAgICAgICAgICAgICAgICAgY2FzZSAxOlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRvYy5jbGFzc0xpc3QuYWRkKCd3cmFwcGVyLWRvYy1jZW50ZXInKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGJyZWFrXG4gICAgICAgICAgICAgICAgICAgICAgICBjYXNlIDI6XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZG9jLmNsYXNzTGlzdC5hZGQoJ3dyYXBwZXItZG9jLXJpZ2h0JylcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBicmVha1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIH0sXG5cbiAgICAgICAgICAgIHRvZ2dsZShjdXJyZW50KSB7XG4gICAgICAgICAgICAgICAgRG9jcy5jb25maWcubm9kZXMuZG9jcy5mb3JFYWNoKGRvYyA9PiB7XG4gICAgICAgICAgICAgICAgICAgIGlmIChkb2MuZ2V0QXR0cmlidXRlKCdpbmRleCcpICE9IGN1cnJlbnQuZ2V0QXR0cmlidXRlKCdpbmRleCcpKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBEb2NzLmRvYy5tZXRob2RzLmNvbnRyYWN0KGRvYylcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChkb2MuY2xhc3NMaXN0LmNvbnRhaW5zKCd3cmFwcGVyLWRvYy1leHBhbmQnKSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIERvY3MuZG9jLm1ldGhvZHMudG9nZ2xlU2VtaSgpXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZG9jLmNsYXNzTGlzdC5hZGQoJ2RvYy1leHBhbmQnKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIERvY3MuY29uZmlnLnN0YXRlLmluZGV4KytcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjbGVhclRpbWVvdXQoRG9jcy5jb25maWcuc3RhdGUudGltZW91dFRvcClcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjdXJyZW50LnN0eWxlLnpJbmRleCA9IERvY3MuY29uZmlnLnN0YXRlLmluZGV4XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgIGxldCBpbmRleCA9IGN1cnJlbnQuZ2V0QXR0cmlidXRlKCdpbmRleCcpICUgM1xuICAgICAgICAgICAgICAgIGxldCBkb2MgPSBjdXJyZW50LmNsYXNzTGlzdFxuICAgICAgICAgICAgICAgIGxldCBleHBhbmRcblxuICAgICAgICAgICAgICAgIHN3aXRjaCAoaW5kZXgpIHtcbiAgICAgICAgICAgICAgICAgICAgY2FzZSAwOlxuICAgICAgICAgICAgICAgICAgICAgICAgZXhwYW5kID0gJ2RvYy1leHBhbmQtbGVmdCdcbiAgICAgICAgICAgICAgICAgICAgICAgIGJyZWFrXG4gICAgICAgICAgICAgICAgICAgIGNhc2UgMTpcbiAgICAgICAgICAgICAgICAgICAgICAgIGV4cGFuZCA9ICdkb2MtZXhwYW5kLWNlbnRlcidcbiAgICAgICAgICAgICAgICAgICAgICAgIGJyZWFrXG4gICAgICAgICAgICAgICAgICAgIGNhc2UgMjpcbiAgICAgICAgICAgICAgICAgICAgICAgIGV4cGFuZCA9ICdkb2MtZXhwYW5kLXJpZ2h0J1xuICAgICAgICAgICAgICAgICAgICAgICAgYnJlYWtcbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICBpZiAoIWRvYy5jb250YWlucygnZG9jLWV4cGFuZCcpKSB7XG4gICAgICAgICAgICAgICAgICAgIERvY3MuY29uZmlnLnN0YXRlLmluZGV4KytcbiAgICAgICAgICAgICAgICAgICAgY2xlYXJUaW1lb3V0KERvY3MuY29uZmlnLnN0YXRlLnRpbWVvdXRUb3ApXG4gICAgICAgICAgICAgICAgICAgIGRvYy5hZGQoJ2RvYy1leHBhbmQnKVxuICAgICAgICAgICAgICAgICAgICBjdXJyZW50LnN0eWxlLnpJbmRleCA9IERvY3MuY29uZmlnLnN0YXRlLmluZGV4XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICBpZiAoIWRvYy5jb250YWlucyhleHBhbmQpKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBkb2MuYWRkKGV4cGFuZClcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGRvYy5yZW1vdmUoZXhwYW5kLCAnZG9jLWV4cGFuZCcpXG4gICAgICAgICAgICAgICAgICAgICAgICBEb2NzLmNvbmZpZy5zdGF0ZS50aW1lb3V0VG9wID0gc2V0VGltZW91dCgoKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY3VycmVudC5zdHlsZS56SW5kZXggPSAnMSdcbiAgICAgICAgICAgICAgICAgICAgICAgIH0sIDIwMDApXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9LFxuXG4gICAgICAgICAgICB0b2dnbGVTZW1pKCkge1xuICAgICAgICAgICAgICAgIERvY3MuY29uZmlnLm5vZGVzLmRvY3NWaXNpYmxlLmZvckVhY2goZG9jID0+IHtcbiAgICAgICAgICAgICAgICAgICAgaWYgKCFkb2MuY2xhc3NMaXN0LmNvbnRhaW5zKCd3cmFwcGVyLWRvYy1leHBhbmQnKSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgZG9jLmNsYXNzTGlzdC5hZGQoJ3dyYXBwZXItZG9jLWV4cGFuZCcsICdkb2MtZXhwYW5kJylcbiAgICAgICAgICAgICAgICAgICAgICAgIERvY3MuY29uZmlnLm5vZGVzLmJ1dHRvbkV4cGFuZC5jbGFzc0xpc3QuYWRkKCdidXR0b24tY29sbGFwc2UnKVxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgZG9jLmNsYXNzTGlzdC5yZW1vdmUoJ3dyYXBwZXItZG9jLWV4cGFuZCcsICdkb2MtZXhwYW5kJylcbiAgICAgICAgICAgICAgICAgICAgICAgIERvY3MuY29uZmlnLm5vZGVzLmJ1dHRvbkV4cGFuZC5jbGFzc0xpc3QucmVtb3ZlKCdidXR0b24tY29sbGFwc2UnKVxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIH0sXG5cbiAgICAgICAgICAgIHRvZ2dsZU1vYmlsZShjdXJyZW50KSB7XG4gICAgICAgICAgICAgICAgRG9jcy5jb25maWcubm9kZXMuZG9jcy5mb3JFYWNoKGRvYyA9PiB7XG4gICAgICAgICAgICAgICAgICAgIGlmIChkb2MuZ2V0QXR0cmlidXRlKCdpbmRleCcpICE9IGN1cnJlbnQuZ2V0QXR0cmlidXRlKCdpbmRleCcpKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBkb2MuY2xhc3NMaXN0LnJlbW92ZSgnZG9jLWV4cGFuZCcpXG4gICAgICAgICAgICAgICAgICAgICAgICBzZXRUaW1lb3V0KCgpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBkb2Muc3R5bGUuekluZGV4ID0gMVxuICAgICAgICAgICAgICAgICAgICAgICAgfSwgMzAwKVxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgY3VycmVudC5jbGFzc0xpc3QudG9nZ2xlKCdkb2MtZXhwYW5kJylcbiAgICAgICAgICAgICAgICAgICAgICAgIERvY3MuY29uZmlnLnN0YXRlLmluZGV4KytcbiAgICAgICAgICAgICAgICAgICAgICAgIGN1cnJlbnQuc3R5bGUuekluZGV4ID0gRG9jcy5jb25maWcuc3RhdGUuaW5kZXhcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICB9LFxuXG4gICAgICAgICAgICBjb250cmFjdChkb2MpIHtcbiAgICAgICAgICAgICAgICBpZiAoZG9jKSB7XG4gICAgICAgICAgICAgICAgICAgIGRvYy5jbGFzc0xpc3QucmVtb3ZlKCdkb2MtZXhwYW5kLWxlZnQnLCAnZG9jLWV4cGFuZC1jZW50ZXInLCAnZG9jLWV4cGFuZC1yaWdodCcsICdkb2MtZXhwYW5kJylcbiAgICAgICAgICAgICAgICAgICAgc2V0VGltZW91dCgoKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICBkb2Muc3R5bGUuekluZGV4ID0gMVxuICAgICAgICAgICAgICAgICAgICB9LCA1MDApXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICBEb2NzLmNvbmZpZy5ub2Rlcy5kb2NzLmZvckVhY2goZG9jID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGRvYy5jbGFzc0xpc3QucmVtb3ZlKCdkb2MtZXhwYW5kLWxlZnQnLCAnZG9jLWV4cGFuZC1jZW50ZXInLCAnZG9jLWV4cGFuZC1yaWdodCcsICdkb2MtZXhwYW5kJywgJ3dyYXBwZXItZG9jLWV4cGFuZCcpXG4gICAgICAgICAgICAgICAgICAgICAgICBzZXRUaW1lb3V0KCgpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBkb2Muc3R5bGUuekluZGV4ID0gMVxuICAgICAgICAgICAgICAgICAgICAgICAgfSwgNTAwKVxuICAgICAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0sXG5cbiAgICAgICAgICAgIGFkanVzdENvbnRhaW5lckhlaWdodChkb2MpIHtcbiAgICAgICAgICAgICAgICBpZiAoZG9jKSB7XG4gICAgICAgICAgICAgICAgICAgIGxldCB3cmFwcGVyTWFyZ2luID0gcGFyc2VJbnQod2luZG93LmdldENvbXB1dGVkU3R5bGUoZG9jKVsnbWFyZ2luLXRvcCddKVxuICAgICAgICAgICAgICAgICAgICBsZXQgd3JhcHBlckhlaWdodCA9IChwYXJzZUludCh3aW5kb3cuZ2V0Q29tcHV0ZWRTdHlsZShkb2MpWydmb250LXNpemUnXSkgKiA5LjkpICsgd3JhcHBlck1hcmdpblxuICAgICAgICAgICAgICAgICAgICBsZXQgZG9jSGVpZ2h0ID0gd3JhcHBlckhlaWdodCAqIDRcbiAgICAgICAgICAgICAgICAgICAgbGV0IHJvd3MgPSBNYXRoLmNlaWwoKERvY3MuY29uZmlnLm5vZGVzLmRvY3NWaXNpYmxlLmZpbHRlcihkb2MgPT4gdHlwZW9mIChkb2MpICE9ICdudW1iZXInKSkubGVuZ3RoIC8gMylcbiAgICAgICAgICAgICAgICAgICAgbGV0IHByb2JsZW1TdGFydCA9IHJvd3MgLSAzXG4gICAgICAgICAgICAgICAgICAgIGxldCBjb250YWluZXJIZWlnaHQgPSB3cmFwcGVySGVpZ2h0ICogcm93c1xuICAgICAgICAgICAgICAgICAgICBsZXQgY3VycmVudFJvdyA9IE1hdGguY2VpbCgocGFyc2VJbnQoZG9jLmdldEF0dHJpYnV0ZSgnaW5kZXgnKSkgKyAxKSAvIDMpXG4gICAgICAgICAgICAgICAgICAgIHByb2JsZW1TdGFydCA9IHByb2JsZW1TdGFydCA8IDEgPyAwIDogcHJvYmxlbVN0YXJ0XG4gICAgICAgICAgICAgICAgICAgIGxldCBwcm9ibGVtUm93cyA9IHJvd3MgLSBwcm9ibGVtU3RhcnRcblxuICAgICAgICAgICAgICAgICAgICBmdW5jdGlvbiByZXNldCgpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIERvY3MuY29uZmlnLm5vZGVzLmRvY3NDb250YWluZXIuc3R5bGUudHJhbnNpdGlvbkRlbGF5ID0gJzAuM3MnXG4gICAgICAgICAgICAgICAgICAgICAgICBEb2NzLmNvbmZpZy5ub2Rlcy5kb2NzQ29udGFpbmVyLnN0eWxlLmhlaWdodCA9ICh3cmFwcGVySGVpZ2h0ICogcm93cykgKyAncHgnXG4gICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgICAgICBpZiAoZG9jLmNsYXNzTGlzdC5jb250YWlucygnZG9jLWV4cGFuZCcpKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoY3VycmVudFJvdyA+IHByb2JsZW1TdGFydCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxldCBwb3NpdGlvbiA9IHByb2JsZW1Sb3dzICsgKHByb2JsZW1TdGFydCAtIGN1cnJlbnRSb3cpICsgMVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxldCBpbmNyZWFzZSA9IGNvbnRhaW5lckhlaWdodCArIChkb2NIZWlnaHQgLSAoKHdyYXBwZXJIZWlnaHQgLSB3cmFwcGVyTWFyZ2luKSAqIHBvc2l0aW9uKSAtICh3cmFwcGVyTWFyZ2luICogcG9zaXRpb24pKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIERvY3MuY29uZmlnLm5vZGVzLmRvY3NDb250YWluZXIuc3R5bGUudHJhbnNpdGlvbkRlbGF5ID0gJzBzJ1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIERvY3MuY29uZmlnLm5vZGVzLmRvY3NDb250YWluZXIuc3R5bGUuaGVpZ2h0ID0gaW5jcmVhc2UgKyAncHgnXG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXNldCgpXG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICByZXNldCgpXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9LFxuXG4gICAgICAgICAgICBhZGp1c3RDb250YWluZXJIZWlnaHRNb2JpbGUoZG9jKSB7XG4gICAgICAgICAgICAgICAgaWYgKGRvYykge1xuICAgICAgICAgICAgICAgICAgICBsZXQgd3JhcHBlck1hcmdpbiA9IHBhcnNlSW50KHdpbmRvdy5nZXRDb21wdXRlZFN0eWxlKGRvYylbJ21hcmdpbi10b3AnXSlcbiAgICAgICAgICAgICAgICAgICAgbGV0IHdyYXBwZXJIZWlnaHQgPSAocGFyc2VJbnQod2luZG93LmdldENvbXB1dGVkU3R5bGUoZG9jKVsnZm9udC1zaXplJ10pICogMTApICsgd3JhcHBlck1hcmdpblxuICAgICAgICAgICAgICAgICAgICBsZXQgZG9jSGVpZ2h0ID0gd3JhcHBlckhlaWdodCAqIDRcbiAgICAgICAgICAgICAgICAgICAgbGV0IHJvd3MgPSBNYXRoLmNlaWwoKERvY3MuY29uZmlnLm5vZGVzLmRvY3NWaXNpYmxlLmZpbHRlcihkb2MgPT4gdHlwZW9mIChkb2MpICE9ICdudW1iZXInKSkubGVuZ3RoIC8gMilcbiAgICAgICAgICAgICAgICAgICAgbGV0IHByb2JsZW1TdGFydCA9IHJvd3MgLSAzXG4gICAgICAgICAgICAgICAgICAgIGxldCBjb250YWluZXJIZWlnaHQgPSB3cmFwcGVySGVpZ2h0ICogcm93c1xuICAgICAgICAgICAgICAgICAgICBsZXQgY3VycmVudFJvdyA9IE1hdGguY2VpbCgocGFyc2VJbnQoZG9jLmdldEF0dHJpYnV0ZSgnaW5kZXgnKSkgKyAxKSAvIDIpXG4gICAgICAgICAgICAgICAgICAgIHByb2JsZW1TdGFydCA9IHByb2JsZW1TdGFydCA8IDEgPyAwIDogcHJvYmxlbVN0YXJ0XG4gICAgICAgICAgICAgICAgICAgIGxldCBwcm9ibGVtUm93cyA9IHJvd3MgLSBwcm9ibGVtU3RhcnRcblxuICAgICAgICAgICAgICAgICAgICBmdW5jdGlvbiByZXNldCgpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIERvY3MuY29uZmlnLm5vZGVzLmRvY3NDb250YWluZXIuc3R5bGUudHJhbnNpdGlvbkRlbGF5ID0gJzAuM3MnXG4gICAgICAgICAgICAgICAgICAgICAgICBEb2NzLmNvbmZpZy5ub2Rlcy5kb2NzQ29udGFpbmVyLnN0eWxlLmhlaWdodCA9ICh3cmFwcGVySGVpZ2h0ICogcm93cykgKyAncHgnXG4gICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgICAgICBpZiAoZG9jLmNsYXNzTGlzdC5jb250YWlucygnZG9jLWV4cGFuZCcpKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoY3VycmVudFJvdyA+IHByb2JsZW1TdGFydCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxldCBwb3NpdGlvbiA9IHByb2JsZW1Sb3dzICsgKHByb2JsZW1TdGFydCAtIGN1cnJlbnRSb3cpICsgMVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxldCBpbmNyZWFzZSA9IGNvbnRhaW5lckhlaWdodCArIChkb2NIZWlnaHQgLSAoKHdyYXBwZXJIZWlnaHQgLSB3cmFwcGVyTWFyZ2luKSAqIHBvc2l0aW9uKSAtICh3cmFwcGVyTWFyZ2luICogcG9zaXRpb24pKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIERvY3MuY29uZmlnLm5vZGVzLmRvY3NDb250YWluZXIuc3R5bGUudHJhbnNpdGlvbkRlbGF5ID0gJzBzJ1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIERvY3MuY29uZmlnLm5vZGVzLmRvY3NDb250YWluZXIuc3R5bGUuaGVpZ2h0ID0gaW5jcmVhc2UgKyAncHgnXG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXNldCgpXG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICByZXNldCgpXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9LFxuXG4gICAgICAgICAgICBmaWx0ZXIoKSB7XG4gICAgICAgICAgICAgICAgaWYgKERvY3MuY29uZmlnLm5vZGVzLmRvY3MgIT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICAgICAgICAgIGxldCBhY3RpdmVXaWRnZXRzID0gRG9jcy5jb25maWcudmlld3NbRG9jcy5jb25maWcuc3RhdGUudmlld10ud2lkZ2V0c1xuICAgICAgICAgICAgICAgICAgICBEb2NzLmNvbmZpZy5ub2Rlcy5kb2NzLmZvckVhY2goZG9jID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGxldCB3aWRnZXQgPSBkb2MuZ2V0QXR0cmlidXRlKCd3aWRnZXQnKVxuICAgICAgICAgICAgICAgICAgICAgICAgbGV0IHByZXNlbnQgPSBhY3RpdmVXaWRnZXRzLmluZGV4T2Yod2lkZ2V0KVxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHByZXNlbnQgPCAwKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZG9jLmNsYXNzTGlzdC5hZGQoJ2RvYy1oaWRlJylcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBkb2MuY2xhc3NMaXN0LnJlbW92ZSgnZG9jLXNob3cnKVxuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZG9jLmNsYXNzTGlzdC5hZGQoJ2RvYy1zaG93JylcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBkb2MuY2xhc3NMaXN0LnJlbW92ZSgnZG9jLWhpZGUnKVxuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0sXG5cbiAgICAgICAgICAgIHRvZ2dsZU1hbmRhdG9yeSh2aWV3KSB7XG4gICAgICAgICAgICAgICAgbGV0IG1hbmRhdG9yeSA9IERvY3MuY29uZmlnLnZpZXdzW0RvY3MuY29uZmlnLnN0YXRlLnZpZXddLm1hbmRhdG9yeVxuICAgICAgICAgICAgICAgIERvY3MuY29uZmlnLm5vZGVzLmRvY3NWaXNpYmxlLmZvckVhY2goZG9jID0+IHtcbiAgICAgICAgICAgICAgICAgICAgbGV0IHdpZGdldCA9IGRvYy5nZXRBdHRyaWJ1dGUoJ3dpZGdldCcpXG4gICAgICAgICAgICAgICAgICAgIGxldCBwcmVzZW50ID0gbWFuZGF0b3J5LmluZGV4T2Yod2lkZ2V0KVxuICAgICAgICAgICAgICAgICAgICBpZiAocHJlc2VudCA8IDApIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGRvYy5jaGlsZE5vZGVzWzFdLmNsYXNzTGlzdC5yZW1vdmUoJ2RvYy1tYW5kYXRvcnknKVxuICAgICAgICAgICAgICAgICAgICAgICAgZG9jLmNoaWxkTm9kZXNbMV0uY2xhc3NMaXN0LmFkZCgnZG9jLW9wdGlvbmFsJylcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGRvYy5jaGlsZE5vZGVzWzFdLmNsYXNzTGlzdC5hZGQoJ2RvYy1tYW5kYXRvcnknKVxuICAgICAgICAgICAgICAgICAgICAgICAgZG9jLmNoaWxkTm9kZXNbMV0uY2xhc3NMaXN0LnJlbW92ZSgnZG9jLW9wdGlvbmFsJylcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICB9LFxuXG4gICAgICAgICAgICBtYW5kYXRvcnlGaXJzdCgpIHtcbiAgICAgICAgICAgICAgICBpZiAoRG9jcy5jb25maWcuc3RhdGUudmlldyAhPSAnbWFzdGVyJykge1xuICAgICAgICAgICAgICAgICAgICBsZXQgaW5kZXggPSAwXG4gICAgICAgICAgICAgICAgICAgIERvY3MuY29uZmlnLm5vZGVzLmRvY3NWaXNpYmxlLmZvckVhY2goZG9jID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChkb2MuY2hpbGROb2Rlc1sxXS5jbGFzc0xpc3QuY29udGFpbnMoJ2RvYy1tYW5kYXRvcnknKSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxldCBjdXJyZW50ID0gZG9jXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZG9jLnJlbW92ZSgpXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgRG9jcy5jb25maWcubm9kZXMuZG9jc0NvbnRhaW5lci5pbnNlcnRCZWZvcmUoY3VycmVudCwgRG9jcy5jb25maWcubm9kZXMuZG9jc0NvbnRhaW5lci5jaGlsZE5vZGVzW2luZGV4XSlcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpbmRleCsrXG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfSxcblxuICAgIGluaXQoKSB7XG4gICAgICAgIERvY3MuZHJvcERvd24uaW5pdCgpXG4gICAgICAgIERvY3MuZG9jLmluaXQoKVxuICAgIH1cbn1cblxuZXhwb3J0IGRlZmF1bHQgRG9jcyIsIi8vMTI4MCBYIDU5NVxuXG5pbXBvcnQgeyByZXR1cm5BcnJheSB9IGZyb20gJy4vaGVscGVycy5qcydcbmltcG9ydCBza3JvbGxyIGZyb20gJy4vc2tyb2xsci5taW4uanMnXG5cbmxldCBGZWF0dXJlcyA9IHtcbiAgICBjb25maWc6IHtcbiAgICAgICAgbm9kZXM6IHtcbiAgICAgICAgICAgIGJhbm5lcjogZG9jdW1lbnQucXVlcnlTZWxlY3Rvcignc2VjdGlvbi5iYW5uZXInKSxcbiAgICAgICAgICAgIGludHJvOiBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCdzZWN0aW9uLmludHJvJyksXG4gICAgICAgICAgICBnZXRGZWF0dXJlKGZlYXR1cmUpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihgLmZlYXR1cmVzIC5mZWF0dXJlLSR7ZmVhdHVyZX1gKVxuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIGZlYXR1cmVzOiB7XG4gICAgICAgICAgICAgICAgMToge1xuICAgICAgICAgICAgICAgICAgICByb2NrZXQ6IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5mZWF0dXJlLTEgLmNvbnRhaW5lci1wYXRoJyksXG4gICAgICAgICAgICAgICAgICAgIGFycm93OiBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcuZmVhdHVyZS0xIC5hcnJvdycpXG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAyOiB7XG4gICAgICAgICAgICAgICAgICAgIGFybTogZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLmZlYXR1cmUtMiAuYXJtJyksXG4gICAgICAgICAgICAgICAgICAgIHNjcmVlbjogZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLmZlYXR1cmUtMiAudGhhbmt5b3UnKSxcbiAgICAgICAgICAgICAgICAgICAgbGlnaHQ6IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5mZWF0dXJlLTIgLmxpZ2h0JylcbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIDM6IHtcbiAgICAgICAgICAgICAgICAgICAgY2l0eTE6IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5mZWF0dXJlLTMgLmNpdHktMScpLFxuICAgICAgICAgICAgICAgICAgICBjaXR5MjogZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLmZlYXR1cmUtMyAuY2l0eS0yJyksXG4gICAgICAgICAgICAgICAgICAgIGNpdHkzOiBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcuZmVhdHVyZS0zIC5jaXR5LTMnKSxcbiAgICAgICAgICAgICAgICAgICAgc2l0ZTE6IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5mZWF0dXJlLTMgLnNpdGUtMScpLFxuICAgICAgICAgICAgICAgICAgICBzaXRlMjogZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLmZlYXR1cmUtMyAuc2l0ZS0yJyksXG4gICAgICAgICAgICAgICAgICAgIHNpdGUzOiBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcuZmVhdHVyZS0zIC5zaXRlLTMnKVxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgNDoge1xuICAgICAgICAgICAgICAgICAgICBjYWJsZTogZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLmZlYXR1cmUtNCAuY2FibGUnKSxcbiAgICAgICAgICAgICAgICAgICAgYm94OiBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcuZmVhdHVyZS00IC5ib3gnKVxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgNToge1xuICAgICAgICAgICAgICAgICAgICBjb2luMTogZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLmZlYXR1cmUtNSAuY29pbi0xJyksXG4gICAgICAgICAgICAgICAgICAgIGNvaW5zOiBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcuZmVhdHVyZS01IC5jb250YWluZXItY29pbnMnKVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfSxcbiAgICAgICAgd2luZG93V2lkdGg6IHVuZGVmaW5lZFxuICAgIH0sXG5cbiAgICBoZWxwZXJzOiB7XG4gICAgICAgIGdldEludHJvSGVpZ2h0KCkge1xuICAgICAgICAgICAgbGV0IGJhbm5lckhlaWdodCA9IHBhcnNlSW50KHdpbmRvdy5nZXRDb21wdXRlZFN0eWxlKEZlYXR1cmVzLmNvbmZpZy5ub2Rlcy5iYW5uZXIsIG51bGwpLmhlaWdodClcbiAgICAgICAgICAgIGxldCBiYW5uZXJQYWRkaW5nID0gcGFyc2VJbnQod2luZG93LmdldENvbXB1dGVkU3R5bGUoRmVhdHVyZXMuY29uZmlnLm5vZGVzLmJhbm5lciwgbnVsbCkuZm9udFNpemUpIC0gMTBcbiAgICAgICAgICAgIGxldCBpbnRyb0hlaWdodCA9IHBhcnNlSW50KHdpbmRvdy5nZXRDb21wdXRlZFN0eWxlKEZlYXR1cmVzLmNvbmZpZy5ub2Rlcy5pbnRybywgbnVsbCkuaGVpZ2h0KVxuICAgICAgICAgICAgcmV0dXJuIGludHJvSGVpZ2h0ICsgYmFubmVySGVpZ2h0ICsgYmFubmVyUGFkZGluZ1xuICAgICAgICB9LFxuXG4gICAgICAgIGdldEZlYXR1cmVIZWlnaHQoZmVhdHVyZSkge1xuICAgICAgICAgICAgaWYgKGZlYXR1cmUgPT0gMCkge1xuICAgICAgICAgICAgICAgIHJldHVybiAwXG4gICAgICAgICAgICB9XG4gICAgICAgICAgICByZXR1cm4gcGFyc2VJbnQod2luZG93LmdldENvbXB1dGVkU3R5bGUoRmVhdHVyZXMuY29uZmlnLm5vZGVzLmdldEZlYXR1cmUoZmVhdHVyZSksIG51bGwpLmhlaWdodCkgKyBwYXJzZUludCh3aW5kb3cuZ2V0Q29tcHV0ZWRTdHlsZShGZWF0dXJlcy5jb25maWcubm9kZXMuZ2V0RmVhdHVyZShmZWF0dXJlKSwgbnVsbCkucGFkZGluZ0JvdHRvbSkgXG4gICAgICAgIH0sXG5cbiAgICAgICAgc2V0V2luZG93V2lkdGgoKSB7XG4gICAgICAgICAgICBGZWF0dXJlcy5jb25maWcud2luZG93V2lkdGggPSB3aW5kb3cub3V0ZXJXaWR0aFxuICAgICAgICB9LFxuXG4gICAgICAgIGdldEZyYWN0aW9uKGZyYWN0aW9uKSB7XG5cbiAgICAgICAgICAgIGxldCBwcm9wb3J0aW9uID0gdW5kZWZpbmVkXG5cbiAgICAgICAgICAgIGlmICh3aW5kb3cub3V0ZXJXaWR0aCA+IDE0MDApIHtcbiAgICAgICAgICAgICAgICBwcm9wb3J0aW9uID0gMVxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZSBpZiAod2luZG93Lm91dGVyV2lkdGggPiAxMDUwICYmIHdpbmRvdy5vdXRlcldpZHRoIDw9IDE0MDApIHtcbiAgICAgICAgICAgICAgICBwcm9wb3J0aW9uID0gMS4xXG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlIGlmICh3aW5kb3cub3V0ZXJXaWR0aCA8PSAxMDUwICYmIHdpbmRvdy5vdXRlcldpZHRoID4gODAwKSB7XG4gICAgICAgICAgICAgICAgcHJvcG9ydGlvbiA9IDAuOTVcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2UgaWYgKHdpbmRvdy5vdXRlcldpZHRoIDw9IDgwMCAmJiB3aW5kb3cub3V0ZXJXaWR0aCA+IDY5OSkge1xuICAgICAgICAgICAgICAgIHByb3BvcnRpb24gPSAwLjZcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgcmV0dXJuIHByb3BvcnRpb24gKiBmcmFjdGlvblxuICAgICAgICB9LFxuXG4gICAgICAgIGdldE5vZGVzKCkge1xuICAgICAgICAgICAgbGV0IG5vZGVzID0gcmV0dXJuQXJyYXkoRmVhdHVyZXMuY29uZmlnLm5vZGVzLmZlYXR1cmVzKS5tYXAoKGZlYXR1cmUsIGluZGV4KSA9PiB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIHJldHVybkFycmF5KEZlYXR1cmVzLmNvbmZpZy5ub2Rlcy5mZWF0dXJlc1tpbmRleCArIDFdKS5tYXAobm9kZSA9PiB7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBub2RlXG4gICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIH0pXG5cbiAgICAgICAgICAgIG5vZGVzID0gbm9kZXMucmVkdWNlKChwcmV2LCBuZXh0KSA9PiB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIHByZXYuY29uY2F0KG5leHQpXG4gICAgICAgICAgICB9KVxuXG4gICAgICAgICAgICByZXR1cm4gbm9kZXNcbiAgICAgICAgfSxcblxuICAgICAgICByZXNldEFuY2hvcnMobm9kZSkge1xuICAgICAgICAgICAgcmV0dXJuQXJyYXkobm9kZS5hdHRyaWJ1dGVzKS5mb3JFYWNoKGF0dHJpYnV0ZSA9PiB7XG4gICAgICAgICAgICAgICAgaWYgKGF0dHJpYnV0ZS5uYW1lLnNwbGl0KCctJylbMF0gPT0gJ2RhdGEnKSB7XG4gICAgICAgICAgICAgICAgICAgIG5vZGUucmVtb3ZlQXR0cmlidXRlKGF0dHJpYnV0ZS5uYW1lKVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pXG4gICAgICAgIH1cbiAgICB9LFxuXG4gICAgbWV0aG9kczoge1xuICAgICAgICBpbml0KCkge1xuICAgICAgICAgICAgRmVhdHVyZXMubWV0aG9kcy5zZXRGZWF0dXJlMSgpXG4gICAgICAgICAgICBGZWF0dXJlcy5tZXRob2RzLnNldEZlYXR1cmUyKClcbiAgICAgICAgICAgIEZlYXR1cmVzLm1ldGhvZHMuc2V0RmVhdHVyZTMoKVxuICAgICAgICAgICAgRmVhdHVyZXMubWV0aG9kcy5zZXRGZWF0dXJlNCgpXG4gICAgICAgICAgICBGZWF0dXJlcy5tZXRob2RzLnNldEZlYXR1cmU1KClcbiAgICAgICAgICAgIHNrcm9sbHIuaW5pdCh7IGZvcmNlSGVpZ2h0OiBmYWxzZSB9KVxuICAgICAgICB9LFxuXG4gICAgICAgIHNldEZlYXR1cmUxKCkge1xuICAgICAgICAgICAgbGV0IHJvY2tldFN0YXJ0ID0gTWF0aC5mbG9vcihGZWF0dXJlcy5oZWxwZXJzLmdldEludHJvSGVpZ2h0KCkgKiBGZWF0dXJlcy5oZWxwZXJzLmdldEZyYWN0aW9uKDAuNTUpKVxuICAgICAgICAgICAgbGV0IGFycm93U3RhcnQgPSBNYXRoLmZsb29yKEZlYXR1cmVzLmhlbHBlcnMuZ2V0SW50cm9IZWlnaHQoKSAqIEZlYXR1cmVzLmhlbHBlcnMuZ2V0RnJhY3Rpb24oMC43OCkpXG4gICAgICAgICAgICBsZXQgcm9ja2V0RW5kID0gTWF0aC5mbG9vcihGZWF0dXJlcy5oZWxwZXJzLmdldEludHJvSGVpZ2h0KCkgKiBGZWF0dXJlcy5oZWxwZXJzLmdldEZyYWN0aW9uKDAuODUpKVxuXG4gICAgICAgICAgICBGZWF0dXJlcy5jb25maWcubm9kZXMuZmVhdHVyZXNbMV0ucm9ja2V0LnNldEF0dHJpYnV0ZShgZGF0YS0ke3JvY2tldFN0YXJ0fWAsICdoZWlnaHQ6N2VtOycpXG4gICAgICAgICAgICBGZWF0dXJlcy5jb25maWcubm9kZXMuZmVhdHVyZXNbMV0ucm9ja2V0LnNldEF0dHJpYnV0ZShgZGF0YS0ke3JvY2tldEVuZH1gLCAnaGVpZ2h0OjMxZW07JylcbiAgICAgICAgICAgIEZlYXR1cmVzLmNvbmZpZy5ub2Rlcy5mZWF0dXJlc1sxXS5hcnJvdy5zZXRBdHRyaWJ1dGUoYGRhdGEtJHtyb2NrZXRTdGFydH1gLCAnb3BhY2l0eTohMDsnKVxuICAgICAgICAgICAgRmVhdHVyZXMuY29uZmlnLm5vZGVzLmZlYXR1cmVzWzFdLmFycm93LnNldEF0dHJpYnV0ZShgZGF0YS0ke2Fycm93U3RhcnR9YCwgJ29wYWNpdHk6ITE7cmlnaHQ6MGVtOycpXG4gICAgICAgICAgICBGZWF0dXJlcy5jb25maWcubm9kZXMuZmVhdHVyZXNbMV0uYXJyb3cuc2V0QXR0cmlidXRlKGBkYXRhLSR7cm9ja2V0RW5kfWAsICdyaWdodDoxNi4xNWVtOycpXG4gICAgICAgIH0sXG5cbiAgICAgICAgc2V0RmVhdHVyZTIoKSB7XG4gICAgICAgICAgICBsZXQgY2FyZFN0YXJ0ID0gTWF0aC5mbG9vcihGZWF0dXJlcy5oZWxwZXJzLmdldEludHJvSGVpZ2h0KCkgKyBGZWF0dXJlcy5oZWxwZXJzLmdldEZlYXR1cmVIZWlnaHQoMSkgKiBGZWF0dXJlcy5oZWxwZXJzLmdldEZyYWN0aW9uKDAuNSkpXG4gICAgICAgICAgICBsZXQgY2FyZEVuZCA9IE1hdGguZmxvb3IoRmVhdHVyZXMuaGVscGVycy5nZXRJbnRyb0hlaWdodCgpICsgRmVhdHVyZXMuaGVscGVycy5nZXRGZWF0dXJlSGVpZ2h0KDEpICogRmVhdHVyZXMuaGVscGVycy5nZXRGcmFjdGlvbigwLjcpKVxuICAgICAgICAgICAgbGV0IGZsYXNoUmFuZ2UgPSBNYXRoLmZsb29yKEZlYXR1cmVzLmhlbHBlcnMuZ2V0SW50cm9IZWlnaHQoKSArIEZlYXR1cmVzLmhlbHBlcnMuZ2V0RmVhdHVyZUhlaWdodCgxKSAqIEZlYXR1cmVzLmhlbHBlcnMuZ2V0RnJhY3Rpb24oMC44NCkgLSBjYXJkRW5kKVxuICAgICAgICAgICAgbGV0IGZsYXNoU3RlcCA9IGZsYXNoUmFuZ2UgLyA2XG5cbiAgICAgICAgICAgIGxldCBsaWdodEZsYXN0MCA9IGNhcmRFbmQgLSBmbGFzaFN0ZXBcbiAgICAgICAgICAgIGxldCBsaWdodEZsYXN0MSA9IGxpZ2h0Rmxhc3QwICsgZmxhc2hTdGVwXG4gICAgICAgICAgICBsZXQgbGlnaHRGbGFzdDIgPSBsaWdodEZsYXN0MSArIGZsYXNoU3RlcFxuICAgICAgICAgICAgbGV0IGxpZ2h0Rmxhc3QzID0gbGlnaHRGbGFzdDIgKyBmbGFzaFN0ZXBcbiAgICAgICAgICAgIGxldCBsaWdodEZsYXN0NCA9IGxpZ2h0Rmxhc3QzICsgZmxhc2hTdGVwXG4gICAgICAgICAgICBsZXQgbGlnaHRGbGFzdDUgPSBsaWdodEZsYXN0NCArIGZsYXNoU3RlcFxuICAgICAgICAgICAgbGV0IGxpZ2h0Rmxhc3Q2ID0gbGlnaHRGbGFzdDUgKyBmbGFzaFN0ZXBcblxuICAgICAgICAgICAgRmVhdHVyZXMuY29uZmlnLm5vZGVzLmZlYXR1cmVzWzJdLmFybS5zZXRBdHRyaWJ1dGUoYGRhdGEtJHtjYXJkU3RhcnR9YCwgJ2hlaWdodDogMGVtOycpXG4gICAgICAgICAgICBGZWF0dXJlcy5jb25maWcubm9kZXMuZmVhdHVyZXNbMl0uYXJtLnNldEF0dHJpYnV0ZShgZGF0YS0ke2NhcmRFbmR9YCwgJ2hlaWdodDogNy40ZW07JylcblxuICAgICAgICAgICAgRmVhdHVyZXMuY29uZmlnLm5vZGVzLmZlYXR1cmVzWzJdLmxpZ2h0LnNldEF0dHJpYnV0ZShgZGF0YS0ke2xpZ2h0Rmxhc3QwfWAsICdvcGFjaXR5OiAhMDsnKVxuICAgICAgICAgICAgRmVhdHVyZXMuY29uZmlnLm5vZGVzLmZlYXR1cmVzWzJdLmxpZ2h0LnNldEF0dHJpYnV0ZShgZGF0YS0ke2xpZ2h0Rmxhc3QxfWAsICdvcGFjaXR5OiAhMTsnKVxuICAgICAgICAgICAgRmVhdHVyZXMuY29uZmlnLm5vZGVzLmZlYXR1cmVzWzJdLmxpZ2h0LnNldEF0dHJpYnV0ZShgZGF0YS0ke2xpZ2h0Rmxhc3QyfWAsICdvcGFjaXR5OiAhMDsnKVxuICAgICAgICAgICAgRmVhdHVyZXMuY29uZmlnLm5vZGVzLmZlYXR1cmVzWzJdLmxpZ2h0LnNldEF0dHJpYnV0ZShgZGF0YS0ke2xpZ2h0Rmxhc3QzfWAsICdvcGFjaXR5OiAhMTsnKVxuICAgICAgICAgICAgRmVhdHVyZXMuY29uZmlnLm5vZGVzLmZlYXR1cmVzWzJdLmxpZ2h0LnNldEF0dHJpYnV0ZShgZGF0YS0ke2xpZ2h0Rmxhc3Q0fWAsICdvcGFjaXR5OiAhMDsnKVxuICAgICAgICAgICAgRmVhdHVyZXMuY29uZmlnLm5vZGVzLmZlYXR1cmVzWzJdLmxpZ2h0LnNldEF0dHJpYnV0ZShgZGF0YS0ke2xpZ2h0Rmxhc3Q1fWAsICdvcGFjaXR5OiAhMTsnKVxuICAgICAgICAgICAgRmVhdHVyZXMuY29uZmlnLm5vZGVzLmZlYXR1cmVzWzJdLmxpZ2h0LnNldEF0dHJpYnV0ZShgZGF0YS0ke2xpZ2h0Rmxhc3Q2fWAsICdvcGFjaXR5OiAhMDsnKVxuXG4gICAgICAgICAgICBGZWF0dXJlcy5jb25maWcubm9kZXMuZmVhdHVyZXNbMl0uc2NyZWVuLnNldEF0dHJpYnV0ZShgZGF0YS0ke2NhcmRTdGFydH1gLCAnb3BhY2l0eTogITA7JylcbiAgICAgICAgICAgIEZlYXR1cmVzLmNvbmZpZy5ub2Rlcy5mZWF0dXJlc1syXS5zY3JlZW4uc2V0QXR0cmlidXRlKGBkYXRhLSR7bGlnaHRGbGFzdDZ9YCwgJ29wYWNpdHk6ICExOycpXG4gICAgICAgIH0sXG5cbiAgICAgICAgc2V0RmVhdHVyZTMoKSB7XG4gICAgICAgICAgICBsZXQgY2l0eVN0YXJ0ID0gTWF0aC5mbG9vcihGZWF0dXJlcy5oZWxwZXJzLmdldEludHJvSGVpZ2h0KCkgKyBGZWF0dXJlcy5oZWxwZXJzLmdldEZlYXR1cmVIZWlnaHQoMSkgKiBGZWF0dXJlcy5oZWxwZXJzLmdldEZyYWN0aW9uKDEuNSkpXG4gICAgICAgICAgICBsZXQgY2l0eUVuZCA9IE1hdGguZmxvb3IoRmVhdHVyZXMuaGVscGVycy5nZXRJbnRyb0hlaWdodCgpICsgRmVhdHVyZXMuaGVscGVycy5nZXRGZWF0dXJlSGVpZ2h0KDEpICsgRmVhdHVyZXMuaGVscGVycy5nZXRGZWF0dXJlSGVpZ2h0KDIpICogRmVhdHVyZXMuaGVscGVycy5nZXRGcmFjdGlvbigwLjkpKVxuICAgICAgICAgICAgbGV0IGNpdHlSYW5nZSA9IGNpdHlFbmQgLSBjaXR5U3RhcnRcbiAgICAgICAgICAgIGxldCBjaXR5U3RlcCA9IGNpdHlSYW5nZSAvIDJcbiAgICAgICAgICAgIGxldCBjaXR5U3RlcDEgPSBjaXR5U3RhcnQgKyBjaXR5U3RlcFxuICAgICAgICAgICAgbGV0IGNpdHlTdGVwMiA9IGNpdHlTdGVwMSArIGNpdHlTdGVwXG5cbiAgICAgICAgICAgIEZlYXR1cmVzLmNvbmZpZy5ub2Rlcy5mZWF0dXJlc1szXS5jaXR5MS5zZXRBdHRyaWJ1dGUoYGRhdGEtJHtjaXR5U3RhcnQgLSAxMDB9YCwgJ29wYWNpdHk6ITEnKVxuICAgICAgICAgICAgRmVhdHVyZXMuY29uZmlnLm5vZGVzLmZlYXR1cmVzWzNdLnNpdGUxLnNldEF0dHJpYnV0ZShgZGF0YS0ke2NpdHlTdGFydH1gLCAnbGVmdDowZW0nKVxuXG4gICAgICAgICAgICBGZWF0dXJlcy5jb25maWcubm9kZXMuZmVhdHVyZXNbM10uY2l0eTEuc2V0QXR0cmlidXRlKGBkYXRhLSR7Y2l0eVN0YXJ0fWAsICdvcGFjaXR5OiEwJylcbiAgICAgICAgICAgIEZlYXR1cmVzLmNvbmZpZy5ub2Rlcy5mZWF0dXJlc1szXS5zaXRlMS5zZXRBdHRyaWJ1dGUoYGRhdGEtJHtjaXR5U3RlcDF9YCwgJ2xlZnQ6MGVtJylcblxuICAgICAgICAgICAgRmVhdHVyZXMuY29uZmlnLm5vZGVzLmZlYXR1cmVzWzNdLmNpdHkyLnNldEF0dHJpYnV0ZShgZGF0YS0ke2NpdHlTdGFydCAtIDEwMH1gLCAnb3BhY2l0eTohMCcpXG4gICAgICAgICAgICBGZWF0dXJlcy5jb25maWcubm9kZXMuZmVhdHVyZXNbM10uc2l0ZTIuc2V0QXR0cmlidXRlKGBkYXRhLSR7Y2l0eVN0YXJ0fWAsICdsZWZ0Oi00M2VtJylcblxuICAgICAgICAgICAgRmVhdHVyZXMuY29uZmlnLm5vZGVzLmZlYXR1cmVzWzNdLmNpdHkyLnNldEF0dHJpYnV0ZShgZGF0YS0ke2NpdHlTdGFydH1gLCAnb3BhY2l0eTohMScpXG4gICAgICAgICAgICBGZWF0dXJlcy5jb25maWcubm9kZXMuZmVhdHVyZXNbM10uc2l0ZTIuc2V0QXR0cmlidXRlKGBkYXRhLSR7Y2l0eVN0ZXAxfWAsICdsZWZ0OjBlbScpXG5cbiAgICAgICAgICAgIEZlYXR1cmVzLmNvbmZpZy5ub2Rlcy5mZWF0dXJlc1szXS5jaXR5Mi5zZXRBdHRyaWJ1dGUoYGRhdGEtJHtjaXR5U3RlcDJ9YCwgJ29wYWNpdHk6ITAnKVxuICAgICAgICAgICAgRmVhdHVyZXMuY29uZmlnLm5vZGVzLmZlYXR1cmVzWzNdLnNpdGUyLnNldEF0dHJpYnV0ZShgZGF0YS0ke2NpdHlTdGVwMn1gLCAnbGVmdDowZW0nKVxuXG4gICAgICAgICAgICBGZWF0dXJlcy5jb25maWcubm9kZXMuZmVhdHVyZXNbM10uY2l0eTMuc2V0QXR0cmlidXRlKGBkYXRhLSR7Y2l0eVN0ZXAxfWAsICdvcGFjaXR5OiEwJylcbiAgICAgICAgICAgIEZlYXR1cmVzLmNvbmZpZy5ub2Rlcy5mZWF0dXJlc1szXS5zaXRlMy5zZXRBdHRyaWJ1dGUoYGRhdGEtJHtjaXR5U3RlcDEgKyAxMDB9YCwgJ2xlZnQ6LTQzZW0nKVxuXG4gICAgICAgICAgICBGZWF0dXJlcy5jb25maWcubm9kZXMuZmVhdHVyZXNbM10uY2l0eTMuc2V0QXR0cmlidXRlKGBkYXRhLSR7Y2l0eVN0ZXAyfWAsICdvcGFjaXR5OiExJylcbiAgICAgICAgICAgIEZlYXR1cmVzLmNvbmZpZy5ub2Rlcy5mZWF0dXJlc1szXS5zaXRlMy5zZXRBdHRyaWJ1dGUoYGRhdGEtJHtjaXR5U3RlcDIgKyAxMDB9YCwgJ2xlZnQ6MGVtJylcbiAgICAgICAgfSxcblxuICAgICAgICBzZXRGZWF0dXJlNCgpIHtcbiAgICAgICAgICAgIGxldCBjcmFuZVN0YXJ0ID0gTWF0aC5mbG9vcihGZWF0dXJlcy5oZWxwZXJzLmdldEludHJvSGVpZ2h0KCkgKyBGZWF0dXJlcy5oZWxwZXJzLmdldEZlYXR1cmVIZWlnaHQoMSkgKyBGZWF0dXJlcy5oZWxwZXJzLmdldEZlYXR1cmVIZWlnaHQoMikgKyBGZWF0dXJlcy5oZWxwZXJzLmdldEZlYXR1cmVIZWlnaHQoMykgKiBGZWF0dXJlcy5oZWxwZXJzLmdldEZyYWN0aW9uKDAuNDUpKVxuICAgICAgICAgICAgbGV0IGJveEVuZCA9IE1hdGguZmxvb3IoRmVhdHVyZXMuaGVscGVycy5nZXRJbnRyb0hlaWdodCgpICsgRmVhdHVyZXMuaGVscGVycy5nZXRGZWF0dXJlSGVpZ2h0KDEpICsgRmVhdHVyZXMuaGVscGVycy5nZXRGZWF0dXJlSGVpZ2h0KDIpICsgRmVhdHVyZXMuaGVscGVycy5nZXRGZWF0dXJlSGVpZ2h0KDMpICogRmVhdHVyZXMuaGVscGVycy5nZXRGcmFjdGlvbigwLjg1KSlcbiAgICAgICAgICAgIGxldCBjYWJsZUVuZCA9IE1hdGguZmxvb3IoRmVhdHVyZXMuaGVscGVycy5nZXRJbnRyb0hlaWdodCgpICsgRmVhdHVyZXMuaGVscGVycy5nZXRGZWF0dXJlSGVpZ2h0KDEpICsgRmVhdHVyZXMuaGVscGVycy5nZXRGZWF0dXJlSGVpZ2h0KDIpICsgRmVhdHVyZXMuaGVscGVycy5nZXRGZWF0dXJlSGVpZ2h0KDMpKVxuXG4gICAgICAgICAgICBGZWF0dXJlcy5jb25maWcubm9kZXMuZmVhdHVyZXNbNF0uY2FibGUuc2V0QXR0cmlidXRlKGBkYXRhLSR7Y3JhbmVTdGFydH1gLCAnaGVpZ2h0OjE1LjJlbTsnKVxuICAgICAgICAgICAgRmVhdHVyZXMuY29uZmlnLm5vZGVzLmZlYXR1cmVzWzRdLmNhYmxlLnNldEF0dHJpYnV0ZShgZGF0YS0ke2JveEVuZH1gLCAnaGVpZ2h0OjVlbTsnKVxuICAgICAgICAgICAgRmVhdHVyZXMuY29uZmlnLm5vZGVzLmZlYXR1cmVzWzRdLmNhYmxlLnNldEF0dHJpYnV0ZShgZGF0YS0ke2NhYmxlRW5kfWAsICdoZWlnaHQ6Mi43ZW07JylcbiAgICAgICAgICAgIEZlYXR1cmVzLmNvbmZpZy5ub2Rlcy5mZWF0dXJlc1s0XS5ib3guc2V0QXR0cmlidXRlKGBkYXRhLSR7Y3JhbmVTdGFydH1gLCAnYm90dG9tOjExLjFlbTsnKVxuICAgICAgICAgICAgRmVhdHVyZXMuY29uZmlnLm5vZGVzLmZlYXR1cmVzWzRdLmJveC5zZXRBdHRyaWJ1dGUoYGRhdGEtJHtib3hFbmR9YCwgJ2JvdHRvbToyMS4xNWVtOycpXG5cbiAgICAgICAgfSxcblxuICAgICAgICBzZXRGZWF0dXJlNSgpIHtcbiAgICAgICAgICAgIGxldCBiYW5rU3RhcnQgPSBNYXRoLmZsb29yKEZlYXR1cmVzLmhlbHBlcnMuZ2V0SW50cm9IZWlnaHQoKSArIEZlYXR1cmVzLmhlbHBlcnMuZ2V0RmVhdHVyZUhlaWdodCgxKSArIEZlYXR1cmVzLmhlbHBlcnMuZ2V0RmVhdHVyZUhlaWdodCgyKSArIEZlYXR1cmVzLmhlbHBlcnMuZ2V0RmVhdHVyZUhlaWdodCgzKSArIEZlYXR1cmVzLmhlbHBlcnMuZ2V0RmVhdHVyZUhlaWdodCg0KSAqIEZlYXR1cmVzLmhlbHBlcnMuZ2V0RnJhY3Rpb24oMC43KSlcbiAgICAgICAgICAgIGxldCBiYW5rQ29pbkVuZCA9IGJhbmtTdGFydCArIE1hdGguZmxvb3IoRmVhdHVyZXMuaGVscGVycy5nZXRGZWF0dXJlSGVpZ2h0KDQpICogRmVhdHVyZXMuaGVscGVycy5nZXRGcmFjdGlvbigwLjY1KSlcbiAgICAgICAgICAgIGxldCBiYW5rRW5kID0gYmFua1N0YXJ0ICsgTWF0aC5mbG9vcihGZWF0dXJlcy5oZWxwZXJzLmdldEZlYXR1cmVIZWlnaHQoNCkgKiBGZWF0dXJlcy5oZWxwZXJzLmdldEZyYWN0aW9uKDEuMSkpXG5cbiAgICAgICAgICAgIEZlYXR1cmVzLmNvbmZpZy5ub2Rlcy5mZWF0dXJlc1s1XS5jb2lucy5zZXRBdHRyaWJ1dGUoYGRhdGEtJHtiYW5rU3RhcnR9YCwgJ3RvcDotMi42ZW0nKVxuICAgICAgICAgICAgRmVhdHVyZXMuY29uZmlnLm5vZGVzLmZlYXR1cmVzWzVdLmNvaW4xLnNldEF0dHJpYnV0ZShgZGF0YS0ke2JhbmtTdGFydH1gLCAnb3BhY2l0eTohMScpXG4gICAgICAgICAgICBGZWF0dXJlcy5jb25maWcubm9kZXMuZmVhdHVyZXNbNV0uY29pbnMuc2V0QXR0cmlidXRlKGBkYXRhLSR7YmFua0VuZH1gLCAndG9wOjIzZW0nKVxuICAgICAgICAgICAgRmVhdHVyZXMuY29uZmlnLm5vZGVzLmZlYXR1cmVzWzVdLmNvaW4xLnNldEF0dHJpYnV0ZShgZGF0YS0ke2JhbmtDb2luRW5kfWAsICdvcGFjaXR5OiEwJylcbiAgICAgICAgfVxuICAgIH0sXG5cbiAgICBiaW5kRXZlbnRzKCkge1xuICAgICAgICAvL1doZW4gd2luZG93IGlzIGJlaW5nIHJlc2l6ZWRcbiAgICAgICAgd2luZG93LmFkZEV2ZW50TGlzdGVuZXIoJ3Jlc2l6ZScsICgpID0+IHtcbiAgICAgICAgICAgIEZlYXR1cmVzLmhlbHBlcnMuc2V0V2luZG93V2lkdGgoKVxuICAgICAgICAgICAgRmVhdHVyZXMuaGVscGVycy5nZXROb2RlcygpLmZvckVhY2gobm9kZSA9PiBGZWF0dXJlcy5oZWxwZXJzLnJlc2V0QW5jaG9ycyhub2RlKSlcbiAgICAgICAgICAgIEZlYXR1cmVzLm1ldGhvZHMuaW5pdCgpXG4gICAgICAgIH0pXG4gICAgfSxcblxuICAgIGluaXQoKSB7XG4gICAgICAgIEZlYXR1cmVzLmJpbmRFdmVudHMoKVxuICAgICAgICBGZWF0dXJlcy5oZWxwZXJzLnNldFdpbmRvd1dpZHRoKClcbiAgICAgICAgRmVhdHVyZXMubWV0aG9kcy5pbml0KClcbiAgICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IEZlYXR1cmVzIiwibGV0IHJldHVybkFycmF5ID0gKG9iamVjdCkgPT4ge1xuICAgIGxldCBrZXlzID0gT2JqZWN0LmtleXMob2JqZWN0KVxuICAgIHJldHVybiBrZXlzLm1hcChrZXkgPT4gb2JqZWN0W2tleV0pLmZpbHRlcihpdGVtID0+IHR5cGVvZiAoaXRlbSkgIT0gJ251bWJlcicpXG59XG5cbndpbmRvdy5yZXF1ZXN0QW5pbUZyYW1lID0gKGZ1bmN0aW9uICgpIHtcbiAgICByZXR1cm4gd2luZG93LnJlcXVlc3RBbmltYXRpb25GcmFtZSB8fFxuICAgICAgICB3aW5kb3cud2Via2l0UmVxdWVzdEFuaW1hdGlvbkZyYW1lIHx8XG4gICAgICAgIHdpbmRvdy5tb3pSZXF1ZXN0QW5pbWF0aW9uRnJhbWUgfHxcbiAgICAgICAgZnVuY3Rpb24gKGNhbGxiYWNrKSB7XG4gICAgICAgICAgICB3aW5kb3cuc2V0VGltZW91dChjYWxsYmFjaywgMTAwMCAvIDYwKTtcbiAgICAgICAgfTtcbn0pKClcblxuZnVuY3Rpb24gc2Nyb2xsVG9ZKHNjcm9sbFRhcmdldFksIHNwZWVkLCBlYXNpbmcpIHtcbiAgICAvLyBzY3JvbGxUYXJnZXRZOiB0aGUgdGFyZ2V0IHNjcm9sbFkgcHJvcGVydHkgb2YgdGhlIHdpbmRvd1xuICAgIC8vIHNwZWVkOiB0aW1lIGluIHBpeGVscyBwZXIgc2Vjb25kXG4gICAgLy8gZWFzaW5nOiBlYXNpbmcgZXF1YXRpb24gdG8gdXNlXG5cbiAgICB2YXIgc2Nyb2xsWSA9IHdpbmRvdy5zY3JvbGxZLFxuICAgICAgICBzY3JvbGxUYXJnZXRZID0gc2Nyb2xsVGFyZ2V0WSB8fCAwLFxuICAgICAgICBzcGVlZCA9IHNwZWVkIHx8IDIwMDAsXG4gICAgICAgIGVhc2luZyA9IGVhc2luZyB8fCAnZWFzZU91dFNpbmUnLFxuICAgICAgICBjdXJyZW50VGltZSA9IDA7XG5cbiAgICAvLyBtaW4gdGltZSAuMSwgbWF4IHRpbWUgLjggc2Vjb25kc1xuICAgIHZhciB0aW1lID0gTWF0aC5tYXgoLjEsIE1hdGgubWluKE1hdGguYWJzKHNjcm9sbFkgLSBzY3JvbGxUYXJnZXRZKSAvIHNwZWVkLCAuOCkpO1xuXG4gICAgLy8gZWFzaW5nIGVxdWF0aW9ucyBmcm9tIGh0dHBzOi8vZ2l0aHViLmNvbS9kYW5yby9lYXNpbmctanMvYmxvYi9tYXN0ZXIvZWFzaW5nLmpzXG4gICAgdmFyIFBJX0QyID0gTWF0aC5QSSAvIDIsXG4gICAgICAgIGVhc2luZ0VxdWF0aW9ucyA9IHtcbiAgICAgICAgICAgIGVhc2VPdXRTaW5lOiBmdW5jdGlvbiAocG9zKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIE1hdGguc2luKHBvcyAqIChNYXRoLlBJIC8gMikpO1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIGVhc2VJbk91dFNpbmU6IGZ1bmN0aW9uIChwb3MpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gKC0wLjUgKiAoTWF0aC5jb3MoTWF0aC5QSSAqIHBvcykgLSAxKSk7XG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgZWFzZUluT3V0UXVpbnQ6IGZ1bmN0aW9uIChwb3MpIHtcbiAgICAgICAgICAgICAgICBpZiAoKHBvcyAvPSAwLjUpIDwgMSkge1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gMC41ICogTWF0aC5wb3cocG9zLCA1KTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgcmV0dXJuIDAuNSAqIChNYXRoLnBvdygocG9zIC0gMiksIDUpICsgMik7XG4gICAgICAgICAgICB9XG4gICAgICAgIH07XG5cbiAgICAvLyBhZGQgYW5pbWF0aW9uIGxvb3BcbiAgICBmdW5jdGlvbiB0aWNrKCkge1xuICAgICAgICBjdXJyZW50VGltZSArPSAxIC8gNjA7XG5cbiAgICAgICAgdmFyIHAgPSBjdXJyZW50VGltZSAvIHRpbWU7XG4gICAgICAgIHZhciB0ID0gZWFzaW5nRXF1YXRpb25zW2Vhc2luZ10ocCk7XG5cbiAgICAgICAgaWYgKHAgPCAxKSB7XG4gICAgICAgICAgICByZXF1ZXN0QW5pbUZyYW1lKHRpY2spO1xuXG4gICAgICAgICAgICB3aW5kb3cuc2Nyb2xsVG8oMCwgc2Nyb2xsWSArICgoc2Nyb2xsVGFyZ2V0WSAtIHNjcm9sbFkpICogdCkpO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgd2luZG93LnNjcm9sbFRvKDAsIHNjcm9sbFRhcmdldFkpO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgLy8gY2FsbCBpdCBvbmNlIHRvIGdldCBzdGFydGVkXG4gICAgdGljaygpO1xufVxuXG5leHBvcnQge3JldHVybkFycmF5LCBzY3JvbGxUb1l9IiwiaW1wb3J0IE5hdiBmcm9tICcuL25hdi5qcydcbmltcG9ydCBEb2NzIGZyb20gJy4vZG9jLmpzJ1xuaW1wb3J0IENvbnNvbGUgZnJvbSAnLi9jb25zb2xlLmpzJ1xuaW1wb3J0IEJvb3N0ZXJzIGZyb20gJy4vYm9vc3RlcnMuanMnXG5pbXBvcnQgRmVhdHVyZXMgZnJvbSAnLi9mZWF0dXJlcy5qcydcbmltcG9ydCBza3JvbGxyIGZyb20gJy4vc2tyb2xsci5taW4uanMnXG5pbXBvcnQgYXhpb3MgZnJvbSAnYXhpb3MnXG5pbXBvcnQgeyByZXR1cm5BcnJheSwgc2Nyb2xsVG9ZIH0gZnJvbSAnLi9oZWxwZXJzLmpzJ1xuXG5pZiAod2luZG93LmxvY2F0aW9uLnBhdGhuYW1lID09ICcvYml6JyB8fCB3aW5kb3cubG9jYXRpb24ucGF0aG5hbWUgPT0gJy8nKSB7XG5cbiAgICBmdW5jdGlvbiBnZXRJbnRyb0hlaWdodCgpIHtcbiAgICAgICAgbGV0IGJhbm5lciA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJ3NlY3Rpb24uYmFubmVyJylcbiAgICAgICAgbGV0IGhlaWdodCA9IHBhcnNlSW50KHdpbmRvdy5nZXRDb21wdXRlZFN0eWxlKGJhbm5lciwgbnVsbCkuaGVpZ2h0KVxuICAgICAgICBsZXQgcGFkZGluZyA9IHBhcnNlSW50KHdpbmRvdy5nZXRDb21wdXRlZFN0eWxlKGJhbm5lciwgbnVsbCkuZm9udFNpemUpIC0gMTBcbiAgICAgICAgcmV0dXJuIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJ3NlY3Rpb24uaW50cm8nKS5vZmZzZXRIZWlnaHQgKyBoZWlnaHQgKyBwYWRkaW5nXG4gICAgfVxuXG4gICAgZnVuY3Rpb24gZ2V0RmVhdHVyZUhlaWdodChmZWF0dXJlKSB7XG4gICAgICAgIGlmIChmZWF0dXJlICE9IDApIHtcbiAgICAgICAgICAgIHJldHVybiBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKGAuZmVhdHVyZXMgLmZlYXR1cmUtJHtmZWF0dXJlfWApLm9mZnNldEhlaWdodFxuICAgICAgICB9XG4gICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgcmV0dXJuIDBcbiAgICAgICAgfVxuICAgIH1cblxuICAgIC8vIE5hdmlnYXRpb25cblxuICAgIGxldCBoYW0gPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcuaGFtYnVyZ2VyJylcbiAgICBsZXQgbmF2ID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLm5hdi1tb2JpbGUnKVxuICAgIGxldCBpdGVtcyA9IHJldHVybkFycmF5KGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoJy5uYXYtbW9iaWxlIGxpJykpXG5cbiAgICBmdW5jdGlvbiBnZXRGZWF0dXJlc0hlaWdodChmZWF0dXJlKSB7XG4gICAgICAgIGxldCBoZWlnaHQgPSAwXG4gICAgICAgIGZvciAobGV0IHggPSAxOyB4IDw9IGZlYXR1cmU7IHgrKykge1xuICAgICAgICAgICAgaGVpZ2h0ID0gaGVpZ2h0ICsgZ2V0RmVhdHVyZUhlaWdodCh4IC0gMSlcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gaGVpZ2h0ICsgZ2V0SW50cm9IZWlnaHQoKVxuICAgIH1cblxuICAgIGhhbS5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgbmF2LmNsYXNzTGlzdC50b2dnbGUoJ25hdi1tb2JpbGUtb3BlbicpXG4gICAgfSlcblxuICAgIGl0ZW1zLmZpbHRlcihpdGVtID0+IHR5cGVvZiAoaXRlbSkgIT0gJ251bWJlcicpLmZvckVhY2goaXRlbSA9PiB7XG4gICAgICAgIGl0ZW0uYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICBuYXYuY2xhc3NMaXN0LnJlbW92ZSgnbmF2LW1vYmlsZS1vcGVuJylcbiAgICAgICAgICAgIGxldCBmZWF0dXJlID0gaXRlbS5nZXRBdHRyaWJ1dGUoJ2RhdGEtc2Nyb2xsJylcbiAgICAgICAgICAgIGlmIChmZWF0dXJlID09IDYpIHtcbiAgICAgICAgICAgICAgICBzY3JvbGxUb1koY29udGFjdEhlaWdodCArIDEwMCwgMTAwMCwgJ2Vhc2VJbk91dFF1aW50JylcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgIHNjcm9sbFRvWShnZXRGZWF0dXJlc0hlaWdodChmZWF0dXJlKSwgMTAwMCwgJ2Vhc2VJbk91dFF1aW50JylcbiAgICAgICAgICAgIH1cblxuICAgICAgICB9KVxuICAgIH0pXG5cbiAgICBmdW5jdGlvbiBnZXRBY3RpdmVXaWR0aChhY3RpdmUpIHtcbiAgICAgICAgbGV0IGxpbmsgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKGAubmF2LW1haW4gbGk6bnRoLWNoaWxkKCR7YWN0aXZlfSlgKVxuICAgICAgICByZXR1cm4gd2luZG93LmdldENvbXB1dGVkU3R5bGUobGluaywgbnVsbCkud2lkdGhcbiAgICB9XG5cbiAgICBmdW5jdGlvbiBnZXRBY3RpdmVMZWZ0KGFjdGl2ZSkge1xuICAgICAgICBsZXQgbGlua3MgPSByZXR1cm5BcnJheShkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKGAubmF2LW1haW4gbGlgKSlcbiAgICAgICAgbGV0IGxlZnQgPSBsaW5rcy5tYXAoKGxpbmssIGluZGV4KSA9PiB7XG4gICAgICAgICAgICBpZiAoaW5kZXggPCBhY3RpdmUgLSAxKSB7XG4gICAgICAgICAgICAgICAgbGV0IHdpZHRoID0gcGFyc2VJbnQod2luZG93LmdldENvbXB1dGVkU3R5bGUobGluaywgbnVsbCkud2lkdGgpXG4gICAgICAgICAgICAgICAgbGV0IG1hcmdpbiA9IHBhcnNlSW50KHdpbmRvdy5nZXRDb21wdXRlZFN0eWxlKGxpbmssIG51bGwpLm1hcmdpblJpZ2h0KVxuICAgICAgICAgICAgICAgIHJldHVybiB3aWR0aCArIG1hcmdpblxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgcmV0dXJuIDBcbiAgICAgICAgfSlcbiAgICAgICAgbGVmdCA9IGxlZnQucmVkdWNlKChwcmUsIG5leHQpID0+IHtcbiAgICAgICAgICAgIHJldHVybiBwcmUgKyBuZXh0XG4gICAgICAgIH0pXG5cbiAgICAgICAgcmV0dXJuIGxlZnQgKyAncHgnXG4gICAgfVxuXG4gICAgdmFyIGNvbnRhY3RIZWlnaHQgPSBwYXJzZUludCh3aW5kb3cuZ2V0Q29tcHV0ZWRTdHlsZShkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCdib2R5JyksIG51bGwpLmhlaWdodCkgLSB3aW5kb3cuaW5uZXJIZWlnaHRcblxuICAgIGxldCBhY3RpdmUgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcubmF2LW1haW4gLnVuZGVybGluZScpXG4gICAgYWN0aXZlLnNldEF0dHJpYnV0ZShgZGF0YS0wYCwgYG9wYWNpdHk6ITBgKVxuICAgIGFjdGl2ZS5zZXRBdHRyaWJ1dGUoYGRhdGEtJHtnZXRGZWF0dXJlc0hlaWdodCgxKX1gLCBgd2lkdGg6ISR7Z2V0QWN0aXZlV2lkdGgoMSl9OyBsZWZ0OiEke2dldEFjdGl2ZUxlZnQoMSl9O29wYWNpdHk6ITE7YClcbiAgICBhY3RpdmUuc2V0QXR0cmlidXRlKGBkYXRhLSR7Z2V0RmVhdHVyZXNIZWlnaHQoMil9YCwgYHdpZHRoOiEke2dldEFjdGl2ZVdpZHRoKDIpfTsgbGVmdDohJHtnZXRBY3RpdmVMZWZ0KDIpfTtgKVxuICAgIGFjdGl2ZS5zZXRBdHRyaWJ1dGUoYGRhdGEtJHtnZXRGZWF0dXJlc0hlaWdodCgzKX1gLCBgd2lkdGg6ISR7Z2V0QWN0aXZlV2lkdGgoMyl9OyBsZWZ0OiEke2dldEFjdGl2ZUxlZnQoMyl9O2ApXG4gICAgYWN0aXZlLnNldEF0dHJpYnV0ZShgZGF0YS0ke2dldEZlYXR1cmVzSGVpZ2h0KDQpfWAsIGB3aWR0aDohJHtnZXRBY3RpdmVXaWR0aCg0KX07IGxlZnQ6ISR7Z2V0QWN0aXZlTGVmdCg0KX07YClcbiAgICBhY3RpdmUuc2V0QXR0cmlidXRlKGBkYXRhLSR7Z2V0RmVhdHVyZXNIZWlnaHQoNSl9YCwgYHdpZHRoOiEke2dldEFjdGl2ZVdpZHRoKDUpfTsgbGVmdDohJHtnZXRBY3RpdmVMZWZ0KDUpfTtgKVxuICAgIGFjdGl2ZS5zZXRBdHRyaWJ1dGUoYGRhdGEtJHtjb250YWN0SGVpZ2h0fWAsIGB3aWR0aDohJHtnZXRBY3RpdmVXaWR0aCg2KX07IGxlZnQ6ISR7Z2V0QWN0aXZlTGVmdCg2KX07YClcblxuICAgIHJldHVybkFycmF5KGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoYC5uYXYtbWFpbiBsaWApKS5mb3JFYWNoKChsaW5rLCBpbmRleCkgPT4ge1xuICAgICAgICBsaW5rLmFkZEV2ZW50TGlzdGVuZXIoJ2NsaWNrJywgKCkgPT4ge1xuICAgICAgICAgICAgaWYgKGluZGV4ICsgMSA9PSA2KSB7XG4gICAgICAgICAgICAgICAgc2Nyb2xsVG9ZKGNvbnRhY3RIZWlnaHQgKyAxMDAsIDEwMDAsICdlYXNlSW5PdXRRdWludCcpXG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgICBzY3JvbGxUb1koZ2V0RmVhdHVyZXNIZWlnaHQoaW5kZXggKyAxKSwgMTAwMCwgJ2Vhc2VJbk91dFF1aW50JylcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSlcbiAgICB9KVxuXG4gICAgLy8gPCEtLS0tLS0gRm9ybSBTdWJtaXNzaW9uIC0tLS0tLS0hPlxuXG4gICAgbGV0IGZvcm0gPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCdzZWN0aW9uLmVucXVpcnkgZm9ybScpXG4gICAgbGV0IG92ZXJsYXkgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCdzZWN0aW9uLmVucXVpcnkgLm92ZXJsYXknKVxuICAgIGxldCBjbGllbnRJZCA9ICc3MzFFQkFCODU5NjY0OEU3OTg4MjA5NkU0NzMzRTcxNzNCMzE0QkVDQzY0OTQyM0RCNjc4OTdCRjhDQzU5NkVDJ1xuICAgIGxldCBrZXkgPSAnOE5ORXpvcnVSUHZaMHNCSnhuT2NndydcblxuICAgIGZvcm0uYWRkRXZlbnRMaXN0ZW5lcignc3VibWl0JywgKGV2ZW50KSA9PiB7XG4gICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KClcbiAgICAgICAgbGV0IG5hbWUgPSBmb3JtLmNoaWxkTm9kZXNbMV0uY2hpbGROb2Rlc1sxXS52YWx1ZVxuICAgICAgICBsZXQgZW1haWwgPSBmb3JtLmNoaWxkTm9kZXNbMV0uY2hpbGROb2Rlc1szXS52YWx1ZVxuICAgICAgICBsZXQgbWVzc2FnZSA9IGZvcm0uY2hpbGROb2Rlc1szXS5jaGlsZE5vZGVzWzFdLnZhbHVlXG4gICAgICAgIGF4aW9zLnBvc3QoJ2h0dHBzOi8vbWFuZHJpbGxhcHAuY29tL2FwaS8xLjAvbWVzc2FnZXMvc2VuZC5qc29uJywge1xuICAgICAgICAgICAgXCJrZXlcIjoga2V5LFxuICAgICAgICAgICAgJ21lc3NhZ2UnOiB7XG4gICAgICAgICAgICAgICAgJ2Zyb21fZW1haWwnOiAnaW5mb0BraXRzdW5lLnRvb2xzJyxcbiAgICAgICAgICAgICAgICAnZnJhbV9uYW1lJzogbmFtZSxcbiAgICAgICAgICAgICAgICAnc3ViamVjdCc6ICdFbnF1aXJ5IGZyb20gS2l0c3VuZSBXZWJzaXRlLicsXG4gICAgICAgICAgICAgICAgJ2h0bWwnOiBgPGgzPiBGcm9tIC0gJHtuYW1lfTxoMz48aDM+RW1haWwgLSAke2VtYWlsfTwvaDM+PGgzPk1lc3NhZ2U8L2gzPjxwPiR7bWVzc2FnZX08L3A+YCxcbiAgICAgICAgICAgICAgICAndG8nOiBbXG4gICAgICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICdlbWFpbCc6ICdraXRzdW5lQG5vd2Zsb2F0cy5jb20nLFxuICAgICAgICAgICAgICAgICAgICAgICAgJ25hbWUnOiAnS2l0c3VuZSdcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIF0sXG4gICAgICAgICAgICB9XG4gICAgICAgIH0pXG4gICAgICAgICAgICAudGhlbihmdW5jdGlvbiAocmVzcG9uc2UpIHtcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhyZXNwb25zZS5kYXRhKVxuICAgICAgICAgICAgICAgIG92ZXJsYXkuY2xhc3NMaXN0LmFkZCgnb3ZlcmxheS1zaG93JylcbiAgICAgICAgICAgICAgICBzZXRUaW1lb3V0KCgpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgZm9ybS5yZXNldCgpXG4gICAgICAgICAgICAgICAgICAgIG92ZXJsYXkuY2xhc3NMaXN0LnJlbW92ZSgnb3ZlcmxheS1zaG93JylcbiAgICAgICAgICAgICAgICB9LCAzMDAwKVxuICAgICAgICAgICAgfSlcbiAgICB9KVxufVxuXG5pZiAod2luZG93LmxvY2F0aW9uLnBhdGhuYW1lID09ICcvZGV2Jykge1xuICAgIE5hdi5pbml0KClcbiAgICBDb25zb2xlLmluaXQoKVxuICAgIERvY3MuaW5pdCgpXG4gICAgQm9vc3RlcnMuaW5pdCgpXG59XG5cbmlmICgod2luZG93LmxvY2F0aW9uLnBhdGhuYW1lID09ICcvYml6J3x8IHdpbmRvdy5sb2NhdGlvbi5wYXRobmFtZSA9PSAnLycpICYmIHdpbmRvdy5vdXRlcldpZHRoID4gNjk5KSB7XG4gICAgRmVhdHVyZXMuaW5pdCgpXG59IiwiaW1wb3J0IHsgcmV0dXJuQXJyYXksIHNjcm9sbFRvWSB9IGZyb20gJy4vaGVscGVycy5qcydcblxubGV0IE5hdiA9IHtcbiAgICBjb25maWc6IHtcbiAgICAgICAgbm9kZXM6IHtcbiAgICAgICAgICAgIG5hdjogZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLnNlY3Rpb24tbmF2aWdhdGlvbicpLFxuICAgICAgICAgICAgbGlua3M6IHJldHVybkFycmF5KGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoYC5zZWN0aW9uLW5hdmlnYXRpb24gLm5hdi1tYWluIGxpYCkpLFxuICAgICAgICAgICAgbW9iaWxlOiBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcubmF2LW1vYmlsZScpLFxuICAgICAgICAgICAgaGFtYnVyZ2VyOiBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcubmF2LW1vYmlsZSAuaGFtYnVyZ2VyJyksXG4gICAgICAgICAgICBtb2JpbGVMaW5rczogcmV0dXJuQXJyYXkoZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbCgnLm5hdi1tb2JpbGUgLmxpbmtzIGxpJykpXG4gICAgICAgIH0sXG4gICAgICAgIHRyaWdnZXJzOiB7XG4gICAgICAgICAgICBzdGljazogdW5kZWZpbmVkLFxuICAgICAgICAgICAgaG9tZTogdW5kZWZpbmVkLFxuICAgICAgICAgICAgZmVhdHVyZXM6IHVuZGVmaW5lZCxcbiAgICAgICAgICAgIHN0cnVjdHVyZTogdW5kZWZpbmVkXG4gICAgICAgIH0sXG4gICAgICAgIHBvczogdW5kZWZpbmVkXG4gICAgfSxcblxuICAgIGluaXQoKSB7XG4gICAgICAgIGlmICh3aW5kb3cubG9jYXRpb24ucGF0aG5hbWUgPT0gJy9kZXYnKSB7XG4gICAgICAgICAgICBOYXYubWV0aG9kcy5zZXRUcmlnZ2VycygpXG4gICAgICAgICAgICBOYXYuYmluZEV2ZW50cygpXG4gICAgICAgIH1cbiAgICB9LFxuXG4gICAgYmluZEV2ZW50cygpIHtcblxuICAgICAgICAvL09uIHBhZ2Ugc2Nyb2xsXG4gICAgICAgIGRvY3VtZW50LmFkZEV2ZW50TGlzdGVuZXIoJ3Njcm9sbCcsICgpID0+IHtcbiAgICAgICAgICAgIE5hdi5tZXRob2RzLnNldFBvcygpXG4gICAgICAgICAgICBOYXYubWV0aG9kcy5zZXRUcmlnZ2VycygpXG4gICAgICAgICAgICBOYXYubWV0aG9kcy5zdGljaygpXG4gICAgICAgICAgICBOYXYubWV0aG9kcy5zZXRBY3RpdmUoKVxuICAgICAgICB9KVxuXG4gICAgICAgIC8vT24gbmF2IGNsaWNrXG4gICAgICAgIE5hdi5jb25maWcubm9kZXMubGlua3MuZm9yRWFjaCgobGluaywgaW5kZXgpID0+IHtcbiAgICAgICAgICAgIGxpbmsuYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCAoKSA9PiB7XG4gICAgICAgICAgICAgICAgTmF2Lm1ldGhvZHMuc2Nyb2xsKGluZGV4ICsgMSlcbiAgICAgICAgICAgIH0pXG4gICAgICAgIH0pXG5cbiAgICAgICAgLy9PbiBoYW1idXJnZXIgY2xpY2tcbiAgICAgICAgTmF2LmNvbmZpZy5ub2Rlcy5tb2JpbGUuYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCAoZXZlbnQpID0+IHtcbiAgICAgICAgICAgIE5hdi5jb25maWcubm9kZXMubW9iaWxlLmNsYXNzTGlzdC50b2dnbGUoJ25hdi1tb2JpbGUtb3BlbicpXG4gICAgICAgIH0pXG5cbiAgICAgICAgLy9PbiBtb2JpbGUgbGlua3MgY2xpY2tcbiAgICAgICAgTmF2LmNvbmZpZy5ub2Rlcy5tb2JpbGVMaW5rcy5maWx0ZXIobGluayA9PiB0eXBlb2YgKGxpbmspICE9ICdudW1iZXInKS5mb3JFYWNoKChsaW5rLCBpbmRleCkgPT4ge1xuICAgICAgICAgICAgbGluay5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsICgpID0+IHtcbiAgICAgICAgICAgICAgICBOYXYubWV0aG9kcy5zY3JvbGwoaW5kZXggKyAxKVxuICAgICAgICAgICAgfSlcbiAgICAgICAgfSlcbiAgICB9LFxuXG4gICAgbWV0aG9kczoge1xuXG4gICAgICAgIHNldFBvcygpIHtcbiAgICAgICAgICAgIGlmICh3aW5kb3cucGFnZVlPZmZzZXQgPiAwKSB7XG4gICAgICAgICAgICAgICAgTmF2LmNvbmZpZy5wb3MgPSB3aW5kb3cucGFnZVlPZmZzZXRcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2UgaWYgKGRvY3VtZW50LmRvY3VtZW50RWxlbWVudCkge1xuICAgICAgICAgICAgICAgIE5hdi5jb25maWcucG9zID0gZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LnNjcm9sbFRvcFxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZSBpZiAoZG9jdW1lbnQuYm9keSkge1xuICAgICAgICAgICAgICAgIE5hdi5jb25maWcucG9zID0gZG9jdW1lbnQuYm9keS5zY3JvbGxUb3BcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2UgaWYgKGRvY3VtZW50LmJvZHkucGFyZW50Tm9kZSkge1xuICAgICAgICAgICAgICAgIE5hdi5jb25maWcucG9zID0gZG9jdW1lbnQuYm9keS5wYXJlbnROb2RlLnNjcm9sbFRvcFxuICAgICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIHNldFRyaWdnZXJzKCkge1xuICAgICAgICAgICAgTmF2LmNvbmZpZy50cmlnZ2Vycy5zdGljayA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5zZWN0aW9uLWludHJvIC5iYW5uZXInKS5jbGllbnRIZWlnaHQgLyAxLjgsXG4gICAgICAgICAgICAgICAgTmF2LmNvbmZpZy50cmlnZ2Vycy5ob21lID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLnNlY3Rpb24taW50cm8gLndyYXBwZXItYmFubmVyJykuY2xpZW50SGVpZ2h0LFxuICAgICAgICAgICAgICAgIE5hdi5jb25maWcudHJpZ2dlcnMuZmVhdHVyZXMgPSBOYXYuY29uZmlnLnRyaWdnZXJzLmhvbWUgKyBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcuc2VjdGlvbi1pbnRybyAud3JhcHBlci1jb250ZW50JykuY2xpZW50SGVpZ2h0LFxuICAgICAgICAgICAgICAgIE5hdi5jb25maWcudHJpZ2dlcnMuc3RydWN0dXJlID0gTmF2LmNvbmZpZy50cmlnZ2Vycy5mZWF0dXJlcyArIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5zZWN0aW9uLWludHJvIC53cmFwcGVyLWZlYXR1cmVzJykuY2xpZW50SGVpZ2h0XG4gICAgICAgIH0sXG5cbiAgICAgICAgc3RpY2soKSB7XG4gICAgICAgICAgICBpZiAoTmF2LmNvbmZpZy5wb3MgPiBOYXYuY29uZmlnLnRyaWdnZXJzLnN0aWNrKSB7XG4gICAgICAgICAgICAgICAgaWYgKCFOYXYuY29uZmlnLm5vZGVzLm5hdi5jbGFzc0xpc3QuY29udGFpbnMoJ25hdi1zdGljaycpKSB7XG4gICAgICAgICAgICAgICAgICAgIE5hdi5jb25maWcubm9kZXMubmF2LmNsYXNzTGlzdC5hZGQoJ25hdi1zdGljaycpXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgICAgTmF2LmNvbmZpZy5ub2Rlcy5uYXYuY2xhc3NMaXN0LnJlbW92ZSgnbmF2LXN0aWNrJylcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICBzZXRBY3RpdmUoKSB7XG5cbiAgICAgICAgICAgIGlmIChOYXYuY29uZmlnLnBvcyA8IE5hdi5jb25maWcudHJpZ2dlcnMuaG9tZSkge1xuICAgICAgICAgICAgICAgIGFjdGl2YXRlKDEpXG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlIGlmIChOYXYuY29uZmlnLnBvcyA+PSBOYXYuY29uZmlnLnRyaWdnZXJzLmhvbWUgJiYgTmF2LmNvbmZpZy5wb3MgPCBOYXYuY29uZmlnLnRyaWdnZXJzLmZlYXR1cmVzKSB7XG4gICAgICAgICAgICAgICAgYWN0aXZhdGUoMilcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2UgaWYgKE5hdi5jb25maWcucG9zID49IE5hdi5jb25maWcudHJpZ2dlcnMuZmVhdHVyZXMgJiYgTmF2LmNvbmZpZy5wb3MgPCBOYXYuY29uZmlnLnRyaWdnZXJzLnN0cnVjdHVyZSkge1xuICAgICAgICAgICAgICAgIGFjdGl2YXRlKDMpXG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlIGlmIChOYXYuY29uZmlnLnBvcyA+PSBOYXYuY29uZmlnLnRyaWdnZXJzLnN0cnVjdHVyZSkge1xuICAgICAgICAgICAgICAgIGFjdGl2YXRlKDQpXG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGZ1bmN0aW9uIGFjdGl2YXRlKGNoaWxkKSB7XG4gICAgICAgICAgICAgICAgTmF2LmNvbmZpZy5ub2Rlcy5saW5rcy5mb3JFYWNoKGxpbmsgPT4ge1xuICAgICAgICAgICAgICAgICAgICBsaW5rLmNsYXNzTGlzdC5yZW1vdmUoJ2FjdGl2ZScpXG4gICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKGAuc2VjdGlvbi1uYXZpZ2F0aW9uIC5uYXYtbWFpbiBsaTpudGgtY2hpbGQoJHtjaGlsZH0pYCkuY2xhc3NMaXN0LmFkZCgnYWN0aXZlJylcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICBzY3JvbGwoY2hpbGQpIHtcbiAgICAgICAgICAgIHN3aXRjaCAoY2hpbGQpIHtcbiAgICAgICAgICAgICAgICBjYXNlIDE6XG4gICAgICAgICAgICAgICAgICAgIHNjcm9sbCgwKVxuICAgICAgICAgICAgICAgICAgICBicmVha1xuICAgICAgICAgICAgICAgIGNhc2UgMjpcbiAgICAgICAgICAgICAgICAgICAgc2Nyb2xsKE5hdi5jb25maWcudHJpZ2dlcnMuaG9tZSlcbiAgICAgICAgICAgICAgICAgICAgYnJlYWtcbiAgICAgICAgICAgICAgICBjYXNlIDM6XG4gICAgICAgICAgICAgICAgICAgIHNjcm9sbChOYXYuY29uZmlnLnRyaWdnZXJzLmZlYXR1cmVzKVxuICAgICAgICAgICAgICAgICAgICBicmVha1xuICAgICAgICAgICAgICAgIGNhc2UgNDpcbiAgICAgICAgICAgICAgICAgICAgc2Nyb2xsKE5hdi5jb25maWcudHJpZ2dlcnMuc3RydWN0dXJlKVxuICAgICAgICAgICAgICAgICAgICBicmVha1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBmdW5jdGlvbiBzY3JvbGwocG9zKSB7XG4gICAgICAgICAgICAgICAgc2Nyb2xsVG9ZKHBvcywgMTAwMCwgJ2Vhc2VJbk91dFF1aW50JylcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH1cbn1cblxuZXhwb3J0IGRlZmF1bHQgTmF2IiwiLyohIHNrcm9sbHIgMC42LjMwICgyMDE1LTA4LTEyKSB8IEFsZXhhbmRlciBQcmluemhvcm4gLSBodHRwczovL2dpdGh1Yi5jb20vUHJpbnpob3JuL3Nrcm9sbHIgfCBGcmVlIHRvIHVzZSB1bmRlciB0ZXJtcyBvZiBNSVQgbGljZW5zZSAqL1xuIWZ1bmN0aW9uKGEsYixjKXtcInVzZSBzdHJpY3RcIjtmdW5jdGlvbiBkKGMpe2lmKGU9Yi5kb2N1bWVudEVsZW1lbnQsZj1iLmJvZHksVCgpLGhhPXRoaXMsYz1jfHx7fSxtYT1jLmNvbnN0YW50c3x8e30sYy5lYXNpbmcpZm9yKHZhciBkIGluIGMuZWFzaW5nKVdbZF09Yy5lYXNpbmdbZF07dGE9Yy5lZGdlU3RyYXRlZ3l8fFwic2V0XCIsa2E9e2JlZm9yZXJlbmRlcjpjLmJlZm9yZXJlbmRlcixyZW5kZXI6Yy5yZW5kZXIsa2V5ZnJhbWU6Yy5rZXlmcmFtZX0sbGE9Yy5mb3JjZUhlaWdodCE9PSExLGxhJiYoS2E9Yy5zY2FsZXx8MSksbmE9Yy5tb2JpbGVEZWNlbGVyYXRpb258fHkscGE9Yy5zbW9vdGhTY3JvbGxpbmchPT0hMSxxYT1jLnNtb290aFNjcm9sbGluZ0R1cmF0aW9ufHxBLHJhPXt0YXJnZXRUb3A6aGEuZ2V0U2Nyb2xsVG9wKCl9LFNhPShjLm1vYmlsZUNoZWNrfHxmdW5jdGlvbigpe3JldHVybi9BbmRyb2lkfGlQaG9uZXxpUGFkfGlQb2R8QmxhY2tCZXJyeS9pLnRlc3QobmF2aWdhdG9yLnVzZXJBZ2VudHx8bmF2aWdhdG9yLnZlbmRvcnx8YS5vcGVyYSl9KSgpLFNhPyhqYT1iLmdldEVsZW1lbnRCeUlkKGMuc2tyb2xsckJvZHl8fHopLGphJiZnYSgpLFgoKSxFYShlLFtzLHZdLFt0XSkpOkVhKGUsW3MsdV0sW3RdKSxoYS5yZWZyZXNoKCksd2EoYSxcInJlc2l6ZSBvcmllbnRhdGlvbmNoYW5nZVwiLGZ1bmN0aW9uKCl7dmFyIGE9ZS5jbGllbnRXaWR0aCxiPWUuY2xpZW50SGVpZ2h0OyhiIT09UGF8fGEhPT1PYSkmJihQYT1iLE9hPWEsUWE9ITApfSk7dmFyIGc9VSgpO3JldHVybiBmdW5jdGlvbiBoKCl7JCgpLHZhPWcoaCl9KCksaGF9dmFyIGUsZixnPXtnZXQ6ZnVuY3Rpb24oKXtyZXR1cm4gaGF9LGluaXQ6ZnVuY3Rpb24oYSl7cmV0dXJuIGhhfHxuZXcgZChhKX0sVkVSU0lPTjpcIjAuNi4zMFwifSxoPU9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHksaT1hLk1hdGgsaj1hLmdldENvbXB1dGVkU3R5bGUsaz1cInRvdWNoc3RhcnRcIixsPVwidG91Y2htb3ZlXCIsbT1cInRvdWNoY2FuY2VsXCIsbj1cInRvdWNoZW5kXCIsbz1cInNrcm9sbGFibGVcIixwPW8rXCItYmVmb3JlXCIscT1vK1wiLWJldHdlZW5cIixyPW8rXCItYWZ0ZXJcIixzPVwic2tyb2xsclwiLHQ9XCJuby1cIitzLHU9cytcIi1kZXNrdG9wXCIsdj1zK1wiLW1vYmlsZVwiLHc9XCJsaW5lYXJcIix4PTFlMyx5PS4wMDQsej1cInNrcm9sbHItYm9keVwiLEE9MjAwLEI9XCJzdGFydFwiLEM9XCJlbmRcIixEPVwiY2VudGVyXCIsRT1cImJvdHRvbVwiLEY9XCJfX19za3JvbGxhYmxlX2lkXCIsRz0vXig/OmlucHV0fHRleHRhcmVhfGJ1dHRvbnxzZWxlY3QpJC9pLEg9L15cXHMrfFxccyskL2csST0vXmRhdGEoPzotKF9cXHcrKSk/KD86LT8oLT9cXGQqXFwuP1xcZCtwPykpPyg/Oi0/KHN0YXJ0fGVuZHx0b3B8Y2VudGVyfGJvdHRvbSkpPyg/Oi0/KHRvcHxjZW50ZXJ8Ym90dG9tKSk/JC8sSj0vXFxzKihAP1tcXHdcXC1cXFtcXF1dKylcXHMqOlxccyooLis/KVxccyooPzo7fCQpL2dpLEs9L14oQD9bYS16XFwtXSspXFxbKFxcdyspXFxdJC8sTD0vLShbYS16MC05X10pL2csTT1mdW5jdGlvbihhLGIpe3JldHVybiBiLnRvVXBwZXJDYXNlKCl9LE49L1tcXC0rXT9bXFxkXSpcXC4/W1xcZF0rL2csTz0vXFx7XFw/XFx9L2csUD0vcmdiYT9cXChcXHMqLT9cXGQrXFxzKixcXHMqLT9cXGQrXFxzKixcXHMqLT9cXGQrL2csUT0vW2EtelxcLV0rLWdyYWRpZW50L2csUj1cIlwiLFM9XCJcIixUPWZ1bmN0aW9uKCl7dmFyIGE9L14oPzpPfE1venx3ZWJraXR8bXMpfCg/Oi0oPzpvfG1venx3ZWJraXR8bXMpLSkvO2lmKGope3ZhciBiPWooZixudWxsKTtmb3IodmFyIGMgaW4gYilpZihSPWMubWF0Y2goYSl8fCtjPT1jJiZiW2NdLm1hdGNoKGEpKWJyZWFrO2lmKCFSKXJldHVybiB2b2lkKFI9Uz1cIlwiKTtSPVJbMF0sXCItXCI9PT1SLnNsaWNlKDAsMSk/KFM9UixSPXtcIi13ZWJraXQtXCI6XCJ3ZWJraXRcIixcIi1tb3otXCI6XCJNb3pcIixcIi1tcy1cIjpcIm1zXCIsXCItby1cIjpcIk9cIn1bUl0pOlM9XCItXCIrUi50b0xvd2VyQ2FzZSgpK1wiLVwifX0sVT1mdW5jdGlvbigpe3ZhciBiPWEucmVxdWVzdEFuaW1hdGlvbkZyYW1lfHxhW1IudG9Mb3dlckNhc2UoKStcIlJlcXVlc3RBbmltYXRpb25GcmFtZVwiXSxjPUhhKCk7cmV0dXJuKFNhfHwhYikmJihiPWZ1bmN0aW9uKGIpe3ZhciBkPUhhKCktYyxlPWkubWF4KDAsMWUzLzYwLWQpO3JldHVybiBhLnNldFRpbWVvdXQoZnVuY3Rpb24oKXtjPUhhKCksYigpfSxlKX0pLGJ9LFY9ZnVuY3Rpb24oKXt2YXIgYj1hLmNhbmNlbEFuaW1hdGlvbkZyYW1lfHxhW1IudG9Mb3dlckNhc2UoKStcIkNhbmNlbEFuaW1hdGlvbkZyYW1lXCJdO3JldHVybihTYXx8IWIpJiYoYj1mdW5jdGlvbihiKXtyZXR1cm4gYS5jbGVhclRpbWVvdXQoYil9KSxifSxXPXtiZWdpbjpmdW5jdGlvbigpe3JldHVybiAwfSxlbmQ6ZnVuY3Rpb24oKXtyZXR1cm4gMX0sbGluZWFyOmZ1bmN0aW9uKGEpe3JldHVybiBhfSxxdWFkcmF0aWM6ZnVuY3Rpb24oYSl7cmV0dXJuIGEqYX0sY3ViaWM6ZnVuY3Rpb24oYSl7cmV0dXJuIGEqYSphfSxzd2luZzpmdW5jdGlvbihhKXtyZXR1cm4taS5jb3MoYSppLlBJKS8yKy41fSxzcXJ0OmZ1bmN0aW9uKGEpe3JldHVybiBpLnNxcnQoYSl9LG91dEN1YmljOmZ1bmN0aW9uKGEpe3JldHVybiBpLnBvdyhhLTEsMykrMX0sYm91bmNlOmZ1bmN0aW9uKGEpe3ZhciBiO2lmKC41MDgzPj1hKWI9MztlbHNlIGlmKC44NDg5Pj1hKWI9OTtlbHNlIGlmKC45NjIwOD49YSliPTI3O2Vsc2V7aWYoISguOTk5ODE+PWEpKXJldHVybiAxO2I9OTF9cmV0dXJuIDEtaS5hYnMoMyppLmNvcyhhKmIqMS4wMjgpL2IpfX07ZC5wcm90b3R5cGUucmVmcmVzaD1mdW5jdGlvbihhKXt2YXIgZCxlLGY9ITE7Zm9yKGE9PT1jPyhmPSEwLGlhPVtdLFJhPTAsYT1iLmdldEVsZW1lbnRzQnlUYWdOYW1lKFwiKlwiKSk6YS5sZW5ndGg9PT1jJiYoYT1bYV0pLGQ9MCxlPWEubGVuZ3RoO2U+ZDtkKyspe3ZhciBnPWFbZF0saD1nLGk9W10saj1wYSxrPXRhLGw9ITE7aWYoZiYmRiBpbiBnJiZkZWxldGUgZ1tGXSxnLmF0dHJpYnV0ZXMpe2Zvcih2YXIgbT0wLG49Zy5hdHRyaWJ1dGVzLmxlbmd0aDtuPm07bSsrKXt2YXIgcD1nLmF0dHJpYnV0ZXNbbV07aWYoXCJkYXRhLWFuY2hvci10YXJnZXRcIiE9PXAubmFtZSlpZihcImRhdGEtc21vb3RoLXNjcm9sbGluZ1wiIT09cC5uYW1lKWlmKFwiZGF0YS1lZGdlLXN0cmF0ZWd5XCIhPT1wLm5hbWUpaWYoXCJkYXRhLWVtaXQtZXZlbnRzXCIhPT1wLm5hbWUpe3ZhciBxPXAubmFtZS5tYXRjaChJKTtpZihudWxsIT09cSl7dmFyIHI9e3Byb3BzOnAudmFsdWUsZWxlbWVudDpnLGV2ZW50VHlwZTpwLm5hbWUucmVwbGFjZShMLE0pfTtpLnB1c2gocik7dmFyIHM9cVsxXTtzJiYoci5jb25zdGFudD1zLnN1YnN0cigxKSk7dmFyIHQ9cVsyXTsvcCQvLnRlc3QodCk/KHIuaXNQZXJjZW50YWdlPSEwLHIub2Zmc2V0PSgwfHQuc2xpY2UoMCwtMSkpLzEwMCk6ci5vZmZzZXQ9MHx0O3ZhciB1PXFbM10sdj1xWzRdfHx1O3UmJnUhPT1CJiZ1IT09Qz8oci5tb2RlPVwicmVsYXRpdmVcIixyLmFuY2hvcnM9W3Usdl0pOihyLm1vZGU9XCJhYnNvbHV0ZVwiLHU9PT1DP3IuaXNFbmQ9ITA6ci5pc1BlcmNlbnRhZ2V8fChyLm9mZnNldD1yLm9mZnNldCpLYSkpfX1lbHNlIGw9ITA7ZWxzZSBrPXAudmFsdWU7ZWxzZSBqPVwib2ZmXCIhPT1wLnZhbHVlO2Vsc2UgaWYoaD1iLnF1ZXJ5U2VsZWN0b3IocC52YWx1ZSksbnVsbD09PWgpdGhyb3cnVW5hYmxlIHRvIGZpbmQgYW5jaG9yIHRhcmdldCBcIicrcC52YWx1ZSsnXCInfWlmKGkubGVuZ3RoKXt2YXIgdyx4LHk7IWYmJkYgaW4gZz8oeT1nW0ZdLHc9aWFbeV0uc3R5bGVBdHRyLHg9aWFbeV0uY2xhc3NBdHRyKTooeT1nW0ZdPVJhKyssdz1nLnN0eWxlLmNzc1RleHQseD1EYShnKSksaWFbeV09e2VsZW1lbnQ6ZyxzdHlsZUF0dHI6dyxjbGFzc0F0dHI6eCxhbmNob3JUYXJnZXQ6aCxrZXlGcmFtZXM6aSxzbW9vdGhTY3JvbGxpbmc6aixlZGdlU3RyYXRlZ3k6ayxlbWl0RXZlbnRzOmwsbGFzdEZyYW1lSW5kZXg6LTF9LEVhKGcsW29dLFtdKX19fWZvcihBYSgpLGQ9MCxlPWEubGVuZ3RoO2U+ZDtkKyspe3ZhciB6PWlhW2FbZF1bRl1dO3ohPT1jJiYoXyh6KSxiYSh6KSl9cmV0dXJuIGhhfSxkLnByb3RvdHlwZS5yZWxhdGl2ZVRvQWJzb2x1dGU9ZnVuY3Rpb24oYSxiLGMpe3ZhciBkPWUuY2xpZW50SGVpZ2h0LGY9YS5nZXRCb3VuZGluZ0NsaWVudFJlY3QoKSxnPWYudG9wLGg9Zi5ib3R0b20tZi50b3A7cmV0dXJuIGI9PT1FP2ctPWQ6Yj09PUQmJihnLT1kLzIpLGM9PT1FP2crPWg6Yz09PUQmJihnKz1oLzIpLGcrPWhhLmdldFNjcm9sbFRvcCgpLGcrLjV8MH0sZC5wcm90b3R5cGUuYW5pbWF0ZVRvPWZ1bmN0aW9uKGEsYil7Yj1ifHx7fTt2YXIgZD1IYSgpLGU9aGEuZ2V0U2Nyb2xsVG9wKCksZj1iLmR1cmF0aW9uPT09Yz94OmIuZHVyYXRpb247cmV0dXJuIG9hPXtzdGFydFRvcDplLHRvcERpZmY6YS1lLHRhcmdldFRvcDphLGR1cmF0aW9uOmYsc3RhcnRUaW1lOmQsZW5kVGltZTpkK2YsZWFzaW5nOldbYi5lYXNpbmd8fHddLGRvbmU6Yi5kb25lfSxvYS50b3BEaWZmfHwob2EuZG9uZSYmb2EuZG9uZS5jYWxsKGhhLCExKSxvYT1jKSxoYX0sZC5wcm90b3R5cGUuc3RvcEFuaW1hdGVUbz1mdW5jdGlvbigpe29hJiZvYS5kb25lJiZvYS5kb25lLmNhbGwoaGEsITApLG9hPWN9LGQucHJvdG90eXBlLmlzQW5pbWF0aW5nVG89ZnVuY3Rpb24oKXtyZXR1cm4hIW9hfSxkLnByb3RvdHlwZS5pc01vYmlsZT1mdW5jdGlvbigpe3JldHVybiBTYX0sZC5wcm90b3R5cGUuc2V0U2Nyb2xsVG9wPWZ1bmN0aW9uKGIsYyl7cmV0dXJuIHNhPWM9PT0hMCxTYT9UYT1pLm1pbihpLm1heChiLDApLEphKTphLnNjcm9sbFRvKDAsYiksaGF9LGQucHJvdG90eXBlLmdldFNjcm9sbFRvcD1mdW5jdGlvbigpe3JldHVybiBTYT9UYTphLnBhZ2VZT2Zmc2V0fHxlLnNjcm9sbFRvcHx8Zi5zY3JvbGxUb3B8fDB9LGQucHJvdG90eXBlLmdldE1heFNjcm9sbFRvcD1mdW5jdGlvbigpe3JldHVybiBKYX0sZC5wcm90b3R5cGUub249ZnVuY3Rpb24oYSxiKXtyZXR1cm4ga2FbYV09YixoYX0sZC5wcm90b3R5cGUub2ZmPWZ1bmN0aW9uKGEpe3JldHVybiBkZWxldGUga2FbYV0saGF9LGQucHJvdG90eXBlLmRlc3Ryb3k9ZnVuY3Rpb24oKXt2YXIgYT1WKCk7YSh2YSkseWEoKSxFYShlLFt0XSxbcyx1LHZdKTtmb3IodmFyIGI9MCxkPWlhLmxlbmd0aDtkPmI7YisrKWZhKGlhW2JdLmVsZW1lbnQpO2Uuc3R5bGUub3ZlcmZsb3c9Zi5zdHlsZS5vdmVyZmxvdz1cIlwiLGUuc3R5bGUuaGVpZ2h0PWYuc3R5bGUuaGVpZ2h0PVwiXCIsamEmJmcuc2V0U3R5bGUoamEsXCJ0cmFuc2Zvcm1cIixcIm5vbmVcIiksaGE9YyxqYT1jLGthPWMsbGE9YyxKYT0wLEthPTEsbWE9YyxuYT1jLExhPVwiZG93blwiLE1hPS0xLE9hPTAsUGE9MCxRYT0hMSxvYT1jLHBhPWMscWE9YyxyYT1jLHNhPWMsUmE9MCx0YT1jLFNhPSExLFRhPTAsdWE9Y307dmFyIFg9ZnVuY3Rpb24oKXt2YXIgZCxnLGgsaixvLHAscSxyLHMsdCx1LHY7d2EoZSxbayxsLG0sbl0uam9pbihcIiBcIiksZnVuY3Rpb24oYSl7dmFyIGU9YS5jaGFuZ2VkVG91Y2hlc1swXTtmb3Ioaj1hLnRhcmdldDszPT09ai5ub2RlVHlwZTspaj1qLnBhcmVudE5vZGU7c3dpdGNoKG89ZS5jbGllbnRZLHA9ZS5jbGllbnRYLHQ9YS50aW1lU3RhbXAsRy50ZXN0KGoudGFnTmFtZSl8fGEucHJldmVudERlZmF1bHQoKSxhLnR5cGUpe2Nhc2UgazpkJiZkLmJsdXIoKSxoYS5zdG9wQW5pbWF0ZVRvKCksZD1qLGc9cT1vLGg9cCxzPXQ7YnJlYWs7Y2FzZSBsOkcudGVzdChqLnRhZ05hbWUpJiZiLmFjdGl2ZUVsZW1lbnQhPT1qJiZhLnByZXZlbnREZWZhdWx0KCkscj1vLXEsdj10LXUsaGEuc2V0U2Nyb2xsVG9wKFRhLXIsITApLHE9byx1PXQ7YnJlYWs7ZGVmYXVsdDpjYXNlIG06Y2FzZSBuOnZhciBmPWctbyx3PWgtcCx4PXcqdytmKmY7aWYoNDk+eCl7aWYoIUcudGVzdChkLnRhZ05hbWUpKXtkLmZvY3VzKCk7dmFyIHk9Yi5jcmVhdGVFdmVudChcIk1vdXNlRXZlbnRzXCIpO3kuaW5pdE1vdXNlRXZlbnQoXCJjbGlja1wiLCEwLCEwLGEudmlldywxLGUuc2NyZWVuWCxlLnNjcmVlblksZS5jbGllbnRYLGUuY2xpZW50WSxhLmN0cmxLZXksYS5hbHRLZXksYS5zaGlmdEtleSxhLm1ldGFLZXksMCxudWxsKSxkLmRpc3BhdGNoRXZlbnQoeSl9cmV0dXJufWQ9Yzt2YXIgej1yL3Y7ej1pLm1heChpLm1pbih6LDMpLC0zKTt2YXIgQT1pLmFicyh6L25hKSxCPXoqQSsuNSpuYSpBKkEsQz1oYS5nZXRTY3JvbGxUb3AoKS1CLEQ9MDtDPkphPyhEPShKYS1DKS9CLEM9SmEpOjA+QyYmKEQ9LUMvQixDPTApLEEqPTEtRCxoYS5hbmltYXRlVG8oQysuNXwwLHtlYXNpbmc6XCJvdXRDdWJpY1wiLGR1cmF0aW9uOkF9KX19KSxhLnNjcm9sbFRvKDAsMCksZS5zdHlsZS5vdmVyZmxvdz1mLnN0eWxlLm92ZXJmbG93PVwiaGlkZGVuXCJ9LFk9ZnVuY3Rpb24oKXt2YXIgYSxiLGMsZCxmLGcsaCxqLGssbCxtLG49ZS5jbGllbnRIZWlnaHQsbz1CYSgpO2ZvcihqPTAsaz1pYS5sZW5ndGg7az5qO2orKylmb3IoYT1pYVtqXSxiPWEuZWxlbWVudCxjPWEuYW5jaG9yVGFyZ2V0LGQ9YS5rZXlGcmFtZXMsZj0wLGc9ZC5sZW5ndGg7Zz5mO2YrKyloPWRbZl0sbD1oLm9mZnNldCxtPW9baC5jb25zdGFudF18fDAsaC5mcmFtZT1sLGguaXNQZXJjZW50YWdlJiYobCo9bixoLmZyYW1lPWwpLFwicmVsYXRpdmVcIj09PWgubW9kZSYmKGZhKGIpLGguZnJhbWU9aGEucmVsYXRpdmVUb0Fic29sdXRlKGMsaC5hbmNob3JzWzBdLGguYW5jaG9yc1sxXSktbCxmYShiLCEwKSksaC5mcmFtZSs9bSxsYSYmIWguaXNFbmQmJmguZnJhbWU+SmEmJihKYT1oLmZyYW1lKTtmb3IoSmE9aS5tYXgoSmEsQ2EoKSksaj0wLGs9aWEubGVuZ3RoO2s+ajtqKyspe2ZvcihhPWlhW2pdLGQ9YS5rZXlGcmFtZXMsZj0wLGc9ZC5sZW5ndGg7Zz5mO2YrKyloPWRbZl0sbT1vW2guY29uc3RhbnRdfHwwLGguaXNFbmQmJihoLmZyYW1lPUphLWgub2Zmc2V0K20pO2Eua2V5RnJhbWVzLnNvcnQoSWEpfX0sWj1mdW5jdGlvbihhLGIpe2Zvcih2YXIgYz0wLGQ9aWEubGVuZ3RoO2Q+YztjKyspe3ZhciBlLGYsaT1pYVtjXSxqPWkuZWxlbWVudCxrPWkuc21vb3RoU2Nyb2xsaW5nP2E6YixsPWkua2V5RnJhbWVzLG09bC5sZW5ndGgsbj1sWzBdLHM9bFtsLmxlbmd0aC0xXSx0PWs8bi5mcmFtZSx1PWs+cy5mcmFtZSx2PXQ/bjpzLHc9aS5lbWl0RXZlbnRzLHg9aS5sYXN0RnJhbWVJbmRleDtpZih0fHx1KXtpZih0JiYtMT09PWkuZWRnZXx8dSYmMT09PWkuZWRnZSljb250aW51ZTtzd2l0Y2godD8oRWEoaixbcF0sW3IscV0pLHcmJng+LTEmJih6YShqLG4uZXZlbnRUeXBlLExhKSxpLmxhc3RGcmFtZUluZGV4PS0xKSk6KEVhKGosW3JdLFtwLHFdKSx3JiZtPngmJih6YShqLHMuZXZlbnRUeXBlLExhKSxpLmxhc3RGcmFtZUluZGV4PW0pKSxpLmVkZ2U9dD8tMToxLGkuZWRnZVN0cmF0ZWd5KXtjYXNlXCJyZXNldFwiOmZhKGopO2NvbnRpbnVlO2Nhc2VcImVhc2VcIjprPXYuZnJhbWU7YnJlYWs7ZGVmYXVsdDpjYXNlXCJzZXRcIjp2YXIgeT12LnByb3BzO2ZvcihlIGluIHkpaC5jYWxsKHksZSkmJihmPWVhKHlbZV0udmFsdWUpLDA9PT1lLmluZGV4T2YoXCJAXCIpP2ouc2V0QXR0cmlidXRlKGUuc3Vic3RyKDEpLGYpOmcuc2V0U3R5bGUoaixlLGYpKTtjb250aW51ZX19ZWxzZSAwIT09aS5lZGdlJiYoRWEoaixbbyxxXSxbcCxyXSksaS5lZGdlPTApO2Zvcih2YXIgej0wO20tMT56O3orKylpZihrPj1sW3pdLmZyYW1lJiZrPD1sW3orMV0uZnJhbWUpe3ZhciBBPWxbel0sQj1sW3orMV07Zm9yKGUgaW4gQS5wcm9wcylpZihoLmNhbGwoQS5wcm9wcyxlKSl7dmFyIEM9KGstQS5mcmFtZSkvKEIuZnJhbWUtQS5mcmFtZSk7Qz1BLnByb3BzW2VdLmVhc2luZyhDKSxmPWRhKEEucHJvcHNbZV0udmFsdWUsQi5wcm9wc1tlXS52YWx1ZSxDKSxmPWVhKGYpLDA9PT1lLmluZGV4T2YoXCJAXCIpP2ouc2V0QXR0cmlidXRlKGUuc3Vic3RyKDEpLGYpOmcuc2V0U3R5bGUoaixlLGYpfXcmJnghPT16JiYoXCJkb3duXCI9PT1MYT96YShqLEEuZXZlbnRUeXBlLExhKTp6YShqLEIuZXZlbnRUeXBlLExhKSxpLmxhc3RGcmFtZUluZGV4PXopO2JyZWFrfX19LCQ9ZnVuY3Rpb24oKXtRYSYmKFFhPSExLEFhKCkpO3ZhciBhLGIsZD1oYS5nZXRTY3JvbGxUb3AoKSxlPUhhKCk7aWYob2EpZT49b2EuZW5kVGltZT8oZD1vYS50YXJnZXRUb3AsYT1vYS5kb25lLG9hPWMpOihiPW9hLmVhc2luZygoZS1vYS5zdGFydFRpbWUpL29hLmR1cmF0aW9uKSxkPW9hLnN0YXJ0VG9wK2Iqb2EudG9wRGlmZnwwKSxoYS5zZXRTY3JvbGxUb3AoZCwhMCk7ZWxzZSBpZighc2Epe3ZhciBmPXJhLnRhcmdldFRvcC1kO2YmJihyYT17c3RhcnRUb3A6TWEsdG9wRGlmZjpkLU1hLHRhcmdldFRvcDpkLHN0YXJ0VGltZTpOYSxlbmRUaW1lOk5hK3FhfSksZTw9cmEuZW5kVGltZSYmKGI9Vy5zcXJ0KChlLXJhLnN0YXJ0VGltZSkvcWEpLGQ9cmEuc3RhcnRUb3ArYipyYS50b3BEaWZmfDApfWlmKHNhfHxNYSE9PWQpe0xhPWQ+TWE/XCJkb3duXCI6TWE+ZD9cInVwXCI6TGEsc2E9ITE7dmFyIGg9e2N1clRvcDpkLGxhc3RUb3A6TWEsbWF4VG9wOkphLGRpcmVjdGlvbjpMYX0saT1rYS5iZWZvcmVyZW5kZXImJmthLmJlZm9yZXJlbmRlci5jYWxsKGhhLGgpO2khPT0hMSYmKFooZCxoYS5nZXRTY3JvbGxUb3AoKSksU2EmJmphJiZnLnNldFN0eWxlKGphLFwidHJhbnNmb3JtXCIsXCJ0cmFuc2xhdGUoMCwgXCIrLVRhK1wicHgpIFwiK3VhKSxNYT1kLGthLnJlbmRlciYma2EucmVuZGVyLmNhbGwoaGEsaCkpLGEmJmEuY2FsbChoYSwhMSl9TmE9ZX0sXz1mdW5jdGlvbihhKXtmb3IodmFyIGI9MCxjPWEua2V5RnJhbWVzLmxlbmd0aDtjPmI7YisrKXtmb3IodmFyIGQsZSxmLGcsaD1hLmtleUZyYW1lc1tiXSxpPXt9O251bGwhPT0oZz1KLmV4ZWMoaC5wcm9wcykpOylmPWdbMV0sZT1nWzJdLGQ9Zi5tYXRjaChLKSxudWxsIT09ZD8oZj1kWzFdLGQ9ZFsyXSk6ZD13LGU9ZS5pbmRleE9mKFwiIVwiKT9hYShlKTpbZS5zbGljZSgxKV0saVtmXT17dmFsdWU6ZSxlYXNpbmc6V1tkXX07aC5wcm9wcz1pfX0sYWE9ZnVuY3Rpb24oYSl7dmFyIGI9W107cmV0dXJuIFAubGFzdEluZGV4PTAsYT1hLnJlcGxhY2UoUCxmdW5jdGlvbihhKXtyZXR1cm4gYS5yZXBsYWNlKE4sZnVuY3Rpb24oYSl7cmV0dXJuIGEvMjU1KjEwMCtcIiVcIn0pfSksUyYmKFEubGFzdEluZGV4PTAsYT1hLnJlcGxhY2UoUSxmdW5jdGlvbihhKXtyZXR1cm4gUythfSkpLGE9YS5yZXBsYWNlKE4sZnVuY3Rpb24oYSl7cmV0dXJuIGIucHVzaCgrYSksXCJ7P31cIn0pLGIudW5zaGlmdChhKSxifSxiYT1mdW5jdGlvbihhKXt2YXIgYixjLGQ9e307Zm9yKGI9MCxjPWEua2V5RnJhbWVzLmxlbmd0aDtjPmI7YisrKWNhKGEua2V5RnJhbWVzW2JdLGQpO2ZvcihkPXt9LGI9YS5rZXlGcmFtZXMubGVuZ3RoLTE7Yj49MDtiLS0pY2EoYS5rZXlGcmFtZXNbYl0sZCl9LGNhPWZ1bmN0aW9uKGEsYil7dmFyIGM7Zm9yKGMgaW4gYiloLmNhbGwoYS5wcm9wcyxjKXx8KGEucHJvcHNbY109YltjXSk7Zm9yKGMgaW4gYS5wcm9wcyliW2NdPWEucHJvcHNbY119LGRhPWZ1bmN0aW9uKGEsYixjKXt2YXIgZCxlPWEubGVuZ3RoO2lmKGUhPT1iLmxlbmd0aCl0aHJvd1wiQ2FuJ3QgaW50ZXJwb2xhdGUgYmV0d2VlbiBcXFwiXCIrYVswXSsnXCIgYW5kIFwiJytiWzBdKydcIic7dmFyIGY9W2FbMF1dO2ZvcihkPTE7ZT5kO2QrKylmW2RdPWFbZF0rKGJbZF0tYVtkXSkqYztyZXR1cm4gZn0sZWE9ZnVuY3Rpb24oYSl7dmFyIGI9MTtyZXR1cm4gTy5sYXN0SW5kZXg9MCxhWzBdLnJlcGxhY2UoTyxmdW5jdGlvbigpe3JldHVybiBhW2IrK119KX0sZmE9ZnVuY3Rpb24oYSxiKXthPVtdLmNvbmNhdChhKTtmb3IodmFyIGMsZCxlPTAsZj1hLmxlbmd0aDtmPmU7ZSsrKWQ9YVtlXSxjPWlhW2RbRl1dLGMmJihiPyhkLnN0eWxlLmNzc1RleHQ9Yy5kaXJ0eVN0eWxlQXR0cixFYShkLGMuZGlydHlDbGFzc0F0dHIpKTooYy5kaXJ0eVN0eWxlQXR0cj1kLnN0eWxlLmNzc1RleHQsYy5kaXJ0eUNsYXNzQXR0cj1EYShkKSxkLnN0eWxlLmNzc1RleHQ9Yy5zdHlsZUF0dHIsRWEoZCxjLmNsYXNzQXR0cikpKX0sZ2E9ZnVuY3Rpb24oKXt1YT1cInRyYW5zbGF0ZVooMClcIixnLnNldFN0eWxlKGphLFwidHJhbnNmb3JtXCIsdWEpO3ZhciBhPWooamEpLGI9YS5nZXRQcm9wZXJ0eVZhbHVlKFwidHJhbnNmb3JtXCIpLGM9YS5nZXRQcm9wZXJ0eVZhbHVlKFMrXCJ0cmFuc2Zvcm1cIiksZD1iJiZcIm5vbmVcIiE9PWJ8fGMmJlwibm9uZVwiIT09YztkfHwodWE9XCJcIil9O2cuc2V0U3R5bGU9ZnVuY3Rpb24oYSxiLGMpe3ZhciBkPWEuc3R5bGU7aWYoYj1iLnJlcGxhY2UoTCxNKS5yZXBsYWNlKFwiLVwiLFwiXCIpLFwiekluZGV4XCI9PT1iKWlzTmFOKGMpP2RbYl09YzpkW2JdPVwiXCIrKDB8Yyk7ZWxzZSBpZihcImZsb2F0XCI9PT1iKWQuc3R5bGVGbG9hdD1kLmNzc0Zsb2F0PWM7ZWxzZSB0cnl7UiYmKGRbUitiLnNsaWNlKDAsMSkudG9VcHBlckNhc2UoKStiLnNsaWNlKDEpXT1jKSxkW2JdPWN9Y2F0Y2goZSl7fX07dmFyIGhhLGlhLGphLGthLGxhLG1hLG5hLG9hLHBhLHFhLHJhLHNhLHRhLHVhLHZhLHdhPWcuYWRkRXZlbnQ9ZnVuY3Rpb24oYixjLGQpe3ZhciBlPWZ1bmN0aW9uKGIpe3JldHVybiBiPWJ8fGEuZXZlbnQsYi50YXJnZXR8fChiLnRhcmdldD1iLnNyY0VsZW1lbnQpLGIucHJldmVudERlZmF1bHR8fChiLnByZXZlbnREZWZhdWx0PWZ1bmN0aW9uKCl7Yi5yZXR1cm5WYWx1ZT0hMSxiLmRlZmF1bHRQcmV2ZW50ZWQ9ITB9KSxkLmNhbGwodGhpcyxiKX07Yz1jLnNwbGl0KFwiIFwiKTtmb3IodmFyIGYsZz0wLGg9Yy5sZW5ndGg7aD5nO2crKylmPWNbZ10sYi5hZGRFdmVudExpc3RlbmVyP2IuYWRkRXZlbnRMaXN0ZW5lcihmLGQsITEpOmIuYXR0YWNoRXZlbnQoXCJvblwiK2YsZSksVWEucHVzaCh7ZWxlbWVudDpiLG5hbWU6ZixsaXN0ZW5lcjpkfSl9LHhhPWcucmVtb3ZlRXZlbnQ9ZnVuY3Rpb24oYSxiLGMpe2I9Yi5zcGxpdChcIiBcIik7Zm9yKHZhciBkPTAsZT1iLmxlbmd0aDtlPmQ7ZCsrKWEucmVtb3ZlRXZlbnRMaXN0ZW5lcj9hLnJlbW92ZUV2ZW50TGlzdGVuZXIoYltkXSxjLCExKTphLmRldGFjaEV2ZW50KFwib25cIitiW2RdLGMpfSx5YT1mdW5jdGlvbigpe2Zvcih2YXIgYSxiPTAsYz1VYS5sZW5ndGg7Yz5iO2IrKylhPVVhW2JdLHhhKGEuZWxlbWVudCxhLm5hbWUsYS5saXN0ZW5lcik7VWE9W119LHphPWZ1bmN0aW9uKGEsYixjKXtrYS5rZXlmcmFtZSYma2Eua2V5ZnJhbWUuY2FsbChoYSxhLGIsYyl9LEFhPWZ1bmN0aW9uKCl7dmFyIGE9aGEuZ2V0U2Nyb2xsVG9wKCk7SmE9MCxsYSYmIVNhJiYoZi5zdHlsZS5oZWlnaHQ9XCJcIiksWSgpLGxhJiYhU2EmJihmLnN0eWxlLmhlaWdodD1KYStlLmNsaWVudEhlaWdodCtcInB4XCIpLFNhP2hhLnNldFNjcm9sbFRvcChpLm1pbihoYS5nZXRTY3JvbGxUb3AoKSxKYSkpOmhhLnNldFNjcm9sbFRvcChhLCEwKSxzYT0hMH0sQmE9ZnVuY3Rpb24oKXt2YXIgYSxiLGM9ZS5jbGllbnRIZWlnaHQsZD17fTtmb3IoYSBpbiBtYSliPW1hW2FdLFwiZnVuY3Rpb25cIj09dHlwZW9mIGI/Yj1iLmNhbGwoaGEpOi9wJC8udGVzdChiKSYmKGI9Yi5zbGljZSgwLC0xKS8xMDAqYyksZFthXT1iO3JldHVybiBkfSxDYT1mdW5jdGlvbigpe3ZhciBhLGI9MDtyZXR1cm4gamEmJihiPWkubWF4KGphLm9mZnNldEhlaWdodCxqYS5zY3JvbGxIZWlnaHQpKSxhPWkubWF4KGIsZi5zY3JvbGxIZWlnaHQsZi5vZmZzZXRIZWlnaHQsZS5zY3JvbGxIZWlnaHQsZS5vZmZzZXRIZWlnaHQsZS5jbGllbnRIZWlnaHQpLGEtZS5jbGllbnRIZWlnaHR9LERhPWZ1bmN0aW9uKGIpe3ZhciBjPVwiY2xhc3NOYW1lXCI7cmV0dXJuIGEuU1ZHRWxlbWVudCYmYiBpbnN0YW5jZW9mIGEuU1ZHRWxlbWVudCYmKGI9YltjXSxjPVwiYmFzZVZhbFwiKSxiW2NdfSxFYT1mdW5jdGlvbihiLGQsZSl7dmFyIGY9XCJjbGFzc05hbWVcIjtpZihhLlNWR0VsZW1lbnQmJmIgaW5zdGFuY2VvZiBhLlNWR0VsZW1lbnQmJihiPWJbZl0sZj1cImJhc2VWYWxcIiksZT09PWMpcmV0dXJuIHZvaWQoYltmXT1kKTtmb3IodmFyIGc9YltmXSxoPTAsaT1lLmxlbmd0aDtpPmg7aCsrKWc9R2EoZykucmVwbGFjZShHYShlW2hdKSxcIiBcIik7Zz1GYShnKTtmb3IodmFyIGo9MCxrPWQubGVuZ3RoO2s+ajtqKyspLTE9PT1HYShnKS5pbmRleE9mKEdhKGRbal0pKSYmKGcrPVwiIFwiK2Rbal0pO2JbZl09RmEoZyl9LEZhPWZ1bmN0aW9uKGEpe3JldHVybiBhLnJlcGxhY2UoSCxcIlwiKX0sR2E9ZnVuY3Rpb24oYSl7cmV0dXJuXCIgXCIrYStcIiBcIn0sSGE9RGF0ZS5ub3d8fGZ1bmN0aW9uKCl7cmV0dXJuK25ldyBEYXRlfSxJYT1mdW5jdGlvbihhLGIpe3JldHVybiBhLmZyYW1lLWIuZnJhbWV9LEphPTAsS2E9MSxMYT1cImRvd25cIixNYT0tMSxOYT1IYSgpLE9hPTAsUGE9MCxRYT0hMSxSYT0wLFNhPSExLFRhPTAsVWE9W107XCJmdW5jdGlvblwiPT10eXBlb2YgZGVmaW5lJiZkZWZpbmUuYW1kP2RlZmluZShbXSxmdW5jdGlvbigpe3JldHVybiBnfSk6XCJ1bmRlZmluZWRcIiE9dHlwZW9mIG1vZHVsZSYmbW9kdWxlLmV4cG9ydHM/bW9kdWxlLmV4cG9ydHM9ZzphLnNrcm9sbHI9Z30od2luZG93LGRvY3VtZW50KTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoJy4vbGliL2F4aW9zJyk7IiwiJ3VzZSBzdHJpY3QnO1xuXG52YXIgdXRpbHMgPSByZXF1aXJlKCcuLy4uL3V0aWxzJyk7XG52YXIgYnVpbGRVUkwgPSByZXF1aXJlKCcuLy4uL2hlbHBlcnMvYnVpbGRVUkwnKTtcbnZhciBwYXJzZUhlYWRlcnMgPSByZXF1aXJlKCcuLy4uL2hlbHBlcnMvcGFyc2VIZWFkZXJzJyk7XG52YXIgdHJhbnNmb3JtRGF0YSA9IHJlcXVpcmUoJy4vLi4vaGVscGVycy90cmFuc2Zvcm1EYXRhJyk7XG52YXIgaXNVUkxTYW1lT3JpZ2luID0gcmVxdWlyZSgnLi8uLi9oZWxwZXJzL2lzVVJMU2FtZU9yaWdpbicpO1xudmFyIGJ0b2EgPSAodHlwZW9mIHdpbmRvdyAhPT0gJ3VuZGVmaW5lZCcgJiYgd2luZG93LmJ0b2EpIHx8IHJlcXVpcmUoJy4vLi4vaGVscGVycy9idG9hJyk7XG52YXIgc2V0dGxlID0gcmVxdWlyZSgnLi4vaGVscGVycy9zZXR0bGUnKTtcblxubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiB4aHJBZGFwdGVyKHJlc29sdmUsIHJlamVjdCwgY29uZmlnKSB7XG4gIHZhciByZXF1ZXN0RGF0YSA9IGNvbmZpZy5kYXRhO1xuICB2YXIgcmVxdWVzdEhlYWRlcnMgPSBjb25maWcuaGVhZGVycztcblxuICBpZiAodXRpbHMuaXNGb3JtRGF0YShyZXF1ZXN0RGF0YSkpIHtcbiAgICBkZWxldGUgcmVxdWVzdEhlYWRlcnNbJ0NvbnRlbnQtVHlwZSddOyAvLyBMZXQgdGhlIGJyb3dzZXIgc2V0IGl0XG4gIH1cblxuICB2YXIgcmVxdWVzdCA9IG5ldyBYTUxIdHRwUmVxdWVzdCgpO1xuICB2YXIgbG9hZEV2ZW50ID0gJ29ucmVhZHlzdGF0ZWNoYW5nZSc7XG4gIHZhciB4RG9tYWluID0gZmFsc2U7XG5cbiAgLy8gRm9yIElFIDgvOSBDT1JTIHN1cHBvcnRcbiAgLy8gT25seSBzdXBwb3J0cyBQT1NUIGFuZCBHRVQgY2FsbHMgYW5kIGRvZXNuJ3QgcmV0dXJucyB0aGUgcmVzcG9uc2UgaGVhZGVycy5cbiAgLy8gRE9OJ1QgZG8gdGhpcyBmb3IgdGVzdGluZyBiL2MgWE1MSHR0cFJlcXVlc3QgaXMgbW9ja2VkLCBub3QgWERvbWFpblJlcXVlc3QuXG4gIGlmIChwcm9jZXNzLmVudi5OT0RFX0VOViAhPT0gJ3Rlc3QnICYmIHR5cGVvZiB3aW5kb3cgIT09ICd1bmRlZmluZWQnICYmIHdpbmRvdy5YRG9tYWluUmVxdWVzdCAmJiAhKCd3aXRoQ3JlZGVudGlhbHMnIGluIHJlcXVlc3QpICYmICFpc1VSTFNhbWVPcmlnaW4oY29uZmlnLnVybCkpIHtcbiAgICByZXF1ZXN0ID0gbmV3IHdpbmRvdy5YRG9tYWluUmVxdWVzdCgpO1xuICAgIGxvYWRFdmVudCA9ICdvbmxvYWQnO1xuICAgIHhEb21haW4gPSB0cnVlO1xuICAgIHJlcXVlc3Qub25wcm9ncmVzcyA9IGZ1bmN0aW9uIGhhbmRsZVByb2dyZXNzKCkge307XG4gICAgcmVxdWVzdC5vbnRpbWVvdXQgPSBmdW5jdGlvbiBoYW5kbGVUaW1lb3V0KCkge307XG4gIH1cblxuICAvLyBIVFRQIGJhc2ljIGF1dGhlbnRpY2F0aW9uXG4gIGlmIChjb25maWcuYXV0aCkge1xuICAgIHZhciB1c2VybmFtZSA9IGNvbmZpZy5hdXRoLnVzZXJuYW1lIHx8ICcnO1xuICAgIHZhciBwYXNzd29yZCA9IGNvbmZpZy5hdXRoLnBhc3N3b3JkIHx8ICcnO1xuICAgIHJlcXVlc3RIZWFkZXJzLkF1dGhvcml6YXRpb24gPSAnQmFzaWMgJyArIGJ0b2EodXNlcm5hbWUgKyAnOicgKyBwYXNzd29yZCk7XG4gIH1cblxuICByZXF1ZXN0Lm9wZW4oY29uZmlnLm1ldGhvZC50b1VwcGVyQ2FzZSgpLCBidWlsZFVSTChjb25maWcudXJsLCBjb25maWcucGFyYW1zLCBjb25maWcucGFyYW1zU2VyaWFsaXplciksIHRydWUpO1xuXG4gIC8vIFNldCB0aGUgcmVxdWVzdCB0aW1lb3V0IGluIE1TXG4gIHJlcXVlc3QudGltZW91dCA9IGNvbmZpZy50aW1lb3V0O1xuXG4gIC8vIExpc3RlbiBmb3IgcmVhZHkgc3RhdGVcbiAgcmVxdWVzdFtsb2FkRXZlbnRdID0gZnVuY3Rpb24gaGFuZGxlTG9hZCgpIHtcbiAgICBpZiAoIXJlcXVlc3QgfHwgKHJlcXVlc3QucmVhZHlTdGF0ZSAhPT0gNCAmJiAheERvbWFpbikpIHtcbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICAvLyBUaGUgcmVxdWVzdCBlcnJvcmVkIG91dCBhbmQgd2UgZGlkbid0IGdldCBhIHJlc3BvbnNlLCB0aGlzIHdpbGwgYmVcbiAgICAvLyBoYW5kbGVkIGJ5IG9uZXJyb3IgaW5zdGVhZFxuICAgIGlmIChyZXF1ZXN0LnN0YXR1cyA9PT0gMCkge1xuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIC8vIFByZXBhcmUgdGhlIHJlc3BvbnNlXG4gICAgdmFyIHJlc3BvbnNlSGVhZGVycyA9ICdnZXRBbGxSZXNwb25zZUhlYWRlcnMnIGluIHJlcXVlc3QgPyBwYXJzZUhlYWRlcnMocmVxdWVzdC5nZXRBbGxSZXNwb25zZUhlYWRlcnMoKSkgOiBudWxsO1xuICAgIHZhciByZXNwb25zZURhdGEgPSAhY29uZmlnLnJlc3BvbnNlVHlwZSB8fCBjb25maWcucmVzcG9uc2VUeXBlID09PSAndGV4dCcgPyByZXF1ZXN0LnJlc3BvbnNlVGV4dCA6IHJlcXVlc3QucmVzcG9uc2U7XG4gICAgdmFyIHJlc3BvbnNlID0ge1xuICAgICAgZGF0YTogdHJhbnNmb3JtRGF0YShcbiAgICAgICAgcmVzcG9uc2VEYXRhLFxuICAgICAgICByZXNwb25zZUhlYWRlcnMsXG4gICAgICAgIGNvbmZpZy50cmFuc2Zvcm1SZXNwb25zZVxuICAgICAgKSxcbiAgICAgIC8vIElFIHNlbmRzIDEyMjMgaW5zdGVhZCBvZiAyMDQgKGh0dHBzOi8vZ2l0aHViLmNvbS9temFicmlza2llL2F4aW9zL2lzc3Vlcy8yMDEpXG4gICAgICBzdGF0dXM6IHJlcXVlc3Quc3RhdHVzID09PSAxMjIzID8gMjA0IDogcmVxdWVzdC5zdGF0dXMsXG4gICAgICBzdGF0dXNUZXh0OiByZXF1ZXN0LnN0YXR1cyA9PT0gMTIyMyA/ICdObyBDb250ZW50JyA6IHJlcXVlc3Quc3RhdHVzVGV4dCxcbiAgICAgIGhlYWRlcnM6IHJlc3BvbnNlSGVhZGVycyxcbiAgICAgIGNvbmZpZzogY29uZmlnLFxuICAgICAgcmVxdWVzdDogcmVxdWVzdFxuICAgIH07XG5cbiAgICBzZXR0bGUocmVzb2x2ZSwgcmVqZWN0LCByZXNwb25zZSk7XG5cbiAgICAvLyBDbGVhbiB1cCByZXF1ZXN0XG4gICAgcmVxdWVzdCA9IG51bGw7XG4gIH07XG5cbiAgLy8gSGFuZGxlIGxvdyBsZXZlbCBuZXR3b3JrIGVycm9yc1xuICByZXF1ZXN0Lm9uZXJyb3IgPSBmdW5jdGlvbiBoYW5kbGVFcnJvcigpIHtcbiAgICAvLyBSZWFsIGVycm9ycyBhcmUgaGlkZGVuIGZyb20gdXMgYnkgdGhlIGJyb3dzZXJcbiAgICAvLyBvbmVycm9yIHNob3VsZCBvbmx5IGZpcmUgaWYgaXQncyBhIG5ldHdvcmsgZXJyb3JcbiAgICByZWplY3QobmV3IEVycm9yKCdOZXR3b3JrIEVycm9yJykpO1xuXG4gICAgLy8gQ2xlYW4gdXAgcmVxdWVzdFxuICAgIHJlcXVlc3QgPSBudWxsO1xuICB9O1xuXG4gIC8vIEhhbmRsZSB0aW1lb3V0XG4gIHJlcXVlc3Qub250aW1lb3V0ID0gZnVuY3Rpb24gaGFuZGxlVGltZW91dCgpIHtcbiAgICB2YXIgZXJyID0gbmV3IEVycm9yKCd0aW1lb3V0IG9mICcgKyBjb25maWcudGltZW91dCArICdtcyBleGNlZWRlZCcpO1xuICAgIGVyci50aW1lb3V0ID0gY29uZmlnLnRpbWVvdXQ7XG4gICAgZXJyLmNvZGUgPSAnRUNPTk5BQk9SVEVEJztcbiAgICByZWplY3QoZXJyKTtcblxuICAgIC8vIENsZWFuIHVwIHJlcXVlc3RcbiAgICByZXF1ZXN0ID0gbnVsbDtcbiAgfTtcblxuICAvLyBBZGQgeHNyZiBoZWFkZXJcbiAgLy8gVGhpcyBpcyBvbmx5IGRvbmUgaWYgcnVubmluZyBpbiBhIHN0YW5kYXJkIGJyb3dzZXIgZW52aXJvbm1lbnQuXG4gIC8vIFNwZWNpZmljYWxseSBub3QgaWYgd2UncmUgaW4gYSB3ZWIgd29ya2VyLCBvciByZWFjdC1uYXRpdmUuXG4gIGlmICh1dGlscy5pc1N0YW5kYXJkQnJvd3NlckVudigpKSB7XG4gICAgdmFyIGNvb2tpZXMgPSByZXF1aXJlKCcuLy4uL2hlbHBlcnMvY29va2llcycpO1xuXG4gICAgLy8gQWRkIHhzcmYgaGVhZGVyXG4gICAgdmFyIHhzcmZWYWx1ZSA9IGNvbmZpZy53aXRoQ3JlZGVudGlhbHMgfHwgaXNVUkxTYW1lT3JpZ2luKGNvbmZpZy51cmwpID9cbiAgICAgICAgY29va2llcy5yZWFkKGNvbmZpZy54c3JmQ29va2llTmFtZSkgOlxuICAgICAgICB1bmRlZmluZWQ7XG5cbiAgICBpZiAoeHNyZlZhbHVlKSB7XG4gICAgICByZXF1ZXN0SGVhZGVyc1tjb25maWcueHNyZkhlYWRlck5hbWVdID0geHNyZlZhbHVlO1xuICAgIH1cbiAgfVxuXG4gIC8vIEFkZCBoZWFkZXJzIHRvIHRoZSByZXF1ZXN0XG4gIGlmICgnc2V0UmVxdWVzdEhlYWRlcicgaW4gcmVxdWVzdCkge1xuICAgIHV0aWxzLmZvckVhY2gocmVxdWVzdEhlYWRlcnMsIGZ1bmN0aW9uIHNldFJlcXVlc3RIZWFkZXIodmFsLCBrZXkpIHtcbiAgICAgIGlmICh0eXBlb2YgcmVxdWVzdERhdGEgPT09ICd1bmRlZmluZWQnICYmIGtleS50b0xvd2VyQ2FzZSgpID09PSAnY29udGVudC10eXBlJykge1xuICAgICAgICAvLyBSZW1vdmUgQ29udGVudC1UeXBlIGlmIGRhdGEgaXMgdW5kZWZpbmVkXG4gICAgICAgIGRlbGV0ZSByZXF1ZXN0SGVhZGVyc1trZXldO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgLy8gT3RoZXJ3aXNlIGFkZCBoZWFkZXIgdG8gdGhlIHJlcXVlc3RcbiAgICAgICAgcmVxdWVzdC5zZXRSZXF1ZXN0SGVhZGVyKGtleSwgdmFsKTtcbiAgICAgIH1cbiAgICB9KTtcbiAgfVxuXG4gIC8vIEFkZCB3aXRoQ3JlZGVudGlhbHMgdG8gcmVxdWVzdCBpZiBuZWVkZWRcbiAgaWYgKGNvbmZpZy53aXRoQ3JlZGVudGlhbHMpIHtcbiAgICByZXF1ZXN0LndpdGhDcmVkZW50aWFscyA9IHRydWU7XG4gIH1cblxuICAvLyBBZGQgcmVzcG9uc2VUeXBlIHRvIHJlcXVlc3QgaWYgbmVlZGVkXG4gIGlmIChjb25maWcucmVzcG9uc2VUeXBlKSB7XG4gICAgdHJ5IHtcbiAgICAgIHJlcXVlc3QucmVzcG9uc2VUeXBlID0gY29uZmlnLnJlc3BvbnNlVHlwZTtcbiAgICB9IGNhdGNoIChlKSB7XG4gICAgICBpZiAocmVxdWVzdC5yZXNwb25zZVR5cGUgIT09ICdqc29uJykge1xuICAgICAgICB0aHJvdyBlO1xuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIC8vIEhhbmRsZSBwcm9ncmVzcyBpZiBuZWVkZWRcbiAgaWYgKGNvbmZpZy5wcm9ncmVzcykge1xuICAgIGlmIChjb25maWcubWV0aG9kID09PSAncG9zdCcgfHwgY29uZmlnLm1ldGhvZCA9PT0gJ3B1dCcpIHtcbiAgICAgIHJlcXVlc3QudXBsb2FkLmFkZEV2ZW50TGlzdGVuZXIoJ3Byb2dyZXNzJywgY29uZmlnLnByb2dyZXNzKTtcbiAgICB9IGVsc2UgaWYgKGNvbmZpZy5tZXRob2QgPT09ICdnZXQnKSB7XG4gICAgICByZXF1ZXN0LmFkZEV2ZW50TGlzdGVuZXIoJ3Byb2dyZXNzJywgY29uZmlnLnByb2dyZXNzKTtcbiAgICB9XG4gIH1cblxuICBpZiAocmVxdWVzdERhdGEgPT09IHVuZGVmaW5lZCkge1xuICAgIHJlcXVlc3REYXRhID0gbnVsbDtcbiAgfVxuXG4gIC8vIFNlbmQgdGhlIHJlcXVlc3RcbiAgcmVxdWVzdC5zZW5kKHJlcXVlc3REYXRhKTtcbn07XG4iLCIndXNlIHN0cmljdCc7XG5cbnZhciBkZWZhdWx0cyA9IHJlcXVpcmUoJy4vZGVmYXVsdHMnKTtcbnZhciB1dGlscyA9IHJlcXVpcmUoJy4vdXRpbHMnKTtcbnZhciBkaXNwYXRjaFJlcXVlc3QgPSByZXF1aXJlKCcuL2NvcmUvZGlzcGF0Y2hSZXF1ZXN0Jyk7XG52YXIgSW50ZXJjZXB0b3JNYW5hZ2VyID0gcmVxdWlyZSgnLi9jb3JlL0ludGVyY2VwdG9yTWFuYWdlcicpO1xudmFyIGlzQWJzb2x1dGVVUkwgPSByZXF1aXJlKCcuL2hlbHBlcnMvaXNBYnNvbHV0ZVVSTCcpO1xudmFyIGNvbWJpbmVVUkxzID0gcmVxdWlyZSgnLi9oZWxwZXJzL2NvbWJpbmVVUkxzJyk7XG52YXIgYmluZCA9IHJlcXVpcmUoJy4vaGVscGVycy9iaW5kJyk7XG52YXIgdHJhbnNmb3JtRGF0YSA9IHJlcXVpcmUoJy4vaGVscGVycy90cmFuc2Zvcm1EYXRhJyk7XG5cbmZ1bmN0aW9uIEF4aW9zKGRlZmF1bHRDb25maWcpIHtcbiAgdGhpcy5kZWZhdWx0cyA9IHV0aWxzLm1lcmdlKHt9LCBkZWZhdWx0Q29uZmlnKTtcbiAgdGhpcy5pbnRlcmNlcHRvcnMgPSB7XG4gICAgcmVxdWVzdDogbmV3IEludGVyY2VwdG9yTWFuYWdlcigpLFxuICAgIHJlc3BvbnNlOiBuZXcgSW50ZXJjZXB0b3JNYW5hZ2VyKClcbiAgfTtcbn1cblxuQXhpb3MucHJvdG90eXBlLnJlcXVlc3QgPSBmdW5jdGlvbiByZXF1ZXN0KGNvbmZpZykge1xuICAvKmVzbGludCBuby1wYXJhbS1yZWFzc2lnbjowKi9cbiAgLy8gQWxsb3cgZm9yIGF4aW9zKCdleGFtcGxlL3VybCdbLCBjb25maWddKSBhIGxhIGZldGNoIEFQSVxuICBpZiAodHlwZW9mIGNvbmZpZyA9PT0gJ3N0cmluZycpIHtcbiAgICBjb25maWcgPSB1dGlscy5tZXJnZSh7XG4gICAgICB1cmw6IGFyZ3VtZW50c1swXVxuICAgIH0sIGFyZ3VtZW50c1sxXSk7XG4gIH1cblxuICBjb25maWcgPSB1dGlscy5tZXJnZShkZWZhdWx0cywgdGhpcy5kZWZhdWx0cywgeyBtZXRob2Q6ICdnZXQnIH0sIGNvbmZpZyk7XG5cbiAgLy8gU3VwcG9ydCBiYXNlVVJMIGNvbmZpZ1xuICBpZiAoY29uZmlnLmJhc2VVUkwgJiYgIWlzQWJzb2x1dGVVUkwoY29uZmlnLnVybCkpIHtcbiAgICBjb25maWcudXJsID0gY29tYmluZVVSTHMoY29uZmlnLmJhc2VVUkwsIGNvbmZpZy51cmwpO1xuICB9XG5cbiAgLy8gRG9uJ3QgYWxsb3cgb3ZlcnJpZGluZyBkZWZhdWx0cy53aXRoQ3JlZGVudGlhbHNcbiAgY29uZmlnLndpdGhDcmVkZW50aWFscyA9IGNvbmZpZy53aXRoQ3JlZGVudGlhbHMgfHwgdGhpcy5kZWZhdWx0cy53aXRoQ3JlZGVudGlhbHM7XG5cbiAgLy8gVHJhbnNmb3JtIHJlcXVlc3QgZGF0YVxuICBjb25maWcuZGF0YSA9IHRyYW5zZm9ybURhdGEoXG4gICAgY29uZmlnLmRhdGEsXG4gICAgY29uZmlnLmhlYWRlcnMsXG4gICAgY29uZmlnLnRyYW5zZm9ybVJlcXVlc3RcbiAgKTtcblxuICAvLyBGbGF0dGVuIGhlYWRlcnNcbiAgY29uZmlnLmhlYWRlcnMgPSB1dGlscy5tZXJnZShcbiAgICBjb25maWcuaGVhZGVycy5jb21tb24gfHwge30sXG4gICAgY29uZmlnLmhlYWRlcnNbY29uZmlnLm1ldGhvZF0gfHwge30sXG4gICAgY29uZmlnLmhlYWRlcnMgfHwge31cbiAgKTtcblxuICB1dGlscy5mb3JFYWNoKFxuICAgIFsnZGVsZXRlJywgJ2dldCcsICdoZWFkJywgJ3Bvc3QnLCAncHV0JywgJ3BhdGNoJywgJ2NvbW1vbiddLFxuICAgIGZ1bmN0aW9uIGNsZWFuSGVhZGVyQ29uZmlnKG1ldGhvZCkge1xuICAgICAgZGVsZXRlIGNvbmZpZy5oZWFkZXJzW21ldGhvZF07XG4gICAgfVxuICApO1xuXG4gIC8vIEhvb2sgdXAgaW50ZXJjZXB0b3JzIG1pZGRsZXdhcmVcbiAgdmFyIGNoYWluID0gW2Rpc3BhdGNoUmVxdWVzdCwgdW5kZWZpbmVkXTtcbiAgdmFyIHByb21pc2UgPSBQcm9taXNlLnJlc29sdmUoY29uZmlnKTtcblxuICB0aGlzLmludGVyY2VwdG9ycy5yZXF1ZXN0LmZvckVhY2goZnVuY3Rpb24gdW5zaGlmdFJlcXVlc3RJbnRlcmNlcHRvcnMoaW50ZXJjZXB0b3IpIHtcbiAgICBjaGFpbi51bnNoaWZ0KGludGVyY2VwdG9yLmZ1bGZpbGxlZCwgaW50ZXJjZXB0b3IucmVqZWN0ZWQpO1xuICB9KTtcblxuICB0aGlzLmludGVyY2VwdG9ycy5yZXNwb25zZS5mb3JFYWNoKGZ1bmN0aW9uIHB1c2hSZXNwb25zZUludGVyY2VwdG9ycyhpbnRlcmNlcHRvcikge1xuICAgIGNoYWluLnB1c2goaW50ZXJjZXB0b3IuZnVsZmlsbGVkLCBpbnRlcmNlcHRvci5yZWplY3RlZCk7XG4gIH0pO1xuXG4gIHdoaWxlIChjaGFpbi5sZW5ndGgpIHtcbiAgICBwcm9taXNlID0gcHJvbWlzZS50aGVuKGNoYWluLnNoaWZ0KCksIGNoYWluLnNoaWZ0KCkpO1xuICB9XG5cbiAgcmV0dXJuIHByb21pc2U7XG59O1xuXG52YXIgZGVmYXVsdEluc3RhbmNlID0gbmV3IEF4aW9zKGRlZmF1bHRzKTtcbnZhciBheGlvcyA9IG1vZHVsZS5leHBvcnRzID0gYmluZChBeGlvcy5wcm90b3R5cGUucmVxdWVzdCwgZGVmYXVsdEluc3RhbmNlKTtcbmF4aW9zLnJlcXVlc3QgPSBiaW5kKEF4aW9zLnByb3RvdHlwZS5yZXF1ZXN0LCBkZWZhdWx0SW5zdGFuY2UpO1xuXG4vLyBFeHBvc2UgQXhpb3MgY2xhc3MgdG8gYWxsb3cgY2xhc3MgaW5oZXJpdGFuY2VcbmF4aW9zLkF4aW9zID0gQXhpb3M7XG5cbi8vIEV4cG9zZSBwcm9wZXJ0aWVzIGZyb20gZGVmYXVsdEluc3RhbmNlXG5heGlvcy5kZWZhdWx0cyA9IGRlZmF1bHRJbnN0YW5jZS5kZWZhdWx0cztcbmF4aW9zLmludGVyY2VwdG9ycyA9IGRlZmF1bHRJbnN0YW5jZS5pbnRlcmNlcHRvcnM7XG5cbi8vIEZhY3RvcnkgZm9yIGNyZWF0aW5nIG5ldyBpbnN0YW5jZXNcbmF4aW9zLmNyZWF0ZSA9IGZ1bmN0aW9uIGNyZWF0ZShkZWZhdWx0Q29uZmlnKSB7XG4gIHJldHVybiBuZXcgQXhpb3MoZGVmYXVsdENvbmZpZyk7XG59O1xuXG4vLyBFeHBvc2UgYWxsL3NwcmVhZFxuYXhpb3MuYWxsID0gZnVuY3Rpb24gYWxsKHByb21pc2VzKSB7XG4gIHJldHVybiBQcm9taXNlLmFsbChwcm9taXNlcyk7XG59O1xuYXhpb3Muc3ByZWFkID0gcmVxdWlyZSgnLi9oZWxwZXJzL3NwcmVhZCcpO1xuXG4vLyBQcm92aWRlIGFsaWFzZXMgZm9yIHN1cHBvcnRlZCByZXF1ZXN0IG1ldGhvZHNcbnV0aWxzLmZvckVhY2goWydkZWxldGUnLCAnZ2V0JywgJ2hlYWQnXSwgZnVuY3Rpb24gZm9yRWFjaE1ldGhvZE5vRGF0YShtZXRob2QpIHtcbiAgLyplc2xpbnQgZnVuYy1uYW1lczowKi9cbiAgQXhpb3MucHJvdG90eXBlW21ldGhvZF0gPSBmdW5jdGlvbih1cmwsIGNvbmZpZykge1xuICAgIHJldHVybiB0aGlzLnJlcXVlc3QodXRpbHMubWVyZ2UoY29uZmlnIHx8IHt9LCB7XG4gICAgICBtZXRob2Q6IG1ldGhvZCxcbiAgICAgIHVybDogdXJsXG4gICAgfSkpO1xuICB9O1xuICBheGlvc1ttZXRob2RdID0gYmluZChBeGlvcy5wcm90b3R5cGVbbWV0aG9kXSwgZGVmYXVsdEluc3RhbmNlKTtcbn0pO1xuXG51dGlscy5mb3JFYWNoKFsncG9zdCcsICdwdXQnLCAncGF0Y2gnXSwgZnVuY3Rpb24gZm9yRWFjaE1ldGhvZFdpdGhEYXRhKG1ldGhvZCkge1xuICAvKmVzbGludCBmdW5jLW5hbWVzOjAqL1xuICBBeGlvcy5wcm90b3R5cGVbbWV0aG9kXSA9IGZ1bmN0aW9uKHVybCwgZGF0YSwgY29uZmlnKSB7XG4gICAgcmV0dXJuIHRoaXMucmVxdWVzdCh1dGlscy5tZXJnZShjb25maWcgfHwge30sIHtcbiAgICAgIG1ldGhvZDogbWV0aG9kLFxuICAgICAgdXJsOiB1cmwsXG4gICAgICBkYXRhOiBkYXRhXG4gICAgfSkpO1xuICB9O1xuICBheGlvc1ttZXRob2RdID0gYmluZChBeGlvcy5wcm90b3R5cGVbbWV0aG9kXSwgZGVmYXVsdEluc3RhbmNlKTtcbn0pO1xuIiwiJ3VzZSBzdHJpY3QnO1xuXG52YXIgdXRpbHMgPSByZXF1aXJlKCcuLy4uL3V0aWxzJyk7XG5cbmZ1bmN0aW9uIEludGVyY2VwdG9yTWFuYWdlcigpIHtcbiAgdGhpcy5oYW5kbGVycyA9IFtdO1xufVxuXG4vKipcbiAqIEFkZCBhIG5ldyBpbnRlcmNlcHRvciB0byB0aGUgc3RhY2tcbiAqXG4gKiBAcGFyYW0ge0Z1bmN0aW9ufSBmdWxmaWxsZWQgVGhlIGZ1bmN0aW9uIHRvIGhhbmRsZSBgdGhlbmAgZm9yIGEgYFByb21pc2VgXG4gKiBAcGFyYW0ge0Z1bmN0aW9ufSByZWplY3RlZCBUaGUgZnVuY3Rpb24gdG8gaGFuZGxlIGByZWplY3RgIGZvciBhIGBQcm9taXNlYFxuICpcbiAqIEByZXR1cm4ge051bWJlcn0gQW4gSUQgdXNlZCB0byByZW1vdmUgaW50ZXJjZXB0b3IgbGF0ZXJcbiAqL1xuSW50ZXJjZXB0b3JNYW5hZ2VyLnByb3RvdHlwZS51c2UgPSBmdW5jdGlvbiB1c2UoZnVsZmlsbGVkLCByZWplY3RlZCkge1xuICB0aGlzLmhhbmRsZXJzLnB1c2goe1xuICAgIGZ1bGZpbGxlZDogZnVsZmlsbGVkLFxuICAgIHJlamVjdGVkOiByZWplY3RlZFxuICB9KTtcbiAgcmV0dXJuIHRoaXMuaGFuZGxlcnMubGVuZ3RoIC0gMTtcbn07XG5cbi8qKlxuICogUmVtb3ZlIGFuIGludGVyY2VwdG9yIGZyb20gdGhlIHN0YWNrXG4gKlxuICogQHBhcmFtIHtOdW1iZXJ9IGlkIFRoZSBJRCB0aGF0IHdhcyByZXR1cm5lZCBieSBgdXNlYFxuICovXG5JbnRlcmNlcHRvck1hbmFnZXIucHJvdG90eXBlLmVqZWN0ID0gZnVuY3Rpb24gZWplY3QoaWQpIHtcbiAgaWYgKHRoaXMuaGFuZGxlcnNbaWRdKSB7XG4gICAgdGhpcy5oYW5kbGVyc1tpZF0gPSBudWxsO1xuICB9XG59O1xuXG4vKipcbiAqIEl0ZXJhdGUgb3ZlciBhbGwgdGhlIHJlZ2lzdGVyZWQgaW50ZXJjZXB0b3JzXG4gKlxuICogVGhpcyBtZXRob2QgaXMgcGFydGljdWxhcmx5IHVzZWZ1bCBmb3Igc2tpcHBpbmcgb3ZlciBhbnlcbiAqIGludGVyY2VwdG9ycyB0aGF0IG1heSBoYXZlIGJlY29tZSBgbnVsbGAgY2FsbGluZyBgZWplY3RgLlxuICpcbiAqIEBwYXJhbSB7RnVuY3Rpb259IGZuIFRoZSBmdW5jdGlvbiB0byBjYWxsIGZvciBlYWNoIGludGVyY2VwdG9yXG4gKi9cbkludGVyY2VwdG9yTWFuYWdlci5wcm90b3R5cGUuZm9yRWFjaCA9IGZ1bmN0aW9uIGZvckVhY2goZm4pIHtcbiAgdXRpbHMuZm9yRWFjaCh0aGlzLmhhbmRsZXJzLCBmdW5jdGlvbiBmb3JFYWNoSGFuZGxlcihoKSB7XG4gICAgaWYgKGggIT09IG51bGwpIHtcbiAgICAgIGZuKGgpO1xuICAgIH1cbiAgfSk7XG59O1xuXG5tb2R1bGUuZXhwb3J0cyA9IEludGVyY2VwdG9yTWFuYWdlcjtcbiIsIid1c2Ugc3RyaWN0JztcblxuLyoqXG4gKiBEaXNwYXRjaCBhIHJlcXVlc3QgdG8gdGhlIHNlcnZlciB1c2luZyB3aGljaGV2ZXIgYWRhcHRlclxuICogaXMgc3VwcG9ydGVkIGJ5IHRoZSBjdXJyZW50IGVudmlyb25tZW50LlxuICpcbiAqIEBwYXJhbSB7b2JqZWN0fSBjb25maWcgVGhlIGNvbmZpZyB0aGF0IGlzIHRvIGJlIHVzZWQgZm9yIHRoZSByZXF1ZXN0XG4gKiBAcmV0dXJucyB7UHJvbWlzZX0gVGhlIFByb21pc2UgdG8gYmUgZnVsZmlsbGVkXG4gKi9cbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gZGlzcGF0Y2hSZXF1ZXN0KGNvbmZpZykge1xuICByZXR1cm4gbmV3IFByb21pc2UoZnVuY3Rpb24gZXhlY3V0b3IocmVzb2x2ZSwgcmVqZWN0KSB7XG4gICAgdHJ5IHtcbiAgICAgIHZhciBhZGFwdGVyO1xuXG4gICAgICBpZiAodHlwZW9mIGNvbmZpZy5hZGFwdGVyID09PSAnZnVuY3Rpb24nKSB7XG4gICAgICAgIC8vIEZvciBjdXN0b20gYWRhcHRlciBzdXBwb3J0XG4gICAgICAgIGFkYXB0ZXIgPSBjb25maWcuYWRhcHRlcjtcbiAgICAgIH0gZWxzZSBpZiAodHlwZW9mIFhNTEh0dHBSZXF1ZXN0ICE9PSAndW5kZWZpbmVkJykge1xuICAgICAgICAvLyBGb3IgYnJvd3NlcnMgdXNlIFhIUiBhZGFwdGVyXG4gICAgICAgIGFkYXB0ZXIgPSByZXF1aXJlKCcuLi9hZGFwdGVycy94aHInKTtcbiAgICAgIH0gZWxzZSBpZiAodHlwZW9mIHByb2Nlc3MgIT09ICd1bmRlZmluZWQnKSB7XG4gICAgICAgIC8vIEZvciBub2RlIHVzZSBIVFRQIGFkYXB0ZXJcbiAgICAgICAgYWRhcHRlciA9IHJlcXVpcmUoJy4uL2FkYXB0ZXJzL2h0dHAnKTtcbiAgICAgIH1cblxuICAgICAgaWYgKHR5cGVvZiBhZGFwdGVyID09PSAnZnVuY3Rpb24nKSB7XG4gICAgICAgIGFkYXB0ZXIocmVzb2x2ZSwgcmVqZWN0LCBjb25maWcpO1xuICAgICAgfVxuICAgIH0gY2F0Y2ggKGUpIHtcbiAgICAgIHJlamVjdChlKTtcbiAgICB9XG4gIH0pO1xufTtcblxuIiwiJ3VzZSBzdHJpY3QnO1xuXG52YXIgdXRpbHMgPSByZXF1aXJlKCcuL3V0aWxzJyk7XG52YXIgbm9ybWFsaXplSGVhZGVyTmFtZSA9IHJlcXVpcmUoJy4vaGVscGVycy9ub3JtYWxpemVIZWFkZXJOYW1lJyk7XG5cbnZhciBQUk9URUNUSU9OX1BSRUZJWCA9IC9eXFwpXFxdXFx9Jyw/XFxuLztcbnZhciBERUZBVUxUX0NPTlRFTlRfVFlQRSA9IHtcbiAgJ0NvbnRlbnQtVHlwZSc6ICdhcHBsaWNhdGlvbi94LXd3dy1mb3JtLXVybGVuY29kZWQnXG59O1xuXG5mdW5jdGlvbiBzZXRDb250ZW50VHlwZUlmVW5zZXQoaGVhZGVycywgdmFsdWUpIHtcbiAgaWYgKCF1dGlscy5pc1VuZGVmaW5lZChoZWFkZXJzKSAmJiB1dGlscy5pc1VuZGVmaW5lZChoZWFkZXJzWydDb250ZW50LVR5cGUnXSkpIHtcbiAgICBoZWFkZXJzWydDb250ZW50LVR5cGUnXSA9IHZhbHVlO1xuICB9XG59XG5cbm1vZHVsZS5leHBvcnRzID0ge1xuICB0cmFuc2Zvcm1SZXF1ZXN0OiBbZnVuY3Rpb24gdHJhbnNmb3JtUmVxdWVzdChkYXRhLCBoZWFkZXJzKSB7XG4gICAgbm9ybWFsaXplSGVhZGVyTmFtZShoZWFkZXJzLCAnQ29udGVudC1UeXBlJyk7XG4gICAgaWYgKHV0aWxzLmlzRm9ybURhdGEoZGF0YSkgfHxcbiAgICAgIHV0aWxzLmlzQXJyYXlCdWZmZXIoZGF0YSkgfHxcbiAgICAgIHV0aWxzLmlzU3RyZWFtKGRhdGEpIHx8XG4gICAgICB1dGlscy5pc0ZpbGUoZGF0YSkgfHxcbiAgICAgIHV0aWxzLmlzQmxvYihkYXRhKVxuICAgICkge1xuICAgICAgcmV0dXJuIGRhdGE7XG4gICAgfVxuICAgIGlmICh1dGlscy5pc0FycmF5QnVmZmVyVmlldyhkYXRhKSkge1xuICAgICAgcmV0dXJuIGRhdGEuYnVmZmVyO1xuICAgIH1cbiAgICBpZiAodXRpbHMuaXNVUkxTZWFyY2hQYXJhbXMoZGF0YSkpIHtcbiAgICAgIHNldENvbnRlbnRUeXBlSWZVbnNldChoZWFkZXJzLCAnYXBwbGljYXRpb24veC13d3ctZm9ybS11cmxlbmNvZGVkO2NoYXJzZXQ9dXRmLTgnKTtcbiAgICAgIHJldHVybiBkYXRhLnRvU3RyaW5nKCk7XG4gICAgfVxuICAgIGlmICh1dGlscy5pc09iamVjdChkYXRhKSkge1xuICAgICAgc2V0Q29udGVudFR5cGVJZlVuc2V0KGhlYWRlcnMsICdhcHBsaWNhdGlvbi9qc29uO2NoYXJzZXQ9dXRmLTgnKTtcbiAgICAgIHJldHVybiBKU09OLnN0cmluZ2lmeShkYXRhKTtcbiAgICB9XG4gICAgcmV0dXJuIGRhdGE7XG4gIH1dLFxuXG4gIHRyYW5zZm9ybVJlc3BvbnNlOiBbZnVuY3Rpb24gdHJhbnNmb3JtUmVzcG9uc2UoZGF0YSkge1xuICAgIC8qZXNsaW50IG5vLXBhcmFtLXJlYXNzaWduOjAqL1xuICAgIGlmICh0eXBlb2YgZGF0YSA9PT0gJ3N0cmluZycpIHtcbiAgICAgIGRhdGEgPSBkYXRhLnJlcGxhY2UoUFJPVEVDVElPTl9QUkVGSVgsICcnKTtcbiAgICAgIHRyeSB7XG4gICAgICAgIGRhdGEgPSBKU09OLnBhcnNlKGRhdGEpO1xuICAgICAgfSBjYXRjaCAoZSkgeyAvKiBJZ25vcmUgKi8gfVxuICAgIH1cbiAgICByZXR1cm4gZGF0YTtcbiAgfV0sXG5cbiAgaGVhZGVyczoge1xuICAgIGNvbW1vbjoge1xuICAgICAgJ0FjY2VwdCc6ICdhcHBsaWNhdGlvbi9qc29uLCB0ZXh0L3BsYWluLCAqLyonXG4gICAgfSxcbiAgICBwYXRjaDogdXRpbHMubWVyZ2UoREVGQVVMVF9DT05URU5UX1RZUEUpLFxuICAgIHBvc3Q6IHV0aWxzLm1lcmdlKERFRkFVTFRfQ09OVEVOVF9UWVBFKSxcbiAgICBwdXQ6IHV0aWxzLm1lcmdlKERFRkFVTFRfQ09OVEVOVF9UWVBFKVxuICB9LFxuXG4gIHRpbWVvdXQ6IDAsXG5cbiAgeHNyZkNvb2tpZU5hbWU6ICdYU1JGLVRPS0VOJyxcbiAgeHNyZkhlYWRlck5hbWU6ICdYLVhTUkYtVE9LRU4nLFxuXG4gIG1heENvbnRlbnRMZW5ndGg6IC0xLFxuXG4gIHZhbGlkYXRlU3RhdHVzOiBmdW5jdGlvbiB2YWxpZGF0ZVN0YXR1cyhzdGF0dXMpIHtcbiAgICByZXR1cm4gc3RhdHVzID49IDIwMCAmJiBzdGF0dXMgPCAzMDA7XG4gIH1cbn07XG4iLCIndXNlIHN0cmljdCc7XG5cbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gYmluZChmbiwgdGhpc0FyZykge1xuICByZXR1cm4gZnVuY3Rpb24gd3JhcCgpIHtcbiAgICB2YXIgYXJncyA9IG5ldyBBcnJheShhcmd1bWVudHMubGVuZ3RoKTtcbiAgICBmb3IgKHZhciBpID0gMDsgaSA8IGFyZ3MubGVuZ3RoOyBpKyspIHtcbiAgICAgIGFyZ3NbaV0gPSBhcmd1bWVudHNbaV07XG4gICAgfVxuICAgIHJldHVybiBmbi5hcHBseSh0aGlzQXJnLCBhcmdzKTtcbiAgfTtcbn07XG4iLCIndXNlIHN0cmljdCc7XG5cbi8vIGJ0b2EgcG9seWZpbGwgZm9yIElFPDEwIGNvdXJ0ZXN5IGh0dHBzOi8vZ2l0aHViLmNvbS9kYXZpZGNoYW1iZXJzL0Jhc2U2NC5qc1xuXG52YXIgY2hhcnMgPSAnQUJDREVGR0hJSktMTU5PUFFSU1RVVldYWVphYmNkZWZnaGlqa2xtbm9wcXJzdHV2d3h5ejAxMjM0NTY3ODkrLz0nO1xuXG5mdW5jdGlvbiBFKCkge1xuICB0aGlzLm1lc3NhZ2UgPSAnU3RyaW5nIGNvbnRhaW5zIGFuIGludmFsaWQgY2hhcmFjdGVyJztcbn1cbkUucHJvdG90eXBlID0gbmV3IEVycm9yO1xuRS5wcm90b3R5cGUuY29kZSA9IDU7XG5FLnByb3RvdHlwZS5uYW1lID0gJ0ludmFsaWRDaGFyYWN0ZXJFcnJvcic7XG5cbmZ1bmN0aW9uIGJ0b2EoaW5wdXQpIHtcbiAgdmFyIHN0ciA9IFN0cmluZyhpbnB1dCk7XG4gIHZhciBvdXRwdXQgPSAnJztcbiAgZm9yIChcbiAgICAvLyBpbml0aWFsaXplIHJlc3VsdCBhbmQgY291bnRlclxuICAgIHZhciBibG9jaywgY2hhckNvZGUsIGlkeCA9IDAsIG1hcCA9IGNoYXJzO1xuICAgIC8vIGlmIHRoZSBuZXh0IHN0ciBpbmRleCBkb2VzIG5vdCBleGlzdDpcbiAgICAvLyAgIGNoYW5nZSB0aGUgbWFwcGluZyB0YWJsZSB0byBcIj1cIlxuICAgIC8vICAgY2hlY2sgaWYgZCBoYXMgbm8gZnJhY3Rpb25hbCBkaWdpdHNcbiAgICBzdHIuY2hhckF0KGlkeCB8IDApIHx8IChtYXAgPSAnPScsIGlkeCAlIDEpO1xuICAgIC8vIFwiOCAtIGlkeCAlIDEgKiA4XCIgZ2VuZXJhdGVzIHRoZSBzZXF1ZW5jZSAyLCA0LCA2LCA4XG4gICAgb3V0cHV0ICs9IG1hcC5jaGFyQXQoNjMgJiBibG9jayA+PiA4IC0gaWR4ICUgMSAqIDgpXG4gICkge1xuICAgIGNoYXJDb2RlID0gc3RyLmNoYXJDb2RlQXQoaWR4ICs9IDMgLyA0KTtcbiAgICBpZiAoY2hhckNvZGUgPiAweEZGKSB7XG4gICAgICB0aHJvdyBuZXcgRSgpO1xuICAgIH1cbiAgICBibG9jayA9IGJsb2NrIDw8IDggfCBjaGFyQ29kZTtcbiAgfVxuICByZXR1cm4gb3V0cHV0O1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IGJ0b2E7XG4iLCIndXNlIHN0cmljdCc7XG5cbnZhciB1dGlscyA9IHJlcXVpcmUoJy4vLi4vdXRpbHMnKTtcblxuZnVuY3Rpb24gZW5jb2RlKHZhbCkge1xuICByZXR1cm4gZW5jb2RlVVJJQ29tcG9uZW50KHZhbCkuXG4gICAgcmVwbGFjZSgvJTQwL2dpLCAnQCcpLlxuICAgIHJlcGxhY2UoLyUzQS9naSwgJzonKS5cbiAgICByZXBsYWNlKC8lMjQvZywgJyQnKS5cbiAgICByZXBsYWNlKC8lMkMvZ2ksICcsJykuXG4gICAgcmVwbGFjZSgvJTIwL2csICcrJykuXG4gICAgcmVwbGFjZSgvJTVCL2dpLCAnWycpLlxuICAgIHJlcGxhY2UoLyU1RC9naSwgJ10nKTtcbn1cblxuLyoqXG4gKiBCdWlsZCBhIFVSTCBieSBhcHBlbmRpbmcgcGFyYW1zIHRvIHRoZSBlbmRcbiAqXG4gKiBAcGFyYW0ge3N0cmluZ30gdXJsIFRoZSBiYXNlIG9mIHRoZSB1cmwgKGUuZy4sIGh0dHA6Ly93d3cuZ29vZ2xlLmNvbSlcbiAqIEBwYXJhbSB7b2JqZWN0fSBbcGFyYW1zXSBUaGUgcGFyYW1zIHRvIGJlIGFwcGVuZGVkXG4gKiBAcmV0dXJucyB7c3RyaW5nfSBUaGUgZm9ybWF0dGVkIHVybFxuICovXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIGJ1aWxkVVJMKHVybCwgcGFyYW1zLCBwYXJhbXNTZXJpYWxpemVyKSB7XG4gIC8qZXNsaW50IG5vLXBhcmFtLXJlYXNzaWduOjAqL1xuICBpZiAoIXBhcmFtcykge1xuICAgIHJldHVybiB1cmw7XG4gIH1cblxuICB2YXIgc2VyaWFsaXplZFBhcmFtcztcbiAgaWYgKHBhcmFtc1NlcmlhbGl6ZXIpIHtcbiAgICBzZXJpYWxpemVkUGFyYW1zID0gcGFyYW1zU2VyaWFsaXplcihwYXJhbXMpO1xuICB9IGVsc2UgaWYgKHV0aWxzLmlzVVJMU2VhcmNoUGFyYW1zKHBhcmFtcykpIHtcbiAgICBzZXJpYWxpemVkUGFyYW1zID0gcGFyYW1zLnRvU3RyaW5nKCk7XG4gIH0gZWxzZSB7XG4gICAgdmFyIHBhcnRzID0gW107XG5cbiAgICB1dGlscy5mb3JFYWNoKHBhcmFtcywgZnVuY3Rpb24gc2VyaWFsaXplKHZhbCwga2V5KSB7XG4gICAgICBpZiAodmFsID09PSBudWxsIHx8IHR5cGVvZiB2YWwgPT09ICd1bmRlZmluZWQnKSB7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cblxuICAgICAgaWYgKHV0aWxzLmlzQXJyYXkodmFsKSkge1xuICAgICAgICBrZXkgPSBrZXkgKyAnW10nO1xuICAgICAgfVxuXG4gICAgICBpZiAoIXV0aWxzLmlzQXJyYXkodmFsKSkge1xuICAgICAgICB2YWwgPSBbdmFsXTtcbiAgICAgIH1cblxuICAgICAgdXRpbHMuZm9yRWFjaCh2YWwsIGZ1bmN0aW9uIHBhcnNlVmFsdWUodikge1xuICAgICAgICBpZiAodXRpbHMuaXNEYXRlKHYpKSB7XG4gICAgICAgICAgdiA9IHYudG9JU09TdHJpbmcoKTtcbiAgICAgICAgfSBlbHNlIGlmICh1dGlscy5pc09iamVjdCh2KSkge1xuICAgICAgICAgIHYgPSBKU09OLnN0cmluZ2lmeSh2KTtcbiAgICAgICAgfVxuICAgICAgICBwYXJ0cy5wdXNoKGVuY29kZShrZXkpICsgJz0nICsgZW5jb2RlKHYpKTtcbiAgICAgIH0pO1xuICAgIH0pO1xuXG4gICAgc2VyaWFsaXplZFBhcmFtcyA9IHBhcnRzLmpvaW4oJyYnKTtcbiAgfVxuXG4gIGlmIChzZXJpYWxpemVkUGFyYW1zKSB7XG4gICAgdXJsICs9ICh1cmwuaW5kZXhPZignPycpID09PSAtMSA/ICc/JyA6ICcmJykgKyBzZXJpYWxpemVkUGFyYW1zO1xuICB9XG5cbiAgcmV0dXJuIHVybDtcbn07XG4iLCIndXNlIHN0cmljdCc7XG5cbi8qKlxuICogQ3JlYXRlcyBhIG5ldyBVUkwgYnkgY29tYmluaW5nIHRoZSBzcGVjaWZpZWQgVVJMc1xuICpcbiAqIEBwYXJhbSB7c3RyaW5nfSBiYXNlVVJMIFRoZSBiYXNlIFVSTFxuICogQHBhcmFtIHtzdHJpbmd9IHJlbGF0aXZlVVJMIFRoZSByZWxhdGl2ZSBVUkxcbiAqIEByZXR1cm5zIHtzdHJpbmd9IFRoZSBjb21iaW5lZCBVUkxcbiAqL1xubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiBjb21iaW5lVVJMcyhiYXNlVVJMLCByZWxhdGl2ZVVSTCkge1xuICByZXR1cm4gYmFzZVVSTC5yZXBsYWNlKC9cXC8rJC8sICcnKSArICcvJyArIHJlbGF0aXZlVVJMLnJlcGxhY2UoL15cXC8rLywgJycpO1xufTtcbiIsIid1c2Ugc3RyaWN0JztcblxudmFyIHV0aWxzID0gcmVxdWlyZSgnLi8uLi91dGlscycpO1xuXG5tb2R1bGUuZXhwb3J0cyA9IChcbiAgdXRpbHMuaXNTdGFuZGFyZEJyb3dzZXJFbnYoKSA/XG5cbiAgLy8gU3RhbmRhcmQgYnJvd3NlciBlbnZzIHN1cHBvcnQgZG9jdW1lbnQuY29va2llXG4gIChmdW5jdGlvbiBzdGFuZGFyZEJyb3dzZXJFbnYoKSB7XG4gICAgcmV0dXJuIHtcbiAgICAgIHdyaXRlOiBmdW5jdGlvbiB3cml0ZShuYW1lLCB2YWx1ZSwgZXhwaXJlcywgcGF0aCwgZG9tYWluLCBzZWN1cmUpIHtcbiAgICAgICAgdmFyIGNvb2tpZSA9IFtdO1xuICAgICAgICBjb29raWUucHVzaChuYW1lICsgJz0nICsgZW5jb2RlVVJJQ29tcG9uZW50KHZhbHVlKSk7XG5cbiAgICAgICAgaWYgKHV0aWxzLmlzTnVtYmVyKGV4cGlyZXMpKSB7XG4gICAgICAgICAgY29va2llLnB1c2goJ2V4cGlyZXM9JyArIG5ldyBEYXRlKGV4cGlyZXMpLnRvR01UU3RyaW5nKCkpO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKHV0aWxzLmlzU3RyaW5nKHBhdGgpKSB7XG4gICAgICAgICAgY29va2llLnB1c2goJ3BhdGg9JyArIHBhdGgpO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKHV0aWxzLmlzU3RyaW5nKGRvbWFpbikpIHtcbiAgICAgICAgICBjb29raWUucHVzaCgnZG9tYWluPScgKyBkb21haW4pO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKHNlY3VyZSA9PT0gdHJ1ZSkge1xuICAgICAgICAgIGNvb2tpZS5wdXNoKCdzZWN1cmUnKTtcbiAgICAgICAgfVxuXG4gICAgICAgIGRvY3VtZW50LmNvb2tpZSA9IGNvb2tpZS5qb2luKCc7ICcpO1xuICAgICAgfSxcblxuICAgICAgcmVhZDogZnVuY3Rpb24gcmVhZChuYW1lKSB7XG4gICAgICAgIHZhciBtYXRjaCA9IGRvY3VtZW50LmNvb2tpZS5tYXRjaChuZXcgUmVnRXhwKCcoXnw7XFxcXHMqKSgnICsgbmFtZSArICcpPShbXjtdKiknKSk7XG4gICAgICAgIHJldHVybiAobWF0Y2ggPyBkZWNvZGVVUklDb21wb25lbnQobWF0Y2hbM10pIDogbnVsbCk7XG4gICAgICB9LFxuXG4gICAgICByZW1vdmU6IGZ1bmN0aW9uIHJlbW92ZShuYW1lKSB7XG4gICAgICAgIHRoaXMud3JpdGUobmFtZSwgJycsIERhdGUubm93KCkgLSA4NjQwMDAwMCk7XG4gICAgICB9XG4gICAgfTtcbiAgfSkoKSA6XG5cbiAgLy8gTm9uIHN0YW5kYXJkIGJyb3dzZXIgZW52ICh3ZWIgd29ya2VycywgcmVhY3QtbmF0aXZlKSBsYWNrIG5lZWRlZCBzdXBwb3J0LlxuICAoZnVuY3Rpb24gbm9uU3RhbmRhcmRCcm93c2VyRW52KCkge1xuICAgIHJldHVybiB7XG4gICAgICB3cml0ZTogZnVuY3Rpb24gd3JpdGUoKSB7fSxcbiAgICAgIHJlYWQ6IGZ1bmN0aW9uIHJlYWQoKSB7IHJldHVybiBudWxsOyB9LFxuICAgICAgcmVtb3ZlOiBmdW5jdGlvbiByZW1vdmUoKSB7fVxuICAgIH07XG4gIH0pKClcbik7XG4iLCIndXNlIHN0cmljdCc7XG5cbi8qKlxuICogRGV0ZXJtaW5lcyB3aGV0aGVyIHRoZSBzcGVjaWZpZWQgVVJMIGlzIGFic29sdXRlXG4gKlxuICogQHBhcmFtIHtzdHJpbmd9IHVybCBUaGUgVVJMIHRvIHRlc3RcbiAqIEByZXR1cm5zIHtib29sZWFufSBUcnVlIGlmIHRoZSBzcGVjaWZpZWQgVVJMIGlzIGFic29sdXRlLCBvdGhlcndpc2UgZmFsc2VcbiAqL1xubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiBpc0Fic29sdXRlVVJMKHVybCkge1xuICAvLyBBIFVSTCBpcyBjb25zaWRlcmVkIGFic29sdXRlIGlmIGl0IGJlZ2lucyB3aXRoIFwiPHNjaGVtZT46Ly9cIiBvciBcIi8vXCIgKHByb3RvY29sLXJlbGF0aXZlIFVSTCkuXG4gIC8vIFJGQyAzOTg2IGRlZmluZXMgc2NoZW1lIG5hbWUgYXMgYSBzZXF1ZW5jZSBvZiBjaGFyYWN0ZXJzIGJlZ2lubmluZyB3aXRoIGEgbGV0dGVyIGFuZCBmb2xsb3dlZFxuICAvLyBieSBhbnkgY29tYmluYXRpb24gb2YgbGV0dGVycywgZGlnaXRzLCBwbHVzLCBwZXJpb2QsIG9yIGh5cGhlbi5cbiAgcmV0dXJuIC9eKFthLXpdW2EtelxcZFxcK1xcLVxcLl0qOik/XFwvXFwvL2kudGVzdCh1cmwpO1xufTtcbiIsIid1c2Ugc3RyaWN0JztcblxudmFyIHV0aWxzID0gcmVxdWlyZSgnLi8uLi91dGlscycpO1xuXG5tb2R1bGUuZXhwb3J0cyA9IChcbiAgdXRpbHMuaXNTdGFuZGFyZEJyb3dzZXJFbnYoKSA/XG5cbiAgLy8gU3RhbmRhcmQgYnJvd3NlciBlbnZzIGhhdmUgZnVsbCBzdXBwb3J0IG9mIHRoZSBBUElzIG5lZWRlZCB0byB0ZXN0XG4gIC8vIHdoZXRoZXIgdGhlIHJlcXVlc3QgVVJMIGlzIG9mIHRoZSBzYW1lIG9yaWdpbiBhcyBjdXJyZW50IGxvY2F0aW9uLlxuICAoZnVuY3Rpb24gc3RhbmRhcmRCcm93c2VyRW52KCkge1xuICAgIHZhciBtc2llID0gLyhtc2llfHRyaWRlbnQpL2kudGVzdChuYXZpZ2F0b3IudXNlckFnZW50KTtcbiAgICB2YXIgdXJsUGFyc2luZ05vZGUgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdhJyk7XG4gICAgdmFyIG9yaWdpblVSTDtcblxuICAgIC8qKlxuICAgICogUGFyc2UgYSBVUkwgdG8gZGlzY292ZXIgaXQncyBjb21wb25lbnRzXG4gICAgKlxuICAgICogQHBhcmFtIHtTdHJpbmd9IHVybCBUaGUgVVJMIHRvIGJlIHBhcnNlZFxuICAgICogQHJldHVybnMge09iamVjdH1cbiAgICAqL1xuICAgIGZ1bmN0aW9uIHJlc29sdmVVUkwodXJsKSB7XG4gICAgICB2YXIgaHJlZiA9IHVybDtcblxuICAgICAgaWYgKG1zaWUpIHtcbiAgICAgICAgLy8gSUUgbmVlZHMgYXR0cmlidXRlIHNldCB0d2ljZSB0byBub3JtYWxpemUgcHJvcGVydGllc1xuICAgICAgICB1cmxQYXJzaW5nTm9kZS5zZXRBdHRyaWJ1dGUoJ2hyZWYnLCBocmVmKTtcbiAgICAgICAgaHJlZiA9IHVybFBhcnNpbmdOb2RlLmhyZWY7XG4gICAgICB9XG5cbiAgICAgIHVybFBhcnNpbmdOb2RlLnNldEF0dHJpYnV0ZSgnaHJlZicsIGhyZWYpO1xuXG4gICAgICAvLyB1cmxQYXJzaW5nTm9kZSBwcm92aWRlcyB0aGUgVXJsVXRpbHMgaW50ZXJmYWNlIC0gaHR0cDovL3VybC5zcGVjLndoYXR3Zy5vcmcvI3VybHV0aWxzXG4gICAgICByZXR1cm4ge1xuICAgICAgICBocmVmOiB1cmxQYXJzaW5nTm9kZS5ocmVmLFxuICAgICAgICBwcm90b2NvbDogdXJsUGFyc2luZ05vZGUucHJvdG9jb2wgPyB1cmxQYXJzaW5nTm9kZS5wcm90b2NvbC5yZXBsYWNlKC86JC8sICcnKSA6ICcnLFxuICAgICAgICBob3N0OiB1cmxQYXJzaW5nTm9kZS5ob3N0LFxuICAgICAgICBzZWFyY2g6IHVybFBhcnNpbmdOb2RlLnNlYXJjaCA/IHVybFBhcnNpbmdOb2RlLnNlYXJjaC5yZXBsYWNlKC9eXFw/LywgJycpIDogJycsXG4gICAgICAgIGhhc2g6IHVybFBhcnNpbmdOb2RlLmhhc2ggPyB1cmxQYXJzaW5nTm9kZS5oYXNoLnJlcGxhY2UoL14jLywgJycpIDogJycsXG4gICAgICAgIGhvc3RuYW1lOiB1cmxQYXJzaW5nTm9kZS5ob3N0bmFtZSxcbiAgICAgICAgcG9ydDogdXJsUGFyc2luZ05vZGUucG9ydCxcbiAgICAgICAgcGF0aG5hbWU6ICh1cmxQYXJzaW5nTm9kZS5wYXRobmFtZS5jaGFyQXQoMCkgPT09ICcvJykgP1xuICAgICAgICAgICAgICAgICAgdXJsUGFyc2luZ05vZGUucGF0aG5hbWUgOlxuICAgICAgICAgICAgICAgICAgJy8nICsgdXJsUGFyc2luZ05vZGUucGF0aG5hbWVcbiAgICAgIH07XG4gICAgfVxuXG4gICAgb3JpZ2luVVJMID0gcmVzb2x2ZVVSTCh3aW5kb3cubG9jYXRpb24uaHJlZik7XG5cbiAgICAvKipcbiAgICAqIERldGVybWluZSBpZiBhIFVSTCBzaGFyZXMgdGhlIHNhbWUgb3JpZ2luIGFzIHRoZSBjdXJyZW50IGxvY2F0aW9uXG4gICAgKlxuICAgICogQHBhcmFtIHtTdHJpbmd9IHJlcXVlc3RVUkwgVGhlIFVSTCB0byB0ZXN0XG4gICAgKiBAcmV0dXJucyB7Ym9vbGVhbn0gVHJ1ZSBpZiBVUkwgc2hhcmVzIHRoZSBzYW1lIG9yaWdpbiwgb3RoZXJ3aXNlIGZhbHNlXG4gICAgKi9cbiAgICByZXR1cm4gZnVuY3Rpb24gaXNVUkxTYW1lT3JpZ2luKHJlcXVlc3RVUkwpIHtcbiAgICAgIHZhciBwYXJzZWQgPSAodXRpbHMuaXNTdHJpbmcocmVxdWVzdFVSTCkpID8gcmVzb2x2ZVVSTChyZXF1ZXN0VVJMKSA6IHJlcXVlc3RVUkw7XG4gICAgICByZXR1cm4gKHBhcnNlZC5wcm90b2NvbCA9PT0gb3JpZ2luVVJMLnByb3RvY29sICYmXG4gICAgICAgICAgICBwYXJzZWQuaG9zdCA9PT0gb3JpZ2luVVJMLmhvc3QpO1xuICAgIH07XG4gIH0pKCkgOlxuXG4gIC8vIE5vbiBzdGFuZGFyZCBicm93c2VyIGVudnMgKHdlYiB3b3JrZXJzLCByZWFjdC1uYXRpdmUpIGxhY2sgbmVlZGVkIHN1cHBvcnQuXG4gIChmdW5jdGlvbiBub25TdGFuZGFyZEJyb3dzZXJFbnYoKSB7XG4gICAgcmV0dXJuIGZ1bmN0aW9uIGlzVVJMU2FtZU9yaWdpbigpIHtcbiAgICAgIHJldHVybiB0cnVlO1xuICAgIH07XG4gIH0pKClcbik7XG4iLCIndXNlIHN0cmljdCc7XG5cbnZhciB1dGlscyA9IHJlcXVpcmUoJy4uL3V0aWxzJyk7XG5cbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gbm9ybWFsaXplSGVhZGVyTmFtZShoZWFkZXJzLCBub3JtYWxpemVkTmFtZSkge1xuICB1dGlscy5mb3JFYWNoKGhlYWRlcnMsIGZ1bmN0aW9uIHByb2Nlc3NIZWFkZXIodmFsdWUsIG5hbWUpIHtcbiAgICBpZiAobmFtZSAhPT0gbm9ybWFsaXplZE5hbWUgJiYgbmFtZS50b1VwcGVyQ2FzZSgpID09PSBub3JtYWxpemVkTmFtZS50b1VwcGVyQ2FzZSgpKSB7XG4gICAgICBoZWFkZXJzW25vcm1hbGl6ZWROYW1lXSA9IHZhbHVlO1xuICAgICAgZGVsZXRlIGhlYWRlcnNbbmFtZV07XG4gICAgfVxuICB9KTtcbn07XG4iLCIndXNlIHN0cmljdCc7XG5cbnZhciB1dGlscyA9IHJlcXVpcmUoJy4vLi4vdXRpbHMnKTtcblxuLyoqXG4gKiBQYXJzZSBoZWFkZXJzIGludG8gYW4gb2JqZWN0XG4gKlxuICogYGBgXG4gKiBEYXRlOiBXZWQsIDI3IEF1ZyAyMDE0IDA4OjU4OjQ5IEdNVFxuICogQ29udGVudC1UeXBlOiBhcHBsaWNhdGlvbi9qc29uXG4gKiBDb25uZWN0aW9uOiBrZWVwLWFsaXZlXG4gKiBUcmFuc2Zlci1FbmNvZGluZzogY2h1bmtlZFxuICogYGBgXG4gKlxuICogQHBhcmFtIHtTdHJpbmd9IGhlYWRlcnMgSGVhZGVycyBuZWVkaW5nIHRvIGJlIHBhcnNlZFxuICogQHJldHVybnMge09iamVjdH0gSGVhZGVycyBwYXJzZWQgaW50byBhbiBvYmplY3RcbiAqL1xubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiBwYXJzZUhlYWRlcnMoaGVhZGVycykge1xuICB2YXIgcGFyc2VkID0ge307XG4gIHZhciBrZXk7XG4gIHZhciB2YWw7XG4gIHZhciBpO1xuXG4gIGlmICghaGVhZGVycykgeyByZXR1cm4gcGFyc2VkOyB9XG5cbiAgdXRpbHMuZm9yRWFjaChoZWFkZXJzLnNwbGl0KCdcXG4nKSwgZnVuY3Rpb24gcGFyc2VyKGxpbmUpIHtcbiAgICBpID0gbGluZS5pbmRleE9mKCc6Jyk7XG4gICAga2V5ID0gdXRpbHMudHJpbShsaW5lLnN1YnN0cigwLCBpKSkudG9Mb3dlckNhc2UoKTtcbiAgICB2YWwgPSB1dGlscy50cmltKGxpbmUuc3Vic3RyKGkgKyAxKSk7XG5cbiAgICBpZiAoa2V5KSB7XG4gICAgICBwYXJzZWRba2V5XSA9IHBhcnNlZFtrZXldID8gcGFyc2VkW2tleV0gKyAnLCAnICsgdmFsIDogdmFsO1xuICAgIH1cbiAgfSk7XG5cbiAgcmV0dXJuIHBhcnNlZDtcbn07XG4iLCIndXNlIHN0cmljdCc7XG5cbi8qKlxuICogUmVzb2x2ZSBvciByZWplY3QgYSBQcm9taXNlIGJhc2VkIG9uIHJlc3BvbnNlIHN0YXR1cy5cbiAqXG4gKiBAcGFyYW0ge0Z1bmN0aW9ufSByZXNvbHZlIEEgZnVuY3Rpb24gdGhhdCByZXNvbHZlcyB0aGUgcHJvbWlzZS5cbiAqIEBwYXJhbSB7RnVuY3Rpb259IHJlamVjdCBBIGZ1bmN0aW9uIHRoYXQgcmVqZWN0cyB0aGUgcHJvbWlzZS5cbiAqIEBwYXJhbSB7b2JqZWN0fSByZXNwb25zZSBUaGUgcmVzcG9uc2UuXG4gKi9cbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gc2V0dGxlKHJlc29sdmUsIHJlamVjdCwgcmVzcG9uc2UpIHtcbiAgdmFyIHZhbGlkYXRlU3RhdHVzID0gcmVzcG9uc2UuY29uZmlnLnZhbGlkYXRlU3RhdHVzO1xuICAvLyBOb3RlOiBzdGF0dXMgaXMgbm90IGV4cG9zZWQgYnkgWERvbWFpblJlcXVlc3RcbiAgaWYgKCFyZXNwb25zZS5zdGF0dXMgfHwgIXZhbGlkYXRlU3RhdHVzIHx8IHZhbGlkYXRlU3RhdHVzKHJlc3BvbnNlLnN0YXR1cykpIHtcbiAgICByZXNvbHZlKHJlc3BvbnNlKTtcbiAgfSBlbHNlIHtcbiAgICByZWplY3QocmVzcG9uc2UpO1xuICB9XG59O1xuIiwiJ3VzZSBzdHJpY3QnO1xuXG4vKipcbiAqIFN5bnRhY3RpYyBzdWdhciBmb3IgaW52b2tpbmcgYSBmdW5jdGlvbiBhbmQgZXhwYW5kaW5nIGFuIGFycmF5IGZvciBhcmd1bWVudHMuXG4gKlxuICogQ29tbW9uIHVzZSBjYXNlIHdvdWxkIGJlIHRvIHVzZSBgRnVuY3Rpb24ucHJvdG90eXBlLmFwcGx5YC5cbiAqXG4gKiAgYGBganNcbiAqICBmdW5jdGlvbiBmKHgsIHksIHopIHt9XG4gKiAgdmFyIGFyZ3MgPSBbMSwgMiwgM107XG4gKiAgZi5hcHBseShudWxsLCBhcmdzKTtcbiAqICBgYGBcbiAqXG4gKiBXaXRoIGBzcHJlYWRgIHRoaXMgZXhhbXBsZSBjYW4gYmUgcmUtd3JpdHRlbi5cbiAqXG4gKiAgYGBganNcbiAqICBzcHJlYWQoZnVuY3Rpb24oeCwgeSwgeikge30pKFsxLCAyLCAzXSk7XG4gKiAgYGBgXG4gKlxuICogQHBhcmFtIHtGdW5jdGlvbn0gY2FsbGJhY2tcbiAqIEByZXR1cm5zIHtGdW5jdGlvbn1cbiAqL1xubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiBzcHJlYWQoY2FsbGJhY2spIHtcbiAgcmV0dXJuIGZ1bmN0aW9uIHdyYXAoYXJyKSB7XG4gICAgcmV0dXJuIGNhbGxiYWNrLmFwcGx5KG51bGwsIGFycik7XG4gIH07XG59O1xuIiwiJ3VzZSBzdHJpY3QnO1xuXG52YXIgdXRpbHMgPSByZXF1aXJlKCcuLy4uL3V0aWxzJyk7XG5cbi8qKlxuICogVHJhbnNmb3JtIHRoZSBkYXRhIGZvciBhIHJlcXVlc3Qgb3IgYSByZXNwb25zZVxuICpcbiAqIEBwYXJhbSB7T2JqZWN0fFN0cmluZ30gZGF0YSBUaGUgZGF0YSB0byBiZSB0cmFuc2Zvcm1lZFxuICogQHBhcmFtIHtBcnJheX0gaGVhZGVycyBUaGUgaGVhZGVycyBmb3IgdGhlIHJlcXVlc3Qgb3IgcmVzcG9uc2VcbiAqIEBwYXJhbSB7QXJyYXl8RnVuY3Rpb259IGZucyBBIHNpbmdsZSBmdW5jdGlvbiBvciBBcnJheSBvZiBmdW5jdGlvbnNcbiAqIEByZXR1cm5zIHsqfSBUaGUgcmVzdWx0aW5nIHRyYW5zZm9ybWVkIGRhdGFcbiAqL1xubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiB0cmFuc2Zvcm1EYXRhKGRhdGEsIGhlYWRlcnMsIGZucykge1xuICAvKmVzbGludCBuby1wYXJhbS1yZWFzc2lnbjowKi9cbiAgdXRpbHMuZm9yRWFjaChmbnMsIGZ1bmN0aW9uIHRyYW5zZm9ybShmbikge1xuICAgIGRhdGEgPSBmbihkYXRhLCBoZWFkZXJzKTtcbiAgfSk7XG5cbiAgcmV0dXJuIGRhdGE7XG59O1xuIiwiJ3VzZSBzdHJpY3QnO1xuXG4vKmdsb2JhbCB0b1N0cmluZzp0cnVlKi9cblxuLy8gdXRpbHMgaXMgYSBsaWJyYXJ5IG9mIGdlbmVyaWMgaGVscGVyIGZ1bmN0aW9ucyBub24tc3BlY2lmaWMgdG8gYXhpb3NcblxudmFyIHRvU3RyaW5nID0gT2JqZWN0LnByb3RvdHlwZS50b1N0cmluZztcblxuLyoqXG4gKiBEZXRlcm1pbmUgaWYgYSB2YWx1ZSBpcyBhbiBBcnJheVxuICpcbiAqIEBwYXJhbSB7T2JqZWN0fSB2YWwgVGhlIHZhbHVlIHRvIHRlc3RcbiAqIEByZXR1cm5zIHtib29sZWFufSBUcnVlIGlmIHZhbHVlIGlzIGFuIEFycmF5LCBvdGhlcndpc2UgZmFsc2VcbiAqL1xuZnVuY3Rpb24gaXNBcnJheSh2YWwpIHtcbiAgcmV0dXJuIHRvU3RyaW5nLmNhbGwodmFsKSA9PT0gJ1tvYmplY3QgQXJyYXldJztcbn1cblxuLyoqXG4gKiBEZXRlcm1pbmUgaWYgYSB2YWx1ZSBpcyBhbiBBcnJheUJ1ZmZlclxuICpcbiAqIEBwYXJhbSB7T2JqZWN0fSB2YWwgVGhlIHZhbHVlIHRvIHRlc3RcbiAqIEByZXR1cm5zIHtib29sZWFufSBUcnVlIGlmIHZhbHVlIGlzIGFuIEFycmF5QnVmZmVyLCBvdGhlcndpc2UgZmFsc2VcbiAqL1xuZnVuY3Rpb24gaXNBcnJheUJ1ZmZlcih2YWwpIHtcbiAgcmV0dXJuIHRvU3RyaW5nLmNhbGwodmFsKSA9PT0gJ1tvYmplY3QgQXJyYXlCdWZmZXJdJztcbn1cblxuLyoqXG4gKiBEZXRlcm1pbmUgaWYgYSB2YWx1ZSBpcyBhIEZvcm1EYXRhXG4gKlxuICogQHBhcmFtIHtPYmplY3R9IHZhbCBUaGUgdmFsdWUgdG8gdGVzdFxuICogQHJldHVybnMge2Jvb2xlYW59IFRydWUgaWYgdmFsdWUgaXMgYW4gRm9ybURhdGEsIG90aGVyd2lzZSBmYWxzZVxuICovXG5mdW5jdGlvbiBpc0Zvcm1EYXRhKHZhbCkge1xuICByZXR1cm4gKHR5cGVvZiBGb3JtRGF0YSAhPT0gJ3VuZGVmaW5lZCcpICYmICh2YWwgaW5zdGFuY2VvZiBGb3JtRGF0YSk7XG59XG5cbi8qKlxuICogRGV0ZXJtaW5lIGlmIGEgdmFsdWUgaXMgYSB2aWV3IG9uIGFuIEFycmF5QnVmZmVyXG4gKlxuICogQHBhcmFtIHtPYmplY3R9IHZhbCBUaGUgdmFsdWUgdG8gdGVzdFxuICogQHJldHVybnMge2Jvb2xlYW59IFRydWUgaWYgdmFsdWUgaXMgYSB2aWV3IG9uIGFuIEFycmF5QnVmZmVyLCBvdGhlcndpc2UgZmFsc2VcbiAqL1xuZnVuY3Rpb24gaXNBcnJheUJ1ZmZlclZpZXcodmFsKSB7XG4gIHZhciByZXN1bHQ7XG4gIGlmICgodHlwZW9mIEFycmF5QnVmZmVyICE9PSAndW5kZWZpbmVkJykgJiYgKEFycmF5QnVmZmVyLmlzVmlldykpIHtcbiAgICByZXN1bHQgPSBBcnJheUJ1ZmZlci5pc1ZpZXcodmFsKTtcbiAgfSBlbHNlIHtcbiAgICByZXN1bHQgPSAodmFsKSAmJiAodmFsLmJ1ZmZlcikgJiYgKHZhbC5idWZmZXIgaW5zdGFuY2VvZiBBcnJheUJ1ZmZlcik7XG4gIH1cbiAgcmV0dXJuIHJlc3VsdDtcbn1cblxuLyoqXG4gKiBEZXRlcm1pbmUgaWYgYSB2YWx1ZSBpcyBhIFN0cmluZ1xuICpcbiAqIEBwYXJhbSB7T2JqZWN0fSB2YWwgVGhlIHZhbHVlIHRvIHRlc3RcbiAqIEByZXR1cm5zIHtib29sZWFufSBUcnVlIGlmIHZhbHVlIGlzIGEgU3RyaW5nLCBvdGhlcndpc2UgZmFsc2VcbiAqL1xuZnVuY3Rpb24gaXNTdHJpbmcodmFsKSB7XG4gIHJldHVybiB0eXBlb2YgdmFsID09PSAnc3RyaW5nJztcbn1cblxuLyoqXG4gKiBEZXRlcm1pbmUgaWYgYSB2YWx1ZSBpcyBhIE51bWJlclxuICpcbiAqIEBwYXJhbSB7T2JqZWN0fSB2YWwgVGhlIHZhbHVlIHRvIHRlc3RcbiAqIEByZXR1cm5zIHtib29sZWFufSBUcnVlIGlmIHZhbHVlIGlzIGEgTnVtYmVyLCBvdGhlcndpc2UgZmFsc2VcbiAqL1xuZnVuY3Rpb24gaXNOdW1iZXIodmFsKSB7XG4gIHJldHVybiB0eXBlb2YgdmFsID09PSAnbnVtYmVyJztcbn1cblxuLyoqXG4gKiBEZXRlcm1pbmUgaWYgYSB2YWx1ZSBpcyB1bmRlZmluZWRcbiAqXG4gKiBAcGFyYW0ge09iamVjdH0gdmFsIFRoZSB2YWx1ZSB0byB0ZXN0XG4gKiBAcmV0dXJucyB7Ym9vbGVhbn0gVHJ1ZSBpZiB0aGUgdmFsdWUgaXMgdW5kZWZpbmVkLCBvdGhlcndpc2UgZmFsc2VcbiAqL1xuZnVuY3Rpb24gaXNVbmRlZmluZWQodmFsKSB7XG4gIHJldHVybiB0eXBlb2YgdmFsID09PSAndW5kZWZpbmVkJztcbn1cblxuLyoqXG4gKiBEZXRlcm1pbmUgaWYgYSB2YWx1ZSBpcyBhbiBPYmplY3RcbiAqXG4gKiBAcGFyYW0ge09iamVjdH0gdmFsIFRoZSB2YWx1ZSB0byB0ZXN0XG4gKiBAcmV0dXJucyB7Ym9vbGVhbn0gVHJ1ZSBpZiB2YWx1ZSBpcyBhbiBPYmplY3QsIG90aGVyd2lzZSBmYWxzZVxuICovXG5mdW5jdGlvbiBpc09iamVjdCh2YWwpIHtcbiAgcmV0dXJuIHZhbCAhPT0gbnVsbCAmJiB0eXBlb2YgdmFsID09PSAnb2JqZWN0Jztcbn1cblxuLyoqXG4gKiBEZXRlcm1pbmUgaWYgYSB2YWx1ZSBpcyBhIERhdGVcbiAqXG4gKiBAcGFyYW0ge09iamVjdH0gdmFsIFRoZSB2YWx1ZSB0byB0ZXN0XG4gKiBAcmV0dXJucyB7Ym9vbGVhbn0gVHJ1ZSBpZiB2YWx1ZSBpcyBhIERhdGUsIG90aGVyd2lzZSBmYWxzZVxuICovXG5mdW5jdGlvbiBpc0RhdGUodmFsKSB7XG4gIHJldHVybiB0b1N0cmluZy5jYWxsKHZhbCkgPT09ICdbb2JqZWN0IERhdGVdJztcbn1cblxuLyoqXG4gKiBEZXRlcm1pbmUgaWYgYSB2YWx1ZSBpcyBhIEZpbGVcbiAqXG4gKiBAcGFyYW0ge09iamVjdH0gdmFsIFRoZSB2YWx1ZSB0byB0ZXN0XG4gKiBAcmV0dXJucyB7Ym9vbGVhbn0gVHJ1ZSBpZiB2YWx1ZSBpcyBhIEZpbGUsIG90aGVyd2lzZSBmYWxzZVxuICovXG5mdW5jdGlvbiBpc0ZpbGUodmFsKSB7XG4gIHJldHVybiB0b1N0cmluZy5jYWxsKHZhbCkgPT09ICdbb2JqZWN0IEZpbGVdJztcbn1cblxuLyoqXG4gKiBEZXRlcm1pbmUgaWYgYSB2YWx1ZSBpcyBhIEJsb2JcbiAqXG4gKiBAcGFyYW0ge09iamVjdH0gdmFsIFRoZSB2YWx1ZSB0byB0ZXN0XG4gKiBAcmV0dXJucyB7Ym9vbGVhbn0gVHJ1ZSBpZiB2YWx1ZSBpcyBhIEJsb2IsIG90aGVyd2lzZSBmYWxzZVxuICovXG5mdW5jdGlvbiBpc0Jsb2IodmFsKSB7XG4gIHJldHVybiB0b1N0cmluZy5jYWxsKHZhbCkgPT09ICdbb2JqZWN0IEJsb2JdJztcbn1cblxuLyoqXG4gKiBEZXRlcm1pbmUgaWYgYSB2YWx1ZSBpcyBhIEZ1bmN0aW9uXG4gKlxuICogQHBhcmFtIHtPYmplY3R9IHZhbCBUaGUgdmFsdWUgdG8gdGVzdFxuICogQHJldHVybnMge2Jvb2xlYW59IFRydWUgaWYgdmFsdWUgaXMgYSBGdW5jdGlvbiwgb3RoZXJ3aXNlIGZhbHNlXG4gKi9cbmZ1bmN0aW9uIGlzRnVuY3Rpb24odmFsKSB7XG4gIHJldHVybiB0b1N0cmluZy5jYWxsKHZhbCkgPT09ICdbb2JqZWN0IEZ1bmN0aW9uXSc7XG59XG5cbi8qKlxuICogRGV0ZXJtaW5lIGlmIGEgdmFsdWUgaXMgYSBTdHJlYW1cbiAqXG4gKiBAcGFyYW0ge09iamVjdH0gdmFsIFRoZSB2YWx1ZSB0byB0ZXN0XG4gKiBAcmV0dXJucyB7Ym9vbGVhbn0gVHJ1ZSBpZiB2YWx1ZSBpcyBhIFN0cmVhbSwgb3RoZXJ3aXNlIGZhbHNlXG4gKi9cbmZ1bmN0aW9uIGlzU3RyZWFtKHZhbCkge1xuICByZXR1cm4gaXNPYmplY3QodmFsKSAmJiBpc0Z1bmN0aW9uKHZhbC5waXBlKTtcbn1cblxuLyoqXG4gKiBEZXRlcm1pbmUgaWYgYSB2YWx1ZSBpcyBhIFVSTFNlYXJjaFBhcmFtcyBvYmplY3RcbiAqXG4gKiBAcGFyYW0ge09iamVjdH0gdmFsIFRoZSB2YWx1ZSB0byB0ZXN0XG4gKiBAcmV0dXJucyB7Ym9vbGVhbn0gVHJ1ZSBpZiB2YWx1ZSBpcyBhIFVSTFNlYXJjaFBhcmFtcyBvYmplY3QsIG90aGVyd2lzZSBmYWxzZVxuICovXG5mdW5jdGlvbiBpc1VSTFNlYXJjaFBhcmFtcyh2YWwpIHtcbiAgcmV0dXJuIHR5cGVvZiBVUkxTZWFyY2hQYXJhbXMgIT09ICd1bmRlZmluZWQnICYmIHZhbCBpbnN0YW5jZW9mIFVSTFNlYXJjaFBhcmFtcztcbn1cblxuLyoqXG4gKiBUcmltIGV4Y2VzcyB3aGl0ZXNwYWNlIG9mZiB0aGUgYmVnaW5uaW5nIGFuZCBlbmQgb2YgYSBzdHJpbmdcbiAqXG4gKiBAcGFyYW0ge1N0cmluZ30gc3RyIFRoZSBTdHJpbmcgdG8gdHJpbVxuICogQHJldHVybnMge1N0cmluZ30gVGhlIFN0cmluZyBmcmVlZCBvZiBleGNlc3Mgd2hpdGVzcGFjZVxuICovXG5mdW5jdGlvbiB0cmltKHN0cikge1xuICByZXR1cm4gc3RyLnJlcGxhY2UoL15cXHMqLywgJycpLnJlcGxhY2UoL1xccyokLywgJycpO1xufVxuXG4vKipcbiAqIERldGVybWluZSBpZiB3ZSdyZSBydW5uaW5nIGluIGEgc3RhbmRhcmQgYnJvd3NlciBlbnZpcm9ubWVudFxuICpcbiAqIFRoaXMgYWxsb3dzIGF4aW9zIHRvIHJ1biBpbiBhIHdlYiB3b3JrZXIsIGFuZCByZWFjdC1uYXRpdmUuXG4gKiBCb3RoIGVudmlyb25tZW50cyBzdXBwb3J0IFhNTEh0dHBSZXF1ZXN0LCBidXQgbm90IGZ1bGx5IHN0YW5kYXJkIGdsb2JhbHMuXG4gKlxuICogd2ViIHdvcmtlcnM6XG4gKiAgdHlwZW9mIHdpbmRvdyAtPiB1bmRlZmluZWRcbiAqICB0eXBlb2YgZG9jdW1lbnQgLT4gdW5kZWZpbmVkXG4gKlxuICogcmVhY3QtbmF0aXZlOlxuICogIHR5cGVvZiBkb2N1bWVudC5jcmVhdGVFbGVtZW50IC0+IHVuZGVmaW5lZFxuICovXG5mdW5jdGlvbiBpc1N0YW5kYXJkQnJvd3NlckVudigpIHtcbiAgcmV0dXJuIChcbiAgICB0eXBlb2Ygd2luZG93ICE9PSAndW5kZWZpbmVkJyAmJlxuICAgIHR5cGVvZiBkb2N1bWVudCAhPT0gJ3VuZGVmaW5lZCcgJiZcbiAgICB0eXBlb2YgZG9jdW1lbnQuY3JlYXRlRWxlbWVudCA9PT0gJ2Z1bmN0aW9uJ1xuICApO1xufVxuXG4vKipcbiAqIEl0ZXJhdGUgb3ZlciBhbiBBcnJheSBvciBhbiBPYmplY3QgaW52b2tpbmcgYSBmdW5jdGlvbiBmb3IgZWFjaCBpdGVtLlxuICpcbiAqIElmIGBvYmpgIGlzIGFuIEFycmF5IGNhbGxiYWNrIHdpbGwgYmUgY2FsbGVkIHBhc3NpbmdcbiAqIHRoZSB2YWx1ZSwgaW5kZXgsIGFuZCBjb21wbGV0ZSBhcnJheSBmb3IgZWFjaCBpdGVtLlxuICpcbiAqIElmICdvYmonIGlzIGFuIE9iamVjdCBjYWxsYmFjayB3aWxsIGJlIGNhbGxlZCBwYXNzaW5nXG4gKiB0aGUgdmFsdWUsIGtleSwgYW5kIGNvbXBsZXRlIG9iamVjdCBmb3IgZWFjaCBwcm9wZXJ0eS5cbiAqXG4gKiBAcGFyYW0ge09iamVjdHxBcnJheX0gb2JqIFRoZSBvYmplY3QgdG8gaXRlcmF0ZVxuICogQHBhcmFtIHtGdW5jdGlvbn0gZm4gVGhlIGNhbGxiYWNrIHRvIGludm9rZSBmb3IgZWFjaCBpdGVtXG4gKi9cbmZ1bmN0aW9uIGZvckVhY2gob2JqLCBmbikge1xuICAvLyBEb24ndCBib3RoZXIgaWYgbm8gdmFsdWUgcHJvdmlkZWRcbiAgaWYgKG9iaiA9PT0gbnVsbCB8fCB0eXBlb2Ygb2JqID09PSAndW5kZWZpbmVkJykge1xuICAgIHJldHVybjtcbiAgfVxuXG4gIC8vIEZvcmNlIGFuIGFycmF5IGlmIG5vdCBhbHJlYWR5IHNvbWV0aGluZyBpdGVyYWJsZVxuICBpZiAodHlwZW9mIG9iaiAhPT0gJ29iamVjdCcgJiYgIWlzQXJyYXkob2JqKSkge1xuICAgIC8qZXNsaW50IG5vLXBhcmFtLXJlYXNzaWduOjAqL1xuICAgIG9iaiA9IFtvYmpdO1xuICB9XG5cbiAgaWYgKGlzQXJyYXkob2JqKSkge1xuICAgIC8vIEl0ZXJhdGUgb3ZlciBhcnJheSB2YWx1ZXNcbiAgICBmb3IgKHZhciBpID0gMCwgbCA9IG9iai5sZW5ndGg7IGkgPCBsOyBpKyspIHtcbiAgICAgIGZuLmNhbGwobnVsbCwgb2JqW2ldLCBpLCBvYmopO1xuICAgIH1cbiAgfSBlbHNlIHtcbiAgICAvLyBJdGVyYXRlIG92ZXIgb2JqZWN0IGtleXNcbiAgICBmb3IgKHZhciBrZXkgaW4gb2JqKSB7XG4gICAgICBpZiAob2JqLmhhc093blByb3BlcnR5KGtleSkpIHtcbiAgICAgICAgZm4uY2FsbChudWxsLCBvYmpba2V5XSwga2V5LCBvYmopO1xuICAgICAgfVxuICAgIH1cbiAgfVxufVxuXG4vKipcbiAqIEFjY2VwdHMgdmFyYXJncyBleHBlY3RpbmcgZWFjaCBhcmd1bWVudCB0byBiZSBhbiBvYmplY3QsIHRoZW5cbiAqIGltbXV0YWJseSBtZXJnZXMgdGhlIHByb3BlcnRpZXMgb2YgZWFjaCBvYmplY3QgYW5kIHJldHVybnMgcmVzdWx0LlxuICpcbiAqIFdoZW4gbXVsdGlwbGUgb2JqZWN0cyBjb250YWluIHRoZSBzYW1lIGtleSB0aGUgbGF0ZXIgb2JqZWN0IGluXG4gKiB0aGUgYXJndW1lbnRzIGxpc3Qgd2lsbCB0YWtlIHByZWNlZGVuY2UuXG4gKlxuICogRXhhbXBsZTpcbiAqXG4gKiBgYGBqc1xuICogdmFyIHJlc3VsdCA9IG1lcmdlKHtmb286IDEyM30sIHtmb286IDQ1Nn0pO1xuICogY29uc29sZS5sb2cocmVzdWx0LmZvbyk7IC8vIG91dHB1dHMgNDU2XG4gKiBgYGBcbiAqXG4gKiBAcGFyYW0ge09iamVjdH0gb2JqMSBPYmplY3QgdG8gbWVyZ2VcbiAqIEByZXR1cm5zIHtPYmplY3R9IFJlc3VsdCBvZiBhbGwgbWVyZ2UgcHJvcGVydGllc1xuICovXG5mdW5jdGlvbiBtZXJnZSgvKiBvYmoxLCBvYmoyLCBvYmozLCAuLi4gKi8pIHtcbiAgdmFyIHJlc3VsdCA9IHt9O1xuICBmdW5jdGlvbiBhc3NpZ25WYWx1ZSh2YWwsIGtleSkge1xuICAgIGlmICh0eXBlb2YgcmVzdWx0W2tleV0gPT09ICdvYmplY3QnICYmIHR5cGVvZiB2YWwgPT09ICdvYmplY3QnKSB7XG4gICAgICByZXN1bHRba2V5XSA9IG1lcmdlKHJlc3VsdFtrZXldLCB2YWwpO1xuICAgIH0gZWxzZSB7XG4gICAgICByZXN1bHRba2V5XSA9IHZhbDtcbiAgICB9XG4gIH1cblxuICBmb3IgKHZhciBpID0gMCwgbCA9IGFyZ3VtZW50cy5sZW5ndGg7IGkgPCBsOyBpKyspIHtcbiAgICBmb3JFYWNoKGFyZ3VtZW50c1tpXSwgYXNzaWduVmFsdWUpO1xuICB9XG4gIHJldHVybiByZXN1bHQ7XG59XG5cbm1vZHVsZS5leHBvcnRzID0ge1xuICBpc0FycmF5OiBpc0FycmF5LFxuICBpc0FycmF5QnVmZmVyOiBpc0FycmF5QnVmZmVyLFxuICBpc0Zvcm1EYXRhOiBpc0Zvcm1EYXRhLFxuICBpc0FycmF5QnVmZmVyVmlldzogaXNBcnJheUJ1ZmZlclZpZXcsXG4gIGlzU3RyaW5nOiBpc1N0cmluZyxcbiAgaXNOdW1iZXI6IGlzTnVtYmVyLFxuICBpc09iamVjdDogaXNPYmplY3QsXG4gIGlzVW5kZWZpbmVkOiBpc1VuZGVmaW5lZCxcbiAgaXNEYXRlOiBpc0RhdGUsXG4gIGlzRmlsZTogaXNGaWxlLFxuICBpc0Jsb2I6IGlzQmxvYixcbiAgaXNGdW5jdGlvbjogaXNGdW5jdGlvbixcbiAgaXNTdHJlYW06IGlzU3RyZWFtLFxuICBpc1VSTFNlYXJjaFBhcmFtczogaXNVUkxTZWFyY2hQYXJhbXMsXG4gIGlzU3RhbmRhcmRCcm93c2VyRW52OiBpc1N0YW5kYXJkQnJvd3NlckVudixcbiAgZm9yRWFjaDogZm9yRWFjaCxcbiAgbWVyZ2U6IG1lcmdlLFxuICB0cmltOiB0cmltXG59O1xuIiwiLy8gc2hpbSBmb3IgdXNpbmcgcHJvY2VzcyBpbiBicm93c2VyXG5cbnZhciBwcm9jZXNzID0gbW9kdWxlLmV4cG9ydHMgPSB7fTtcbnZhciBxdWV1ZSA9IFtdO1xudmFyIGRyYWluaW5nID0gZmFsc2U7XG52YXIgY3VycmVudFF1ZXVlO1xudmFyIHF1ZXVlSW5kZXggPSAtMTtcblxuZnVuY3Rpb24gY2xlYW5VcE5leHRUaWNrKCkge1xuICAgIGlmICghZHJhaW5pbmcgfHwgIWN1cnJlbnRRdWV1ZSkge1xuICAgICAgICByZXR1cm47XG4gICAgfVxuICAgIGRyYWluaW5nID0gZmFsc2U7XG4gICAgaWYgKGN1cnJlbnRRdWV1ZS5sZW5ndGgpIHtcbiAgICAgICAgcXVldWUgPSBjdXJyZW50UXVldWUuY29uY2F0KHF1ZXVlKTtcbiAgICB9IGVsc2Uge1xuICAgICAgICBxdWV1ZUluZGV4ID0gLTE7XG4gICAgfVxuICAgIGlmIChxdWV1ZS5sZW5ndGgpIHtcbiAgICAgICAgZHJhaW5RdWV1ZSgpO1xuICAgIH1cbn1cblxuZnVuY3Rpb24gZHJhaW5RdWV1ZSgpIHtcbiAgICBpZiAoZHJhaW5pbmcpIHtcbiAgICAgICAgcmV0dXJuO1xuICAgIH1cbiAgICB2YXIgdGltZW91dCA9IHNldFRpbWVvdXQoY2xlYW5VcE5leHRUaWNrKTtcbiAgICBkcmFpbmluZyA9IHRydWU7XG5cbiAgICB2YXIgbGVuID0gcXVldWUubGVuZ3RoO1xuICAgIHdoaWxlKGxlbikge1xuICAgICAgICBjdXJyZW50UXVldWUgPSBxdWV1ZTtcbiAgICAgICAgcXVldWUgPSBbXTtcbiAgICAgICAgd2hpbGUgKCsrcXVldWVJbmRleCA8IGxlbikge1xuICAgICAgICAgICAgaWYgKGN1cnJlbnRRdWV1ZSkge1xuICAgICAgICAgICAgICAgIGN1cnJlbnRRdWV1ZVtxdWV1ZUluZGV4XS5ydW4oKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICBxdWV1ZUluZGV4ID0gLTE7XG4gICAgICAgIGxlbiA9IHF1ZXVlLmxlbmd0aDtcbiAgICB9XG4gICAgY3VycmVudFF1ZXVlID0gbnVsbDtcbiAgICBkcmFpbmluZyA9IGZhbHNlO1xuICAgIGNsZWFyVGltZW91dCh0aW1lb3V0KTtcbn1cblxucHJvY2Vzcy5uZXh0VGljayA9IGZ1bmN0aW9uIChmdW4pIHtcbiAgICB2YXIgYXJncyA9IG5ldyBBcnJheShhcmd1bWVudHMubGVuZ3RoIC0gMSk7XG4gICAgaWYgKGFyZ3VtZW50cy5sZW5ndGggPiAxKSB7XG4gICAgICAgIGZvciAodmFyIGkgPSAxOyBpIDwgYXJndW1lbnRzLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgICBhcmdzW2kgLSAxXSA9IGFyZ3VtZW50c1tpXTtcbiAgICAgICAgfVxuICAgIH1cbiAgICBxdWV1ZS5wdXNoKG5ldyBJdGVtKGZ1biwgYXJncykpO1xuICAgIGlmIChxdWV1ZS5sZW5ndGggPT09IDEgJiYgIWRyYWluaW5nKSB7XG4gICAgICAgIHNldFRpbWVvdXQoZHJhaW5RdWV1ZSwgMCk7XG4gICAgfVxufTtcblxuLy8gdjggbGlrZXMgcHJlZGljdGlibGUgb2JqZWN0c1xuZnVuY3Rpb24gSXRlbShmdW4sIGFycmF5KSB7XG4gICAgdGhpcy5mdW4gPSBmdW47XG4gICAgdGhpcy5hcnJheSA9IGFycmF5O1xufVxuSXRlbS5wcm90b3R5cGUucnVuID0gZnVuY3Rpb24gKCkge1xuICAgIHRoaXMuZnVuLmFwcGx5KG51bGwsIHRoaXMuYXJyYXkpO1xufTtcbnByb2Nlc3MudGl0bGUgPSAnYnJvd3Nlcic7XG5wcm9jZXNzLmJyb3dzZXIgPSB0cnVlO1xucHJvY2Vzcy5lbnYgPSB7fTtcbnByb2Nlc3MuYXJndiA9IFtdO1xucHJvY2Vzcy52ZXJzaW9uID0gJyc7IC8vIGVtcHR5IHN0cmluZyB0byBhdm9pZCByZWdleHAgaXNzdWVzXG5wcm9jZXNzLnZlcnNpb25zID0ge307XG5cbmZ1bmN0aW9uIG5vb3AoKSB7fVxuXG5wcm9jZXNzLm9uID0gbm9vcDtcbnByb2Nlc3MuYWRkTGlzdGVuZXIgPSBub29wO1xucHJvY2Vzcy5vbmNlID0gbm9vcDtcbnByb2Nlc3Mub2ZmID0gbm9vcDtcbnByb2Nlc3MucmVtb3ZlTGlzdGVuZXIgPSBub29wO1xucHJvY2Vzcy5yZW1vdmVBbGxMaXN0ZW5lcnMgPSBub29wO1xucHJvY2Vzcy5lbWl0ID0gbm9vcDtcblxucHJvY2Vzcy5iaW5kaW5nID0gZnVuY3Rpb24gKG5hbWUpIHtcbiAgICB0aHJvdyBuZXcgRXJyb3IoJ3Byb2Nlc3MuYmluZGluZyBpcyBub3Qgc3VwcG9ydGVkJyk7XG59O1xuXG5wcm9jZXNzLmN3ZCA9IGZ1bmN0aW9uICgpIHsgcmV0dXJuICcvJyB9O1xucHJvY2Vzcy5jaGRpciA9IGZ1bmN0aW9uIChkaXIpIHtcbiAgICB0aHJvdyBuZXcgRXJyb3IoJ3Byb2Nlc3MuY2hkaXIgaXMgbm90IHN1cHBvcnRlZCcpO1xufTtcbnByb2Nlc3MudW1hc2sgPSBmdW5jdGlvbigpIHsgcmV0dXJuIDA7IH07XG4iXX0=
