import gulp from 'gulp'
import sass from 'gulp-sass'
import watch from 'gulp-watch'
import sourcemaps from 'gulp-sourcemaps'
import babel from 'gulp-babel'
import nodemon from 'gulp-nodemon'
import browserSync from 'browser-sync'
import browserify from 'browserify'
import babelify from 'babelify'
import source from 'vinyl-source-stream'

gulp.task('BundleScript', () => {
    return browserify({
        entries: './client/scripts/vanilla/index.js',
        extensions: ['.js'],
        debug: true
    })
        .transform(babelify, { presets: ["es2015"] })
        .bundle()
        .pipe(source('bundle.js'))
        .pipe(gulp.dest('./dist/client/scripts'))
})

gulp.task('BundleStyles', () => {
    return gulp.src('./client/styles/styles.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./dist/client/styles'))
})

gulp.task('BundleImages', () => {
    return gulp.src('./client/images/**/*.*')
        .pipe(gulp.dest('./dist/client/images'))
})

gulp.task('BundleFonts', () => {
    return gulp.src('./client/fonts/*.**')
        .pipe(gulp.dest('./dist/client/fonts'))
})

gulp.task('BundleHtml', () => {
    return gulp.src('./client/*.html')
        .pipe(gulp.dest('./dist/client'))
})

gulp.task('TranspileClient', () => {
    browserify({
        entries: './client/scripts/app.js',
        extensions: ['.js'],
        debug: true
    })
        .transform(babelify)
        .bundle()
        .pipe(source('app.js'))
        .pipe(gulp.dest('./dist/client/scripts'))
})

gulp.task('Watch', () => {
    gulp.watch(['./client/scripts/**/*'], ['BundleScript','TranspileClient'])
    gulp.watch(['./client/styles/*.**'], ['BundleStyles'])
    gulp.watch(['./client/images/*.**'], ['BundleImages'])
    gulp.watch(['./client/fonts/*.**'], ['BundleFonts'])
    gulp.watch(['./client/*.html'], ['BundleHtml'])
    gulp.watch(['./server/*.js'], ['TranspileServer'])
})

gulp.task('TranspileServer', () => {
    gulp.src('./server/index.js')
        .pipe(sourcemaps.init())
        .pipe(babel())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('./dist/server'))
})

gulp.task('RunServer', () => {
    nodemon({
        script: './dist/server/index.js',
        watch: ['dist/server/index.js']
    })
})

gulp.task('BrowserSync', () => {
    browserSync({
        proxy: "http://localhost:8081/",
        port: 8080,
        files: ["./dist/**/*.*"],
        open: false
    })
})

gulp.task('default', ['BundleScript', 'TranspileClient', 'BundleStyles', 'BundleHtml', 'BundleImages', 'BundleFonts', 'TranspileServer', 'RunServer', 'BrowserSync', 'Watch'])